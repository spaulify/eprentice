<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

if ( isset($_GET['param']) ) {
	$_SESSION['Ref_Code'] = $_GET['param'];
	SMRedirect::go($config['BASE_URL'] . '/register/?code=' . $_GET['param']);
	return false;
}
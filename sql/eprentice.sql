-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 03:40 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eprentice`
--

-- --------------------------------------------------------

--
-- Table structure for table `ep_admin`
--

CREATE TABLE `ep_admin` (
  `Admin_ID` int(11) NOT NULL,
  `Admin_Email` varchar(255) NOT NULL,
  `Admin_Username` varchar(255) NOT NULL,
  `Admin_Password` varchar(255) NOT NULL,
  `Admin_Name` varchar(255) NOT NULL,
  `Admin_Address` longtext NOT NULL,
  `Admin_Facebook` varchar(255) NOT NULL,
  `Admin_Twitter` varchar(255) NOT NULL,
  `Admin_Linkedin` varchar(255) NOT NULL,
  `Admin_Google` varchar(255) NOT NULL,
  `Admin_Added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_admin`
--

INSERT INTO `ep_admin` (`Admin_ID`, `Admin_Email`, `Admin_Username`, `Admin_Password`, `Admin_Name`, `Admin_Address`, `Admin_Facebook`, `Admin_Twitter`, `Admin_Linkedin`, `Admin_Google`, `Admin_Added`) VALUES
(1, 'info@eprentice.com', 'epadmin', '1d50a8d76ef4afc0a601ba3e3c9f597338ff2679', 'ePrentice', '', '', '', '', '', '2016-06-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ep_category`
--

CREATE TABLE `ep_category` (
  `Category_ID` int(11) NOT NULL,
  `Category_Name` varchar(255) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_category`
--

INSERT INTO `ep_category` (`Category_ID`, `Category_Name`, `Created_On`) VALUES
(1, 'Technology', '2016-02-10 13:58:38'),
(2, 'Arts', '2016-02-10 13:58:38'),
(3, 'Philosophy', '2016-02-10 13:58:38'),
(4, 'Social Science', '2016-02-10 13:58:38'),
(5, 'Business', '2016-02-10 13:58:38'),
(6, 'Languages', '2016-02-10 13:58:38'),
(7, 'Marketing', '2016-02-10 13:58:38'),
(8, 'Sales', '2016-02-10 13:58:38');

-- --------------------------------------------------------

--
-- Table structure for table `ep_email_tracking`
--

CREATE TABLE `ep_email_tracking` (
  `Email_ID` int(11) NOT NULL,
  `Sender_ID` int(11) NOT NULL,
  `Sender_Email_Address` varchar(255) NOT NULL,
  `Receiver_Email_Address` varchar(255) NOT NULL,
  `Email_Product_ID` int(11) NOT NULL,
  `Email_Subject` longtext NOT NULL,
  `Email_Content` longtext NOT NULL,
  `Email_Product_URL` varchar(255) NOT NULL,
  `Email_Unique_TrackID` varchar(255) NOT NULL,
  `Email_Type` enum('Promotion','Training') NOT NULL,
  `Email_Status` enum('Go','Return') NOT NULL,
  `Email_Link_Status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `Email_Send_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ep_facebook_campaign`
--

CREATE TABLE `ep_facebook_campaign` (
  `FB_CampPage_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `FB_Campaign_Page_ID` varchar(255) NOT NULL,
  `FB_Campaign_Page_Name` varchar(255) NOT NULL,
  `FB_Campaign_Page_Access_Token` longtext NOT NULL,
  `Added_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_facebook_campaign`
--

INSERT INTO `ep_facebook_campaign` (`FB_CampPage_ID`, `Trainer_ID`, `FB_Campaign_Page_ID`, `FB_Campaign_Page_Name`, `FB_Campaign_Page_Access_Token`, `Added_On`) VALUES
(1, 1, '344325515904772', 'Free Softwares', 'EAALZAaaBwjj8BABNYWJxa51YfW3xD44nLChZCyjBUlT10mD2kMYZCXZAFZCoNP9iCpdEqb0JNAe88gsmPlLZA0DoQmMxs8e4D90lsjXYXkfnv4dvtjxz0Sm8UmZAmfYVwJLuQPDXpabArIpRFz5LpBgJbPMdeXpTbw8BdjqpOCrqAZDZD', '2016-09-23 17:52:24'),
(2, 5, '146746445422552', 'Fujifilm Cameras', 'EAALZAaaBwjj8BAEDd7ncmisx84Kl1J7ftKP2LIzOiJJMjZBs6fdhCFNsw0yXzgD1nD2PxYkbLbl18hnOrGMGco07mpjAjiEDiALoXfkhe38lEItBHHja2IF291vUeqYhArSg4sM2rfhHFiEaVIhxmNPUz4TRoZD', '2016-10-05 16:11:21'),
(3, 5, '143439272396926', 'Rishra', 'EAALZAaaBwjj8BAAviw89SmGnY83Ju3xhB4GNN6hYCyelVSakG1gvKSLXaPe2FaYZBiTYTticQ1babF1peGfdYgSz0cyQq1B9TqFJmkHBjC5z5pkQ6xmMazBwmJGt7fObftiTOSnBjT7PGT4a4WxqiWcvZB9AZC4ZD', '2016-10-05 16:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `ep_facebook_campaign_schedule`
--

CREATE TABLE `ep_facebook_campaign_schedule` (
  `FB_Schedule_ID` int(11) NOT NULL,
  `FB_Campaign_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `FB_Schedule_Title` varchar(255) NOT NULL,
  `FB_Schedule_Desc` longtext NOT NULL,
  `FB_Schedule_Promotion_ID` int(11) NOT NULL,
  `FB_Schedule_Start` datetime NOT NULL,
  `FB_Schedule_End` datetime NOT NULL,
  `FB_Schedule_Status` enum('Active','Deactive') NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_facebook_campaign_schedule`
--

INSERT INTO `ep_facebook_campaign_schedule` (`FB_Schedule_ID`, `FB_Campaign_ID`, `Trainer_ID`, `FB_Schedule_Title`, `FB_Schedule_Desc`, `FB_Schedule_Promotion_ID`, `FB_Schedule_Start`, `FB_Schedule_End`, `FB_Schedule_Status`, `Created_On`) VALUES
(1, 2, 5, 'This is a Social Promotion for EngageWise', 'This is a Short Description for EngageWise social Promotion campaign. We would like you to see the Promotion for get better understand the product EngageWise.', 20, '2016-10-05 16:05:52', '2016-10-11 16:06:52', 'Active', '2016-10-05 16:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `ep_favorite`
--

CREATE TABLE `ep_favorite` (
  `Favorite_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Training_ID` int(11) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_favorite`
--

INSERT INTO `ep_favorite` (`Favorite_ID`, `User_ID`, `Training_ID`, `Created_On`) VALUES
(4, 11, 14, '2016-09-28 13:31:57'),
(22, 11, 15, '2016-09-28 15:54:01'),
(27, 11, 16, '2016-09-28 15:58:35'),
(34, 11, 19, '2016-09-29 12:46:24'),
(35, 11, 18, '2016-09-29 12:46:26'),
(36, 11, 17, '2016-09-29 12:46:27'),
(37, 18, 18, '2016-09-29 13:55:52'),
(39, 18, 17, '2016-09-29 13:56:02'),
(40, 18, 19, '2016-09-29 13:56:05'),
(41, 18, 16, '2016-09-29 13:56:32'),
(42, 18, 15, '2016-09-29 13:56:33'),
(43, 18, 14, '2016-09-29 13:56:34'),
(44, 18, 13, '2016-09-29 13:56:35');

-- --------------------------------------------------------

--
-- Table structure for table `ep_follow`
--

CREATE TABLE `ep_follow` (
  `Follow_ID` int(11) NOT NULL,
  `Notification_ID` int(11) NOT NULL,
  `Followee_ID` int(11) NOT NULL,
  `Follower_ID` int(11) NOT NULL,
  `Date_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_follow`
--

INSERT INTO `ep_follow` (`Follow_ID`, `Notification_ID`, `Followee_ID`, `Follower_ID`, `Date_Time`) VALUES
(11, 91, 1, 15, '2016-09-28 13:59:15'),
(13, 93, 5, 15, '2016-09-28 13:59:26'),
(14, 94, 5, 11, '2016-09-28 14:19:40'),
(15, 95, 1, 11, '2016-09-28 14:19:44'),
(17, 98, 16, 11, '2016-09-28 17:48:08'),
(19, 104, 16, 18, '2016-09-29 13:55:10'),
(20, 105, 5, 18, '2016-09-29 13:55:12'),
(21, 106, 1, 18, '2016-09-29 13:55:27');

-- --------------------------------------------------------

--
-- Table structure for table `ep_hits`
--

CREATE TABLE `ep_hits` (
  `Hit_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Training_ID` int(11) NOT NULL,
  `IP_Address` varchar(255) NOT NULL,
  `Visited_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_hits`
--

INSERT INTO `ep_hits` (`Hit_ID`, `Trainer_ID`, `Training_ID`, `IP_Address`, `Visited_On`) VALUES
(1, 1, 14, '173.252.123.133', '2016-09-28 11:48:58'),
(2, 1, 14, '173.252.123.136', '2016-09-28 11:48:58'),
(3, 5, 16, '202.142.82.23', '2016-09-28 16:29:23'),
(4, 5, 16, '23.99.101.118', '2016-09-28 19:00:18'),
(5, 5, 16, '122.171.47.153', '2016-09-28 19:01:50'),
(6, 5, 16, '13.76.241.210', '2016-10-24 14:00:50');

-- --------------------------------------------------------

--
-- Table structure for table `ep_impressions`
--

CREATE TABLE `ep_impressions` (
  `Impression_ID` int(11) NOT NULL,
  `Affiliator_ID` int(11) NOT NULL,
  `Aff_Trainer_ID` int(11) NOT NULL,
  `Member_ID` int(11) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_impressions`
--

INSERT INTO `ep_impressions` (`Impression_ID`, `Affiliator_ID`, `Aff_Trainer_ID`, `Member_ID`, `Created_On`) VALUES
(1, 11, 1, 18, '2016-09-29 13:56:38'),
(2, 11, 16, 18, '2016-09-29 18:16:02');

-- --------------------------------------------------------

--
-- Table structure for table `ep_notifications`
--

CREATE TABLE `ep_notifications` (
  `Notification_ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Notification_Text` longtext NOT NULL,
  `Notification_Link` longtext NOT NULL,
  `Notification_Image` varchar(255) NOT NULL,
  `Notification_Status` enum('Read','Unread') NOT NULL DEFAULT 'Unread',
  `Date_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_notifications`
--

INSERT INTO `ep_notifications` (`Notification_ID`, `User_ID`, `Notification_Text`, `Notification_Link`, `Notification_Image`, `Notification_Status`, `Date_Time`) VALUES
(1, 12, 'Trainer <b>Sangita</b> created a new product <b>f.......................</b>.', 'http://www.google.com', 'http://52.37.110.5/uploads/prd/product-3337413868.jpg', 'Unread', '2016-09-22 14:42:01'),
(2, 13, 'Trainer <b>Sangita</b> created a new product <b>f.......................</b>.', 'http://www.google.com', 'http://52.37.110.5/uploads/prd/product-3337413868.jpg', 'Unread', '2016-09-22 14:42:01'),
(3, 12, 'Trainer <b>Sangita</b> created a new product <b>f</b>.', 'http://www.com', 'http://52.37.110.5/uploads/prd/product-3079479909.jpg', 'Unread', '2016-09-22 16:08:31'),
(4, 13, 'Trainer <b>Sangita</b> created a new product <b>f</b>.', 'http://www.com', 'http://52.37.110.5/uploads/prd/product-3079479909.jpg', 'Unread', '2016-09-22 16:08:31'),
(5, 12, 'Trainer <b>Sangita</b> created a new product <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://www.com', 'http://52.37.110.5/uploads/prd/product-2441611164.jpg', 'Unread', '2016-09-23 12:05:41'),
(6, 13, 'Trainer <b>Sangita</b> created a new product <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://www.com', 'http://52.37.110.5/uploads/prd/product-2441611164.jpg', 'Unread', '2016-09-23 12:05:41'),
(7, 12, 'Trainer <b>Sangita</b> created a new product <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://abc.abc', 'http://52.37.110.5/uploads/prd/product-3682798310.jpg', 'Unread', '2016-09-23 12:08:46'),
(8, 13, 'Trainer <b>Sangita</b> created a new product <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://abc.abc', 'http://52.37.110.5/uploads/prd/product-3682798310.jpg', 'Unread', '2016-09-23 12:08:46'),
(9, 12, 'Trainer <b>Sangita</b> created a new product <b>asada</b>.', 'http://www.........socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-9419603631.jpg', 'Unread', '2016-09-23 12:41:30'),
(10, 13, 'Trainer <b>Sangita</b> created a new product <b>asada</b>.', 'http://www.........socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-9419603631.jpg', 'Unread', '2016-09-23 12:41:30'),
(11, 12, 'Trainer <b>Sangita</b> posted a new training <b>1234567890123456789012345678901234567890</b>.', 'cb67bb13', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 14:42:23'),
(12, 13, 'Trainer <b>Sangita</b> posted a new training <b>1234567890123456789012345678901234567890</b>.', 'cb67bb13', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 14:42:23'),
(13, 12, 'Trainer <b>Sangita</b> posted a new training <b>Manual testing</b>.', 'e214ffba', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 15:14:27'),
(14, 13, 'Trainer <b>Sangita</b> posted a new training <b>Manual testing</b>.', 'e214ffba', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 15:14:27'),
(15, 12, 'Trainer <b>Sangita</b> posted a new training <b>Manual</b>.', 'ddaebcbe', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 16:36:01'),
(16, 13, 'Trainer <b>Sangita</b> posted a new training <b>Manual</b>.', 'ddaebcbe', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 16:36:01'),
(17, 12, 'Trainer <b>Sangita</b> posted a new training <b>Manual11</b>.', '9c180b9e', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 17:02:58'),
(18, 13, 'Trainer <b>Sangita</b> posted a new training <b>Manual11</b>.', '9c180b9e', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-23 17:02:58'),
(20, 12, 'Trainer <b>Sangita</b> created a new product <b>sddfgf</b>.', 'http://www.socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-4779946585.jpg', 'Unread', '2016-09-26 13:44:37'),
(21, 13, 'Trainer <b>Sangita</b> created a new product <b>sddfgf</b>.', 'http://www.socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-4779946585.jpg', 'Unread', '2016-09-26 13:44:37'),
(22, 12, 'Trainer <b>Sangita</b> created a new product <b>dfghjkl</b>.', 'http://www.ww.com', 'http://52.37.110.5/uploads/prd/product-4749401179.jpg', 'Unread', '2016-09-26 13:52:02'),
(23, 13, 'Trainer <b>Sangita</b> created a new product <b>dfghjkl</b>.', 'http://www.ww.com', 'http://52.37.110.5/uploads/prd/product-4749401179.jpg', 'Unread', '2016-09-26 13:52:02'),
(24, 12, 'Trainer <b>Sangita</b> created a new product <b>dsdfghjkklhg</b>.', 'http://www.socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-1664515566.jpg', 'Unread', '2016-09-26 13:57:49'),
(25, 13, 'Trainer <b>Sangita</b> created a new product <b>dsdfghjkklhg</b>.', 'http://www.socialsoniccrm.com', 'http://52.37.110.5/uploads/prd/product-1664515566.jpg', 'Unread', '2016-09-26 13:57:49'),
(26, 12, 'Trainer <b>Sangita</b> created a new product <b>sgfdzg</b>.', 'www.jhkjh.com', 'http://52.37.110.5/uploads/prd/product-8381217890.jpg', 'Unread', '2016-09-26 14:01:48'),
(27, 13, 'Trainer <b>Sangita</b> created a new product <b>sgfdzg</b>.', 'www.jhkjh.com', 'http://52.37.110.5/uploads/prd/product-8381217890.jpg', 'Unread', '2016-09-26 14:01:48'),
(28, 12, 'Trainer <b>Sangita</b> published a new promotion <b>gfthgfd</b>.', 'http://52.37.110.5/promotion/15ef4966/', 'http://52.37.110.5/uploads/prm/promo-9796205600.txt', 'Unread', '2016-09-26 16:24:05'),
(29, 13, 'Trainer <b>Sangita</b> published a new promotion <b>gfthgfd</b>.', 'http://52.37.110.5/promotion/15ef4966/', 'http://52.37.110.5/uploads/prm/promo-9796205600.txt', 'Unread', '2016-09-26 16:24:05'),
(30, 12, 'Trainer <b>Sangita</b> published a new promotion <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://52.37.110.5/promotion/6f624238/', 'http://52.37.110.5/uploads/prm/promo-4717620112.pdf', 'Unread', '2016-09-26 16:31:53'),
(31, 13, 'Trainer <b>Sangita</b> published a new promotion <b>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</b>.', 'http://52.37.110.5/promotion/6f624238/', 'http://52.37.110.5/uploads/prm/promo-4717620112.pdf', 'Unread', '2016-09-26 16:31:53'),
(32, 12, 'Trainer <b>Sangita</b> published a new promotion <b>dgh.ghf</b>.', 'http://52.37.110.5/promotion/dd2484e7/', 'http://52.37.110.5/uploads/prm/promo-2756468602.txt', 'Unread', '2016-09-26 17:11:34'),
(33, 13, 'Trainer <b>Sangita</b> published a new promotion <b>dgh.ghf</b>.', 'http://52.37.110.5/promotion/dd2484e7/', 'http://52.37.110.5/uploads/prm/promo-2756468602.txt', 'Unread', '2016-09-26 17:11:34'),
(34, 12, 'Trainer <b>Sangita</b> published a new promotion <b>ffdgdx</b>.', 'http://52.37.110.5/promotion/3c74e3bd/', 'http://52.37.110.5/uploads/prm/promo-8504307023.js', 'Unread', '2016-09-26 17:32:38'),
(35, 13, 'Trainer <b>Sangita</b> published a new promotion <b>ffdgdx</b>.', 'http://52.37.110.5/promotion/3c74e3bd/', 'http://52.37.110.5/uploads/prm/promo-8504307023.js', 'Unread', '2016-09-26 17:32:38'),
(36, 12, 'Trainer <b>Sangita</b> published a new promotion <b>sdfgfg</b>.', 'http://52.37.110.5/promotion/8b67ad61/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 18:52:07'),
(37, 13, 'Trainer <b>Sangita</b> published a new promotion <b>sdfgfg</b>.', 'http://52.37.110.5/promotion/8b67ad61/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 18:52:07'),
(38, 12, 'Trainer <b>Sangita</b> published a new promotion <b>sfdghh</b>.', 'http://52.37.110.5/promotion/d4593a08/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 18:54:50'),
(39, 13, 'Trainer <b>Sangita</b> published a new promotion <b>sfdghh</b>.', 'http://52.37.110.5/promotion/d4593a08/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 18:54:50'),
(40, 12, 'Trainer <b>Sangita</b> published a new promotion <b>fgtyhj</b>.', 'http://52.37.110.5/promotion/0e5d9286/', 'http://52.37.110.5/uploads/prm/promo-9844071017.mp4', 'Unread', '2016-09-26 18:57:15'),
(41, 13, 'Trainer <b>Sangita</b> published a new promotion <b>fgtyhj</b>.', 'http://52.37.110.5/promotion/0e5d9286/', 'http://52.37.110.5/uploads/prm/promo-9844071017.mp4', 'Unread', '2016-09-26 18:57:15'),
(42, 12, 'Trainer <b>Sangita</b> published a new promotion <b>dfghjj</b>.', 'http://52.37.110.5/promotion/67b6cd73/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 19:08:02'),
(43, 13, 'Trainer <b>Sangita</b> published a new promotion <b>dfghjj</b>.', 'http://52.37.110.5/promotion/67b6cd73/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-26 19:08:02'),
(44, 12, 'Trainer <b>Sangita</b> posted a new training <b>dghfgj</b>.', 'b2517c73', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 13:13:45'),
(45, 13, 'Trainer <b>Sangita</b> posted a new training <b>dghfgj</b>.', 'b2517c73', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 13:13:45'),
(46, 12, 'Trainer <b>Sangita</b> published a new promotion <b>dfytyit</b>.', 'http://52.37.110.5/promotion/4640dd99/', 'http://52.37.110.5/uploads/prm/promo-9534565140.jpg', 'Unread', '2016-09-27 13:28:19'),
(47, 13, 'Trainer <b>Sangita</b> published a new promotion <b>dfytyit</b>.', 'http://52.37.110.5/promotion/4640dd99/', 'http://52.37.110.5/uploads/prm/promo-9534565140.jpg', 'Unread', '2016-09-27 13:28:19'),
(48, 12, 'Trainer <b>Sangita</b> posted a new training <b>Learn PHP</b>.', 'd80f71d3', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 15:15:19'),
(49, 13, 'Trainer <b>Sangita</b> posted a new training <b>Learn PHP</b>.', 'd80f71d3', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 15:15:19'),
(50, 12, 'Trainer <b>Sangita</b> published a new promotion <b>PHP promotion</b>.', 'http://52.37.110.5/promotion/58e243e7/', 'http://52.37.110.5/uploads/prm/promo-8849508864.jpg', 'Unread', '2016-09-27 15:21:24'),
(51, 13, 'Trainer <b>Sangita</b> published a new promotion <b>PHP promotion</b>.', 'http://52.37.110.5/promotion/58e243e7/', 'http://52.37.110.5/uploads/prm/promo-8849508864.jpg', 'Unread', '2016-09-27 15:21:24'),
(52, 12, 'Trainer <b>Sangita</b> published a new promotion <b>Java and PHP tutorials</b>.', 'http://52.37.110.5/promotion/89bb7730/', 'http://52.37.110.5/uploads/prm/promo-8005695692.jpg', 'Unread', '2016-09-27 15:24:47'),
(53, 13, 'Trainer <b>Sangita</b> published a new promotion <b>Java and PHP tutorials</b>.', 'http://52.37.110.5/promotion/89bb7730/', 'http://52.37.110.5/uploads/prm/promo-8005695692.jpg', 'Unread', '2016-09-27 15:24:47'),
(54, 12, 'Trainer <b>Sangita</b> published a new promotion <b>JAVA  </b>.', 'http://52.37.110.5/promotion/469b3297/', 'http://52.37.110.5/uploads/prm/promo-3080986542.jpg', 'Unread', '2016-09-27 15:32:55'),
(55, 13, 'Trainer <b>Sangita</b> published a new promotion <b>JAVA  </b>.', 'http://52.37.110.5/promotion/469b3297/', 'http://52.37.110.5/uploads/prm/promo-3080986542.jpg', 'Unread', '2016-09-27 15:32:55'),
(56, 12, 'Trainer <b>Sangita</b> posted a new training <b>324242322</b>.', '8bb42ac4', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:03:47'),
(57, 13, 'Trainer <b>Sangita</b> posted a new training <b>324242322</b>.', '8bb42ac4', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:03:47'),
(58, 12, 'Trainer <b>Sangita</b> posted a new training <b>5647gh</b>.', '8a4f3355', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:16:07'),
(59, 13, 'Trainer <b>Sangita</b> posted a new training <b>5647gh</b>.', '8a4f3355', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:16:07'),
(60, 12, 'Trainer <b>Sangita</b> posted a new training <b>fgfjgfj</b>.', '10676a67', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:25:35'),
(61, 13, 'Trainer <b>Sangita</b> posted a new training <b>fgfjgfj</b>.', '10676a67', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:25:35'),
(62, 12, 'Trainer <b>Sangita</b> posted a new training <b>ghgdgd</b>.', 'df7fba18', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:30:12'),
(63, 13, 'Trainer <b>Sangita</b> posted a new training <b>ghgdgd</b>.', 'df7fba18', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:30:12'),
(64, 12, 'Trainer <b>Sangita</b> published a new promotion <b>aygh6567</b>.', 'http://52.37.110.5/promotion/d7e0bfdd/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:39:40'),
(65, 13, 'Trainer <b>Sangita</b> published a new promotion <b>aygh6567</b>.', 'http://52.37.110.5/promotion/d7e0bfdd/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:39:40'),
(66, 12, 'Trainer <b>Sangita</b> published a new promotion <b>dfhhfdf</b>.', 'http://52.37.110.5/promotion/46ecaf93/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:46:45'),
(67, 13, 'Trainer <b>Sangita</b> published a new promotion <b>dfhhfdf</b>.', 'http://52.37.110.5/promotion/46ecaf93/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 16:46:45'),
(68, 12, 'Trainer <b>Sangita</b> published a new promotion <b>a536547</b>.', 'http://52.37.110.5/promotion/ae8e9e6d/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 17:11:07'),
(69, 13, 'Trainer <b>Sangita</b> published a new promotion <b>a536547</b>.', 'http://52.37.110.5/promotion/ae8e9e6d/', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 17:11:07'),
(70, 12, 'Trainer <b>Sangita</b> posted a new training <b>fdhdhdh</b>.', 'ad1d2b37', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 17:36:05'),
(71, 13, 'Trainer <b>Sangita</b> posted a new training <b>fdhdhdh</b>.', 'ad1d2b37', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 17:36:05'),
(72, 12, 'Trainer <b>Sangita</b> posted a new training <b>4254252</b>.', '568c066e', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 18:24:18'),
(73, 13, 'Trainer <b>Sangita</b> posted a new training <b>4254252</b>.', '568c066e', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-27 18:24:18'),
(74, 12, 'Trainer <b>Sangita</b> created a new product <b>Engagewise</b>.', 'http://www.ww.com', 'http://52.37.110.5/uploads/prd/product-6348395007.jpg', 'Unread', '2016-09-28 08:47:59'),
(75, 13, 'Trainer <b>Sangita</b> created a new product <b>Engagewise</b>.', 'http://www.ww.com', 'http://52.37.110.5/uploads/prd/product-6348395007.jpg', 'Unread', '2016-09-28 08:47:59'),
(76, 12, 'Trainer <b>Sangita</b> posted a new training <b>EngagewiseEngagewiseEngagewiseEngagewise</b>.', 'ddcba5f1', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 09:24:15'),
(77, 13, 'Trainer <b>Sangita</b> posted a new training <b>EngagewiseEngagewiseEngagewiseEngagewise</b>.', 'ddcba5f1', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 09:24:15'),
(79, 12, 'Trainer <b>Sangita</b> posted a new training <b>ePrentice</b>.', '9bf6ad96', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 12:36:43'),
(80, 13, 'Trainer <b>Sangita</b> posted a new training <b>ePrentice</b>.', '9bf6ad96', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 12:36:43'),
(91, 1, '<strong>Ricky Wilson</strong> is now following you.', 'http://52.37.110.5/profile/ricky.wilson/', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Read', '2016-09-28 13:59:15'),
(93, 5, '<strong>Ricky Wilson</strong> is now following you.', 'http://52.37.110.5/profile/ricky.wilson/', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Read', '2016-09-28 13:59:26'),
(94, 5, '<strong>sneha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/sneha.nagaraj/', 'http://52.37.110.5/uploads/dp/thumb-1474275285_220.jpg', 'Read', '2016-09-28 14:19:40'),
(95, 1, '<strong>sneha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/sneha.nagaraj/', 'http://52.37.110.5/uploads/dp/thumb-1474275285_220.jpg', 'Read', '2016-09-28 14:19:44'),
(97, 12, 'Trainer <b>Sangita</b> posted a new training <b>Manual</b>.', '6df80c7c', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 17:41:31'),
(98, 16, '<strong>sneha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/sneha.nagaraj/', 'http://52.37.110.5/uploads/dp/thumb-1474275285_220.jpg', 'Read', '2016-09-28 17:48:08'),
(99, 12, 'Trainer <b>Sangita</b> published a new promotion <b>Engagewise picture promotion</b>.', 'http://52.37.110.5/promotion/a584b447/', 'http://52.37.110.5/uploads/prm/promo-9018203038.jpg', 'Unread', '2016-09-28 18:15:06'),
(100, 12, 'Trainer <b>Sangita</b> posted a new training <b>How to become mad</b>.', '74664ef8', 'http://52.37.110.5/assets/frontend/images/question.jpg', 'Unread', '2016-09-28 18:30:09'),
(101, 12, 'Trainer <b>Sangita</b> created a new product <b>dsfsfg</b>.', 'http://www.google.com', 'http://52.37.110.5/uploads/prd/product-3796815569.jpg', 'Unread', '2016-09-29 12:41:01'),
(102, 11, '<b>Shobha nagaraj</b>, a member you had invited has joined, ePrentice.', '#', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Read', '2016-09-29 13:51:09'),
(104, 16, '<strong>Shobha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/shobha.nagaraj/', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Unread', '2016-09-29 13:55:10'),
(105, 5, '<strong>Shobha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/shobha.nagaraj/', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Read', '2016-09-29 13:55:12'),
(106, 1, '<strong>Shobha nagaraj</strong> is now following you.', 'http://52.37.110.5/profile/shobha.nagaraj/', 'http://52.37.110.5/assets/frontend/images/profile.jpg', 'Read', '2016-09-29 13:55:27'),
(107, 16, '<strong>Ricky Wilson</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Unread', '2016-09-29 17:02:45'),
(108, 16, '<strong>Ricky Wilson</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Unread', '2016-09-29 17:34:17'),
(109, 16, '<strong>Ricky Wilson</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Unread', '2016-09-29 17:41:20'),
(110, 16, '<strong>Ricky Wilson</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Unread', '2016-09-29 18:01:32'),
(111, 16, '<strong>Ricky Wilson</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Read', '2016-09-29 18:13:38'),
(112, 16, '<strong>Shobha nagaraj</strong> purchased your product <strong>Javaaa</strong> on <strong>$3</strong>.', 'http://52.37.110.5/training/1fd6765f/', 'http://52.37.110.5/uploads/trp/trainingThumb4389132019.png', 'Read', '2016-09-29 18:17:40'),
(113, 5, '<strong>Ricky Wilson</strong> purchased your product <strong>EngageWise</strong> on <strong>$450</strong>.', 'http://52.37.110.5/training/00744bf2/', 'http://52.37.110.5/uploads/trp/trainingThumb4779479443.png', 'Read', '2016-09-29 18:24:49'),
(114, 17, 'Trainer <b>Subhro</b> created a new product <b>Sample</b>.', 'www.inspiredbyem.com', 'http://52.37.110.5/uploads/prd/product-8908736077.png', 'Unread', '2017-04-23 12:14:29'),
(115, 17, 'Trainer <b>Subhro</b> created a new product <b>Test Product</b>.', 'http://aaa.aaa.aaa', 'http://localhost/eprentice/uploads/prd/product-156979938.jpg', 'Unread', '2017-08-09 21:44:47'),
(116, 17, 'Trainer <b>Subhro</b> posted a new training <b>Training for Bike RIde</b>.', 'dcec2ee2', 'http://localhost/eprentice/assets/frontend/images/question.jpg', 'Unread', '2017-08-10 17:34:21'),
(117, 17, 'Trainer <b>Subhro</b> published a new promotion <b>New Yamaha Bike Coming Soon</b>.', 'http://localhost/eprentice/promotion/11838b88/', 'http://localhost/eprentice/uploads/prm/promo-854611175.jpg', 'Unread', '2017-08-10 17:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `ep_ongoing_training`
--

CREATE TABLE `ep_ongoing_training` (
  `Ongoing_ID` int(11) NOT NULL,
  `Training_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Training_CataID` int(11) NOT NULL,
  `Member_ID` int(11) NOT NULL,
  `Last_Seek_Time` double NOT NULL,
  `Training_Status` enum('Ongoing','Finish') NOT NULL,
  `Last_Date_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_ongoing_training`
--

INSERT INTO `ep_ongoing_training` (`Ongoing_ID`, `Training_ID`, `Trainer_ID`, `Training_CataID`, `Member_ID`, `Last_Seek_Time`, `Training_Status`, `Last_Date_Time`) VALUES
(1, 15, 1, 4, 11, 30.999, 'Finish', '2016-09-28 13:06:15'),
(2, 16, 5, 5, 11, 18.251374, 'Ongoing', '2016-09-28 21:12:50'),
(3, 18, 1, 2, 11, 24.369065, 'Ongoing', '2016-09-28 17:47:40'),
(4, 16, 5, 5, 15, 17.267452, 'Ongoing', '2016-09-29 18:24:27'),
(5, 17, 16, 1, 15, 5.616833, 'Ongoing', '2016-09-29 18:12:59'),
(6, 17, 16, 1, 11, 8.865347, 'Ongoing', '2016-09-28 21:13:59'),
(7, 14, 1, 5, 11, 0.01553202098083496, 'Ongoing', '2016-09-29 12:45:51'),
(8, 19, 1, 3, 18, 0.015682062942504883, 'Ongoing', '2016-09-29 13:56:55'),
(9, 17, 16, 1, 18, 13.300652, 'Ongoing', '2016-09-29 18:16:37');

-- --------------------------------------------------------

--
-- Table structure for table `ep_payments`
--

CREATE TABLE `ep_payments` (
  `Payment_ID` int(11) NOT NULL,
  `Stripe_Transaction_ID` varchar(255) NOT NULL,
  `Seller_ID` int(11) NOT NULL,
  `Buyer_ID` int(11) NOT NULL,
  `Buyer_First_Name` varchar(255) NOT NULL,
  `Buyer_Last_Name` varchar(255) NOT NULL,
  `Product_ID` int(11) NOT NULL,
  `Product_Name` varchar(255) NOT NULL,
  `Purchase_Amount` float NOT NULL,
  `Purchased_ON` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_payments`
--

INSERT INTO `ep_payments` (`Payment_ID`, `Stripe_Transaction_ID`, `Seller_ID`, `Buyer_ID`, `Buyer_First_Name`, `Buyer_Last_Name`, `Product_ID`, `Product_Name`, `Purchase_Amount`, `Purchased_ON`) VALUES
(5, 'txn_18z3maFrkG91EV7PunWR27Ft', 16, 15, 'Ricky', 'Wilson', 13, 'Javaaa', 3, '2016-09-29 18:07:00'),
(6, 'txn_18z3qTFrkG91EV7PYjJ80iKH', 16, 18, 'Shobha', 'nagaraj', 13, 'Javaaa', 3, '2016-09-29 18:11:01'),
(7, 'txn_18z3xPFrkG91EV7P1PiHKgIU', 5, 15, 'Ricky', 'Wilson', 12, 'EngageWise', 450, '2016-09-29 18:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `ep_preferences`
--

CREATE TABLE `ep_preferences` (
  `Preference_ID` int(11) NOT NULL,
  `Pref_User_ID` int(11) NOT NULL,
  `Pref_Category` longtext NOT NULL,
  `Added_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_preferences`
--

INSERT INTO `ep_preferences` (`Preference_ID`, `Pref_User_ID`, `Pref_Category`, `Added_On`) VALUES
(1, 2, '1,2,4,5,6,7,8', '2016-09-23 18:37:04'),
(2, 11, '6', '2016-09-28 14:02:07');

-- --------------------------------------------------------

--
-- Table structure for table `ep_products`
--

CREATE TABLE `ep_products` (
  `Product_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Product_Name` varchar(255) NOT NULL,
  `Product_Desc` longtext NOT NULL,
  `Product_Price` varchar(255) NOT NULL,
  `Product_Link` longtext NOT NULL,
  `Product_Refund_URL` longtext NOT NULL,
  `Product_Image` varchar(255) NOT NULL,
  `Product_Create_DateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_products`
--

INSERT INTO `ep_products` (`Product_ID`, `Trainer_ID`, `Product_Name`, `Product_Desc`, `Product_Price`, `Product_Link`, `Product_Refund_URL`, `Product_Image`, `Product_Create_DateTime`) VALUES
(8, 1, 'hgdfghj', 'rdfgh', '5', 'http://www.ww.com', '', 'product-3547220230.jpg', '2016-09-26 13:52:02'),
(9, 1, 'dsdfghjkklhg', 'sdfghjkl', '1', 'http://www.socialsoniccrm.com', '', 'product-1664515566.jpg', '2016-09-26 13:57:49'),
(10, 1, 'dhhhhh', 'dfrfer', '3', '12341.hhhjk.com', '', 'product-8365276525.jpg', '2016-09-26 14:01:48'),
(11, 1, 'Engagewise', 'Product desciption', '999999999', 'http://www.socialsoniccrm.com', '', 'product-9759622048.jpg', '2016-09-28 08:47:59'),
(12, 5, 'EngageWise', 'This is EngageWise product description.', '450', 'http://www.engagewise.com', '', 'product-6617611623.png', '2016-09-28 12:38:16'),
(13, 16, 'Javaaa', 'Description', '3', 'http://www.google.com', '', 'product-7190006296.jpg', '2016-09-28 14:41:55'),
(14, 1, 'ePrentice', '""""""""""""""""""""''', '3', 'http://www.google.com', '', 'product-3796815569.jpg', '2016-09-29 12:41:01'),
(15, 5, 'Sample', 'test', '10', 'www.inspiredbyem.com', '', 'product-8908736077.png', '2017-04-23 12:14:29'),
(16, 5, 'Yamaha R1.5 V3.0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis non turpis odio. Etiam laoreet lacinia augue ut vehicula. Cras interdum vitae ex vel scelerisque. Etiam condimentum efficitur arcu non scelerisque.', '25000', 'https://global.yamaha.com', 'https://global.yamaha.com', 'product-360778454.jpg', '2017-08-09 21:44:47');

-- --------------------------------------------------------

--
-- Table structure for table `ep_promotions`
--

CREATE TABLE `ep_promotions` (
  `Promotion_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Training_ID` int(11) NOT NULL,
  `Promotion_Type` enum('Image','Video','Question') NOT NULL,
  `Promotion_Thumb` varchar(255) NOT NULL,
  `Promotion_Title` varchar(255) NOT NULL,
  `Promotion_Desc` longtext NOT NULL,
  `Promotion_URL` varchar(255) NOT NULL,
  `Promotion_File` enum('Yes','No') NOT NULL,
  `Promotion_Question` enum('Yes','No') NOT NULL,
  `Promotion_Start` datetime NOT NULL,
  `Promotion_End` datetime NOT NULL,
  `Shared` varchar(255) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_promotions`
--

INSERT INTO `ep_promotions` (`Promotion_ID`, `Trainer_ID`, `Training_ID`, `Promotion_Type`, `Promotion_Thumb`, `Promotion_Title`, `Promotion_Desc`, `Promotion_URL`, `Promotion_File`, `Promotion_Question`, `Promotion_Start`, `Promotion_End`, `Shared`, `Created_On`) VALUES
(19, 1, 3, 'Question', 'promo-thumb-1616662857.jpg', 'Question Promotion title', 'dhdng', 'ae8e9e6d', 'No', 'Yes', '2016-09-27 17:02:35', '2016-09-28 17:03:35', '', '2016-09-27 17:11:07'),
(20, 5, 16, 'Video', 'promo-thumb-9816208803.jpg', 'Promotion for EngageWise', 'This is promotion description of EngageWise.', '22c5d3e1', 'Yes', 'No', '2016-09-30 13:11:48', '2016-10-08 13:12:48', '', '2016-09-28 13:24:20'),
(21, 16, 17, 'Image', 'promo-thumb-4447015598.jpg', 'Java promotion', 'Description', '3488e810', 'Yes', 'No', '2016-09-28 14:39:07', '2016-09-29 14:40:07', '', '2016-09-28 14:46:17'),
(24, 1, 14, 'Image', 'promo-thumb-1684920471.jpg', 'Engagewise picture promotion', 'Description', 'a584b447', 'Yes', 'No', '2016-09-28 18:07:14', '2016-09-29 18:08:14', '', '2016-09-28 18:15:06'),
(25, 5, 20, 'Image', 'promo-thumb-837699685.jpg', 'New Yamaha Bike Coming Soon', 'This is a promotion for New Yamaha R1.5 V3.0 Bike which is going to launch in India in next month. The stunning Bike is a xerox copy of Superbike Yamaha R1. Soon, it will be in Indian road and give a great headache to the rival bikes like KTM RC200, Honda CBR150, Pulsar RS200, Apache 200V. We all excited to watch the new bike and it''s performance which hopefully can take us a new level.', '11838b88', 'Yes', 'No', '2017-08-10 17:39:35', '2017-08-31 17:40:35', '', '2017-08-10 17:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `ep_promotion_file`
--

CREATE TABLE `ep_promotion_file` (
  `File_ID` int(11) NOT NULL,
  `Promotion_ID` int(11) NOT NULL,
  `Promotion_Type` enum('Image','Video') NOT NULL,
  `Promotion_Filename` varchar(255) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_promotion_file`
--

INSERT INTO `ep_promotion_file` (`File_ID`, `Promotion_ID`, `Promotion_Type`, `Promotion_Filename`, `Created_On`) VALUES
(1, 20, 'Video', 'promo-1053882809.mp4', '2016-09-28 13:24:20'),
(2, 20, 'Video', 'promo-6327240397.mp4', '2016-09-28 13:24:20'),
(3, 21, 'Image', 'promo-6706636212.jpg', '2016-09-28 14:46:17'),
(4, 21, 'Image', 'promo-3806677260.jpg', '2016-09-28 14:46:17'),
(5, 22, 'Image', 'promo-2130559920.jpg', '2016-09-28 18:14:11'),
(6, 23, 'Image', 'promo-6529862345.jpg', '2016-09-28 18:14:20'),
(7, 24, 'Image', 'promo-5743193225.jpg', '2016-09-28 18:15:06'),
(8, 24, 'Image', 'promo-9018203038.jpg', '2016-09-28 18:15:06'),
(9, 25, 'Image', 'promo-1177693789.jpg', '2017-08-10 17:42:36'),
(10, 25, 'Image', 'promo-1112672767.jpg', '2017-08-10 17:42:36'),
(11, 25, 'Image', 'promo-1406192547.jpg', '2017-08-10 17:42:36'),
(12, 25, 'Image', 'promo-329408285.jpg', '2017-08-10 17:42:36'),
(13, 25, 'Image', 'promo-854611175.jpg', '2017-08-10 17:42:36');

-- --------------------------------------------------------

--
-- Table structure for table `ep_promotion_questions`
--

CREATE TABLE `ep_promotion_questions` (
  `Question_ID` int(11) NOT NULL,
  `Promotion_ID` int(11) NOT NULL,
  `Promotion_Type` enum('Question') NOT NULL,
  `Question` varchar(255) NOT NULL,
  `Answer_One` varchar(255) NOT NULL,
  `Answer_One_Marks` int(11) NOT NULL,
  `Answer_Two` varchar(255) NOT NULL,
  `Answer_Two_Marks` int(11) NOT NULL,
  `Answer_Three` varchar(255) NOT NULL,
  `Answer_Three_Marks` int(11) NOT NULL,
  `Answer_Four` varchar(255) NOT NULL,
  `Answer_Four_Marks` int(11) NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_promotion_questions`
--

INSERT INTO `ep_promotion_questions` (`Question_ID`, `Promotion_ID`, `Promotion_Type`, `Question`, `Answer_One`, `Answer_One_Marks`, `Answer_Two`, `Answer_Two_Marks`, `Answer_Three`, `Answer_Three_Marks`, `Answer_Four`, `Answer_Four_Marks`, `Created_On`) VALUES
(1, 19, 'Question', 'gggdfhdg', 'dhdhehdh', 2, 'dhdhehdh', 3, 'dhdhehdh', 3, 'dhdhehdh', 4, '2016-09-27 17:11:07'),
(2, 19, 'Question', 'gggdfhdg', 'dhdhehdh', 2, 'dhdhehdh', 3, 'dhdhehdh', 3, 'dhdhehdh', 4, '2016-09-27 17:11:07');

-- --------------------------------------------------------

--
-- Table structure for table `ep_share_promotion`
--

CREATE TABLE `ep_share_promotion` (
  `Share_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Promotion_ID` int(11) NOT NULL,
  `Share_Via` varchar(255) NOT NULL,
  `Share_Email` varchar(255) NOT NULL,
  `Share_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ep_share_training`
--

CREATE TABLE `ep_share_training` (
  `Share_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Training_ID` int(11) NOT NULL,
  `Share_Via` varchar(255) NOT NULL,
  `Share_Email` varchar(255) NOT NULL,
  `Share_Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_share_training`
--

INSERT INTO `ep_share_training` (`Share_ID`, `Trainer_ID`, `Training_ID`, `Share_Via`, `Share_Email`, `Share_Time`) VALUES
(1, 1, 2, 'email', 'subhashpramanik37@gmail.com', '2016-09-23 17:11:59'),
(2, 1, 2, 'email', 'shobhanagaraj193@gmail.com', '2016-09-23 17:12:00'),
(3, 1, 2, 'email', 'snehanagaraj193@gmail.com', '2016-09-23 17:12:01'),
(4, 1, 2, 'email', 'sangitashruthi@gmail.com', '2016-09-23 17:12:01'),
(5, 1, 4, 'email', 'sangitashruthi@gmail.com', '2016-09-23 19:23:47'),
(6, 1, 4, 'email', '', '2016-09-23 19:24:52'),
(7, 1, 4, 'email', '', '2016-09-23 19:25:37'),
(8, 1, 4, 'email', '', '2016-09-23 19:26:11');

-- --------------------------------------------------------

--
-- Table structure for table `ep_traffic_campaign`
--

CREATE TABLE `ep_traffic_campaign` (
  `Traffic_ID` int(11) NOT NULL,
  `Traffic_Source` enum('Email','Facebook','Twitter','Linkedin','Google') NOT NULL,
  `Traffic_IP` varchar(255) NOT NULL,
  `Traffic_Training` varchar(255) NOT NULL,
  `Traffic_Training_ID` int(11) NOT NULL,
  `Traffic_Trainer_ID` int(11) NOT NULL,
  `Traffic_DateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_traffic_campaign`
--

INSERT INTO `ep_traffic_campaign` (`Traffic_ID`, `Traffic_Source`, `Traffic_IP`, `Traffic_Training`, `Traffic_Training_ID`, `Traffic_Trainer_ID`, `Traffic_DateTime`) VALUES
(1, 'Facebook', '173.252.123.133', 'fb_ddcba5f1', 14, 1, '2016-09-28 11:48:58'),
(2, 'Facebook', '122.171.47.153', 'fb_ddcba5f1', 14, 1, '2016-09-28 11:49:23');

-- --------------------------------------------------------

--
-- Table structure for table `ep_trainings`
--

CREATE TABLE `ep_trainings` (
  `Training_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `Product_ID` int(11) NOT NULL,
  `Training_Name` varchar(255) NOT NULL,
  `Training_Category` int(11) NOT NULL,
  `Training_Headline` varchar(255) NOT NULL,
  `Training_Point_One` varchar(255) NOT NULL,
  `Training_Point_Two` varchar(255) NOT NULL,
  `Training_Point_Three` varchar(255) NOT NULL,
  `Training_Point_Four` varchar(255) NOT NULL,
  `Training_Desc` longtext NOT NULL,
  `Training_URL` varchar(255) NOT NULL,
  `Training_URL_EM` varchar(255) NOT NULL,
  `Training_URL_FB` varchar(255) NOT NULL,
  `Training_URL_TW` varchar(255) NOT NULL,
  `Training_URL_LI` varchar(255) NOT NULL,
  `Training_URL_GP` varchar(255) NOT NULL,
  `Training_Video` varchar(255) NOT NULL,
  `Join_Training_Btn` varchar(255) NOT NULL,
  `Intro_Video` varchar(255) NOT NULL,
  `Training_Tags` varchar(255) NOT NULL,
  `Similar_Section` varchar(25) NOT NULL,
  `CTA_Text` varchar(255) NOT NULL,
  `CTA_Time` int(11) NOT NULL,
  `Training_Thumb` varchar(255) NOT NULL,
  `Training_Status` enum('Published','Pending') NOT NULL DEFAULT 'Pending',
  `Shared` varchar(255) NOT NULL,
  `Training_Published_On` datetime NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_trainings`
--

INSERT INTO `ep_trainings` (`Training_ID`, `Trainer_ID`, `Product_ID`, `Training_Name`, `Training_Category`, `Training_Headline`, `Training_Point_One`, `Training_Point_Two`, `Training_Point_Three`, `Training_Point_Four`, `Training_Desc`, `Training_URL`, `Training_URL_EM`, `Training_URL_FB`, `Training_URL_TW`, `Training_URL_LI`, `Training_URL_GP`, `Training_Video`, `Join_Training_Btn`, `Intro_Video`, `Training_Tags`, `Similar_Section`, `CTA_Text`, `CTA_Time`, `Training_Thumb`, `Training_Status`, `Shared`, `Training_Published_On`, `Created_On`) VALUES
(13, 1, 8, '4254252', 5, 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', '568c066e', 'em_568c066e', 'fb_568c066e', 'tw_568c066e', 'li_568c066e', 'gp_568c066e', 'https://www.youtube.com/watch?v=3u1fu6f8Hto', 'dfhdhd', 'https://www.youtube.com/watch?v=3u1fu6f8Hto', 'g', 'no', 'sjhggfhj', 4, 'trainingThumb8171114954.png', 'Published', '', '2017-07-20 09:25:37', '2016-09-27 18:24:18'),
(14, 1, 11, 'Engagewise', 5, 'Engagewise tutorial', 'This application helps you to find your leads', 'This application helps you to find your leads', 'This application helps you to find your leads', 'This application helps you to find your leads', 'This training shows how to use engagewise application', 'ddcba5f1', 'em_ddcba5f1', 'fb_ddcba5f1', 'tw_ddcba5f1', 'li_ddcba5f1', 'gp_ddcba5f1', 'https://www.youtube.com/watch?v=3u1fu6f8Hto', 'View', 'https://www.youtube.com/watch?v=3u1fu6f8Hto', 'Engage,engagewise', 'no', 'Buy', 5, 'trainingThumb2608963078.png', 'Published', '', '2017-08-01 18:37:22', '2016-09-28 09:24:15'),
(15, 1, 11, 'ePrentice', 4, 'ePrentice heading', 'ePrentice heading 1', 'ePrentice heading 2', 'ePrentice heading 3', 'ePrentice heading 4', 'ePrentice heading', '9bf6ad96', 'em_9bf6ad96', 'fb_9bf6ad96', 'tw_9bf6ad96', 'li_9bf6ad96', 'gp_9bf6ad96', 'https://www.youtube.com/watch?v=WPvGqX-TXP0', 'View', 'intro-8772389497.mp4', 'ePrentice', 'no', 'Buy', 5, 'trainingThumb9371910751.png', 'Published', '', '2017-05-09 09:32:15', '2016-09-28 12:36:43'),
(16, 5, 12, 'Training for EngageWise', 5, 'Training Heading of EngageWise.', 'First point of EngageWIse.', 'Second point of EngageWIse.', 'Third point of EngageWIse.', 'Fourth point of EngageWIse.', 'This is description for EngageWise training.', '00744bf2', 'em_00744bf2', 'fb_00744bf2', 'tw_00744bf2', 'li_00744bf2', 'gp_00744bf2', 'training-1426090057.mp4', 'Join Training!', 'intro-1426090057.mp4', 'EngageWise,Training,Business', 'no', 'Buy Now!', 10, 'trainingThumb4779479443.png', 'Published', '', '2017-07-07 08:38:56', '2016-09-28 13:11:23'),
(17, 16, 13, 'Javaaa', 1, 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', 'Training Headline', '1fd6765f', 'em_1fd6765f', 'fb_1fd6765f', 'tw_1fd6765f', 'li_1fd6765f', 'gp_1fd6765f', 'https://www.youtube.com/watch?v=WPvGqX-TXP0', 'view', 'https://www.youtube.com/watch?v=WPvGqX-TXP0', 'java', 'no', 'Buy', 5, 'trainingThumb4389132019.png', 'Pending', '', '2017-08-26 11:28:24', '2016-09-28 14:45:26'),
(18, 1, 11, 'Manual', 2, 'Training Headline', 'Training Headline1', 'Training Headline2', 'Training Headline3', 'Training Headline4', 'Training Headline', '6df80c7c', 'em_6df80c7c', 'fb_6df80c7c', 'tw_6df80c7c', 'li_6df80c7c', 'gp_6df80c7c', 'https://www.youtube.com/watch?v=WPvGqX-TXP0', 'view', 'intro-2214886522.mp4', 'Manual', 'yes', 'Buy Now!', 2, 'trainingThumb1392682576.png', 'Published', '', '2017-08-10 09:24:21', '2016-09-28 17:41:31'),
(19, 1, 8, 'How to become mad', 3, 'How to become mad', 'How to become mad1', 'How to become mad2', 'How to become mad3', 'How to become mad4', 'How to become mad', '74664ef8', 'em_74664ef8', 'fb_74664ef8', 'tw_74664ef8', 'li_74664ef8', 'gp_74664ef8', 'https://www.youtube.com/watch?v=yMB7yqbH9R0', 'View', 'intro-4080129954.mp4', 'Mad', 'no', 'Buy now!', 3, 'trainingThumb1545357801.png', 'Pending', '', '2017-08-10 21:50:51', '2016-09-28 18:30:09'),
(20, 5, 16, 'Training for Bike RIde', 8, 'How to start riding a bike?', 'Need to know about speed.', 'Need to know about braking.', 'Need to know about gear shifting.', 'Need to know about fuel.', 'In this training we will teach you how to start a basic ride of a motorcycle. After that, need some practice and then you will be become a pro rider. Here also we promoting the new Yamaha R1.5 V3.0 which is going to launch probably in next month in India.', 'dcec2ee2', 'em_dcec2ee2', 'fb_dcec2ee2', 'tw_dcec2ee2', 'li_dcec2ee2', 'gp_dcec2ee2', 'https://www.youtube.com/watch?v=B-nSNAngdV8&t=242s', 'Join Training', 'https://www.youtube.com/watch?v=EGrxmO8Glzc', 'Ride,Bike,Yamaha', 'no', 'Buy Now', 60, 'trainingThumb473091403.png', 'Published', '', '2017-08-10 18:15:10', '2017-08-10 17:34:21');

-- --------------------------------------------------------

--
-- Table structure for table `ep_twitter_campaign`
--

CREATE TABLE `ep_twitter_campaign` (
  `TW_CampPage_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `TW_Campaign_Page_ID` varchar(255) NOT NULL,
  `TW_Campaign_Page_Name` varchar(255) NOT NULL,
  `Added_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ep_twitter_campaign_schedule`
--

CREATE TABLE `ep_twitter_campaign_schedule` (
  `TW_Schedule_ID` int(11) NOT NULL,
  `Trainer_ID` int(11) NOT NULL,
  `TW_Schedule_Desc` longtext NOT NULL,
  `TW_Schedule_Start` datetime NOT NULL,
  `TW_Schedule_End` datetime NOT NULL,
  `TW_Schedule_Status` enum('Active','Deactive') NOT NULL,
  `Created_On` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_twitter_campaign_schedule`
--

INSERT INTO `ep_twitter_campaign_schedule` (`TW_Schedule_ID`, `Trainer_ID`, `TW_Schedule_Desc`, `TW_Schedule_Start`, `TW_Schedule_End`, `TW_Schedule_Status`, `Created_On`) VALUES
(2, 1, 'Hi....hello', '2016-09-23 18:47:09', '2016-09-24 18:48:09', 'Active', '2016-09-23 18:53:56');

-- --------------------------------------------------------

--
-- Table structure for table `ep_users`
--

CREATE TABLE `ep_users` (
  `User_ID` int(11) NOT NULL,
  `Ref_Code` varchar(7) NOT NULL,
  `Ref_Trainer_ID` int(11) NOT NULL,
  `Login_With_Email` enum('yes','no') NOT NULL DEFAULT 'no',
  `Login_With_FB` enum('yes','no') NOT NULL DEFAULT 'no',
  `Login_With_GP` enum('yes','no') NOT NULL DEFAULT 'no',
  `Login_With_TW` enum('yes','no') NOT NULL DEFAULT 'no',
  `Login_With_LI` enum('yes','no') NOT NULL DEFAULT 'no',
  `Facebook_ID` varchar(255) NOT NULL,
  `Google_ID` varchar(255) NOT NULL,
  `Twitter_ID` varchar(255) NOT NULL,
  `Linkedin_ID` varchar(255) NOT NULL,
  `FB_Access_Token` longtext NOT NULL,
  `GP_Access_Token` longtext NOT NULL,
  `TW_Access_Token` longtext NOT NULL,
  `TW_Access_Token_Secret` longtext NOT NULL,
  `LI_Access_Token` longtext NOT NULL,
  `User_Role` enum('trainer','viewer') NOT NULL DEFAULT 'trainer',
  `First_Name` varchar(50) NOT NULL,
  `Last_Name` varchar(50) NOT NULL,
  `Full_Name` varchar(150) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `TW_Screen_Name` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `PayPal_ID` varchar(255) NOT NULL,
  `Email_ID` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Profile_Photo` varchar(255) NOT NULL,
  `Bio` longtext NOT NULL,
  `Domain` varchar(255) NOT NULL,
  `Projects` varchar(255) NOT NULL,
  `Experience` varchar(50) NOT NULL,
  `Facebook_URL` varchar(255) NOT NULL,
  `Google_URL` varchar(255) NOT NULL,
  `Twitter_URL` varchar(255) NOT NULL,
  `LinkedIn_URL` varchar(255) NOT NULL,
  `Payment_Status` enum('yes','no') NOT NULL DEFAULT 'no',
  `Verify_Code` varchar(255) NOT NULL,
  `Profile_Verify` enum('yes','no') NOT NULL DEFAULT 'no',
  `Login_First` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `Joining_Date` datetime NOT NULL,
  `Expiration_Date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ep_users`
--

INSERT INTO `ep_users` (`User_ID`, `Ref_Code`, `Ref_Trainer_ID`, `Login_With_Email`, `Login_With_FB`, `Login_With_GP`, `Login_With_TW`, `Login_With_LI`, `Facebook_ID`, `Google_ID`, `Twitter_ID`, `Linkedin_ID`, `FB_Access_Token`, `GP_Access_Token`, `TW_Access_Token`, `TW_Access_Token_Secret`, `LI_Access_Token`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `TW_Screen_Name`, `Designation`, `PayPal_ID`, `Email_ID`, `Password`, `Profile_Photo`, `Bio`, `Domain`, `Projects`, `Experience`, `Facebook_URL`, `Google_URL`, `Twitter_URL`, `LinkedIn_URL`, `Payment_Status`, `Verify_Code`, `Profile_Verify`, `Login_First`, `Joining_Date`, `Expiration_Date`) VALUES
(1, 'Xgw1mQq', 0, 'yes', 'yes', 'no', 'yes', 'yes', '110198639437630', '', '773166060837015552', 'Z4TXY1bTAY', 'EAALZAaaBwjj8BABPvxZBKC21ZCObnKAPDHQEZAjRoln0glvX2x6wEtqnAGD5q9nESPNBLbfrFTUbn64E9XSMWbUj4lpuWZCy6RaRPjIzfekOz74zWA9ULGUh5RQQuvIeEZCMczKtSNqIrOJSsdaf3qcGkNxkdj2XgZD', '', '773166060837015552-v9lRrghhbzvdy3zj9pTS5eD2fQr2OOq', 'JRNkMk7YM3kjpJcW8zU301vqJE5qZ1LfUWq5zIQXqxbZ1', '78735d5f-4311-40a3-acb9-817370e510ed', 'trainer', 'Sangita', 'LRS', 'Sangita LRS', 'shruthi.lr', 'Shobhanagaraj19', '', 'shobhanagaraj192@gmail.com', 'shobhanagaraj192@gmail.com', 'a4aa860568d8f21b0186474deabb08ddad702e86', 'http://52.37.110.5/uploads/dp/thumb-1474359750_687.jpg', '', 'c2,0, 3, 4,744984938908928kjndjn', 'c.3, 4, 4', '90', '', '', 'https://www.twitter.com/Shobhanagaraj19', 'https://www.linkedin.com/in/shobha-nagaraj-a13971128', 'no', '95f07cdcd30057f7', 'yes', 'No', '2016-09-16 17:47:45', '2016-10-16 17:47:45'),
(5, 'EtvfUY1', 0, 'yes', 'yes', 'no', 'yes', 'yes', '10203721981289463', '', '234300196', 'HrccK63e7_', 'EAALZAaaBwjj8BAEeIlvIfzo7V27UBBDgKipyrEbiC8wUq7UnKfoXQNHpOidQRi7B5qFjsbioUOrgWzMnK7ifTVTZAxbLMxbLJultwuVykiAKj0gutpD5QIaPVtvtKL7KCtxUc1nxCQmbrFesZAR3t2zjQRv26wZD', '', '234300196-hYrCZPWFzEptVZzvrd5W7m4HDaTzRHUllKrwHbul', 'tEFpFcBOb8PcxyQCrfOwUgdVvugomeE5QkaBQR5wmnJFh', 'e09753b6-8242-4d37-9327-8ebf2be263eb', 'trainer', 'Subhro', 'Pramanik', 'Subhro Pramanik', 'subhro.pramanik', 'Subhro_Ricky', '', 'jitdxpert@gmail.com', 'subhro.pramanik7@gmail.com', 'be30e3b987c4038dfbce74a68baabccdd3229ef3', 'http://52.37.110.5/uploads/dp/thumb-1474033067_880.jpg', '', '', '', '', 'https://www.facebook.com/10203721981289463', '', 'https://www.twitter.com/Subhro_Ricky', 'https://www.linkedin.com/in/subhro-pramanik-72a0452a', 'no', '8b0fff7d75d984cb', 'yes', 'No', '2016-09-16 19:05:21', '2016-10-16 19:05:21'),
(11, 'VM2To8D', 0, 'no', 'yes', 'no', 'yes', 'no', '128839304239981', '', '773161203694645248', '', 'EAALZAaaBwjj8BAMJqF4TjPUldPv7C4movJ4k3PTZB5iAZCEHGGMEjgxwnIWHb0B6m7LGIqYQiR319YvziFPkndPOwD4ISQcmZAv86IClbfGrgrdAIfdFy4XBtyNdW85hFpVJz2UR5SX6BajGLNfhxtTBpN8dyiWRUNnM5dJbywZDZD', '', '773161203694645248-ZVu9s8vhAYqCHAtUqUPXdKcIBH29g0C', '', '', 'viewer', 'sneha', 'nagaraj', 'sneha nagaraj', 'sneha.nagaraj', 'Snehanagarajj', '', 'shobhanagaraj192@gmail.com', 'snehanagaraj193@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'http://52.37.110.5/uploads/dp/thumb-1474275285_220.jpg', '', '', '', '', 'https://www.facebook.com/128839304239981', '', 'https://www.twitter.com/Snehanagarajj', '', 'no', '', 'yes', 'No', '2016-09-19 14:18:18', '2016-10-19 14:18:18'),
(12, 'cfvp51m', 1, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'viewer', 'subhash', 'nagaraj', 'subhash nagaraj', 'subhash.nagaraj', '', '', '', 'subhashnagaraj37@gmail.com', 'KC#(m^2W', 'http://52.37.110.5/assets/frontend/images/profile.jpg', '', '', '', '', '', '', '', '', 'no', '53e08a7b1781a06c', 'no', 'Yes', '2016-09-19 19:51:10', '2016-10-19 19:51:10'),
(15, 'XFp9UCf', 0, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'viewer', 'Ricky', 'Wilson', 'Ricky Wilson', 'ricky.wilson', '', '', 'subhro.pramanik7@gmail.com', 'subhro.pramanik4@gmail.com', 'be30e3b987c4038dfbce74a68baabccdd3229ef3', 'http://52.37.110.5/uploads/dp/thumb-1475060058_406.jpg', '', '', '', '', '', '', '', '', 'no', '1616c66625a15cea', 'yes', 'Yes', '2016-09-28 13:27:47', '2016-10-28 13:27:47'),
(16, '9BMDFt4', 0, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'trainer', 'subhash', 'pramanik', 'subhash pramanik', 'subhash.pramanik', '', '', '', 'subhashpramanik37@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'http://52.37.110.5/assets/frontend/images/profile.jpg', '', '', '', '', '', '', '', '', 'no', '894157376e217cf8', 'yes', 'No', '2016-09-28 14:25:05', '2016-10-28 14:25:05'),
(17, 'NsABPKt', 5, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'viewer', '', '', ' ', '.', '', '', '', '', '^j,7K8:%', 'http://52.37.110.5/assets/frontend/images/profile.jpg', '', '', '', '', '', '', '', '', 'no', '364bbe1efcee9296', 'no', 'Yes', '2016-09-28 19:03:32', '2016-10-28 19:03:32'),
(18, 'M2TO7xo', 11, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'viewer', 'Shobha', 'nagaraj', 'Shobha nagaraj', 'shobha.nagaraj', '', '', 'shobhanagaraj192@gmail.com', 'shobhanagaraj193@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'http://52.37.110.5/assets/frontend/images/profile.jpg', '', '', '', '', '', '', '', '', 'no', '2b444539cde2e6dc', 'yes', 'Yes', '2016-09-29 13:51:09', '2016-10-29 13:51:09'),
(19, 'ulAWhOL', 0, 'yes', 'no', 'no', 'no', 'no', '', '', '', '', '', '', '', '', '', 'trainer', 'Jerry', 'Conti', 'Jerry Conti', 'jerry.conti', '', '', '', 'jerry@eprentice.com', '0392f93c6debfaf8f7a3635b79f21688d8624e81', 'http://52.37.110.5/assets/frontend/images/profile.jpg', '', '', '', '', '', '', '', '', 'no', '2913539fba3ceaf1', 'yes', 'No', '2016-10-28 09:50:15', '2016-11-27 09:50:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ep_admin`
--
ALTER TABLE `ep_admin`
  ADD PRIMARY KEY (`Admin_ID`);

--
-- Indexes for table `ep_category`
--
ALTER TABLE `ep_category`
  ADD PRIMARY KEY (`Category_ID`);

--
-- Indexes for table `ep_email_tracking`
--
ALTER TABLE `ep_email_tracking`
  ADD PRIMARY KEY (`Email_ID`),
  ADD KEY `EP_Email_fk0` (`Sender_ID`);

--
-- Indexes for table `ep_facebook_campaign`
--
ALTER TABLE `ep_facebook_campaign`
  ADD PRIMARY KEY (`FB_CampPage_ID`),
  ADD KEY `EP_CampFB_fk0` (`Trainer_ID`) USING BTREE;

--
-- Indexes for table `ep_facebook_campaign_schedule`
--
ALTER TABLE `ep_facebook_campaign_schedule`
  ADD PRIMARY KEY (`FB_Schedule_ID`),
  ADD KEY `EP_FB_CampaignTrainerID_fk1` (`Trainer_ID`) USING BTREE,
  ADD KEY `EP_FB_CampaignID_fk0` (`FB_Campaign_ID`) USING BTREE,
  ADD KEY `EP_FB_CampaignPromo_fk2` (`FB_Schedule_Promotion_ID`) USING BTREE;

--
-- Indexes for table `ep_favorite`
--
ALTER TABLE `ep_favorite`
  ADD PRIMARY KEY (`Favorite_ID`),
  ADD KEY `EP_Favorite_fk0` (`User_ID`),
  ADD KEY `EP_Favorite_fk1` (`Training_ID`);

--
-- Indexes for table `ep_follow`
--
ALTER TABLE `ep_follow`
  ADD PRIMARY KEY (`Follow_ID`),
  ADD KEY `EP_Follow_fk0` (`Followee_ID`),
  ADD KEY `EP_Follow_fk1` (`Follower_ID`),
  ADD KEY `EP_Follow_fk2` (`Notification_ID`);

--
-- Indexes for table `ep_hits`
--
ALTER TABLE `ep_hits`
  ADD PRIMARY KEY (`Hit_ID`),
  ADD KEY `EP_Hits_fk0` (`Trainer_ID`),
  ADD KEY `EP_Hits_fk1` (`Training_ID`);

--
-- Indexes for table `ep_impressions`
--
ALTER TABLE `ep_impressions`
  ADD PRIMARY KEY (`Impression_ID`),
  ADD KEY `EP_Impressions_fk0` (`Affiliator_ID`),
  ADD KEY `EP_Impressions_fk1` (`Aff_Trainer_ID`),
  ADD KEY `EP_Impressions_fk2` (`Member_ID`);

--
-- Indexes for table `ep_notifications`
--
ALTER TABLE `ep_notifications`
  ADD PRIMARY KEY (`Notification_ID`),
  ADD KEY `EP_Notification_fk0` (`User_ID`) USING BTREE;

--
-- Indexes for table `ep_ongoing_training`
--
ALTER TABLE `ep_ongoing_training`
  ADD PRIMARY KEY (`Ongoing_ID`),
  ADD KEY `EP_Ongoing_fk0` (`Training_ID`),
  ADD KEY `EP_Ongoing_fk1` (`Member_ID`),
  ADD KEY `EP_Ongoing_fk2` (`Trainer_ID`),
  ADD KEY `EP_Ongoing_fk3` (`Training_CataID`);

--
-- Indexes for table `ep_payments`
--
ALTER TABLE `ep_payments`
  ADD PRIMARY KEY (`Payment_ID`),
  ADD KEY `EP_Payment_fk0` (`Seller_ID`),
  ADD KEY `EP_Payment_fk1` (`Buyer_ID`),
  ADD KEY `EP_Payment_fk2` (`Product_ID`);

--
-- Indexes for table `ep_preferences`
--
ALTER TABLE `ep_preferences`
  ADD PRIMARY KEY (`Preference_ID`),
  ADD KEY `EP_Pref_UserID_fk0` (`Pref_User_ID`);

--
-- Indexes for table `ep_products`
--
ALTER TABLE `ep_products`
  ADD PRIMARY KEY (`Product_ID`),
  ADD KEY `EP_Product_fk0` (`Trainer_ID`);

--
-- Indexes for table `ep_promotions`
--
ALTER TABLE `ep_promotions`
  ADD PRIMARY KEY (`Promotion_ID`),
  ADD KEY `Trainer_ID` (`Trainer_ID`) USING BTREE,
  ADD KEY `EP_Training_ID_fk0` (`Training_ID`) USING BTREE;

--
-- Indexes for table `ep_promotion_file`
--
ALTER TABLE `ep_promotion_file`
  ADD PRIMARY KEY (`File_ID`),
  ADD KEY `EP_Promotion_File_fk0` (`Promotion_ID`) USING BTREE;

--
-- Indexes for table `ep_promotion_questions`
--
ALTER TABLE `ep_promotion_questions`
  ADD PRIMARY KEY (`Question_ID`),
  ADD KEY `EP_Question_fk0` (`Promotion_ID`) USING BTREE;

--
-- Indexes for table `ep_share_promotion`
--
ALTER TABLE `ep_share_promotion`
  ADD PRIMARY KEY (`Share_ID`),
  ADD KEY `EP_Share_Promotion_fk0` (`Trainer_ID`),
  ADD KEY `EP_Share_Promotion_fk1` (`Promotion_ID`);

--
-- Indexes for table `ep_share_training`
--
ALTER TABLE `ep_share_training`
  ADD PRIMARY KEY (`Share_ID`),
  ADD KEY `EP_Share_Training_fk1` (`Training_ID`),
  ADD KEY `EP_Share_Training_fk0` (`Trainer_ID`);

--
-- Indexes for table `ep_traffic_campaign`
--
ALTER TABLE `ep_traffic_campaign`
  ADD PRIMARY KEY (`Traffic_ID`),
  ADD KEY `EP_Traffic_fk1` (`Traffic_Trainer_ID`),
  ADD KEY `EP_Traffic_fk0` (`Traffic_Training_ID`) USING BTREE;

--
-- Indexes for table `ep_trainings`
--
ALTER TABLE `ep_trainings`
  ADD PRIMARY KEY (`Training_ID`),
  ADD UNIQUE KEY `Training_URL` (`Training_URL`),
  ADD KEY `EP_Trainings_fk0` (`Trainer_ID`),
  ADD KEY `EP_Trainings_fk1` (`Training_Category`),
  ADD KEY `EP_Trainings_fk2` (`Product_ID`) USING BTREE;

--
-- Indexes for table `ep_twitter_campaign`
--
ALTER TABLE `ep_twitter_campaign`
  ADD PRIMARY KEY (`TW_CampPage_ID`),
  ADD KEY `EP_CampTW_fk0` (`Trainer_ID`);

--
-- Indexes for table `ep_twitter_campaign_schedule`
--
ALTER TABLE `ep_twitter_campaign_schedule`
  ADD PRIMARY KEY (`TW_Schedule_ID`),
  ADD KEY `EP_TW_CampaignTrainerID_fk1` (`Trainer_ID`) USING BTREE;

--
-- Indexes for table `ep_users`
--
ALTER TABLE `ep_users`
  ADD PRIMARY KEY (`User_ID`),
  ADD UNIQUE KEY `Email_ID` (`Email_ID`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ep_admin`
--
ALTER TABLE `ep_admin`
  MODIFY `Admin_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ep_category`
--
ALTER TABLE `ep_category`
  MODIFY `Category_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ep_email_tracking`
--
ALTER TABLE `ep_email_tracking`
  MODIFY `Email_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ep_facebook_campaign`
--
ALTER TABLE `ep_facebook_campaign`
  MODIFY `FB_CampPage_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ep_facebook_campaign_schedule`
--
ALTER TABLE `ep_facebook_campaign_schedule`
  MODIFY `FB_Schedule_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ep_favorite`
--
ALTER TABLE `ep_favorite`
  MODIFY `Favorite_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `ep_follow`
--
ALTER TABLE `ep_follow`
  MODIFY `Follow_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `ep_hits`
--
ALTER TABLE `ep_hits`
  MODIFY `Hit_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ep_impressions`
--
ALTER TABLE `ep_impressions`
  MODIFY `Impression_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ep_notifications`
--
ALTER TABLE `ep_notifications`
  MODIFY `Notification_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `ep_ongoing_training`
--
ALTER TABLE `ep_ongoing_training`
  MODIFY `Ongoing_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ep_payments`
--
ALTER TABLE `ep_payments`
  MODIFY `Payment_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ep_preferences`
--
ALTER TABLE `ep_preferences`
  MODIFY `Preference_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ep_products`
--
ALTER TABLE `ep_products`
  MODIFY `Product_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ep_promotions`
--
ALTER TABLE `ep_promotions`
  MODIFY `Promotion_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `ep_promotion_file`
--
ALTER TABLE `ep_promotion_file`
  MODIFY `File_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ep_promotion_questions`
--
ALTER TABLE `ep_promotion_questions`
  MODIFY `Question_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ep_share_promotion`
--
ALTER TABLE `ep_share_promotion`
  MODIFY `Share_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ep_share_training`
--
ALTER TABLE `ep_share_training`
  MODIFY `Share_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ep_traffic_campaign`
--
ALTER TABLE `ep_traffic_campaign`
  MODIFY `Traffic_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ep_trainings`
--
ALTER TABLE `ep_trainings`
  MODIFY `Training_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `ep_twitter_campaign`
--
ALTER TABLE `ep_twitter_campaign`
  MODIFY `TW_CampPage_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ep_twitter_campaign_schedule`
--
ALTER TABLE `ep_twitter_campaign_schedule`
  MODIFY `TW_Schedule_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ep_users`
--
ALTER TABLE `ep_users`
  MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ep_facebook_campaign`
--
ALTER TABLE `ep_facebook_campaign`
  ADD CONSTRAINT `ep_facebook_campaign_ibfk_1` FOREIGN KEY (`Trainer_ID`) REFERENCES `ep_users` (`User_ID`);

--
-- Constraints for table `ep_facebook_campaign_schedule`
--
ALTER TABLE `ep_facebook_campaign_schedule`
  ADD CONSTRAINT `ep_facebook_campaign_schedule_ibfk_1` FOREIGN KEY (`FB_Campaign_ID`) REFERENCES `ep_facebook_campaign` (`FB_CampPage_ID`),
  ADD CONSTRAINT `ep_facebook_campaign_schedule_ibfk_2` FOREIGN KEY (`Trainer_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `ep_facebook_campaign_schedule_ibfk_3` FOREIGN KEY (`FB_Schedule_Promotion_ID`) REFERENCES `ep_promotions` (`Promotion_ID`);

--
-- Constraints for table `ep_favorite`
--
ALTER TABLE `ep_favorite`
  ADD CONSTRAINT `ep_favorite_ibfk_1` FOREIGN KEY (`User_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `ep_favorite_ibfk_2` FOREIGN KEY (`Training_ID`) REFERENCES `ep_trainings` (`Training_ID`);

--
-- Constraints for table `ep_follow`
--
ALTER TABLE `ep_follow`
  ADD CONSTRAINT `ep_follow_ibfk_1` FOREIGN KEY (`Followee_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `ep_follow_ibfk_2` FOREIGN KEY (`Follower_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `ep_follow_ibfk_3` FOREIGN KEY (`Notification_ID`) REFERENCES `ep_notifications` (`Notification_ID`);

--
-- Constraints for table `ep_hits`
--
ALTER TABLE `ep_hits`
  ADD CONSTRAINT `EP_Hits_fk0` FOREIGN KEY (`Trainer_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `EP_Hits_fk1` FOREIGN KEY (`Training_ID`) REFERENCES `ep_trainings` (`Training_ID`);

--
-- Constraints for table `ep_impressions`
--
ALTER TABLE `ep_impressions`
  ADD CONSTRAINT `EP_Impressions_fk0` FOREIGN KEY (`Affiliator_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `EP_Impressions_fk1` FOREIGN KEY (`Aff_Trainer_ID`) REFERENCES `ep_users` (`User_ID`),
  ADD CONSTRAINT `EP_Impressions_fk2` FOREIGN KEY (`Member_ID`) REFERENCES `ep_users` (`User_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

if( strpos($_GET['param'], '/') !== false ) {
	$Training_Param = explode('/', $_GET['param']);
	$Training_URL	= $Training_Param[0];
	$Email_TrackID	= $Training_Param[1];
	$emailInfo 		= get_email_tracking($Email_TrackID);
	if( !empty($emailInfo) && $emailInfo[0]['Email_Link_Status'] == 'Active' && !empty($UID) ) {
		$emquery = "UPDATE `" . $config['db_prefix'] . "email_tracking` SET `Email_Link_Status` = 'Deactive', `Email_Status` = 'Return' WHERE `Email_Unique_TrackID` = '$Email_TrackID'";
		$emrs = $conn->execute($emquery);
	} elseif( !empty($emailInfo) && $emailInfo[0]['Email_Link_Status'] == 'Deactive' && !empty($UID) ) {
		$errorMSG = '<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! This link is already Deactivated.</div>';
	}
} else {
	$Training_URL = $_GET['param'];
}

if( strpos($Training_URL, 'em_') !== false ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL_EM` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'Email';
} elseif( strpos($Training_URL, 'fb_') !== false ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL_FB` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'Facebook';
} elseif( strpos($Training_URL, 'tw_') !== false ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL_TW` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'Twitter';
} elseif( strpos($Training_URL, 'li_') !== false ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL_LI` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'Linkedin';
} elseif( strpos($Training_URL, 'gp_') !== false ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL_GP` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'Google';
} else {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` AS `T`, `" . $config['db_prefix'] . "products` AS `P` WHERE `P`.`Product_ID` = `T`.`Product_ID` AND `T`.`Training_URL` = '$Training_URL' LIMIT 0, 1";
	$Traffic_Source = 'ePrentice';
}
$rs = $conn->execute($query);
if ( $rs ) {
	$training = $rs->getrows();
} else {
	$training = array();
}
$smarty->assign('training', $training[0]);

$Traffic_IP 			= $_SERVER['REMOTE_ADDR'];
$Traffic_Training_ID	= $training[0]['Training_ID'];
$Traffic_Trainer_ID		= $training[0]['Trainer_ID'];
$Traffic_Training		= $Training_URL;

if( $Traffic_Source != 'ePrentice' ) {
	$checkTrafficSQL = "SELECT * FROM `" . $config['db_prefix'] . "traffic_campaign` WHERE `Traffic_IP` = '$Traffic_IP' AND `Traffic_Training_ID` = $Traffic_Training_ID LIMIT 0, 1";
	$checkTrafficRs	 = $conn->execute($checkTrafficSQL);
	$checkTrafficRow = $checkTrafficRs->numrows();	
	if( $checkTrafficRow == 0 ) {
		$trafficSQL = "INSERT INTO `" . $config['db_prefix'] . "traffic_campaign` (`Traffic_Source`, `Traffic_IP`, `Traffic_Training`, `Traffic_Training_ID`, `Traffic_Trainer_ID`, `Traffic_DateTime`) VALUES('$Traffic_Source', '$Traffic_IP', '$Traffic_Training', $Traffic_Training_ID, $Traffic_Trainer_ID, '".date('Y-m-d H:i:s')."')";
		$trafficRs = $conn->execute($trafficSQL);
	}
}

if ( isset($_POST['MemberSubmit']) ) {
	$logwith 		= 'yes';
	$role 			= 'viewer';
	
	$fname 			= addslashes($_POST['fname']);
	$lname 			= addslashes($_POST['lname']);
	$full_name 		= $fname . ' ' . $lname;
	
	$email 			= addslashes($_POST['email']);
	
	$length 		= 8;
    $chars 			= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password 		= substr( str_shuffle( $chars ), 0, $length );
	
	$photo 			= $config['ASSET_URL'] . "/frontend/images/profile.jpg";
	$verify_code 	= substr(md5(uniqid(rand(), true)), 16, 16);
	$verify			= 'no';
	
	$reflength		= 7;
	$refchars 		= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$refcode 		= substr( str_shuffle( $refchars ), 0, $reflength );
	
	$username = strtolower($fname . '.'. $lname);
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Username` = '$username' LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs )
		$userCount = $rs->numrows();
	else 
		$userCount = 0;
	if ( $userCount > 0 ) {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users`";
		$rs = $conn->execute($query);
		if ( $rs )
			$userRows = $rs->numrows() + 1;
		else
			$userRows = 1;
		$username = $username . '.' . $userRows;
	}
	
	$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_Email`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `Email_ID`, `Password`, `Profile_Photo`, `Verify_Code`, `Profile_Verify`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', " . $training[0]['Trainer_ID'] . ", '$logwith', '$role', '$fname', '$lname', '$full_name', '$username', '$email', '$password', '$photo', '$verify_code', '$verify', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$link = $config['BASE_URL'] . '/verify-account/' . $verify_code . '-' . $training[0]['Training_ID'] . '/?verify=Email&setup=true';
		$message = SMEmail::Verify_Email($email, $full_name, $link);
		$subject = 'Verify your ePrentice account';
		if(eprenticeMail($email, $full_name, $subject, $message) == "true") {
			SMRedirect::go($config['BASE_URL'] . '/verify-account/not-verified/');
		} else {
			$error = 'Sorry! We are unable to send the verify mail. <br /><small>Don\'t worry, <a href="#">sent it</a> again.</small>';
		}
	} else {
		$error = 'Something went wrong! Please try again.';
	}
}

$Affiliator_ID = '';
if ( !$UID ) {
	$ipaddress = get_ip_address();
	$query = "SELECT * FROM `" . $config['db_prefix'] . "hits` WHERE `Trainer_ID` = " . $training[0]['Trainer_ID'] . " AND `Training_ID` = " . $training[0]['Training_ID'] . " AND `IP_Address` = '$ipaddress'";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$num = $rs->numrows();
	} else {
		$num = 0;
	}
	if ( $num == 0 ) {
		$query = "INSERT INTO `" . $config['db_prefix'] . "hits`(`Hit_ID`, `Trainer_ID`, `Training_ID`, `IP_Address`, `Visited_On`) VALUES (NULL, " . $training[0]['Trainer_ID'] . ", " . $training[0]['Training_ID'] . ", '$ipaddress', '".date('Y-m-d H:i:s')."')";
	$rs = $conn->execute($query);
	}
} else {
	$Trainer_ID = $training[0]['Trainer_ID'];
	$Member_ID  = $UID;
	$query = "SELECT `Ref_Trainer_ID` FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = $Member_ID LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$user = $rs->getrows();
	} else {
		$user = array();
	}
	$Affiliator_ID = ($user[0]['Ref_Trainer_ID'] == 0 ? $Trainer_ID : $user[0]['Ref_Trainer_ID']);
	
	if ( $Affiliator_ID != $Trainer_ID ) {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "impressions` WHERE `Affiliator_ID` = $Affiliator_ID AND `Aff_Trainer_ID` = $Trainer_ID AND `Member_ID` = $Member_ID LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$ImpNum = $rs->numrows();
		} else {
			$ImpNum = 0;
		}
		if ( $ImpNum == 0 ) {
			$query = "INSERT INTO `" . $config['db_prefix'] . "impressions`(`Impression_ID`, `Affiliator_ID`, `Aff_Trainer_ID`, `Member_ID`, `Created_On`) VALUES (NULL, $Affiliator_ID, $Trainer_ID, $Member_ID, '".date('Y-m-d H:i:s')."')";
			$rs = $conn->execute($query);
		}
	}
	
	if ( isset($_POST['BuySubmit']) ) {
		$Training_ID 	  	= addslashes($_POST['Training_ID']);
		$Trainer_ID 	  	= addslashes($_POST['Trainer_ID']);
		$Affiliator_ID 	  	= $Affiliator_ID;
		$Member_ID 		   	= $UID;
		$Transaction_ID	    = substr(md5(uniqid(rand(), true)), 10, 10);
		$Transaction_Amount = addslashes($_POST['Training_Payment']);
		$Training_URL 		= addslashes($_POST['Training_URL']);
		
		$query = "SELECT * FROM `" . $config['db_prefix'] . "payments` WHERE `Training_ID` = $Training_ID AND `Trainer_ID` = $Trainer_ID AND `Affiliator_ID` = $Affiliator_ID AND `Member_ID` = $Member_ID LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$NumPay = $rs->numrows();
		} else {
			$NumPay = 0;
		}
		
		if ( $NumPay == 0 ) {
			$query = "INSERT INTO `" . $config['db_prefix'] . "payments`(`Payment_ID`, `Training_ID`, `Trainer_ID`, `Affiliator_ID`, `Member_ID`, `Transaction_ID`, `Transaction_Amount`, `Transaction_On`) VALUES (NULL, $Training_ID, $Trainer_ID, $Affiliator_ID, $Member_ID, '$Transaction_ID', $Transaction_Amount, '".date('Y-m-d H:i:s')."')";
			$rs = $conn->execute($query);
			if ( $rs ) {
				SMRedirect::go($config['BASE_URL'] . '/training/' . $Training_URL . '/');
			}
		}
	}
}


if ( $training[0]['Similar_Section'] == 'yes' ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_Category` = " . $training[0]['Training_Category'] . " AND `Training_ID` != " . $training[0]['Training_ID'] . " AND `Trainer_ID` != " . $training[0]['Trainer_ID'];
	$rs = $conn->execute($query);
	if ( $rs ) {
		$similars = $rs->getrows();
	} else {
		$similars = array();
	}
	$smarty->assign('similars', $similars);
} elseif ( $training[0]['Similar_Section'] == 'other' ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_Category` != " . $training[0]['Training_Category'] . " AND `Training_ID` != " . $training[0]['Training_ID'] . " AND `Trainer_ID` != " . $training[0]['Trainer_ID'];
	$rs = $conn->execute($query);
	if ( $rs ) {
		$others = $rs->getrows();
	} else {
		$others = array();
	}
	$smarty->assign('others', $others);
}

$smarty->assign('Affiliator_ID',	$Affiliator_ID);
$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$training[0]['Training_Name'] . ' - ' . $seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
if ( $UID ) {
    $smarty->display('sidebar.tpl');
}
$smarty->display('training.tpl');
$smarty->display('footer.tpl');
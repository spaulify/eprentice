// Form validation APP for Angular.JS

var ePrenticeApp = angular.module('ePrenticeApp', ['ngMessages']);
    
	// Registration Form Controller
	ePrenticeApp.controller('RegisterCtrl', function($scope, $http) {
		$scope.submit = function($event) {
			if($scope.EprenticeRegister.$invalid) {
				$scope.EprenticeRegister.$submitted = true;
				$event.preventDefault();
			}
		},
		$scope.init = function() {
			$scope.register.fname = document.getElementById('fnameReg').getAttribute('title');
			$scope.register.lname = document.getElementById('lnameReg').getAttribute('title');
			$scope.register.email = document.getElementById('emailReg').getAttribute('title');
		}
	});
	
	// Login Form Controller
	ePrenticeApp.controller('LoginCtrl', function($scope, $http) {
		$scope.submit = function($event) {
			if($scope.EprenticeLogin.$invalid) {
				$scope.EprenticeLogin.$submitted = true;
				$event.preventDefault();
			}
		}
	});
	
	// Profile General Info Form Controller
	ePrenticeApp.controller('GeneralInfoCtrl', function($scope, $http) {
		$scope.submit = function($event) {
			if($scope.EprenticeGeneralInfo.$invalid) {
				$scope.EprenticeGeneralInfo.$submitted = true;
				$event.preventDefault();
			}
		},
		$scope.init = function() {
			$scope.generalinfo.fname 	= document.getElementById('fnameOld').getAttribute('title');
			$scope.generalinfo.lname 	= document.getElementById('lnameOld').getAttribute('title');
			$scope.generalinfo.facebook = document.getElementById('facebookOld').getAttribute('title');
			$scope.generalinfo.google 	= document.getElementById('googleOld').getAttribute('title');
			$scope.generalinfo.twitter 	= document.getElementById('twitterOld').getAttribute('title');
			$scope.generalinfo.linkedin = document.getElementById('linkedinOld').getAttribute('title');
		}
	});
	
	// Directive for HTTP Prefix
	ePrenticeApp.directive('httpPrefix', function() {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function(scope, element, attrs, controller) {
				function ensureHttpPrefix(value) {
					if(value && !/^(https?):\/\//i.test(value) && 'https://'.indexOf(value) === -1) {
						controller.$setViewValue('https://' + value);
						controller.$render();
						return 'https://' + value;
					}
					else
						return value;
				}
				controller.$formatters.push(ensureHttpPrefix);
				controller.$parsers.splice(0, 0, ensureHttpPrefix);
			}
		};
	});
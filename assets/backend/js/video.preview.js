(function LocalFileIntroVideoPlayerInit(win) {
	var IntroURL = win.URL || win.webkitURL;
	
	var DisplayIntroMessage = (function DisplayIntroMessageInit() {
		var node = document.querySelector('#IntroVideoMessage');
		return function DisplayIntroMessage(message, isError) {
			node.innerHTML = message;
			node.className = isError ? 'small text-danger' : 'small text-success';
		};
	}());
	
	var PlaySelectedIntroFile = function PlaySelectedIntroFileInit(event) {
		var file = this.files[0];
		var type = file.type;
		var videoInput = document.querySelector('#intro_video');
		var videoNode = document.querySelector('#IntroVideoPlayer');
		var canPlay = videoNode.canPlayType(type);
		canPlay = (canPlay === '' ? 'no' : canPlay);
		if ( type != 'video/mp4' && type != 'video/webm' && type != 'video/ogg' && type != 'video/quicktime' ) {
			var message =  'Video format or MIME type is not supported. Supported formats: MP4, WebM, OGG, MOV'; // + type + '": ' + canPlay;
			videoNode.src = '';
			videoInput.value = '';
		} else {
			var message = 'You can play the video.';
		}
		var isError = canPlay === 'no';
		DisplayIntroMessage(message, isError);
		if (isError) {
			return;
		}
		var fileURL = IntroURL.createObjectURL(file);
		videoNode.src = fileURL;
		setTimeout(function() {
			if ( videoNode.duration > 120 ) {
				videoNode.src = '';
				videoInput.value = '';
				var message = 'Video length is exceeded, select a small video.';
				var isError = videoNode.duration >= 120;
				DisplayIntroMessage(message, isError);
				if (isError) {
					return;
				}
			}
		}, 1000);
	};
	
	if( $('#intro_video').length > 0 ) {
		var intro_video = document.querySelector('#intro_video');
		if ( !IntroURL ) {
			DisplayTrainingMessage('Your browser is not supported!', true);
			return;
		}
		intro_video.addEventListener('change', PlaySelectedIntroFile, false);
	}
}(window));


(function LocalFileTrainingVideoPlayerInit(win) {
	var TrainingURL = win.URL || win.webkitURL;
	
	var DisplayTrainingMessage = (function DisplayTrainingMessageInit() {
		var node = document.querySelector('#TrainingVideoMessage');
		return function DisplayTrainingMessage(message, isError) {
			node.innerHTML = message;
			node.className = isError ? 'small text-danger' : 'small text-success';
		};
	}());
	
	var PlaySelectedTrainingFile = function PlaySelectedTrainingFileInit(event) {
		var file = this.files[0];
		var type = file.type;
		var videoInput = document.querySelector('#training_video');
		var videoNode = document.querySelector('#TrainingVideoPlayer');
		var canPlay = videoNode.canPlayType(type);
		canPlay = (canPlay === '' ? 'no' : canPlay);
		if ( type != 'video/mp4' && type != 'video/webm' && type != 'video/ogg' && type != 'video/quicktime' ) {
			var message =  'Video format or MIME type is not supported. Supported formats: MP4, WebM, OGG, MOV'; // + type + '": ' + canPlay;
			videoNode.src = '';
			videoInput.value = '';
		} else {
			var message = 'You can play the video.';
		}
		var isError = canPlay === 'no';
		DisplayTrainingMessage(message, isError);
		if (isError) {
			return;
		}
		var fileURL = TrainingURL.createObjectURL(file);
		videoNode.src = fileURL;
	};
	
	if( $('#training_video').length > 0 ) {
		var training_video = document.querySelector('#training_video');
		if ( !TrainingURL ) {
			DisplayTrainingMessage('Your browser is not supported!', true);
			return;
		}
		training_video.addEventListener('change', PlaySelectedTrainingFile, false);
	}
	
	if( $('#TrainingVideoPlayer').length > 0 ) {
		var training_player = document.querySelector('#TrainingVideoPlayer');
		var visible_time = document.querySelector('#visible_time');
		training_player.onseeking = function() {
			visible_time.setAttribute('max', Math.round(training_player.duration));
			visible_time.value = Math.round(training_player.currentTime);
		};
		visible_time.onfocus = function() {
			visible_time.setAttribute('max', Math.round(training_player.duration));
		};
	}
}(window));
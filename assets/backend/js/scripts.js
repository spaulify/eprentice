// Admin Part
$(document).ready(function() {
	"use strict";
	
	Stripe.setPublishableKey(Stripe_Publication_Key);
	
	// Fixed of Modal Issue
	$('.backdropFixed .modal').appendTo($("body"));
	
	// Remove # sign after facebook login
	if ( (location.hash == "#_=_" || location.href.slice(-1) == "#_=_") ) {
		RemoveHash();
	}
	// Script end
	
	$('#wrapper.toggled-2 #sidebar-wrapper').hover(function() {
		
	}, function() {
		$(this).find('.collapse').removeClass('in');
	});
	
	// Digital Clock
	/*DateTime('DateTimeContainer');
	setInterval(function() {
		DateTime('DateTimeContainer');
	}, 1000);*/
	
	// Create & Edit Training Script
	var TrainingFormOptions = {
		target			: '',
		beforeSubmit	: BeforeTrainingFormSubmit,
		success			: AfterTrainingFormSuccess,
		uploadProgress	: OnTrainingFormProgress,
	};
	$('#training_form').submit(function() {
		$(this).ajaxSubmit(TrainingFormOptions);
		return false;
	});
	// Script end
	
	// Create & Edit Promotion Script
	var PromotionFormOptions = {
		target			: '',
		beforeSubmit	: BeforePromotionFormSubmit,
		success			: AfterPromotionFormSuccess,
		uploadProgress	: OnPromotionFormProgress,
	};
	$('#promotion_form').submit(function() {
		$(this).ajaxSubmit(PromotionFormOptions);           
		return false;
	});
	// Script end
	
	// Create & Edit Product Script
	var ProductFormOptions = {
		target			: '',
		beforeSubmit	: BeforeProductFormSubmit,
		success			: AfterProductFormSuccess,
		uploadProgress	: OnProductFormProgress,
	};
	$('#product_form').submit(function() {
		$(this).ajaxSubmit(ProductFormOptions);           
		return false;
	});
	// Script end
	
	// Send Training Invitation Script
	var SendInvitationFormOptions = {
		target			: '',
		beforeSubmit	: BeforeSendInvitationFormSubmit,
		success			: AfterSendInvitationFormSuccess,
		uploadProgress	: OnSendInvitationFormProgress,
	};
	$('#send_invitation').submit(function() {
		$(this).ajaxSubmit(SendInvitationFormOptions);           
		return false;
	});
	// Script end
	
	$('.promo-btn').on('click', function() {
		if ( $(this).hasClass('promo-next') ) {
			if ( $(this).parents('.questionnaire-set').find("input[type='radio']").is(":checked") ) {
				if ( $(this).parents('.questionnaire-set').next('.questionnaire-set').hasClass('hide') ) {
					$(this).parents('.questionnaire-set').next('.questionnaire-set').removeClass('hide');
					$(this).parents('.questionnaire-set').addClass('hide');
					$(this).parents('form').prev('.survey_msg').html('');
				}
			} else {
				$(this).parents('form').prev('.survey_msg').html('<div class="alert alert-danger alert-dismissable" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please select your answer first!</div>');
			}
		} else if ( $(this).hasClass('promo-prev') ) {
			if ( $(this).parents('.questionnaire-set').prev('.questionnaire-set').hasClass('hide') ) {
				$(this).parents('.questionnaire-set').prev('.questionnaire-set').removeClass('hide');
				$(this).parents('.questionnaire-set').addClass('hide');
				$(this).parents('form').prev('.survey_msg').html('');
			}
		} else {
			var SendSurveyFormOptions = {
				success	: function(responseText, statusText, xhr, $form) {
					$form.prev('.survey_msg').html(responseText);
				}
			};
			$(this).parents('form').ajaxSubmit(SendSurveyFormOptions);
			return false;
		}
	});
	
	// Hide/Show edit button on hover
	$('.btnwrap').hover(function() {
		$(this).find('.edit-btn').fadeIn(50);
	}, function() {
		$(this).find('.edit-btn').fadeOut(50);
	});
	// Script end
	
	// Notification scrollbar 
	$('.notification-btn').click(function(e) {
		setTimeout(function() {
	        $(".nano").nanoScroller();
		}, 100);
    });
	// Script end
	
	// Alert message disappear after 5 seconds
	setInterval(function() {
		$('.AlertMessage').slideUp('slow');
	}, 5000);
	// Script end
	
	// Custom scrollbar
	$(".nano").nanoScroller();
	// Script end
	
	// Training Share email address field format
	if ( $('#emails').length > 0 ) {
		$('#emails').tagsInput({
			width:'auto',
			defaultText:'',
		});
	}
	// Script end
	
	// Training cost toggle for free or paid
	$('.training-cost').on('click', function() {
		if ( $(this).val() == 'paid' ) {
			$('#payment_amount').removeClass('hide');
		} else {
			$('#payment_amount').addClass('hide');
		}
	});
	// Script end
	
	if( $('#bio').length > 0 ) {
		$('#bio-div').hide();
		$('#gen-div').hide();
		$('#biolength').text(500 - $('#bio').val().length);
		// Character Counter of bio in profile page
		$('#bio').change(function(){
			var remaining = 500 - $('#bio').val().length;
			$('#biolength').text(remaining);	
		});
		$('#bio').keyup(function(){
			var remaining = 500 - $('#bio').val().length;
			$('#biolength').text(remaining);	
		});
		// Script end
	
		// Bio form validation in profile page
		$('#bio_submit').click(function() {
			if($('#bio').val().length > 500) {
				$('#bio-div').fadeIn("slow");
				$('#bio-msg').html('More than 500 characters inserted!');
				return false;
			} else {
				$('#bio-div').fadeOut("slow");
				$('#bio-msg').html('');
				return true;
			}
		});
		// Script end
	}
	
	if ( $('.bxslider').length > 0 ) {
		$('.bxslider').bxSlider({
			auto: false,
		});
	}
	
	get_notifications();
	$('.notification-btn').on('click', function(e) {
		e.preventDefault();

		get_notifications();
	});
	
	notifications();
	setInterval(function() {
		notifications();
	}, 5000);
	
	if ( $('#payment-form').length > 0 ) {
		new Card({
			form: document.querySelector('#payment-form'),
			container: '.card-wrapper'
		});
		
		$("#payment-form").submit(function(event) {
			var mystr 	 = $('#CardExpiry').val();
			var myarr 	 = mystr.split(" / ");
			var expMonth = myarr[0];
			var expYear  = myarr[1];
			$('.submit-button').attr("disabled", "disabled");
			Stripe.createToken({
				number		: $('#CardNumber').val(),
				cvc			: $('#CardCVC').val(),
				exp_month	: expMonth,
				exp_year	: expYear,
				name		: $('#NameOnCard').val()
			}, stripeResponseHandler);
			return false;
		});
	}
	
	$('input[name="video_type"').on('click', function() {
		if ( $(this).val() == 'custom' ) {
			$('#intro_video').removeClass('hide');
			$('#IntroVideoPlayer').removeClass('hide');
			$('#intro_video_youtube').addClass('hide');
			$('#IntroVideoiFrame').addClass('hide');
		} else {
			$('#intro_video').addClass('hide');
			$('#IntroVideoPlayer').addClass('hide');
			$('#intro_video_youtube').removeClass('hide');
			$('#IntroVideoiFrame').removeClass('hide');
		}
	});
	
	$('input[name="training_video_type"').on('click', function() {
		if ( $(this).val() == 'custom' ) {
			$('#training_video').removeClass('hide');
			$('#TrainingVideoPlayer').removeClass('hide');
			$('#training_video_youtube').addClass('hide');
			$('#TrainingVideoiFrame').addClass('hide');
		} else {
			$('#training_video').addClass('hide');
			$('#TrainingVideoPlayer').addClass('hide');
			$('#training_video_youtube').removeClass('hide');
			$('#TrainingVideoiFrame').removeClass('hide');
		}
	});
});

function validateYouTubeUrl(elem) {
	var url = $(elem).val();
	if (url != undefined || url != '') {
		var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
		var match = url.match(regExp);
		if (match && match[2].length == 11) {
			return true;
		} else {
			return false;
		}
	}
}

function stripeResponseHandler(status, response) {
	if (response.error) {
		$('.submit-button').removeAttr("disabled");
		$("#payment_msg").html(response.error.message);
	} else {
		var form$ = $("#payment-form");
		var token = response['id'];
		form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
		form$.get(0).submit();
	}
}

// JavaScript for Top Clock
/*function DateTime(ContainerID) {
	var date 	= new Date;
	var diem 	= "AM";
	var year 	= date.getFullYear();
	var month 	= date.getMonth();
	var months 	= new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
	var d = date.getDate();
	var day = date.getDay();
	var days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
	var h = date.getHours();
	if(h < 10) {
		h = "0" + h;
	}
	if(h > 12) {
		diem = "PM"
	} else {
		diem = "AM"
	}
	var m = date.getMinutes();
	if(m < 10) {
		m = "0" + m;
	}
	var s = date.getSeconds();
	if(s < 10) {
		s = "0" + s;
	}
	var result = '' + days[day] + ', ' + months[month] + ' ' + d + ', ' + year + ' ' + h + ':' + m + ':' + s + ' ' + diem;
	document.getElementById(ContainerID).innerHTML = result;
	setTimeout('DateTime("' + ContainerID + '");','1000');
	return true;
}*/

function get_notifications() {
	if( $('#notification-zone').length > 0 ) {
		$('#notification-zone').html('<a><div class="notification-item no-item"><h4 class="item-title"><img src="' + asset_url + '/images/ajax-loader.gif" alt="loading..." /></h4></div></a>');
		$.ajax({
			type: "POST",
			url: ajax_url + "/get-notification/",
			success: function(response) {
				setTimeout(function() {
					$('#notification-zone').html(response);
				}, 500);
			}
		});
	}
}

function notifications() {
	if( $('#notification-center').length > 0 ) {
		$.ajax({
			type: "POST",
			url: ajax_url + "/notifications/",
			success: function(response) {
				if( response == 0) {
					$('.notification-count').remove();
				} else if( response > 99 ) {
					$('#notification-center').html('<i class="notification-count">99+</i>');	
				} else {
					$('#notification-center').html('<i class="notification-count">' + response + '</i>');	
				}
			}
		});
	}
}

function RemoveHash() {
    var scrollV, scrollH, loc = window.location;
    if ('replaceState' in history) {
        history.replaceState('', document.title, loc.pathname + loc.search);
    } else {
        scrollV = document.body.scrollTop;
        scrollH = document.body.scrollLeft;
        loc.hash = '';
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
    }
}

/* Training Function */
function BeforeTrainingFormSubmit(formData, jqForm, options) {
	if ( window.File && window.FileReader && window.FileList && window.Blob ) {
		if( $('input[name="video_type"]:checked').val() == 'custom' && $('#intro_video').length > 0 && $('#intro_video').val() != false ) {
			var fsize = $('#intro_video')[0].files[0].size;
			var ftype = $('#intro_video')[0].files[0].type;
			switch(ftype) {
				case 'video/mp4':
				case 'video/webm':
				case 'video/ogg':
				case 'video/quicktime':
					break;
				default:
					$('#training_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Intro Video File!</div>');
					return false;
			}
		}
		if( $('input[name="training_video_type"]:checked').val() == 'custom' && $('#training_video').length > 0 && $('#training_video').val() != false ) {
			var fsize = $('#training_video')[0].files[0].size;
			var ftype = $('#training_video')[0].files[0].type;
			switch(ftype) {
				case 'video/mp4':
				case 'video/webm':
				case 'video/ogg':
				case 'video/quicktime':
					break;
				default:
					$('#training_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Training Video File!</div>');
					return false;
			}
		}
		/*if( $('#training_thumb').length > 0 && $('#training_thumb').val() != false ) {
			var fsize = $('#training_thumb')[0].files[0].size;
			var ftype = $('#training_thumb')[0].files[0].type;
			switch(ftype) {
				case 'image/jpg':
				case 'image/jpeg':
				case 'image/png':
					break;
				default:
					$('#training_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Training Thumbnail File!</div>');
					return false;
			}
		}*/
	} else {
		$('#training_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please upgrade your browser, because your current browser lacks some new features we need!</div>');
		return false
	}
}

function OnTrainingFormProgress(event, position, total, percentComplete) {
	$('#progressbox').show();
	$('#progressbar').width(percentComplete + '%');
	$('#statustxt').html(percentComplete + '%');
	$('#statustxt').css('color','#000');
	if ( percentComplete > 50 ) {
		$('#statustxt').css('color','#000');
	}
}

function AfterTrainingFormSuccess(responseText, statusText, xhr, $form) {
	$('#progressbox').delay(1000).fadeOut();
	if( responseText == 'Success' ) {
		if ( $('#page_type').val() == 'Insert' ) {
			$('#training_form')[0].reset();
			$('#training_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! Training created.</div>');
		} else {
			$('#training_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! Training updated.</div>');
		}
		$('.steps').find('li:first-child').trigger('click');
		setTimeout(function() {
			window.location.href = base_url + '/trainings/';
		}, 2000);
	} else {
		$('#training_msg').html(responseText);	
	}
}
/* Training Function */

/* Promotion Function */
function BeforePromotionFormSubmit(formData, jqForm, options) {
	if ( window.File && window.FileReader && window.FileList && window.Blob ) {
		var promotion_type = $('#promotion_type').val();
		if(promotion_type == "" || promotion_type == false) {
			$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please select promotion type for list before submit!</div>');
			return false;
		} else if(promotion_type == "Image") {
			if ( $('#training_id_img').val() != false && $('#promotion_title_img').val() != false && $('#promo_start_date_img').val() != false && $('#promo_end_date_img').val() != false && $('#promotion_desc_img').val() != false ) {
				if ( $('#promotion_file_img').val() != false ) {
					//var fileID	= $(this).attr('id');
					var fsize   = $('#promotion_file_img')[0].files[0].size;
					var ftype   = $('#promotion_file_img')[0].files[0].type;
					if ( ftype == 'image/jpg' || ftype == 'image/jpeg' || ftype == 'image/png' || ftype == 'image/gif' || ftype == 'image/bmp' ) {
						switch(ftype) {
							case 'image/jpg':
							case 'image/jpeg':
							case 'image/png':
							case 'image/gif':
							case 'image/bmp':
								break;
							default:
								$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Promotion Image File!</div>');
								return false;
						}
					}
				}
			} else {
				if ( $('#page_type').val() == 'Insert' ) {
					$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field(s) cannot be blank.</div>');
					return false;
				} else {
					return true;
				}
			}
		} else if(promotion_type == "Video") {
			if ( $('#training_id_vid').val() != false && $('#promotion_title_vid').val() != false && $('#promo_start_date_vid').val() != false && $('#promo_end_date_vid').val() != false && $('#promotion_desc_vid').val() != false ) {
				if ( $('#promotion_file_vid').val() != false ) {
					//var fileID	= $(this).attr('id');
					var fsize   = $('#promotion_file_vid')[0].files[0].size;
					var ftype   = $('#promotion_file_vid')[0].files[0].type;
					if ( ftype == 'video/mp4' || ftype == 'video/webm' || ftype == 'video/ogg' || ftype == 'video/quicktime' ) {
						switch(ftype) {
							case 'video/mp4':
							case 'video/webm':
							case 'video/ogg':
							case 'video/quicktime':
								break;
							default:
								$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Promotion Video File!</div>');
								return false;
						}
					}
				}
			} else {
				if ( $('#page_type').val() == 'Insert' ) {
					$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field(s) cannot be blank.</div>');
					return false;
				} else {
					return true;
				}
			}
		} else if(promotion_type == "Question") {
			if ( $('#training_id_que').val() != false && $('#promotion_title_que').val() != false && $('#promo_start_date_que').val() != false && $('#promo_end_date_que').val() != false && $('#promotion_desc_que').val() != false && $('#promotion_question').val() != false && $('#promotion_answer_one').val() != false && $('#promotion_marks_one').val() != false && $('#promotion_answer_two').val() != false && $('#promotion_marks_two').val() != false && $('#promotion_answer_three').val() != false && $('#promotion_marks_three').val() != false && $('#promotion_answer_four').val() != false && $('#promotion_marks_four').val() != false ) {
				return true;
			}
		}
	} else {
		$('#promotion_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please upgrade your browser, because your current browser lacks some new features we need!</div>');
		return false;
	}
}

function OnPromotionFormProgress(event, position, total, percentComplete) {
	$('#progressbox').show();
	$('#progressbar').width(percentComplete + '%');
	$('#statustxt').html(percentComplete + '%');
	$('#statustxt').css('color','#000');
	if ( percentComplete > 50 ) {
		$('#statustxt').css('color','#000');
	}
}

function AfterPromotionFormSuccess(responseText, statusText, xhr, $form) {
	$('#progressbox').delay(1000).fadeOut();
	if( responseText == 'Success' ) {
		if ( $('#page_type').val() == 'Insert' ) {
			$('#promotion_form')[0].reset();
			$('#promotion_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! New Promotion created.</div>');
		} else {
			$('#promotion_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! Promotion updated.</div>');
		}
		setTimeout(function() {
			window.location.href = base_url + '/promotions/';
		}, 2000);
	} else {
		$('#promotion_msg').html(responseText);
	}
}
/* Promotion Function */

/* Product Function */
function BeforeProductFormSubmit(formData, jqForm, options) {
	if ( window.File && window.FileReader && window.FileList && window.Blob ) {
		if ( $('#product_name').val() != false && $('#product_image').val() != false && $('#product_link').val() != false && $('#product_price').val() != false && $('#product_refund').val() != false && $('#product_desc').val() != false ) {
			var fsize = $('#product_image')[0].files[0].size;
			var ftype = $('#product_image')[0].files[0].type;
			if ( ftype == 'image/jpg' || ftype == 'image/jpeg' || ftype == 'image/png' || ftype == 'image/gif' || ftype == 'image/bmp' ) {
				switch(ftype) {
					case 'image/jpg':
					case 'image/jpeg':
					case 'image/png':
					case 'image/gif':
					case 'image/bmp':
						break;
					default:
						$('#product_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported Product Image File!</div>');
						return false;
				}
			}
		} else {
			if ( $('#page_type').val() == 'Insert' ) {
				$('#product_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field(s) cannot be blank.</div>');
				return false;
			} else {
				return true;
			}
		}		
	} else {
		$('#product_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please upgrade your browser, because your current browser lacks some new features we need!</div>');
		return false
	}
}

function OnProductFormProgress(event, position, total, percentComplete) {
	$('#progressbox').show();
	$('#progressbar').width(percentComplete + '%');
	$('#statustxt').html(percentComplete + '%');
	$('#statustxt').css('color','#000');
	if ( percentComplete > 50 ) {
		$('#statustxt').css('color','#000');
	}
}

function AfterProductFormSuccess(responseText, statusText, xhr, $form) {
	$('#progressbox').delay(1000).fadeOut();
	if( responseText == 'Success' ) {
		if ( $('#page_type').val() == 'Insert' ) {
			$('#product_form')[0].reset();
			$('#product_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! New Product created.</div>');
		} else {
			$('#product_msg').html('<div class="alert alert-success alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! Product updated.</div>');
		}
		setTimeout(function() {
			window.location.href = base_url + '/products/';
		}, 2000);
	} else {
		$('#product_msg').html(responseText);	
	}
}
/* Product Function */

/* Send Invitation Function */
function BeforeSendInvitationFormSubmit(formData, jqForm, options) {
	if ( window.File && window.FileReader && window.FileList && window.Blob ) {
		if ( $('#send_training').val() != false && $('#email_csv').val() != false ) {
			var fsize = $('#email_csv')[0].files[0].size;
			var ftype = $('#email_csv')[0].files[0].type;
			if ( ftype == 'text/csv' ) {
				switch(ftype) {
					case 'text/csv':
						break;
					default:
						$('#invitation_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unsupported CSV File!</div>');
						return false;
				}
			}
		} else {
			$('#invitation_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field(s) cannot be blank.</div>');
			return false;
		}		
	} else {
		$('#invitation_msg').html('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please upgrade your browser, because your current browser lacks some new features we need!</div>');
		return false
	}
}

function OnSendInvitationFormProgress(event, position, total, percentComplete) {
	$('#progressbox').show();
	$('#progressbar').width(percentComplete + '%');
	$('#statustxt').html(percentComplete + '%');
	$('#statustxt').css('color','#000');
	if ( percentComplete > 50 ) {
		$('#statustxt').css('color','#000');
	}
}

function AfterSendInvitationFormSuccess(responseText, statusText, xhr, $form) {
	$('#progressbox').delay(1000).fadeOut();
	$('#send_invitation')[0].reset();
	$('#invitation_msg').html(responseText);
}
/* Send Invitation Function */

// Display Preview
var _URL = window.URL || window.webkitURL;
function displayPreview(files) {
	var extLower   = $('#photo').val().split('.').pop().toLowerCase();
	var file = files[0];
	var img = new Image();
	var sizeKB = file.size / 1024;
	
	if(($.inArray(extLower, ['jpg' , 'jpeg' , 'png']) != -1)) {
		img.onload = function() {
			if((sizeKB <= 4096) && (img.height >= 480)) {
				$('#previewIMG').remove();
				$('#preview').html(img);
				$('#preview > img').addClass('img-circle img-thumbnail');
				$('#preview > img').css({"width":"80","height":"80","margin":"0"});
				$('#gen-div').fadeOut("slow");
				$('#gen-msg').html('');
				return true;
			} else {			
				if(sizeKB > 4096) {
					$('#gen-div').fadeIn("slow");
					$('#gen-msg').html('Image is too big.');
					return false;	
				}						
				if(img.height < 480) {
					$('#gen-div').fadeIn("slow");
					$('#gen-msg').html('Image height is too small.');
					return false;	
				}
				if(($.inArray(extLower, ['jpg' , 'jpeg' , 'png']) == -1)) {
					$('#gen-div').fadeIn("slow");
					$('#gen-msg').html('File format not supported, only "JPG"/"PNG".');
					return false;
				}
			}
		}
		img.src = _URL.createObjectURL(file);
	} else {
		$('#gen-div').fadeIn("slow");
		$('#gen-msg').html('File format not supported, only "JPG"/"PNG".');
		return false;
	}
}

// Promotions & Training Validation
$(document).ready(function() {
	
	// Tooltip Script
	$('[data-toggle="tooltip"]').tooltip();
	
	// Datatable Scripts
	$('#fb_campaign_list').DataTable();
	$('#fb_schedule_list').DataTable();
	$('#tw_campaign_list').DataTable();
	$('#tw_schedule_list').DataTable();
	
	// Bootstrap Toohle Switch
	$(".fb_campaign_status").switchButton({
	  	on_label: 'Active',
	  	off_label: 'Deactive',
		labels_placement: "right",
		width: 90,
		height: 30,
		button_width: 40
	});
	$(".tw_schedule_status").switchButton({
	  	on_label: 'Active',
	  	off_label: 'Deactive',
		labels_placement: "right",
		width: 90,
		height: 30,
		button_width: 40
	});
	
	// Form's Page Type Detector
	var page_type = $('#page_type').val();
	
	/* Email Data Modal Show */
	$('.EmailData').on('click', function(e) {
		e.preventDefault();
		var modalID = $(this).attr('id');
		$('#EmailDetails_' + modalID).modal('show');
	});

	/* Training Update */
	$('.admin_update_training').on('click', function(e) {
		e.preventDefault();
		var trainingID 	= $(this).attr('id');
		var trainerID 	= $(this).attr('rel');
		var statname    = $(this).attr('name');
		var status 		= $(this).data('status');
		$('#admin_training_status_modal').modal('show');
		$('#stat_training_id').val(trainingID);
		$('#stat_trainer_id').val(trainerID);
		$('.stat').html(statname);
		$('#stat_training_status').val(status);
	});
	
	$('#status_update').on('click', function(e) {
		e.preventDefault();
		var trainingID 	= $('#stat_training_id').val();
		var trainerID	= $('#stat_trainer_id').val();
		var status 		= $('#stat_training_status').val();
		if( ((trainingID == "" || trainingID == false) && (trainerID == "" || trainerID == false) && (status == "" || status == false)) || (trainingID == "" || trainingID == false) || (trainerID == "" || trainerID == false) || (status == "" || status == false) ) {
			$('#training_status_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Training status update went wrong!</div>');
			return false;
		} else {
			var dataString 	= 'trainingID=' + trainingID + '&trainerID=' + trainerID + '&status=' + status;
			$.ajax({
				type: "POST",
				url: ajax_url + "/update-training-status/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#training_status_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Training status successfully updated.</div>');
						setTimeout(function() {
							$('#admin_remove_training_modal').modal('hide');
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 0 ) {
						$('#training_status_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to update status of the training!</div>');
						return false;
					}
				}
			});
		}
	});
	/* Training Update */

	/* Training Erase */
	$('.admin_remove_training').on('click', function(e) {
		e.preventDefault();
		var trainingID 	= $(this).attr('id');
		var trainerID 	= $(this).attr('rel');
		$('#admin_remove_training_modal').modal('show');
		$('#training_id').val(trainingID);
		$('#trainer_id').val(trainerID);
	});
	
	$('#remove_training').on('click', function(e) {
		e.preventDefault();
		var trainingID 	= $('#training_id').val();
		var trainerID	= $('#trainer_id').val();
		if( ((trainingID == "" || trainingID == false) && (trainerID == "" || trainerID == false)) || (trainingID == "" || trainingID == false) || (trainerID == "" || trainerID == false) ) {
			$('#training_erase_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Training erasing went wrong!</div>');
			return false;
		} else {
			var dataString 	= 'trainingID=' + trainingID + '&trainerID=' + trainerID;
			$.ajax({
				type: "POST",
				url: ajax_url + "/remove-training/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#training_erase_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Training successfully erased.</div>');
						setTimeout(function() {
							$('#admin_remove_training_modal').modal('hide');
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 0 ) {
						$('#training_erase_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to erase the training!</div>');
						return false;
					}
				}
			});
		}
	});
	/* Training Erase */
	
	/* Reset Password */
	$('#reset').click(function() {
		var emailaddr 	 = $('#emailaddr').val();
		var confemail 	 = $('#confemail').val();
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if( (emailaddr == "" || emailaddr == false) && (confemail == "" || confemail == false) ) {
			$('#emailaddr_error').show(500);
			$('#emailaddr_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Email address required!</div>');
			$('#confemail_error').show(500);
			$('#confemail_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Confirm email address required!</div>');
			$('#emailaddr').focus();
			return false;
		} else {
			if(emailaddr == "" || emailaddr == false) {
				$('#confemail_error').hide(500);
				$('#confemail_error').html('');
				$('#emailaddr_error').show(500);
				$('#emailaddr_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Email address required!</div>');				
				$('#emailaddr').focus();
				return false;
			} else if(!emailaddr.match(emailPattern)) {
				$('#confemail_error').hide(500);
				$('#confemail_error').html('');
				$('#emailaddr_error').show(500);
				$('#emailaddr_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Invalid email address!</div>');				
				$('#emailaddr').focus();
				return false;
			} else if(confemail == "" || confemail == false) {
				$('#emailaddr_error').hide(500);
				$('#emailaddr_error').html('');
				$('#confemail_error').show(500);
				$('#confemail_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Confrim email address required!</div>');				
				$('#confemail').focus();
				return false;
			} else if(!confemail.match(emailPattern)) {
				$('#emailaddr_error').hide(500);
				$('#emailaddr_error').html('');
				$('#confemail_error').show(500);
				$('#confemail_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Invalid email address!</div>');				
				$('#confemail').focus();
				return false;
			} else if(confemail != emailaddr) {
				$('#emailaddr_error').hide(500);
				$('#emailaddr_error').html('');
				$('#confemail_error').show(500);
				$('#confemail_error').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Confrim email address is not matched!</div>');				
				$('#confemail').focus();
				return false;
			} else {
				$('#emailaddr_error').hide(500);
				$('#emailaddr_error').html('');
				$('#confemail_error').hide(500);
				$('#confemail_error').html('');
				var dataString = 'emailaddr=' + emailaddr;
				$.ajax({
					type: "POST",
					url: ajax_url + "/forgot-password/",
					async: dataString.async,
					data: dataString,
					success: function(data) {
						if( data == 1 ) {
							$('#forgot_password_success').html('<div class="alert alert-success alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Success! new password send to your email.</div>');
						} else if( data == 0 ) {
							$('#forgot_password_success').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong!</div>');
						}
					}
				});
				return true;
			}
		}
	});
	/* Reset Password */
	
	/* Set Preference */
	$('#set_preference').on('click', function(e) {
		e.preventDefault();
		var userCAT = new Array();
		var userID  = $('#user_id').val();
		$('input[name^="user_cat"]').each(function() {
			if( $(this).prop('checked') ) {
				userCAT += $(this).val() + ',';
			}
		});
		if( userCAT == "" || userCAT == false ) {
			$('#preferences_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Setting Preferences went wrong!</div>');
			return false;
		} else {
			var dataString 	= 'userCAT=' + userCAT + '&userID=' + userID;
			$.ajax({
				type: "POST",
				url: ajax_url + "/set-preference/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#preferences_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Preferences set successfully.</div>');
						setTimeout(function() {
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 0 ) {
						$('#preferences_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to set preferences!</div>');
						return false;
					}
				}
			});
		}		
	});
	/* Set Preference */
	
	/* Notification Read & Unread Status */
	$('#notification-zone, .full-notification').on('click', '.notification-link', function(e) {
		e.preventDefault();
		var Button 	 	= $(this);
		var notiID 	 	= Button.attr('rel');
		var notiLink 	= Button.attr('href');
		var notiTarget 	= Button.attr('target');
		var dataString 	= 'Notification_ID=' + notiID;
		$.ajax({
			type: "POST",
			url: ajax_url + "/update-notification/",
			async: dataString.async,
			data: dataString,
			success: function(data) {
				Button.find('.notification-item').addClass(data);
				notifications();
				setTimeout(function() {
					if ( notiTarget == '_blank' ) {
						window.open(notiLink, '_blank');
					} else {
						window.location.href = notiLink;
					}
				}, 500);
			}
		});
	});
	/* Notification Read & Unread Status */
	
	/* Mark all read for Notifications */
	$('#mark_all_read').click(function(e) {
		e.preventDefault();
		var Button 	 	= $(this);
		var userID 	 	= Button.attr('rel');
		var dataString 	= 'User_ID=' + userID;
		$.ajax({
			type: "POST",
			url: ajax_url + "/markallread-notification/",
			async: dataString.async,
			data: dataString,
			success: function(data) {
				Button.find('.notification-item').addClass(data);
				notifications();
			}
		});
	});
	/* Mark all read for Notifications */
	
	/* Favorite Training */
	$('.favorite').click(function(e) {
		e.preventDefault();

		var Button		= $(this);
		var Training_ID	= Button.siblings('input[name="Training_ID"]').val();
		var User_ID		= Button.siblings('input[name="User_ID"]').val();
		var User_Role	= Button.siblings('input[name="User_Role"]').val();
		
		if( User_Role == "" && Training_ID == "" && User_ID == "" ) {
			Button.parent().siblings('.favorite_msg').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Nothing to add favorite!</div>');
			return false;
		} else {
			if( User_Role == "viewer" ) {
				var dataString 	= 'Training_ID=' + Training_ID + '&User_ID=' + User_ID;
				$.ajax({
					type: "POST",
					url: ajax_url + "/favorite/",
					async: dataString.async,
					data: dataString,
					success: function(data) {
						if( data == 'fa-heart' ) {
							Button.parent().siblings('.favorite_msg').html('<div class="alert alert-success alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>You added this training to favorite!</div>');
							Button.children('i').removeClass('fa-heart-o');
							Button.children('i').addClass('fa-heart');
						} else if( data == 'fa-heart-o' ) {
							Button.parent().siblings('.favorite_msg').html('<div class="alert alert-success alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>You removed this training from favorite!</div>');
							Button.children('i').removeClass('fa-heart');
							Button.children('i').addClass('fa-heart-o');
						}
					}
				});
			}
		}
	});
	/* Favorite Training */
	
	/* Follow Trainer */
	$('.follow').click(function(e) {
		e.preventDefault();
		
		var Button			= $(this);
		var Followee_ID		= Button.parent().siblings('input[name="Followee_ID"]').val();
		var Follower_ID		= Button.parent().siblings('input[name="Follower_ID"]').val();
		var User_Role		= Button.parent().siblings('input[name="User_Role"]').val();
		
		if( User_Role == "" && Followee_ID == "" && Follower_ID == "" ) {
			Button.parents('.video-desc').siblings('.video-wrap').find('.follow_msg').html('<div class="alert alert-danger alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Nothing to follow!</div>');
			return false;
		} else {
			if( User_Role == "viewer" ) {
				var dataString 	= 'Followee_ID=' + Followee_ID + '&Follower_ID=' + Follower_ID;
				$.ajax({
					type: "POST",
					url: ajax_url + "/follow/",
					async: dataString.async,
					data: dataString,
					success: function(data) {
						if( data == 'Follow' ) {
							Button.parents('.video-desc').siblings('.video-wrap').find('.follow_msg').html('<div class="alert alert-success alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>You are not following this trainer!</div>');
							Button.removeClass('following');
							$('.followee-' + Followee_ID).removeClass('following');
						} else if( data == 'Unfollow' ) {
							Button.parents('.video-desc').siblings('.video-wrap').find('.follow_msg').html('<div class="alert alert-success alert-dismissable text-center AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>You are now following this trainer!</div>');
							Button.addClass('following');
							$('.followee-' + Followee_ID).addClass('following');
						}
						Button.text('+ '+data);
						$('.followee-' + Followee_ID).text('+ '+data);
					}
				});
			}
		}
	});
	/* Follow Trainer */
	
	/* Facebook Campaign and Schedule Scripts */
	$('.add_fb_campaign').on('click', function(e) {
		e.preventDefault();
		$('#fb_new_campaign').modal('show');
	});	
	$('.fb_add_schedule').on('click', function(e) {
		e.preventDefault();
		
		var btn = $(this);
		var CampID = btn.attr('rel');
		
		$('#fb_campaign_id').val(CampID);
		$('#fb_schedule_start_date').datetimepicker({format: 'YYYY-MM-DD H:m:s',});
		$('#fb_schedule_end_date').datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});
		$('#fb_schedule_start_date').on("dp.change", function (e) {$('#fb_schedule_end_date').data("DateTimePicker").minDate(e.date);});
		$('#fb_schedule_end_date').on("dp.change", function (e) {$('#fb_schedule_start_date').data("DateTimePicker").maxDate(e.date);});
		$('#fb_schedule_start_date_div').on('click', function() {$('#fb_schedule_start_date').focus();});
		$('#fb_schedule_end_date_div').on('click', function() {$('#fb_schedule_end_date').focus();});
		$('#fb_campaign_schedule_modal').modal('show');
		
		if( $('#fb_schedule_desc').length > 0 ) {
			$('#fb_schedule_desc_length').text(500 - $('#fb_schedule_desc').val().length);
			// Character Counter of Facebook Schedule Description
			$('#fb_schedule_desc').change(function(){
				var remaining = 500 - $('#fb_schedule_desc').val().length;
				$('#fb_schedule_desc_length').text(remaining);	
			});
			$('#fb_schedule_desc').keyup(function(){
				var remaining = 500 - $('#fb_schedule_desc').val().length;
				$('#fb_schedule_desc_length').text(remaining);	
			});
			// Script end
		}
		
		$('#fb_save_schedule').on('click', function(e) {
			e.preventDefault();
			var fb_schedule_promo		= $('#fb_schedule_promo').val();
			var fb_schedule_title		= $('#fb_schedule_title').val();
			var fb_schedule_start_date	= $('#fb_schedule_start_date').val();
			var fb_schedule_end_date	= $('#fb_schedule_end_date').val();
			var fb_schedule_desc		= $('#fb_schedule_desc').val();
			var fb_campaign_id			= $('#fb_campaign_id').val();
			var fb_trainer_id			= $('#fb_trainer_id').val();
			if((fb_schedule_promo == "" || fb_schedule_promo == false) && (fb_schedule_title == "" || fb_schedule_title == false) && (fb_schedule_start_date == "" || fb_schedule_start_date == false) && (fb_schedule_end_date == "" || fb_schedule_end_date == false) && (fb_schedule_desc == "" || fb_schedule_desc == false) && (fb_campaign_id == "" || fb_campaign_id == false) && (fb_trainer_id == "" || fb_trainer_id == false)) {
				$('#fb_schedule_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field\'s can\'t be blank!</div>');
				return false;
			} else {
				if(fb_schedule_promo == "" || fb_schedule_promo == false) {
					$('#fb_schedule_promo_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Promotion for campaign!</div>');
					return false;	
				} else if(fb_schedule_title == "" || fb_schedule_title == false) {
					$('#fb_schedule_title_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter Title for the campaign!</div>');
					return false;	
				} else if(fb_schedule_start_date == "" || fb_schedule_start_date == false) {
					$('#fb_schedule_start_date_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select campaign start date!</div>');
					return false;
				} else if(fb_schedule_end_date == "" || fb_schedule_end_date == false) {
					$('#fb_schedule_end_date_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select campaign end date!</div>');
					return false;	
				} else if(fb_schedule_desc == "" || fb_schedule_desc == false) {
					$('#fb_schedule_desc_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter description for the campaign!</div>');
					return false;	
				} else if( fb_schedule_desc.length > 500 ) {
					$('#fb_schedule_desc_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>More than 500 characters inserted!</div>');
					return false;
				} else if(fb_campaign_id == "" || fb_campaign_id == false) {
					$('#fb_campaign_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign cannot be blank!</div>');
					return false;	
				} else if(isNaN(fb_campaign_id)) {
					$('#fb_campaign_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Campaign Info!</div>');
					return false;	
				} else if(fb_trainer_id == "" || fb_trainer_id == false) {
					$('#fb_trainer_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Trainer cannot be blank!</div>');
					return false;	
				} else if(isNaN(fb_trainer_id)) {
					$('#fb_trainer_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Trainer Info!</div>');
					return false;	
				} else {
					var dataString = $('#newFBCampForm').serialize();
					$.ajax({
						type: "POST",
						url: ajax_url + "/facebook-schedule/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#fb_schedule_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Updated successfully for the Campaign.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_modal').modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 2 ) {
								$('#fb_schedule_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Added successfully for the Campaign.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_modal').modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#fb_schedule_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Adding went wrong!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});
	$('.fb_edit_schedule').on('click', function(e) {
		e.preventDefault();
		var campID = $(this).attr('rel');
		$('#fb_campaign_schedule_edit_' + campID).modal('show');		
		$('#fb_campaign_id_'+campID).val(campID);
		$('#fb_schedule_start_date_'+campID).datetimepicker({format: 'YYYY-MM-DD H:m:s',});
		$('#fb_schedule_end_date_'+campID).datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});		
		$('#fb_schedule_start_date_'+campID).on("dp.change", function (e) {$('#fb_schedule_end_date_'+campID).data("DateTimePicker").minDate(e.date);});		
		$('#fb_schedule_end_date_'+campID).on("dp.change", function (e) {$('#fb_schedule_start_date_'+campID).data("DateTimePicker").maxDate(e.date);});		
		$('#fb_schedule_start_date_div_'+campID).on('click', function() {$('#fb_schedule_start_date_'+campID).focus();});
		$('#fb_schedule_end_date_div_'+campID).on('click', function() {$('#fb_schedule_end_date_'+campID).focus();});
		
		if( $('#fb_schedule_desc_'+campID).length > 0 ) {
			$('#fb_schedule_desc_length_'+campID).text(500 - $('#fb_schedule_desc_'+campID).val().length);
			// Character Counter of Facebook Schedule Description
			$('#fb_schedule_desc_').change(function(){
				var remaining = 500 - $('#fb_schedule_desc_'+campID).val().length;
				$('#fb_schedule_desc_length_'+campID).text(remaining);	
			});
			$('#fb_schedule_desc_'+campID).keyup(function(){
				var remaining = 500 - $('#fb_schedule_desc_'+campID).val().length;
				$('#fb_schedule_desc_length_'+campID).text(remaining);	
			});
			// Script end
		}
		
		$('#fb_edit_schedule_'+campID).on('click', function(e) {
			e.preventDefault();
			var fb_schedule_promo		= $('#fb_schedule_promo_'+campID).val();
			var fb_schedule_title		= $('#fb_schedule_title_'+campID).val();
			var fb_schedule_start_date	= $('#fb_schedule_start_date_'+campID).val();
			var fb_schedule_end_date	= $('#fb_schedule_end_date_'+campID).val();
			var fb_schedule_desc		= $('#fb_schedule_desc_'+campID).val();
			var fb_campaign_id			= $('#fb_campaign_id_'+campID).val();
			var fb_trainer_id			= $('#fb_trainer_id_'+campID).val();
			if((fb_schedule_promo == "" || fb_schedule_promo == false) && (fb_schedule_title == "" || fb_schedule_title == false) && (fb_schedule_start_date == "" || fb_schedule_start_date == false) && (fb_schedule_end_date == "" || fb_schedule_end_date == false) && (fb_schedule_desc == "" || fb_schedule_desc == false) && (fb_campaign_id == "" || fb_campaign_id == false) && (fb_trainer_id == "" || fb_trainer_id == false)) {
				$('#fb_schedule_msg_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field\'s can\'t be blank!</div>');
				return false;
			} else {
				if(fb_schedule_promo == "" || fb_schedule_promo == false) {
					$('#fb_schedule_promo_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Promotion for campaign!</div>');
					return false;	
				} else if(fb_schedule_title == "" || fb_schedule_title == false) {
					$('#fb_schedule_title_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter Title for the campaign!</div>');
					return false;	
				} else if(fb_schedule_start_date == "" || fb_schedule_start_date == false) {
					$('#fb_schedule_start_date_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select campaign start date!</div>');
					return false;
				} else if(fb_schedule_end_date == "" || fb_schedule_end_date == false) {
					$('#fb_schedule_end_date_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select campaign end date!</div>');
					return false;	
				} else if(fb_schedule_desc == "" || fb_schedule_desc == false) {
					$('#fb_schedule_desc_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter description for the campaign!</div>');
					return false;	
				} else if( fb_schedule_desc.length > 500 ) {
					$('#fb_schedule_desc_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>More than 500 characters inserted!</div>');
					return false;
				} else if(fb_campaign_id == "" || fb_campaign_id == false) {
					$('#fb_campaign_id_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign cannot be blank!</div>');
					return false;	
				} else if(isNaN(fb_campaign_id)) {
					$('#fb_campaign_id_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Campaign Info!</div>');
					return false;	
				} else if(fb_trainer_id == "" || fb_trainer_id == false) {
					$('#fb_trainer_id_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Trainer cannot be blank!</div>');
					return false;	
				} else if(isNaN(fb_trainer_id)) {
					$('#fb_trainer_id_error_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Trainer Info!</div>');
					return false;	
				} else {
					var dataString 	= 'fb_schedule_promo=' + fb_schedule_promo + '&fb_schedule_title=' + fb_schedule_title + '&fb_schedule_start_date=' + fb_schedule_start_date + '&fb_schedule_end_date=' + fb_schedule_end_date + '&fb_schedule_desc=' + fb_schedule_desc + '&fb_campaign_id=' + fb_campaign_id + '&fb_trainer_id=' + fb_trainer_id;
					$.ajax({
						type: "POST",
						url: ajax_url + "/facebook-schedule/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#fb_schedule_msg_'+campID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Updated successfully for the Campaign.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_edit_'+campID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 2 ) {
								$('#fb_schedule_msg_'+campID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Added successfully for the Campaign.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_edit_'+campID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#fb_schedule_msg_'+campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Adding went wrong!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});
	$('.fb_erase_schedule').on('click', function(e) {
		e.preventDefault();
		var scheduleID 	= $(this).attr('id');
		var campID 		= $(this).attr('rel');
		$('#fb_erase_campaign_schedule').modal('show');
		$('#fb_erase_schedule_id').val(scheduleID);
		$('#fb_erase_campaign_id').val(campID);
	});
	$('#fb_add_campaign').on('click', function(e) {
		e.preventDefault();
		var pageName 	= $('#fb_pages').children(":selected").attr("id");
		var pageID	 	= $('#fb_pages').val();
		var pageAccess	= $('#fb_pages').children(":selected").attr("title");
		if( ((pageID == "" || pageID == false) && (pageName == "" || pageName == false) && (pageAccess == "" || pageAccess == false)) || (pageID == "" || pageID == false) || (pageName == "" || pageName == false) || (pageAccess == "" || pageAccess == false) ) {
			$('#fb_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign page can\'t be blank!</div>');
			return false;
		} else {
			var dataString 	= 'pageID=' + pageID + '&pageName=' + pageName + '&pageAccess=' + pageAccess;
			$.ajax({
				type: "POST",
				url: ajax_url + "/facebook-campaign/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#fb_campaign_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign page added successfully.</div>');
						setTimeout(function() {
							$('#fb_new_campaign').modal('hide');
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 4 ) {
						$('#fb_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign page adding went wrong!</div>');
						return false;
					} else if( data == 2 ) {
						$('#fb_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to check campaign page details!</div>');
						return false;
					} else if( data == 0 ) {
						$('#fb_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Campaign page already added in the list!</div>');
						return false;
					}
				}
			});
		}
	});
	$('.fb_campage_id').on('click', function(e) {
		e.preventDefault();
		var campID = $(this).attr('id');
		$('#fb_campaign_schedule_lists_' + campID).modal('show');
		$('#fb_campaign_status_' + campID).change(function() {	
			if( $(this).prop("checked") ) {
				var fbStatus1 = "Active";
				var userID	 = $('#fb_status_user_id_' + campID).val();
				var scheduleID	 = $('#fb_status_schedule_id_' + campID).val();
				if( ((scheduleID == "" || scheduleID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (userID == "" || userID == false) ) {
					$('#fb_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule activation went wrong!</div>');
					return false;
				} else {
					var dataString 	= 'scheduleID=' + scheduleID + '&userID=' + userID + '&fbStatus=' + fbStatus1;
					$.ajax({
						type: "POST",
						url: ajax_url + "/update-facebook-status/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Scheduled Campaign successfully Activated.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_lists_' + scheduleID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to Activate the scheduled Campaign!</div>');
								return false;
							} else if( data == 2 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Change Start Date and End Date!</div>');
								return false;
							}
						}
					});
				}
			} else {
				var fbStatus2 = "Deactive";
				var userID	 = $('#fb_status_user_id_' + campID).val();
				var scheduleID	 = $('#fb_status_schedule_id_' + campID).val();
				if( ((scheduleID == "" || scheduleID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (userID == "" || userID == false) ) {
					$('#fb_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule deactivation went wrong!</div>');
					return false;
				} else {
					var dataString 	= 'scheduleID=' + scheduleID + '&userID=' + userID + '&fbStatus=' + fbStatus2;
					$.ajax({
						type: "POST",
						url: ajax_url + "/update-facebook-status/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Scheduled Campaign successfully Deactivated.</div>');
								setTimeout(function() {
									$('#fb_campaign_schedule_lists_' + campID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to Deactivate the scheduled Campaign!</div>');
								return false;
							} else if( data == 2 ) {
								$('#fb_status_msg_' + campID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Change Start Date and End Date!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});
	$('#fb_erase_schedule').on('click', function(e) {
		e.preventDefault();
		var scheduleID 	= $('#fb_erase_schedule_id').val();
		var campID		= $('#fb_erase_campaign_id').val();
		var userID		= $('#fb_erase_user_id').val();
		if( ((scheduleID == "" || scheduleID == false) && (campID == "" || campID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (campID == "" || campID == false) || (userID == "" || userID == false) ) {
			$('#fb_erase_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule erasing went wrong!</div>');
			return false;
		} else {
			var dataString 	= 'scheduleID=' + scheduleID + '&campID=' + campID + '&userID=' + userID;
			$.ajax({
				type: "POST",
				url: ajax_url + "/remove-facebook-schedule/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#fb_erase_campaign_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule successfully erased.</div>');
						setTimeout(function() {
							$('#fb_erase_campaign_schedule').modal('hide');
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 0 ) {
						$('#fb_erase_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to erase the schedule!</div>');
						return false;
					}
				}
			});
		}
	});
	/* Facebook Campaign and Schedule Scripts */
	
	/* Twitter Campaign and Schedule Scripts */
	$('.add_tw_campaign').on('click', function(e) {
		e.preventDefault();
		$('#tw_new_campaign').modal('show');
		if( $('#tw_schedule_desc').length > 0 ) {
			$('#tw_schedule_desc_length').text(140 - $('#tw_schedule_desc').val().length);
			// Character Counter of Facebook Schedule Description
			$('#tw_schedule_desc').change(function(){
				var remaining = 140 - $('#tw_schedule_desc').val().length;
				$('#tw_schedule_desc_length').text(remaining);	
			});
			$('#tw_schedule_desc').keyup(function(){
				var remaining = 140 - $('#tw_schedule_desc').val().length;
				$('#tw_schedule_desc_length').text(remaining);	
			});
			// Script end
		}
		
		$('#tw_schedule_start_date').datetimepicker({format: 'YYYY-MM-DD H:m:s',});
		$('#tw_schedule_end_date').datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});
		$('#tw_schedule_start_date').on("dp.change", function (e) {$('#tw_schedule_end_date').data("DateTimePicker").minDate(e.date);});
		$('#tw_schedule_end_date').on("dp.change", function (e) {$('#tw_schedule_start_date').data("DateTimePicker").maxDate(e.date);});
		$('#tw_schedule_start_date_div').on('click', function() {$('#tw_schedule_start_date').focus();});
		$('#tw_schedule_end_date_div').on('click', function() {$('#tw_schedule_end_date').focus();});		
		$('#tw_campaign_schedule_modal').modal('show');
		
		$('#tw_save_schedule').on('click', function(e) {
			e.preventDefault();
			var tw_schedule_desc		= $('#tw_schedule_desc').val();
			var tw_schedule_start_date	= $('#tw_schedule_start_date').val();
			var tw_schedule_end_date	= $('#tw_schedule_end_date').val();
			var tw_trainer_id			= $('#tw_trainer_id').val();
			
			if((tw_schedule_desc == "" || tw_schedule_desc == false) && (tw_schedule_start_date == "" || tw_schedule_start_date == false) && (tw_schedule_end_date == "" || tw_schedule_end_date == false) && (tw_trainer_id == "" || tw_trainer_id == false)) {
				$('#tw_schedule_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field\'s can\'t be blank!</div>');
				return false;
			} else {
				if(tw_schedule_desc == "" || tw_schedule_desc == false) {
					$('#tw_schedule_desc_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter content for Tweet!</div>');
					return false;	
				} else if( tw_schedule_desc.length > 140 ) {
					$('#tw_schedule_desc_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>More than 140 characters inserted!</div>');
					return false;
				} else if(tw_schedule_start_date == "" || tw_schedule_start_date == false) {
					$('#tw_schedule_start_date_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Tweet start date!</div>');
					return false;
				} else if(tw_schedule_end_date == "" || tw_schedule_end_date == false) {
					$('#tw_schedule_end_date_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Tweet end date!</div>');
					return false;	
				} else if(tw_trainer_id == "" || tw_trainer_id == false) {
					$('#tw_trainer_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Trainer cannot be blank!</div>');
					return false;	
				} else if(isNaN(tw_trainer_id)) {
					$('#tw_trainer_id_error').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Trainer Info!</div>');
					return false;	
				} else {
					var dataString = $('#newTWCampForm').serialize();
					$.ajax({
						type: "POST",
						url: ajax_url + "/twitter-schedule/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#tw_schedule_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Tweet Added successfully.</div>');
								setTimeout(function() {
									$('#tw_campaign_schedule').modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#tw_schedule_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Tweet Adding went wrong!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});
	$('.tw_edit_schedule').on('click', function(e) {
		e.preventDefault();
		var scheduleID = $(this).attr('id');
		$('#tw_campaign_schedule_edit_' + scheduleID).modal('show');		
		$('#tw_schedule_id_'+scheduleID).val(scheduleID);
		$('#tw_schedule_start_date_'+scheduleID).datetimepicker({format: 'YYYY-MM-DD H:m:s',});
		$('#tw_schedule_end_date_'+scheduleID).datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});		
		$('#tw_schedule_start_date_'+scheduleID).on("dp.change", function (e) {$('#tw_schedule_end_date_'+scheduleID).data("DateTimePicker").minDate(e.date);});		
		$('#tw_schedule_end_date_'+scheduleID).on("dp.change", function (e) {$('#tw_schedule_start_date_'+scheduleID).data("DateTimePicker").maxDate(e.date);});		
		$('#tw_schedule_start_date_div_'+scheduleID).on('click', function() {$('#tw_schedule_start_date_'+scheduleID).focus();});
		$('#tw_schedule_end_date_div_'+scheduleID).on('click', function() {$('#tw_schedule_end_date_'+scheduleID).focus();});
		
		if( $('#tw_schedule_desc_'+scheduleID).length > 0 ) {
			$('#tw_schedule_desc_length_'+scheduleID).text(140 - $('#tw_schedule_desc_'+scheduleID).val().length);
			// Character Counter of Facebook Schedule Description
			$('#tw_schedule_desc_').change(function(){
				var remaining = 140 - $('#tw_schedule_desc_'+scheduleID).val().length;
				$('#tw_schedule_desc_length_'+scheduleID).text(remaining);	
			});
			$('#tw_schedule_desc_'+scheduleID).keyup(function(){
				var remaining = 140 - $('#tw_schedule_desc_'+scheduleID).val().length;
				$('#tw_schedule_desc_length_'+scheduleID).text(remaining);	
			});
			// Script end
		}
		
		$('#tw_edit_schedule_'+scheduleID).on('click', function(e) {
			e.preventDefault();
			var tw_schedule_start_date	= $('#tw_schedule_start_date_'+scheduleID).val();
			var tw_schedule_end_date	= $('#tw_schedule_end_date_'+scheduleID).val();
			var tw_schedule_desc		= $('#tw_schedule_desc_'+scheduleID).val();
			var tw_schedule_id			= $('#tw_schedule_id_'+scheduleID).val();
			var tw_trainer_id			= $('#tw_trainer_id_'+scheduleID).val();
			if((tw_schedule_start_date == "" || tw_schedule_start_date == false) && (tw_schedule_end_date == "" || tw_schedule_end_date == false) && (tw_schedule_desc == "" || tw_schedule_desc == false) && (tw_schedule_id == "" || tw_schedule_id == false) && (tw_trainer_id == "" || tw_trainer_id == false)) {
				$('#tw_schedule_msg_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Field\'s can\'t be blank!</div>');
				return false;
			} else {
				if(tw_schedule_desc == "" || tw_schedule_desc == false) {
					$('#tw_schedule_desc_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Enter content for Tweet!</div>');
					return false;	
				} else if( tw_schedule_desc.length > 140 ) {
					$('#tw_schedule_desc_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>More than 140 characters inserted!</div>');
					return false;
				} else if(tw_schedule_start_date == "" || tw_schedule_start_date == false) {
					$('#tw_schedule_start_date_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Tweet start date!</div>');
					return false;
				} else if(tw_schedule_end_date == "" || tw_schedule_end_date == false) {
					$('#tw_schedule_end_date_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Select Tweet end date!</div>');
					return false;	
				} else if(tw_schedule_id == "" || tw_schedule_id == false) {
					$('#tw_schedule_id_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule Info cannot be blank!</div>');
					return false;	
				} else if(isNaN(tw_schedule_id)) {
					$('#tw_schedule_id_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Schedule Info!</div>');
					return false;	
				} else if(tw_trainer_id == "" || tw_trainer_id == false) {
					$('#tw_trainer_id_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Trainer cannot be blank!</div>');
					return false;	
				} else if(isNaN(tw_trainer_id)) {
					$('#tw_trainer_id_error_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Something went wrong with Trainer Info!</div>');
					return false;	
				} else {
					var dataString 	= 'tw_schedule_start_date=' + tw_schedule_start_date + '&tw_schedule_end_date=' + tw_schedule_end_date + '&tw_schedule_desc=' + tw_schedule_desc + '&tw_schedule_id=' + tw_schedule_id + '&tw_trainer_id=' + tw_trainer_id;
					$.ajax({
						type: "POST",
						url: ajax_url + "/twitter-update-schedule/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#tw_schedule_msg_'+scheduleID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Tweet Updated successfully.</div>');
								setTimeout(function() {
									$('#tw_campaign_schedule_edit_'+scheduleID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#tw_schedule_msg_'+scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Tweet Update went wrong!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});
	$('.tw_schedule_id').on('click', function(e) {
		e.preventDefault();
		var scheduleID = $(this).attr('id');
		$('#tw_schedule_lists_' + scheduleID).modal('show');
		$('#tw_schedule_status_' + scheduleID).change(function() {
			if( $(this).prop("checked") ) {
				var twStatus1 = "Active";
				var userID	 = $('#tw_status_user_id_' + scheduleID).val();
				if( ((scheduleID == "" || scheduleID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (userID == "" || userID == false) ) {
					$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule activation went wrong!</div>');
					return false;
				} else {
					var dataString 	= 'scheduleID=' + scheduleID + '&userID=' + userID + '&twStatus=' + twStatus1;
					$.ajax({
						type: "POST",
						url: ajax_url + "/update-twitter-status/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Scheduled Tweet successfully Activated.</div>');
								setTimeout(function() {
									$('#tw_schedule_lists_' + scheduleID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to Activate the scheduled Tweet!</div>');
								return false;
							} else if( data == 2 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Change Start Date and End Date!</div>');
								return false;
							}
						}
					});
				}
			} else {
				var twStatus2 = "Deactive";
				var userID	 = $('#tw_status_user_id_' + scheduleID).val();
				if( ((scheduleID == "" || scheduleID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (userID == "" || userID == false) ) {
					$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule deactivation went wrong!</div>');
					return false;
				} else {
					var dataString 	= 'scheduleID=' + scheduleID + '&userID=' + userID + '&twStatus=' + twStatus2;
					$.ajax({
						type: "POST",
						url: ajax_url + "/update-twitter-status/",
						async: dataString.async,
						data: dataString,
						success: function(data) {
							if( data == 1 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Scheduled Tweet successfully Deactivated.</div>');
								setTimeout(function() {
									$('#tw_schedule_lists_' + scheduleID).modal('hide');
									window.location.href = window.location.href;
								}, 2000);
							} else if( data == 0 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to Deactivate the scheduled Tweet!</div>');
								return false;
							} else if( data == 2 ) {
								$('#tw_status_msg_' + scheduleID).html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Change Start Date and End Date!</div>');
								return false;
							}
						}
					});
				}
			}
		});
	});	
	$('.tw_erase_schedule').on('click', function(e) {
		e.preventDefault();
		var scheduleID 	= $(this).attr('id');
		$('#tw_erase_schedule').modal('show');
		$('#tw_erase_schedule_id').val(scheduleID);
	});
	$('#tw_remove_schedule').on('click', function(e) {
		e.preventDefault();
		var scheduleID 	= $('#tw_erase_schedule_id').val();
		var userID		= $('#tw_erase_user_id').val();
		if( ((scheduleID == "" || scheduleID == false) && (userID == "" || userID == false)) || (scheduleID == "" || scheduleID == false) || (userID == "" || userID == false) ) {
			$('#tw_erase_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Schedule erasing went wrong!</div>');
			return false;
		} else {
			var dataString 	= 'scheduleID=' + scheduleID + '&userID=' + userID;
			$.ajax({
				type: "POST",
				url: ajax_url + "/remove-twitter-schedule/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#tw_erase_campaign_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Scheduled Tweet successfully erased.</div>');
						setTimeout(function() {
							$('#tw_erase_campaign_schedule').modal('hide');
							window.location.href = window.location.href;
						}, 2000);
					} else if( data == 0 ) {
						$('#tw_erase_campaign_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Unable to erase the scheduled Tweet!</div>');
						return false;
					}
				}
			});
		}		
	});
	/* Twitter Campaign and Schedule Scripts */
	
	/* PayPal ID Check & Submit */
	$('#send_invite_fb').on('click', function() {$('#paypal_id_check').modal('show');});
	$('#send_invite_tw').on('click', function() {$('#paypal_id_check').modal('show');});
	$('#send_invite_li').on('click', function() {$('#paypal_id_check').modal('show');});
	$('#send_invite_email').on('click', function() {$('#paypal_id_check').modal('show');});
	window.onload = function() {
		if( $('#login_first').length > 0 ) {
			var login_first = $('#login_first').val();
			if( login_first == "Yes" ) {
				$('#login_first_modal').modal('show');	
			}
			$('#login_first_modal').on('hidden.bs.modal', function () {
				var userID		= $('#user_id').val();
				var dataString 	= 'userID=' + userID;
				$.ajax({
					type: "POST",
					url: ajax_url + "/update-user/",
					async: dataString.async,
					data: dataString,
					success: function(data) {
						if( data == 1 ) {
							//window.location.href = window.location.href;
						} else {
							$('#first_login_error').html('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Error! Something went wrong!</div>');
						}
					}
				});
			});
		}
	};
	$('#paypal_save').click(function(e) {
		e.preventDefault();
		
		var userID			= $('#userID').val();
		var paypalID		= $('#paypal_id').val();
		var emailPattern 	= /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		if( paypalID == "" || paypalID == false ) {
			$('#paypal_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Field can\'t be blank!</div>');
			return false;
		} else if(!paypalID.match(emailPattern)) {
			$('#paypal_msg').html('<div class="alert alert-danger alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Invalid email address!</div>');
			return false;
		} else {
			var dataString 	= 'userID=' + userID + '&paypalID=' + paypalID;
			$.ajax({
				type: "POST",
				url: ajax_url + "/update-paypal/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					if( data == 1 ) {
						$('#paypal_msg').html('<div class="alert alert-success alert-dismissable text-left AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>PayPal ID updated successfully.</div>');
					}
					setTimeout(function() {
						$('#paypal_id_check').modal('hide');
						window.location.href = window.location.href;
					}, 2000);
				}
			});
		}
	});
	/* PayPal ID Check & Submit */
	
	/* Promotion Sctipts Start */
	$('#promo_start_date_img').datetimepicker({format: 'YYYY-MM-DD H:m:s'});
	$('#promo_end_date_img').datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});
	$("#promo_start_date_img").on("dp.change", function (e) {$('#promo_end_date_img').data("DateTimePicker").minDate(e.date);});
	$("#promo_end_date_img").on("dp.change", function (e) {$('#promo_start_date_img').data("DateTimePicker").maxDate(e.date);});
	$('#promo_start_date_img_div').on('click', function() {$('#promo_start_date_img').focus();});
	$('#promo_end_date_img_div').on('click', function() {$('#promo_end_date_img').focus();});
	
	$('#promo_start_date_vid').datetimepicker({format: 'YYYY-MM-DD H:m:s'});
	$('#promo_end_date_vid').datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});
	$("#promo_start_date_vid").on("dp.change", function (e) {$('#promo_end_date_vid').data("DateTimePicker").minDate(e.date);});
	$("#promo_end_date_vid").on("dp.change", function (e) {$('#promo_start_date_vid').data("DateTimePicker").maxDate(e.date);});
	$('#promo_start_date_vid_div').on('click', function() {$('#promo_start_date_vid').focus();});
	$('#promo_end_date_vid_div').on('click', function() {$('#promo_end_date_vid').focus();});
	
	$('#promo_start_date_que').datetimepicker({format: 'YYYY-MM-DD H:m:s'});
	$('#promo_end_date_que').datetimepicker({format: 'YYYY-MM-DD H:m:s',useCurrent: false});
	$("#promo_start_date_que").on("dp.change", function (e) {$('#promo_end_date_que').data("DateTimePicker").minDate(e.date);});
	$("#promo_end_date_que").on("dp.change", function (e) {$('#promo_start_date_que').data("DateTimePicker").maxDate(e.date);});
	$('#promo_start_date_que_div').on('click', function() {$('#promo_start_date_que').focus();});
	$('#promo_end_date_que_div').on('click', function() {$('#promo_end_date_que').focus();});
	
	$('#promotion_type').change(function() {
		var promotion_type = $(this).val();
		if( promotion_type == "" || promotion_type == false ) {
			$('#ImageBlock').css('display', 'none');
			$('#VideoBlock').css('display', 'none');
			$('#QuestionBlock').css('display', 'none');
		} else if( promotion_type == "Image" ) {			
			$('#VideoBlock').css('display', 'none');
			$('#QuestionBlock').css('display', 'none');
			$('#ImageBlock').css('display', 'block');
			// Promotions Validation
			$('#promotion_submit_img').click(function() {
				var trainer_id_img		= $('#trainer_id_img').val();				
				var promotion_title_img = $('#promotion_title_img').val();
				var promotion_file_img  = $('#promotion_file_img').val();
				var promotion_desc_img  = $('#promotion_desc_img').val();
				var fsize 		  		= $('#promotion_file_img')[0].files[0].size;
				var fileExt   			= $('#promotion_file_img').val().split('.').pop().toLowerCase();				
				if( page_type == "Insert" ) {
					if( (trainer_id_img == "" || trainer_id_img == false) && (promotion_title_img == "" || promotion_title_img == false) && (promotion_file_img == "" || promotion_file_img == false) && (promotion_desc_img == "" || promotion_desc_img == false) ) {
						$('#training_id_error_img').show(500);
						$('#training_id_error_img').html('Select a training from list!');
						$('#promotion_title_error_img').show(500);
						$('#promotion_title_error_img').html('Promotion title required!');
						$('#promotion_file_error_img').show(500);
						$('#promotion_file_error_img').html('Select Video or Image for Promotion!');
						$('#promotion_desc_error_img').show(500);
						$('#promotion_desc_error_img').html('Please enter description for Promotion!');
						$('#promotion_title_img').focus();
						return false;	
					} else {
						if(trainer_id_img == "" || trainer_id_img == false) {
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_file_error_img').hide(500);
							$('#promotion_file_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#training_id_error_img').show(500);
							$('#training_id_error_img').html('Select a training from list!');
							$('#training_id_img').focus();
							return false;
						}else if( promotion_title_img == "" || promotion_title_img == false ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_file_error_img').hide(500);
							$('#promotion_file_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#promotion_title_error_img').show(500);
							$('#promotion_title_error_img').html('Promotion title required!');
							$('#promotion_title_img').focus();
							return false;	
						} else if( promotion_file_img == "" || promotion_file_img == false ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#promotion_file_error_img').show(500);
							$('#promotion_file_error_img').html('Select Video or Image for Promotion!');
							$('#promotion_file_img').focus();
							return false;	
						} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#promotion_file_error_img').show(500);
							$('#promotion_file_error_img').html('Only JPG|PNG|GIF|BMP!');
							$('#promotion_file_img').focus();
							return false;
						} else if( fsize > 2097152 ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#promotion_file_error_img').show(500);
							$('#promotion_file_error_img').html('File size is more than 2MB!');
							$('#promotion_file_img').focus();
						} else if( promotion_desc_img == "" || promotion_desc_img == false ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_file_error_img').hide(500);
							$('#promotion_file_error_img').html('');
							$('#promotion_desc_error_img').show(500);
							$('#promotion_desc_error_img').html('Please enter description for Promotion!');
							$('#promotion_desc_img').focus();
							return false;	
						} else if( promotion_desc_img.length > 500 ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_file_error_img').hide(500);
							$('#promotion_file_error_img').html('');
							$('#promotion_desc_error_img').show(500);
							$('#promotion_desc_error_img').html('More than 500 characters inserted!');
							$('#promotion_desc_img').focus();
							return false;
						} else {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_file_error_img').hide(500);
							$('#promotion_file_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							return true;
						}
					}
				} else {
					if( (training_id_img == "" || training_id_img == false) && (promotion_title_img == "" || promotion_title_img == false) && (promotion_desc_img == "" || promotion_desc_img == false) ) {
						$('#training_id_error_img').show(500);
						$('#training_id_error_img').html('Select a training from list!');
						$('#promotion_title_error_img').show(500);
						$('#promotion_title_error_img').html('Promotion title required!');
						$('#promotion_desc_error_img').show(500);
						$('#promotion_desc_error_img').html('Please enter description for Promotion!');
						$('#promotion_title_img').focus();
						return false;	
					} else {
						if(training_id_img == "" || training_id_img == false) {
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#training_id_error_img').show(500);
							$('#training_id_error_img').html('Select a training from list!');
							$('#training_id_img').focus();
							return false;
						} else if( promotion_title_img == "" || promotion_title_img == false ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							$('#promotion_title_error_img').show(500);
							$('#promotion_title_error_img').html('Promotion title required!');
							$('#promotion_title_img').focus();
							return false;	
						} else if( promotion_desc_img == "" || promotion_desc_img == false ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').show(500);
							$('#promotion_desc_error_img').html('Please enter description for Promotion!');
							$('#promotion_desc_img').focus();
							return false;	
						} else if( promotion_desc_img.length > 500 ) {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').show(500);
							$('#promotion_desc_error_img').html('More than 500 characters inserted!');
							$('#promotion_desc_img').focus();
							return false;
						} else {
							$('#training_id_error_img').hide(500);
							$('#training_id_error_img').html('');
							$('#promotion_title_error_img').hide(500);
							$('#promotion_title_error_img').html('');
							$('#promotion_desc_error_img').hide(500);
							$('#promotion_desc_error_img').html('');
							return true;
						}
					}
				}
				
			});
			// Script end
		} else if( promotion_type == "Video" ) {
			$('#ImageBlock').css('display', 'none');			
			$('#QuestionBlock').css('display', 'none');
			$('#VideoBlock').css('display', 'block');
			// Promotions Validation
			$('#promotion_submit_vid').click(function() {
				var trainer_id_vid		= $('#trainer_id_vid').val();				
				var promotion_title_vid = $('#promotion_title_vid').val();
				var promotion_file_vid  = $('#promotion_file_vid').val();
				var promotion_desc_vid  = $('#promotion_desc_vid').val();
				var fsize 		  		= $('#promotion_file_vid')[0].files[0].size;
				var fileExt   			= $('#promotion_file_vid').val().split('.').pop().toLowerCase();				
				if( page_type == "Insert" ) {
					if( (trainer_id_vid == "" || trainer_id_vid == false) && (promotion_title_vid == "" || promotion_title_vid == false) && (promotion_file_vid == "" || promotion_file_vid == false) && (promotion_desc_vid == "" || promotion_desc_vid == false) ) {
						$('#training_id_error_vid').show(500);
						$('#training_id_error_vid').html('Select a training from list!');
						$('#promotion_title_error_vid').show(500);
						$('#promotion_title_error_vid').html('Promotion title required!');
						$('#promotion_file_error_vid').show(500);
						$('#promotion_file_error_vid').html('Select Video or Image for Promotion!');
						$('#promotion_desc_error_vid').show(500);
						$('#promotion_desc_error_vid').html('Please enter description for Promotion!');
						$('#promotion_title_vid').focus();
						return false;	
					} else {
						if(trainer_id_vid == "" || trainer_id_vid == false) {
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_file_error_vid').hide(500);
							$('#promotion_file_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#training_id_error_vid').show(500);
							$('#training_id_error_vid').html('Select a training from list!');
							$('#training_id_vid').focus();
							return false;
						}else if( promotion_title_vid == "" || promotion_title_vid == false ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_file_error_vid').hide(500);
							$('#promotion_file_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#promotion_title_error_vid').show(500);
							$('#promotion_title_error_vid').html('Promotion title required!');
							$('#promotion_title_vid').focus();
							return false;	
						} else if( promotion_file_vid == "" || promotion_file_vid == false ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#promotion_file_error_vid').show(500);
							$('#promotion_file_error_vid').html('Select Video or Image for Promotion!');
							$('#promotion_file_vid').focus();
							return false;	
						} else if(($.inArray(fileExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#promotion_file_error_vid').show(500);
							$('#promotion_file_error_vid').html('Only MP4|WebM|OGG|MOV!');
							$('#promotion_file_vid').focus();
							return false;
						} else if( promotion_desc_vid == "" || promotion_desc_vid == false ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_file_error_vid').hide(500);
							$('#promotion_file_error_vid').html('');
							$('#promotion_desc_error_vid').show(500);
							$('#promotion_desc_error_vid').html('Please enter description for Promotion!');
							$('#promotion_desc_vid').focus();
							return false;	
						} else if( promotion_desc_vid.length > 500 ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_file_error_vid').hide(500);
							$('#promotion_file_error_vid').html('');
							$('#promotion_desc_error_vid').show(500);
							$('#promotion_desc_error_vid').html('More than 500 characters inserted!');
							$('#promotion_desc_vid').focus();
							return false;
						} else {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_file_error_vid').hide(500);
							$('#promotion_file_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							return true;
						}
					}
				} else {
					if( (training_id_vid == "" || training_id_vid == false) && (promotion_title_vid == "" || promotion_title_vid == false) && (promotion_desc_vid == "" || promotion_desc_vid == false) ) {
						$('#training_id_error_vid').show(500);
						$('#training_id_error_vid').html('Select a training from list!');
						$('#promotion_title_error_vid').show(500);
						$('#promotion_title_error_vid').html('Promotion title required!');
						$('#promotion_desc_error_vid').show(500);
						$('#promotion_desc_error_vid').html('Please enter description for Promotion!');
						$('#promotion_title_vid').focus();
						return false;	
					} else {
						if(training_id_vid == "" || training_id_vid == false) {
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#training_id_error_vid').show(500);
							$('#training_id_error_vid').html('Select a training from list!');
							$('#training_id_vid').focus();
							return false;
						} else if( promotion_title_vid == "" || promotion_title_vid == false ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							$('#promotion_title_error_vid').show(500);
							$('#promotion_title_error_vid').html('Promotion title required!');
							$('#promotion_title_vid').focus();
							return false;	
						} else if( promotion_desc_vid == "" || promotion_desc_vid == false ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').show(500);
							$('#promotion_desc_error_vid').html('Please enter description for Promotion!');
							$('#promotion_desc_vid').focus();
							return false;	
						} else if( promotion_desc_vid.length > 500 ) {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').show(500);
							$('#promotion_desc_error_vid').html('More than 500 characters inserted!');
							$('#promotion_desc_vid').focus();
							return false;
						} else {
							$('#training_id_error_vid').hide(500);
							$('#training_id_error_vid').html('');
							$('#promotion_title_error_vid').hide(500);
							$('#promotion_title_error_vid').html('');
							$('#promotion_desc_error_vid').hide(500);
							$('#promotion_desc_error_vid').html('');
							return true;
						}
					}
				}
				
			});
			// Script end
		} else if( promotion_type == "Question" ) {
			$('#ImageBlock').css('display', 'none');
			$('#VideoBlock').css('display', 'none');
			$('#QuestionBlock').css('display', 'block');
			$('#promotion_submit_que').click(function() {
				var trainer_id_que			= $('#trainer_id_que').val();				
				var promotion_title_que		= $('#promotion_title_que').val();				
				var promotion_desc_que 		= $('#promotion_desc_que').val();
				var promotion_question  	= $('#promotion_question').val();
				var promotion_answer_one  	= $('#promotion_answer_one').val();
				var promotion_marks_one  	= $('#promotion_marks_one').val();
				var promotion_answer_two  	= $('#promotion_answer_two').val();
				var promotion_marks_two  	= $('#promotion_marks_two').val();
				var promotion_answer_three 	= $('#promotion_answer_three').val();
				var promotion_marks_three  	= $('#promotion_marks_three').val();
				var promotion_answer_four  	= $('#promotion_answer_four').val();
				var promotion_marks_four  	= $('#promotion_marks_four').val();
				
				if( (trainer_id_que == "" || trainer_id_que == false) && (promotion_title_que == "" || promotion_title_que == false) && (promotion_desc_que == "" || promotion_desc_que == false) && (promotion_question == "" || promotion_question == false) && (promotion_answer_one == "" || promotion_answer_one == false) && (promotion_marks_one == "" || promotion_marks_one == false) && (promotion_answer_two == "" || promotion_answer_two == false) && (promotion_marks_two == "" || promotion_marks_two == false) && (promotion_answer_three == "" || promotion_answer_three == false) && (promotion_marks_three == "" || promotion_marks_three == false) && (promotion_answer_four == "" || promotion_answer_four == false) && (promotion_marks_four == "" || promotion_marks_four == false) ) {
					$('#trainer_id_error_que').show(500);
					$('#trainer_id_error_que').html('Select a training from the list!');
					$('#promotion_title_error_que').show(500);
					$('#promotion_title_error_que').html('Promotion title required!');
					$('#promotion_desc_error_que').show(500);
					$('#promotion_desc_error_que').html('Please enter description for promotion!');
					$('#promotion_question_error').show(500);
					$('#promotion_question_error').html('Enter question for Questionnaire');
					$('#promotion_answer_one_error').show(500);
					$('#promotion_answer_one_error').html('Required!');
					$('#promotion_marks_one_error').show(500);
					$('#promotion_marks_one_error').html('Required!');
					$('#promotion_answer_two_error').show(500);
					$('#promotion_answer_two_error').html('Required!');
					$('#promotion_marks_two_error').show(500);
					$('#promotion_marks_two_error').html('Required!');
					$('#promotion_answer_three_error').show(500);
					$('#promotion_answer_three_error').html('Required!');
					$('#promotion_marks_three_error').show(500);
					$('#promotion_marks_three_error').html('Required!');
					$('#promotion_answer_four_error').show(500);
					$('#promotion_answer_four_error').html('Required!');
					$('#promotion_marks_four_error').show(500);
					$('#promotion_marks_four_error').html('Required!');
					$('#trainer_id_que').focus();
					return false;
				} else {
					if(trainer_id_que == "" || trainer_id_que == false) {
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#trainer_id_error_que').show(500);
						$('#trainer_id_error_que').html('Select a training from the list!');
						$('#trainer_id_que').focus();
						return false;
					} else if(promotion_title_que == "" || promotion_title_que == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_title_error_que').show(500);
						$('#promotion_title_error_que').html('Promotion title required!');
						$('#promotion_title_que').focus();
						return false;
					} else if(promotion_desc_que == "" || promotion_desc_que == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_desc_error_que').show(500);
						$('#promotion_desc_error_que').html('Please enter description for promotion!');
						$('#promotion_desc_que').focus();
						return false;
					} else if( promotion_desc_que.length > 500 ) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_desc_error_que').show(500);
						$('#promotion_desc_error_que').html('More than 500 characters inserted!');
						$('#promotion_desc_que').focus();
						return false;
					} else if(promotion_question == "" || promotion_question == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_question_error').show(500);
						$('#promotion_question_error').html('Enter question for Questionnaire');
						$('#promotion_question').focus();
						return false;
					} else if(promotion_answer_one == "" || promotion_answer_one == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_answer_one_error').show(500);
						$('#promotion_answer_one_error').html('Required!');
						$('#promotion_answer_one').focus();
						return false;
					} else if(promotion_marks_one == "" || promotion_marks_one == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_marks_one_error').show(500);
						$('#promotion_marks_one_error').html('Required!');
						$('#promotion_marks_one').focus();
						return false;
					} else if(promotion_answer_two == "" || promotion_answer_two == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_answer_two_error').show(500);
						$('#promotion_answer_two_error').html('Required!');
						$('#promotion_answer_two').focus();
						return false;
					} else if(promotion_marks_two == "" || promotion_marks_two == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_marks_two_error').show(500);
						$('#promotion_marks_two_error').html('Required!');
						$('#promotion_marks_two').focus();
						return false;
					} else if(promotion_answer_three == "" || promotion_answer_three == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_answer_three_error').show(500);
						$('#promotion_answer_three_error').html('Required!');
						$('#promotion_answer_three').focus();
						return false;
					} else if(promotion_marks_three == "" || promotion_marks_three == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_marks_three_error').show(500);
						$('#promotion_marks_three_error').html('Required!');
						$('#promotion_marks_three').focus();
						return false;
					} else if(promotion_answer_four == "" || promotion_answer_four == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						$('#promotion_answer_four_error').show(500);
						$('#promotion_answer_four_error').html('Required!');
						$('#promotion_answer_four').focus();
						return false;
					} else if(promotion_marks_four == "" || promotion_marks_four == false) {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').show(500);
						$('#promotion_marks_four_error').html('Required!');
						$('#promotion_marks_four').show(500);
						return false;
					} else {
						$('#trainer_id_error_que').hide(500);
						$('#trainer_id_error_que').html('');
						$('#promotion_title_error_que').hide(500);
						$('#promotion_title_error_que').html('');
						$('#promotion_desc_error_que').hide(500);
						$('#promotion_desc_error_que').html('');
						$('#promotion_question_error').hide(500);
						$('#promotion_question_error').html('');
						$('#promotion_answer_one_error').hide(500);
						$('#promotion_answer_one_error').html('');
						$('#promotion_marks_one_error').hide(500);
						$('#promotion_marks_one_error').html('');
						$('#promotion_answer_two_error').hide(500);
						$('#promotion_answer_two_error').html('');
						$('#promotion_marks_two_error').hide(500);
						$('#promotion_marks_two_error').html('');
						$('#promotion_answer_three_error').hide(500);
						$('#promotion_answer_three_error').html('');
						$('#promotion_marks_three_error').hide(500);
						$('#promotion_marks_three_error').html('');
						$('#promotion_answer_four_error').hide(500);
						$('#promotion_answer_four_error').html('');
						$('#promotion_marks_four_error').hide(500);
						$('#promotion_marks_four_error').html('');
						return true;
					}
				}				
			});
		}
	});
	if( $('#promotion_desc_img').length > 0 ) {
		$('#promo_desc_length_img').text(500 - $('#promotion_desc_img').val().length);
		// Character Counter of Promotions Description
		$('#promotion_desc_img').change(function(){
			var remaining = 500 - $('#promotion_desc_img').val().length;
			$('#promo_desc_length_img').text(remaining);	
		});
		$('#promotion_desc_img').keyup(function(){
			var remaining = 500 - $('#promotion_desc_img').val().length;
			$('#promo_desc_length_img').text(remaining);	
		});
		// Script end
	}
	if( $('#promotion_desc_vid').length > 0 ) {
		$('#promo_desc_length_vid').text(500 - $('#promotion_desc_vid').val().length);
		// Character Counter of Promotions Description
		$('#promotion_desc').change(function(){
			var remaining = 500 - $('#promotion_desc_vid').val().length;
			$('#promo_desc_length_vid').text(remaining);	
		});
		$('#promotion_desc_vid').keyup(function(){
			var remaining = 500 - $('#promotion_desc_vid').val().length;
			$('#promo_desc_length_vid').text(remaining);	
		});
		// Script end
	}
	if( $('#promotion_desc_que').length > 0 ) {
		$('#promo_desc_length_que').text(500 - $('#promotion_desc_que').val().length);
		// Character Counter of Promotions Description
		$('#promotion_desc_que').change(function(){
			var remaining = 500 - $('#promotion_desc_que').val().length;
			$('#promo_desc_length_que').text(remaining);	
		});
		$('#promotion_desc_que').keyup(function(){
			var remaining = 500 - $('#promotion_desc_que').val().length;
			$('#promo_desc_length_que').text(remaining);	
		});
		// Script end
	}
	$('#promotion_title_img').blur(function() {
		var promotion_title_img = $('#promotion_title_img').val();
		if( promotion_title_img == "" || promotion_title_img == false ) {
			$('#promotion_title_img_error').show(500);
			$('#promotion_title_img_error').html('Promotion title required!');
			$('#promotion_title_img').focus();
			return false;	
		} else {
			$('#promotion_title_img_error').hide(500);
			$('#promotion_title_img_error').html('');
			return true;	
		}
	});
	$('#promotion_title_vid').blur(function() {
		var promotion_title_vid = $('#promotion_title_vid').val();
		if( promotion_title_vid == "" || promotion_title_vid == false ) {
			$('#promotion_title_vid_error').show(500);
			$('#promotion_title_vid_error').html('Promotion title required!');
			$('#promotion_title_vid').focus();
			return false;	
		} else {
			$('#promotion_title_vid_error').hide(500);
			$('#promotion_title_vid_error').html('');
			return true;	
		}
	});
	$('#promotion_title_que').blur(function() {
		var promotion_title_que = $('#promotion_title_que').val();
		if( promotion_title_que == "" || promotion_title_que == false ) {
			$('#promotion_title_que_error').show(500);
			$('#promotion_title_que_error').html('Promotion title required!');
			$('#promotion_title_que').focus();
			return false;	
		} else {
			$('#promotion_title_que_error').hide(500);
			$('#promotion_title_que_error').html('');
			return true;	
		}
	});
	$('#training_id_img').blur(function() {
		var training_id_img = $('#training_id_img').val();
		if( training_id_img == "" || training_id_img == false ) {
			$('#training_id_img_error').show(500);
			$('#training_id_img_error').html('Select training from the list!');
			$('#training_id_img').focus();
			return false;	
		} else {
			$('#training_id_img_error').hide(500);
			$('#training_id_img_error').html('');
			return true;	
		}
	});
	$('#training_id_vid').blur(function() {
		var training_id_vid = $('#training_id_vid').val();
		if( training_id_vid == "" || training_id_vid == false ) {
			$('#training_id_vid_error').show(500);
			$('#training_id_vid_error').html('Select training from the list!');
			$('#training_id_vid').focus();
			return false;	
		} else {
			$('#training_id_vid_error').hide(500);
			$('#training_id_vid_error').html('');
			return true;	
		}
	});
	$('#training_id_que').blur(function() {
		var training_id_que = $('#training_id_que').val();
		if( training_id_que == "" || training_id_que == false ) {
			$('#training_id_que_error').show(500);
			$('#training_id_que_error').html('Select training from the list!');
			$('#training_id_que').focus();
			return false;	
		} else {
			$('#training_id_que_error').hide(500);
			$('#training_id_que_error').html('');
			return true;	
		}
	});
	if( page_type == "Insert" ) {
		$('.promotion_file_img').blur(function(e) {
			/*var promotion_file_img  = $(this).parents('form').serializeArray();
			var file = $(this).attr("files")[0];
			var fileName = file.fileName;
			var fileSize = file.fileSize;
			alert("Uploading: "+fileName+" @ "+fileSize+"bytes");return false;
			alert(JSON.stringify(promotion_file_img));return false;*/
			/*var formData = new FormData();
			formData.append('image', $(this)[0].files[0]);
			alert(JSON.stringify(formData));return false;
			$.ajax({
				type: "POST",
				url: ajax_url + "/check-file/",
				data: formData,
				contentType: false,
				processData: false,
				success: function(data) {
					if( data != 0 ) {
						alert(data);
					}
				}
			});*/
			var promotion_file_img  = $(this).val();
			var fileID				= $(this).attr('id');
			var fsize 		  		= fileID[0].files[0].size;
			var fileExt   			= fileID.val().split('.').pop().toLowerCase();
			if( promotion_file_img == "" || promotion_file_img == false ) {
				$('#promotion_file_img_error').show(500);
				$('#promotion_file_img_error').html('Select Image for Promotion!');
				$('#promotion_file_img').focus();
				return false;	
			} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
				$('#promotion_file_img_error').show(500);
				$('#promotion_file_img_error').html('Only JPG|PNG|GIF|BMP!');
				$('#promotion_file_img').focus();
				return false;
			} else if( fsize > 2097152 ) {
				$('#promotion_file_img_error').show(500);
				$('#promotion_file_img_error').html('File size is more than 2MB!');
				$('#promotion_file_img').focus();
			} else {
				$('#promotion_file_img_error').hide(500);
				$('#promotion_file_img_error').html('');
				return true;	
			}
		});
		$('.promotion_file_vid').blur(function() {
			var promotion_file_vid  = $(this).val();
			var fileID				= $(this).attr('id');
			var fsize 		  		= fileID[0].files[0].size;
			var fileExt   			= fileID.val().split('.').pop().toLowerCase();
			if( promotion_file_vid == "" || promotion_file_vid == false ) {
				$('#promotion_file_vid_error').show(500);
				$('#promotion_file_vid_error').html('Select Video for Promotion!');
				$('#promotion_file_vid').focus();
				return false;	
			} else if(($.inArray(fileExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
				$('#promotion_file_vid_error').show(500);
				$('#promotion_file_vid_error').html('Only MP4|WebM|OGG|MOV!');
				$('#promotion_file_vid').focus();
				return false;
			} else if( fsize > 2147483648 ) {
				$('#promotion_file_vid_error').show(500);
				$('#promotion_file_vid_error').html('File size is more than 2GB!');
				$('#promotion_file_vid').focus();
			} else {
				$('#promotion_file_vid_error').hide(500);
				$('#promotion_file_vid_error').html('');
				return true;	
			}
		});
	}
	$('.promotion_file_img').bind(function() {
		var promotion_file_img  = $(this).val();
		var fileID				= $(this).attr('id');
		var fsize 		  		= fileID[0].files[0].size;
		var fileExt   			= fileID.val().split('.').pop().toLowerCase();
		if( promotion_file_img == "" || promotion_file_img == false ) {
			$('#promotion_file_img_error').show(500);
			$('#promotion_file_img_error').html('Select Image for Promotion!');
			$('#promotion_file_img').focus();
			return false;	
		} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
			$('#promotion_file_img_error').show(500);
			$('#promotion_file_img_error').html('Only JPG|PNG|GIF|BMP!');
			$('#promotion_file_img').focus();
			return false;
		} else if( fsize > 2097152 ) {
			$('#promotion_file_img_error').show(500);
			$('#promotion_file_img_error').html('File size is more than 2MB!');
			$('#promotion_file_img').focus();
		} else {
			$('#promotion_file_img_error').hide(500);
			$('#promotion_file_img_error').html('');
			return true;	
		}
	});
	$('.promotion_file_vid').bind(function() {
		var promotion_file_vid  = $(this).val();
		var fileID				= $(this).attr('id');
		var fsize 		  		= fileID[0].files[0].size;
		var fileExt   			= fileID.val().split('.').pop().toLowerCase();
		if( promotion_file_vid == "" || promotion_file_vid == false ) {
			$('#promotion_file_vid_error').show(500);
			$('#promotion_file_vid_error').html('Select Video for Promotion!');
			$('#promotion_file_vid').focus();
			return false;	
		} else if(($.inArray(fileExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
			$('#promotion_file_vid_error').show(500);
			$('#promotion_file_vid_error').html('Only MP4|WebM|OGG|MOV!');
			$('#promotion_file_vid').focus();
			return false;
		} else if( fsize > 2147483648 ) {
			$('#promotion_file_vid_error').show(500);
			$('#promotion_file_vid_error').html('File size is more than 2GB!');
			$('#promotion_file_vid').focus();
		} else {
			$('#promotion_file_vid_error').hide(500);
			$('#promotion_file_vid_error').html('');
			return true;	
		}
	});
	$('#promotion_desc_img').blur(function() {
		var promotion_desc_img  = $('#promotion_desc_img').val();
		if( promotion_desc_img == "" || promotion_desc_img == false ) {
			$('#promotion_desc_img_error').show(500);
			$('#promotion_desc_img_error').html('Please enter description for Promotion!');
			$('#promotion_desc_img').focus();
			return false;	
		} else if( promotion_desc_img.length > 500 ) {
			$('#promotion_desc_img_error').show(500);
			$('#promotion_desc_img_error').html('More than 500 characters inserted!');
			$('#promotion_desc_img').focus();
			return false;
		} else {
			$('#promotion_desc_img_error').hide(500);
			$('#promotion_desc_img_error').html('');
			return true;	
		}
	});
	$('#promotion_desc_vid').blur(function() {
		var promotion_desc_vid  = $('#promotion_desc_vid').val();
		if( promotion_desc_vid == "" || promotion_desc_vid == false ) {
			$('#promotion_desc_vid_error').show(500);
			$('#promotion_desc_vid_error').html('Please enter description for Promotion!');
			$('#promotion_desc_vid').focus();
			return false;	
		} else if( promotion_desc_vid.length > 500 ) {
			$('#promotion_desc_vid_error').show(500);
			$('#promotion_desc_vid_error').html('More than 500 characters inserted!');
			$('#promotion_desc_vid').focus();
			return false;
		} else {
			$('#promotion_desc_vid_error').hide(500);
			$('#promotion_desc_vid_error').html('');
			return true;	
		}
	});
	$('#promotion_desc_que').blur(function() {
		var promotion_desc_que  = $('#promotion_desc_que').val();
		if( promotion_desc_que == "" || promotion_desc_que == false ) {
			$('#promotion_desc_que_error').show(500);
			$('#promotion_desc_que_error').html('Please enter description for Promotion!');
			$('#promotion_desc_que').focus();
			return false;	
		} else if( promotion_desc_que.length > 500 ) {
			$('#promotion_desc_que_error').show(500);
			$('#promotion_desc_que_error').html('More than 500 characters inserted!');
			$('#promotion_desc_que').focus();
			return false;
		} else {
			$('#promotion_desc_que_error').hide(500);
			$('#promotion_desc_que_error').html('');
			return true;	
		}
	});
	$('#promo_start_date_img').blur(function() {
		var promo_start_date_img = $('#promo_start_date_img').val();
		if( promo_start_date_img == "" || promo_start_date_img == false ) {
			$('#promo_start_date_img_error').show(500);
			$('#promo_start_date_img_error').html('Select start datetime!');
			$('#promo_start_date_img').focus();
			return false;	
		} else {
			$('#promo_start_date_img_error').hide(500);
			$('#promo_start_date_img_error').html('');
			return true;	
		}
	});
	$('#promo_end_date_img').blur(function() {
		var promo_end_date_img = $('#promo_end_date_img').val();
		if( promo_end_date_img == "" || promo_end_date_img == false ) {
			$('#promo_end_date_img_error').show(500);
			$('#promo_end_date_img_error').html('Select end datetime!');
			$('#promo_end_date_img').focus();
			return false;	
		} else {
			$('#promo_end_date_img_error').hide(500);
			$('#promo_end_date_img_error').html('');
			return true;	
		}
	});
	$('#promo_start_date_vid').blur(function() {
		var promo_start_date_vid = $('#promo_start_date_vid').val();
		if( promo_start_date_vid == "" || promo_start_date_vid == false ) {
			$('#promo_start_date_vid_error').show(500);
			$('#promo_start_date_vid_error').html('Select start datetime!');
			$('#promo_start_date_vid').focus();
			return false;	
		} else {
			$('#promo_start_date_vid_error').hide(500);
			$('#promo_start_date_vid_error').html('');
			return true;	
		}
	});
	$('#promo_end_date_vid').blur(function() {
		var promo_end_date_vid = $('#promo_end_date_vid').val();
		if( promo_end_date_vid == "" || promo_end_date_vid == false ) {
			$('#promo_end_date_vid_error').show(500);
			$('#promo_end_date_vid_error').html('Select end datetime!');
			$('#promo_end_date_vid').focus();
			return false;	
		} else {
			$('#promo_end_date_vid_error').hide(500);
			$('#promo_end_date_vid_error').html('');
			return true;	
		}
	});
	$('#promo_start_date_que').blur(function() {
		var promo_start_date_que = $('#promo_start_date_que').val();
		if( promo_start_date_que == "" || promo_start_date_que == false ) {
			$('#promo_start_date_que_error').show(500);
			$('#promo_start_date_que_error').html('Select start datetime!');
			$('#promo_start_date_que').focus();
			return false;	
		} else {
			$('#promo_start_date_que_error').hide(500);
			$('#promo_start_date_que_error').html('');
			return true;	
		}
	});
	$('#promo_end_date_que').blur(function() {
		var promo_end_date_que = $('#promo_end_date_que').val();
		if( promo_end_date_que == "" || promo_end_date_que == false ) {
			$('#promo_end_date_que_error').show(500);
			$('#promo_end_date_que_error').html('Select end datetime!');
			$('#promo_end_date_que').focus();
			return false;	
		} else {
			$('#promo_end_date_que_error').hide(500);
			$('#promo_end_date_que_error').html('');
			return true;	
		}
	});
	
	var imgField = 0;
	$('#add_promotion_img').click(function() {
		$('.promotion_img').append('<div><a href="javascript:;" class="pull-right remove_promotion_img"><small class="glyphicon glyphicon-minus-sign"></small></a><input class="form-control promotion_file_img" type="file" id="promotion_file_img_'+imgField+'" name="promotion_file_img[]" accept="image/*" style="height:auto;" /><div>');
		imgField++;
	});
	$('.promotion_img').on('click', '.remove_promotion_img', function() {
        $(this).parent('div').remove();
        imgField--;
    });
	
	var vidField = 0;
	$('#add_promotion_vid').click(function() {
		$('.promotion_vid').append('<div><a href="javascript:;" class="pull-right remove_promotion_vid"><small class="glyphicon glyphicon-minus-sign"></small></a><input class="form-control promotion_file_vid" type="file" id="promotion_file_vid_'+vidField+'" name="promotion_file_vid[]" accept="video/*" style="height:auto;" /><div>');
		vidField++;
	});
	$('.promotion_vid').on('click', '.remove_promotion_vid', function() {
        $(this).parent('div').remove();
        vidField--;
    });
	
	var queField = 0;
	$('#add_promotion_que').click(function() {
		$('.question_set').append('<div class="new_question_set"><a href="javascript:;" class="pull-right remove_promotion_que"><small class="glyphicon glyphicon-minus-sign"></small></a><div class="col-sm-12"><div class="form-group">'+(page_type=='Update'?'<input type="hidden" name="promotion_question_id[]" id="promotion_question_id_'+queField+'" value="" />':'')+'<label for="promotion_question_'+queField+'">Question: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter question in here."><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_question_'+queField+'" name="promotion_question[]" /></div></div><div class="col-md-2"><div class="form-group"><label for="promotion_answer_one_'+queField+'">Answer #1: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_answer_one_'+queField+'" name="promotion_answer_one[]" /></div></div><div class="col-md-1"><div class="form-group"><label for="promotion_marks_one_'+queField+'">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. \'25\'"><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_marks_one_'+queField+'" name="promotion_marks_one[]" /></div></div><div class="col-md-2"><div class="form-group"><label for="promotion_answer_two_'+queField+'">Answer #2: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_answer_two_'+queField+'" name="promotion_answer_two[]" /></div></div><div class="col-md-1"><div class="form-group"><label for="promotion_marks_two_'+queField+'">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. \'25\'"><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_marks_two_'+queField+'" name="promotion_marks_two[]" /></div></div><div class="col-md-2"><div class="form-group"><label for="promotion_answer_three_'+queField+'">Answer #3: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_answer_three_'+queField+'" name="promotion_answer_three[]" /></div></div><div class="col-md-1"><div class="form-group"><label for="promotion_marks_three_'+queField+'">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. \'25\'"><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_marks_three_'+queField+'" name="promotion_marks_three[]" /></div></div><div class="col-md-2"><div class="form-group"><label for="promotion_answer_four_'+queField+'">Answer #4: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_answer_four_'+queField+'" name="promotion_answer_four[]" /></div></div><div class="col-md-1"><div class="form-group"><label for="promotion_marks_four_'+queField+'">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. \'25\'"><small class="glyphicon glyphicon-info-sign"></small></a></label><input class="form-control" type="text" id="promotion_marks_four_'+queField+'" name="promotion_marks_four[]" /></div></div></div>');
		queField++;
	});
	$('.question_set').on('click', '.remove_promotion_que', function() {
        $(this).parent('div').remove();
        queField--;
    });
	
	$('.remove_promo_file').on('click', function() {
		$('#promotion_file_modal').modal('show');
		var fileName 	= $(this).attr('name');
		var fileID 	 	= $(this).attr('rel');
		$('.promotion_file_remove').attr('id', fileID);
		$('.promotion_file_remove').attr('name', fileName);
	});
	$('.promotion_file_remove').on('click', function() {
		var fileName	= $(this).attr('name');
		var fileID		= $(this).attr('id');
		var dataString 	= 'fileName=' + fileName + '&fileID=' + fileID;
		$.ajax({
			type: "POST",
			url: ajax_url + "/remove-promo-file/",
			async: dataString.async,
			data: dataString,
			success: function(data) {
				if ( data == 1 ) {
					$('#promotion_file_modal').modal('hide');
					$('.remove_promo_file').parent('#file_'+fileID).remove();
				}
			}
		});
	});
	$('.remove_promo_question').on('click', function() {
		$('#promotion_question_modal').modal('show');
		var questionID 	 	= $(this).attr('rel');
		$('.promotion_question_remove').attr('id', questionID);
	});
	$('.promotion_question_remove').on('click', function() {
		var questionID = $(this).attr('id');
		var dataString = 'questionID=' + questionID;
		$.ajax({
			type: "POST",
			url: ajax_url + "/remove-promo-question/",
			async: dataString.async,
			data: dataString,
			success: function(data) {
				if ( data == 1 ) {
					$('#promotion_question_modal').modal('hide');
					$('.remove_promo_question').parents('#question_'+questionID).remove();
				}
			}
		});
	});
	
	$('#promotion_submit_img').click(function() {
		var promotion_title_img = $('#promotion_title_img').val();
		var training_id_img		= $('#training_id_img').val();
		var promotion_file_img 	= $('#promotion_file_img').val();
		var promotion_desc_img 	= $('#promotion_desc_img').val();
		var fileExt   			= $('#promotion_file_img').val().split('.').pop().toLowerCase();
		if(page_type == 'Insert') {
			if((promotion_title_img == "" || promotion_title_img == false) && (training_id_img == "" || training_id_img == false) && (promotion_file_img == "" || promotion_file_img == false) && (promotion_desc_img == "" || promotion_desc_img == false)) {
				$('#promotion_title_img_error').show(500);
				$('#promotion_title_img_error').html('Promotion title required!');
				$('#training_id_img_error').show(500);
				$('#training_id_img_error').html('Select training from the list!');
				$('#promotion_file_img_error').show(500);
				$('#promotion_file_img_error').html('Select Image for Promotion!');
				$('#promotion_desc_img_error').show(500);
				$('#promotion_desc_img_error').html('Please enter description for Promotion!');
				$('#promotion_title_img').focus();
				return false;
			} else {
				if( promotion_title_img == "" || promotion_title_img == false ) {
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_file_img_error').hide(500);
					$('#promotion_file_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#promotion_title_img_error').show(500);
					$('#promotion_title_img_error').html('Promotion title required!');
					$('#promotion_title_img').focus();
					return false;
				} else if( training_id_img == "" || training_id_img == false ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#promotion_file_img_error').hide(500);
					$('#promotion_file_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#training_id_img_error').show(500);
					$('#training_id_img_error').html('Select training from the list!');
					$('#training_id_img').focus();
					return false;
				} else if( promotion_file_img == "" || promotion_file_img == false ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#promotion_file_img_error').show(500);
					$('#promotion_file_img_error').html('Select Image for Promotion!');
					$('#promotion_file_img').focus();
					return false;	
				} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#promotion_file_img_error').show(500);
					$('#promotion_file_img_error').html('Only JPG|PNG|GIF|BMP!');
					$('#promotion_file_img').focus();
					return false;
				} else if( promotion_desc_img == "" || promotion_desc_img == false ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_file_img_error').hide(500);
					$('#promotion_file_img_error').html('');
					$('#promotion_desc_img_error').show(500);
					$('#promotion_desc_img_error').html('Please enter description for Promotion!');
					$('#promotion_desc_img').focus();
					return false;	
				} else if( promotion_desc_img.length > 500 ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_file_img_error').hide(500);
					$('#promotion_file_img_error').html('');
					$('#promotion_desc_img_error').show(500);
					$('#promotion_desc_img_error').html('More than 500 characters inserted!');
					$('#promotion_desc_img').focus();
					return false;
				} else {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_file_img_error').hide(500);
					$('#promotion_file_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					return true
				}
			}
		} else {
			if((promotion_title_img == "" || promotion_title_img == false) && (training_id_img == "" || training_id_img == false) && (promotion_desc_img == "" || promotion_desc_img == false)) {
				$('#promotion_title_img_error').show(500);
				$('#promotion_title_img_error').html('Promotion title required!');
				$('#training_id_img_error').show(500);
				$('#training_id_img_error').html('Select training from the list!');
				$('#promotion_desc_img_error').show(500);
				$('#promotion_desc_img_error').html('Please enter description for Promotion!');
				$('#promotion_title_img').focus();
				return false;
			} else {
				if( promotion_title_img == "" || promotion_title_img == false ) {
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#promotion_title_img_error').show(500);
					$('#promotion_title_img_error').html('Promotion title required!');
					$('#promotion_title_img').focus();
					return false;
				} else if( training_id_img == "" || training_id_img == false ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					$('#training_id_img_error').show(500);
					$('#training_id_img_error').html('Select training from the list!');
					$('#training_id_img').focus();
					return false;
				} else if( promotion_desc_img == "" || promotion_desc_img == false ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').show(500);
					$('#promotion_desc_img_error').html('Please enter description for Promotion!');
					$('#promotion_desc_img').focus();
					return false;	
				} else if( promotion_desc_img.length > 500 ) {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').show(500);
					$('#promotion_desc_img_error').html('More than 500 characters inserted!');
					$('#promotion_desc_img').focus();
					return false;
				} else {
					$('#promotion_title_img_error').hide(500);
					$('#promotion_title_img_error').html('');
					$('#training_id_img_error').hide(500);
					$('#training_id_img_error').html('');
					$('#promotion_desc_img_error').hide(500);
					$('#promotion_desc_img_error').html('');
					return true
				}
			}
		}
	});
	$('#promotion_submit_vid').click(function() {
		var promotion_title_vid = $('#promotion_title_vid').val();
		var training_id_vid		= $('#training_id_vid').val();
		var promotion_file_vid 	= $('#promotion_file_vid').val();
		var promotion_desc_vid 	= $('#promotion_desc_vid').val();
		var fileExt   			= $('#promotion_file_vid').val().split('.').pop().toLowerCase();
		if(page_type == 'Insert') {
			if((promotion_title_vid == "" || promotion_title_vid == false) && (training_id_vid == "" || training_id_vid == false) && (promotion_file_vid == "" || promotion_file_vid == false) && (promotion_desc_vid == "" || promotion_desc_vid == false)) {
				$('#promotion_title_vid_error').show(500);
				$('#promotion_title_vid_error').html('Promotion title required!');
				$('#training_id_vid_error').show(500);
				$('#training_id_vid_error').html('Select training from the list!');
				$('#promotion_file_vid_error').show(500);
				$('#promotion_file_vid_error').html('Select Video for Promotion!');
				$('#promotion_desc_vid_error').show(500);
				$('#promotion_desc_vid_error').html('Please enter description for Promotion!');
				$('#promotion_title_vid').focus();
				return false;
			} else {
				if( promotion_title_vid == "" || promotion_title_vid == false ) {
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_file_vid_error').hide(500);
					$('#promotion_file_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#promotion_title_vid_error').show(500);
					$('#promotion_title_vid_error').html('Promotion title required!');
					$('#promotion_title_vid').focus();
					return false;
				} else if( training_id_vid == "" || training_id_vid == false ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#promotion_file_vid_error').hide(500);
					$('#promotion_file_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#training_id_vid_error').show(500);
					$('#training_id_vid_error').html('Select training from the list!');
					$('#training_id_vid').focus();
					return false;
				} else if( promotion_file_vid == "" || promotion_file_vid == false ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#promotion_file_vid_error').show(500);
					$('#promotion_file_vid_error').html('Select Video for Promotion!');
					$('#promotion_file_vid').focus();
					return false;	
				} else if(($.inArray(fileExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#promotion_file_vid_error').show(500);
					$('#promotion_file_vid_error').html('Only MP4|WebM|OGG|MOV!');
					$('#promotion_file_vid').focus();
					return false;
				} else if( promotion_desc_vid == "" || promotion_desc_vid == false ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_file_vid_error').hide(500);
					$('#promotion_file_vid_error').html('');
					$('#promotion_desc_vid_error').show(500);
					$('#promotion_desc_vid_error').html('Please enter description for Promotion!');
					$('#promotion_desc_vid').focus();
					return false;	
				} else if( promotion_desc_vid.length > 500 ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_file_vid_error').hide(500);
					$('#promotion_file_vid_error').html('');
					$('#promotion_desc_vid_error').show(500);
					$('#promotion_desc_vid_error').html('More than 500 characters inserted!');
					$('#promotion_desc_vid').focus();
					return false;
				} else {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_file_vid_error').hide(500);
					$('#promotion_file_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					return true
				}
			}
		} else {
			if((promotion_title_vid == "" || promotion_title_vid == false) && (training_id_vid == "" || training_id_vid == false) && (promotion_desc_vid == "" || promotion_desc_vid == false)) {
				$('#promotion_title_vid_error').show(500);
				$('#promotion_title_vid_error').html('Promotion title required!');
				$('#training_id_vid_error').show(500);
				$('#training_id_vid_error').html('Select training from the list!');
				$('#promotion_desc_vid_error').show(500);
				$('#promotion_desc_vid_error').html('Please enter description for Promotion!');
				$('#promotion_title_vid').focus();
				return false;
			} else {
				if( promotion_title_vid == "" || promotion_title_vid == false ) {
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#promotion_title_vid_error').show(500);
					$('#promotion_title_vid_error').html('Promotion title required!');
					$('#promotion_title_vid').focus();
					return false;
				} else if( training_id_vid == "" || training_id_vid == false ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					$('#training_id_vid_error').show(500);
					$('#training_id_vid_error').html('Select training from the list!');
					$('#training_id_vid').focus();
					return false;
				} else if( promotion_desc_vid == "" || promotion_desc_vid == false ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').show(500);
					$('#promotion_desc_vid_error').html('Please enter description for Promotion!');
					$('#promotion_desc_vid').focus();
					return false;	
				} else if( promotion_desc_vid.length > 500 ) {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').show(500);
					$('#promotion_desc_vid_error').html('More than 500 characters inserted!');
					$('#promotion_desc_vid').focus();
					return false;
				} else {
					$('#promotion_title_vid_error').hide(500);
					$('#promotion_title_vid_error').html('');
					$('#training_id_vid_error').hide(500);
					$('#training_id_vid_error').html('');
					$('#promotion_desc_vid_error').hide(500);
					$('#promotion_desc_vid_error').html('');
					return true
				}
			}
		}
	});
	$('#promotion_submit_que').click(function() {
		var promotion_title_que 	= $('#promotion_title_que').val();
		var training_id_que			= $('#training_id_que').val();
		var promotion_desc_que 		= $('#promotion_desc_que').val();
		var promotion_question		= $('#promotion_question').val();
		var promotion_answer_one	= $('#promotion_answer_one').val();
		var promotion_marks_one		= $('#promotion_marks_one').val();
		var promotion_answer_two	= $('#promotion_answer_two').val();
		var promotion_marks_two		= $('#promotion_marks_two').val();
		var promotion_answer_three	= $('#promotion_answer_three').val();
		var promotion_marks_three	= $('#promotion_marks_three').val();
		var promotion_answer_four	= $('#promotion_answer_four').val();
		var promotion_marks_four	= $('#promotion_marks_four').val();
		if((promotion_title_que == "" || promotion_title_que == false) && (training_id_que == "" || training_id_que == false) && (promotion_question == "" || promotion_question == false) && (promotion_answer_one == "" || promotion_answer_one == false) && (promotion_marks_one == "" || promotion_marks_one == false) && (promotion_answer_two == "" || promotion_answer_two == false) && (promotion_marks_two == "" || promotion_marks_two == false) && (promotion_answer_three == "" || promotion_answer_three == false) && (promotion_marks_three == "" || promotion_marks_three == false) && (promotion_answer_four == "" || promotion_answer_four == false) && (promotion_marks_four == "" || promotion_marks_four == false)) {
			$('#promotion_title_que_error').show(500);
			$('#promotion_title_que_error').html('Promotion title required!');
			$('#training_id_que_error').show(500);
			$('#training_id_que_error').html('Select training from the list!');
			$('#promotion_desc_que_error').show(500);
			$('#promotion_desc_que_error').html('Please enter description for Promotion!');
			$('#promotion_question_error').show(500);
			$('#promotion_question_error').html('Please enter your question here!');
			$('#promotion_answer_one_error').show(500);
			$('#promotion_answer_one_error').html('Required!');
			$('#promotion_marks_one_error').show(500);
			$('#promotion_marks_one_error').html('Required!');
			$('#promotion_answer_two_error').show(500);
			$('#promotion_answer_two_error').html('Required!');
			$('#promotion_marks_two_error').show(500);
			$('#promotion_marks_two_error').html('Required!');
			$('#promotion_answer_three_error').show(500);
			$('#promotion_answer_three_error').html('Required!');
			$('#promotion_marks_three_error').show(500);
			$('#promotion_marks_three_error').html('Required!');
			$('#promotion_answer_four_error').show(500);
			$('#promotion_answer_four_error').html('Required!');
			$('#promotion_marks_four_error').show(500);
			$('#promotion_marks_four_error').html('Required!');			
			$('#promotion_title_que').focus();
			return false;
		} else {
			if( promotion_title_que == "" || promotion_title_que == false ) {
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_title_que_error').show(500);
				$('#promotion_title_que_error').html('Promotion title required!');
				$('#promotion_title_que').focus();
				return false;
			} else if( training_id_que == "" || training_id_que == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#training_id_que_error').html('Select training from the list!');
				$('#training_id_que').focus();
				return false;
			} else if( promotion_desc_que == "" || promotion_desc_que == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_desc_que_error').show(500);
				$('#promotion_desc_que_error').html('Please enter description for Promotion!');
				$('#promotion_desc_que').focus();
				return false;
			} else if( promotion_desc_que.length > 500 ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_desc_que_error').show(500);
				$('#promotion_desc_que_error').html('More than 500 characters inserted!');
				$('#promotion_desc_que').focus();
				return false;
			} else if( promotion_question == "" || promotion_question == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_question_error').show(500);
				$('#promotion_question_error').html('Please enter your question here!');
				$('#promotion_question').focus();
				return false;
			} else if( promotion_answer_one == "" || promotion_answer_one == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_answer_one_error').show(500);
				$('#promotion_answer_one_error').html('Required!');
				$('#promotion_answer_one').focus();
				return false;
			} else if( promotion_marks_one == "" || promotion_marks_one == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');				
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_one_error').show(500);
				$('#promotion_marks_one_error').html('Required!');
				$('#promotion_marks_one').focus();
				return false;
			} else if( isNaN(promotion_marks_one) ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');				
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_one_error').show(500);
				$('#promotion_marks_one_error').html('Numeric!');
				$('#promotion_marks_one').focus();
				return false;
			} else if( promotion_answer_two == "" || promotion_answer_two == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');				
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_answer_two_error').show(500);
				$('#promotion_answer_two_error').html('Required!');
				$('#promotion_answer_two').focus();
				return false;
			} else if( promotion_marks_two == "" || promotion_marks_two == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');				
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_two_error').show(500);
				$('#promotion_marks_two_error').html('Required!');
				$('#promotion_marks_two').focus();
				return false;
			} else if( isNaN(promotion_marks_two) ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');				
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_two_error').show(500);
				$('#promotion_marks_two_error').html('Numeric!');
				$('#promotion_marks_two').focus();
				return false;
			} else if( promotion_answer_three == "" || promotion_answer_three == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');				
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');				
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_answer_three_error').show(500);
				$('#promotion_answer_three_error').html('Required!');
				$('#promotion_answer_three').focus();
				return false;
			} else if( promotion_marks_three == "" || promotion_marks_three == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');	
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');			
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');				
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_three_error').show(500);
				$('#promotion_marks_three_error').html('Required!');
				$('#promotion_marks_three').focus();
				return false;
			} else if( isNaN(promotion_marks_three) ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');	
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');			
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');				
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_marks_three_error').show(500);
				$('#promotion_marks_three_error').html('Numeric!');
				$('#promotion_marks_three').focus();
				return false;
			} else if( promotion_answer_four == "" || promotion_answer_four == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');				
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');			
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');				
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				$('#promotion_answer_four_error').show(500);
				$('#promotion_answer_four_error').html('Required!');
				$('#promotion_answer_four').focus();
				return false;
			} else if( promotion_marks_four == "" || promotion_marks_four == false ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');	
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');			
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');				
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');				
				$('#promotion_marks_four_error').show(500);
				$('#promotion_marks_four_error').html('Required!');
				$('#promotion_marks_four').focus();
				return false;
			} else if( isNaN(promotion_marks_four) ) {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');	
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');			
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');				
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');				
				$('#promotion_marks_four_error').show(500);
				$('#promotion_marks_four_error').html('Numeric!');
				$('#promotion_marks_four').focus();
				return false;
			} else {
				$('#promotion_title_que_error').hide(500);
				$('#promotion_title_que_error').html('');
				$('#training_id_que_error').hide(500);
				$('#training_id_que_error').html('');
				$('#promotion_desc_que_error').hide(500);
				$('#promotion_desc_que_error').html('');
				$('#promotion_question_error').hide(500);
				$('#promotion_question_error').html('');
				$('#promotion_answer_one_error').hide(500);
				$('#promotion_answer_one_error').html('');
				$('#promotion_marks_one_error').hide(500);
				$('#promotion_marks_one_error').html('');
				$('#promotion_answer_two_error').hide(500);
				$('#promotion_answer_two_error').html('');
				$('#promotion_marks_two_error').hide(500);
				$('#promotion_marks_two_error').html('');
				$('#promotion_answer_three_error').hide(500);
				$('#promotion_answer_three_error').html('');
				$('#promotion_marks_three_error').hide(500);
				$('#promotion_marks_three_error').html('');
				$('#promotion_answer_four_error').hide(500);
				$('#promotion_answer_four_error').html('');
				$('#promotion_marks_four_error').hide(500);
				$('#promotion_marks_four_error').html('');
				return true
			}
		}
	});
	/* Promotion Sctipts End */

	/* Product Sctipts Start */
	if( $('#product_desc').length > 0 ) {
		$('#product_desc_length').text(500 - $('#product_desc').val().length);
		// Character Counter of Promotions Description
		$('#product_desc').change(function(){
			var remaining = 500 - $('#product_desc').val().length;
			$('#product_desc_length').text(remaining);	
		});
		$('#product_desc').keyup(function(){
			var remaining = 500 - $('#product_desc').val().length;
			$('#product_desc_length').text(remaining);	
		});
		// Script end
	}
	$('#product_name').blur(function() {
		var product_name = $('#product_name').val();
		if( product_name == "" || product_name == false ) {
			$('#product_name_error').show(500);
			$('#product_name_error').html('Product name required!');
			$('#product_name').focus();
			return false;	
		} else {
			$('#product_name_error').hide(500);
			$('#product_name_error').html('');
			return true;	
		}
	});
	$('#product_link').blur(function() {
		var product_link = $('#product_link').val();
		var RegExp 		 = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
		if( product_link == "" || product_link == false ) {
			$('#product_link_error').show(500);
			$('#product_link_error').html('Product link required!');
			$('#product_link').focus();
			return false;
		} else if( !RegExp.test(product_link) ) {
			$('#product_link_error').show(500);
			$('#product_link_error').html('This is not a valid link!');
			$('#product_link').focus();
			return false;
		} else {
			$('#product_link_error').hide(500);
			$('#product_link_error').html('');
			return true;	
		}
	});
	$('#product_refund').blur(function() {
		var product_refund = $('#product_refund').val();
		var RegExp 		 = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
		if( product_refund == "" || product_refund == false ) {
			$('#product_refund_error').show(500);
			$('#product_refund_error').html('Product refund policy url required!');
			$('#product_refund').focus();
			return false;
		} else if( !RegExp.test(product_refund) ) {
			$('#product_refund_error').show(500);
			$('#product_refund_error').html('This is not a valid link!');
			$('#product_refund').focus();
			return false;
		} else {
			$('#product_refund_error').hide(500);
			$('#product_refund_error').html('');
			return true;	
		}
	});
	if( page_type == "Insert" ) {
		$('#product_image').blur(function() {
			var product_image  	= $('#product_image').val();
			var fsize 			= $('#product_image')[0].files[0].size;
			var fileExt   		= $('#product_image').val().split('.').pop().toLowerCase();
			if( product_image == "" || product_image == false ) {
				$('#product_image_error').show(500);
				$('#product_image_error').html('Select Image for Product!');
				$('#product_image').focus();
				return false;	
			} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
				$('#product_image_error').show(500);
				$('#product_image_error').html('Only supported JPG|PNG|GIF|BMP!');
				$('#product_image').focus();
				return false;
			} else if( fsize > 2097152 ) {
				$('#product_image_error').show(500);
				$('#product_image_error').html('File size is more than 2MB!');
				$('#product_image').focus();
				return false;
			} else {
				$('#product_image_error').hide(500);
				$('#product_image_error').html('');
				return true;	
			}
		});
	}
	$('#product_image').bind('change', function() {
		var product_image   = $('#product_image').val();
		var fsize 			= $('#product_image')[0].files[0].size;
		var fileExt   		= $('#product_image').val().split('.').pop().toLowerCase();
		if( product_image == "" || product_image == false ) {
			$('#product_image_error').show(500);
			$('#product_image_error').html('Select Image for Product!');
			$('#product_image').focus();
			return false;	
		} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
			$('#product_image_error').show(500);
			$('#product_image_error').html('Only supported JPG|PNG|GIF|BMP!');
			$('#product_image').focus();
			return false;
		} else if( fsize > 2097152 ) {
			$('#product_image_error').show(500);
			$('#product_image_error').html('File size is more than 2MB!');
			$('#product_image').focus();
			return false;
		} else {
			$('#product_image_error').hide(500);
			$('#product_image_error').html('');
			return true;	
		}
	});
	$('#product_price').blur(function() {
		var product_price  = $('#product_price').val();
		if( product_price == "" || product_price == false ) {
			$('#product_price_error').show(500);
			$('#product_price_error').html('Please enter Product price!');
			$('#product_price').focus();
			return false;
		} else if( isNaN(product_price) ) {
			$('#product_price_error').show(500);
			$('#product_price_error').html('Please enter price in number only!');
			$('#product_price').focus();
			return false;
		} else {
			$('#product_price_error').hide(500);
			$('#product_price_error').html('');
			return true;	
		}
	});	
	$('#product_desc').blur(function() {
		var product_desc  = $('#product_desc').val();
		if( product_desc == "" || product_desc == false ) {
			$('#product_desc_error').show(500);
			$('#product_desc_error').html('Please enter description for Product!');
			$('#product_desc').focus();
			return false;	
		} else if( product_desc.length > 500 ) {
			$('#product_desc_error').show(500);
			$('#product_desc_error').html('More than 500 characters inserted!');
			$('#product_desc').focus();
			return false;
		} else {
			$('#product_desc_error').hide(500);
			$('#product_desc_error').html('');
			return true;	
		}
	});	
	
	// Product Validation
	$('#product_submit').click(function() {
		
		var product_name   = $('#product_name').val();
		var product_link   = $('#product_link').val();
		var product_image  = $('#product_image').val();
		var product_price  = $('#product_price').val();
		var product_refund = $('#product_refund').val();
		var product_desc   = $('#product_desc').val();
		var fsize 		   = $('#product_image')[0].files[0].size;
		var fileExt   	   = $('#product_image').val().split('.').pop().toLowerCase();
		var RegExp 		   = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
		
		if( page_type == "Insert" ) {
			if( (product_name == "" || product_name == false) && (product_link == "" || product_link == false) && (product_image == "" || product_image == false) && (product_price == "" || product_price == false) && (product_refund == "" || product_refund == false) && (product_desc == "" || product_desc == false) ) {
				$('#product_name_error').show(500).html('Product name required!');
				$('#product_link_error').show(500).html('Product link required!');
				$('#product_image_error').show(500).html('Select Image for Promotion!');
				$('#product_price_error').show(500).html('Please enter Product price!');
				$('#product_refund_error').show(500).html('Product Refund Policy URL required!');
				$('#product_desc_error').show(500).html('Please enter description for Product!');
				$('#product_name').focus();
				return false;
			} else {
				if( product_name == "" || product_name == false ) {
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_name_error').show(500).html('Product name required!');
					$('#product_name').focus();
					return false;
				} else if( product_link == "" || product_link == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_link_error').show(500).html('Product link required!');
					$('#product_link').focus();
					return false;
				} else if( !RegExp.test(product_link) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_link_error').show(500).html('This is not a valid link!');
					$('#product_link').focus();
					return false;
				} else if( product_image == "" || product_image == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_image_error').show(500).html('Select Image for Product!');
					$('#product_image').focus();
					return false;	
				} else if(($.inArray(fileExt, ['jpg', 'jpeg', 'png', 'gif', 'bmp']) == -1)) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_image_error').show(500).html('Only supported JPG|PNG|GIF|BMP!');
					$('#product_image').focus();
					return false;
				} else if( fsize > 2097152 ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_image_error').show(500).html('File size is more than 2MB!');
					$('#product_image').focus();
					return false;
				} else if( product_price == "" || product_price == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_price_error').show(500).html('Please enter Product price!');
					$('#product_price').focus();
					return false;
				} else if( isNaN(product_price) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_price_error').show(500).html('Please enter price in number only!');
					$('#product_price').focus();
					return false;
				} else if( product_refund == "" || product_refund == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');					
					$('#product_desc_error').hide(500).html('');
					$('#product_refund_error').show(500).html('Product Refund Policy URL required!');
					$('#product_refund').focus();
					return false;
				} else if( !RegExp.test(product_refund) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');					
					$('#product_desc_error').hide(500).html('');
					$('#product_refund_error').show(500).html('This is not a valid link!');
					$('#product_refund').focus();
					return false;
				} else if( product_desc == "" || product_desc == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').show(500).html('Please enter description for Product!');
					$('#product_desc').focus();
					return false;	
				} else if( product_desc.length > 500 ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').show(500).html('More than 500 characters inserted!');
					$('#product_desc').focus();
					return false;
				} else {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_image_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					return true;
				}
			}
		} else {
			if( (product_name == "" || product_name == false) && (product_link == "" || product_link == false) && (product_price == "" || product_price == false) && (product_refund == "" || product_refund == false) && (product_desc == "" || product_desc == false) ) {
				$('#product_name_error').show(500).html('Product name required!');
				$('#product_link_error').show(500).html('Product link required!');
				$('#product_price_error').show(500).html('Please enter Product price!');
				$('#product_refund_error').show(500).html('Product Refund Policy URL required!');
				$('#product_desc_error').show(500).html('Please enter description for Product!');
				$('#product_name').focus();
				return false;
			} else {
				if( product_name == "" || product_name == false ) {
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_name_error').show(500).html('Product name required!');
					$('#product_name').focus();
					return false;
				} else if( product_link == "" || product_link == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_link_error').show(500).html('Product link required!');
					$('#product_link').focus();
					return false;
				} else if( !RegExp.test(product_link) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_link_error').show(500).html('This is not a valid link!');
					$('#product_link').focus();
					return false;
				} else if( product_price == "" || product_price == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_price_error').show(500).html('Please enter Product price!');
					$('#product_price').focus();
					return false;
				} else if( isNaN(product_price) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					$('#product_price_error').show(500).html('Please enter price in number only!');
					$('#product_price').focus();
					return false;
				} else if( product_refund == "" || product_refund == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');					
					$('#product_desc_error').hide(500).html('');
					$('#product_refund_error').show(500).html('Product Refund Policy URL required!');
					$('#product_refund').focus();
					return false;
				} else if( !RegExp.test(product_refund) ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');					
					$('#product_desc_error').hide(500).html('');
					$('#product_refund_error').show(500).html('This is not a valid link!');
					$('#product_refund').focus();
					return false;
				} else if( product_desc == "" || product_desc == false ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').show(500).html('Please enter description for Product!');
					$('#product_desc').focus();
					return false;	
				} else if( product_desc.length > 500 ) {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').show(500).html('More than 500 characters inserted!');
					$('#product_desc').focus();
					return false;
				} else {
					$('#product_name_error').hide(500).html('');
					$('#product_link_error').hide(500).html('');
					$('#product_price_error').hide(500).html('');
					$('#product_refund_error').hide(500).html('');
					$('#product_desc_error').hide(500).html('');
					return true;
				}
			}
		}
		
	});
	// Script end
	/* Product Sctipts End */
	
	/* Training Sctipts Start */
	if ( page_type == 'Insert' ) {
		$('#intro_video').blur(function(){
			var intro_video = $('#intro_video').val();
			if(intro_video == "" || intro_video == false) {
				$('#intro_video_error').show(500);
				$('#intro_video_error').html('Upload intro video for training!');
				return false;
			} else {
				$('#intro_video_error').hide();
				$('#intro_video_error').html('');
				return true;
			}
		});
		$('#intro_video_youtube').blur(function(){
			var intro_video_youtube = $('#intro_video_youtube').val();
			if(intro_video_youtube == "" || intro_video_youtube == false) {
				$('#IntroVideoiFrame').attr('src', '');
				$('#intro_video_error').show(500);
				$('#intro_video_error').html('Enter a youtube video link for training intro!');
				return false;
			} else {
				if ( validateYouTubeUrl('#intro_video_youtube') == false ) {
					$('#IntroVideoiFrame').attr('src', '');
					$('#IntroVideoMessage').html("You can't play the video!");
					$('#intro_video_error').show(500);
					$('#intro_video_error').html('Enter a valid youtube video link!');
					return false;
				} else {
					var videoid = intro_video_youtube.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
					$('#IntroVideoiFrame').attr('src', 'https://www.youtube.com/embed/' + videoid[1] + '?showinfo=0');
					$('#IntroVideoMessage').html("You can play the video!");
					$('#intro_video_error').hide();
					$('#intro_video_error').html('');
					return true;
				}
			}
		});
		$('#training_video').blur(function(){
			var training_video 		= $('#training_video').val();
			if(training_video == "" || training_video == false) {
				$('#training_video_error').show(500);
				$('#training_video_error').html('Upload training video for training!');
				return false;
			} else {
				$('#training_video_error').hide();
				$('#training_video_error').html('');
				return true;
			}
		});
		$('#training_video_youtube').blur(function(){
			var training_video_youtube = $('#training_video_youtube').val();
			if(training_video_youtube == "" || training_video_youtube == false) {
				$('#TrainingVideoiFrame').attr('src', '');
				$('#training_video_error').show(500);
				$('#training_video_error').html('Enter a youtube video link for training!');
				return false;
			} else {
				if ( validateYouTubeUrl('#training_video_youtube') == false ) {
					$('#TrainingVideoiFrame').attr('src', '');
					$('#TrainingVideoMessage').html("You can't play the video!");
					$('#training_video_error').show(500);
					$('#training_video_error').html('Enter a valid youtube video link!');
					return false;
				} else {
					var videoid = training_video_youtube.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
					$('#TrainingVideoiFrame').attr('src', 'https://www.youtube.com/embed/' + videoid[1] + '?showinfo=0');
					$('#TrainingVideoMessage').html("You can play the video!");
					$('#training_video_error').html('');
					return true;
				}
			}
		});
		/*$('#training_thumb').blur(function(){
			var training_thumb 	= $('#training_thumb').val();
			var training_file	= $('#training_thumb')[0];
			var reader = new FileReader();
			reader.readAsDataURL(training_file.files[0]);
			reader.onload = function (e) {
				var image = new Image();
				image.src = e.target.result;
				image.onload = function () {
					var height = this.height;
					var width = this.width;
					if(width != 760 && height != 450) {
						$('#training_thumb_error').show(500);
						$('#training_thumb_error').html('Resolution must be 760x450');
						return false;
					}
				};
			};
			if(training_thumb == "" || training_thumb == false) {
				$('#training_thumb_error').show(500);
				$('#training_thumb_error').html('Upload training thumbnail for display training!');
				return false;
			} else {
				$('#training_thumb_error').hide();
				$('#training_thumb_error').html('');
				return true;
			}
		});*/
	}
	/* Field validation on Blur */
	$('#send_training').blur(function() {
		var send_training     	= $('#send_training').val();
		if(send_training == "" || send_training == false) {
			$('#send_training_error').show(500);
			$('#send_training_error').html('Select Training from the list!');
			return false;
		} else {
			$('#send_training_error').hide();
			$('#send_training_error').html('');
			return true;	
		}
	});
	$('#email_csv').blur(function() {
		var email_csv	 		= $('#email_csv').val();
		var fileExt   			= $('#email_csv').val().split('.').pop().toLowerCase();		
		if(email_csv == "" || email_csv == false) {
			$('#email_csv_error').show(500);
			$('#email_csv_error').html('A CSV file is required!');
			return false;
		} else if(($.inArray(fileExt, ['csv']) == -1)) {
			$('#email_csv_error').show(500);
			$('#email_csv_error').html('Unsupported file, only CSV file!');
			return false;
		} else {
			$('#email_csv_error').hide();
			$('#email_csv_error').html('');
			return true;
		}
	});
	$('#email_csv').bind('change', function() {
		var email_csv	 		= $('#email_csv').val();
		var fileExt   			= $('#email_csv').val().split('.').pop().toLowerCase();		
		if(email_csv == "" || email_csv == false) {
			$('#email_csv_error').show(500);
			$('#email_csv_error').html('A CSV file is required!');
			return false;
		} else if(($.inArray(fileExt, ['csv']) == -1)) {
			$('#email_csv_error').show(500);
			$('#email_csv_error').html('Unsupported file, only CSV file!');
			return false;
		} else {
			$('#email_csv_error').hide();
			$('#email_csv_error').html('');
			return true;
		}
	});
	$('#training_name').blur(function() {
		var training_name     	= $('#training_name').val();
		if(training_name == "" || training_name == false) {
			$('#training_name_error').show(500);
			$('#training_name_error').html('Enter training name!');
			return false;
		} else {
			$('#training_name_error').hide();
			$('#training_name_error').html('');
			return true;	
		}
	});
	$('#training_cat').blur(function() {
		var training_cat    	= $('#training_cat').val();
		if(training_cat == "" || training_cat == false) {
			$('#training_cat_error').show(500);
			$('#training_cat_error').html('Select training category!');
			return false;
		} else {
			$('#training_cat_error').hide();
			$('#training_cat_error').html('');
			return true;	
		}
	});
	$('#training_link_text').blur(function() {
		var training_link_text    	= $('#training_link_text').val();
		var alphabetPattern			= /^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]+$/;
		if(training_link_text == "" || training_link_text == false) {
			$('#training_link_text_error').show(500);
			$('#training_link_text_error').html('Enter Training Link text!');
			return false;
		} else if(!alphabetPattern.test(training_link_text)) {
			$('#training_link_text_error').show(500);
			$('#training_link_text_error').html('Alphabets and Special characters only!');
			return false;
		} else if( training_link_text.length > 20 ) {
			$('#training_link_text_error').show(500);
			$('#training_link_text_error').html('Too long, only 20 characters allowed!');
			return false;
		} else {
			$('#training_link_text_error').hide();
			$('#training_link_text_error').html('');
			return true;	
		}
	});
	$('#intro_video').bind('change', function() {
		var videoExt   			= $('#intro_video').val().split('.').pop().toLowerCase();
		if(($.inArray(videoExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
			$('#intro_video_error').show(500);
			$('#intro_video_error').html('Unsupported file, only MP4|WebM|OGG|MOV!');
			return false;
		} else {
			$('#intro_video_error').hide();
			$('#intro_video_error').html('');
			return true;
		}
	});
	$('#about_training').blur(function() {
		var about_training 		= $('#about_training').val();
		if(about_training == "" || about_training == false) {
			$('#about_training_error').show(500);
			$('#about_training_error').html('Enter description about training!');
			return false;
		} else {
			$('#about_training_error').hide();
			$('#about_training_error').html('');
			return true;
		}
	});
	$('#training_video').bind('change', function() {
		var videoExt   			= $('#training_video').val().split('.').pop().toLowerCase();		
		if(($.inArray(videoExt, ['mp4', 'm4p', 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
			$('#training_video_error').show(500);
			$('#training_video_error').html('Unsupported file, only MP4|WebM|OGG|MOV!');
			return false;
		} else {
			$('#training_video_error').hide();
			$('#training_video_error').html('');
			return true;
		}
	});
	/*$('#training_thumb').bind('change', function() {
		var thumbExt 	= $('#training_thumb').val().split('.').pop().toLowerCase();		
		var training_thumb	= $('#training_thumb')[0];
		var reader = new FileReader();
		reader.readAsDataURL(training_thumb.files[0]);
		reader.onload = function (e) {
			var image = new Image();
			image.src = e.target.result;
			image.onload = function () {
				var height = this.height;
				var width = this.width;
				if(width != 800 && height != 450) {
					$('#training_thumb_error').show(500);
					$('#training_thumb_error').html('Resolution must be 760x450');
					return false;
				}
			};
		};
		if(($.inArray(thumbExt, ['jpg', 'jpeg', 'png']) == -1)) {
			$('#training_thumb_error').show(500);
			$('#training_thumb_error').html('Unsupported file, only JPG|PNG format!');
			return false;
		} else {
			$('#training_thumb_error').hide();
			$('#training_thumb_error').html('');
			return true;
		}
	});*/
	$('#visible_time').blur(function() {
		var visible_time		= $('#visible_time').val();
		if(visible_time == "" || visible_time == false) {
			$('#visible_time_error').show(500);
			$('#visible_time_error').html('Select visible time from video preview!');
			return false;
		} else if( isNaN(visible_time) ) {
			$('#visible_time_error').show(500);
			$('#visible_time_error').html('Time will only in numbers!');
			return false;
		} else {
			$('#visible_time_error').hide();
			$('#visible_time_error').html('');
			return true;
		}
	});
	$('#training_tags').blur(function() {
		var training_tags	= $('#training_tags').val();
		var value 			= $('#training_tags').val().replace(" ", "");
    	var words 			= value.split(",");

		if(training_tags == "" || training_tags == false) {
			$('#training_tags_error').show(500);
			$('#training_tags_error').html('Enter training tags!');
			return false;
		} else if(words.length > 3) {
			$('#training_tags_error').show(500);
			$('#training_tags_error').html('Hey! That\'s more than 3 tags!');
			return false;
		} else {
			$('#training_tags_error').hide();
			$('#training_tags_error').html('');
			return true;
		}
	});
	$('#similar_training').blur(function() {
		var similar_training	= $('#similar_training').val();
		if(similar_training == "" || similar_training == false) {
			$('#similar_training_error').show(500);
			$('#similar_training_error').html('Select option of display similar trainings!');
			return false;
		} else {
			$('#similar_training_error').hide();
			$('#similar_training_error').html('');
			return true;
		}
	});
	$('#cta_text').blur(function(){
		var cta_text		= $('#cta_text').val();
		var alphabetPattern	= /^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]+$/;		
		if(cta_text == "" || cta_text == false) {
			$('#cta_text_error').show(500);
			$('#cta_text_error').html('Enter a Call To Action text!');
			return false;
		} else if(!alphabetPattern.test(cta_text)) {
			$('#cta_text_error').show(500);
			$('#cta_text_error').html('Alphabets and Special characters only!');
			return false;
		} else if( cta_text.length > 20 ) {
			$('#cta_text_error').show(500);
			$('#cta_text_error').html('Too long, only 20 characters allowed!');
			return false;
		} else {
			$('#cta_text_error').hide();
			$('#cta_text_error').html('');
			return true;
		}
	});
	$('#product_list_select').blur(function() {
		var ProductID  = $('#product_list_select').val();		
		if(ProductID == "" || ProductID == false) {
			$('#product_list_select_error').show(500);
			$('#product_list_select_error').html('Error! Select product again from the list!');
			return false;
		} else {
			$('#product_list_select_error').hide();
			$('#product_list_select_error').html('');
			return true;
		}
	});
	/* Field validation on Blur */
	
	/* OnChange AJAX Function for Select Product */
	$('#product_list_select').change(function() {
		$('#product_details').css('display', 'none');
		$('#product_image').attr('src', '');
		$('#product_name').html('');
		$('#product_desc').html('');
		$('#product_price').html('');
		$('#product_link').attr('href', '');
		$('#product_wrapper').prepend('<div class="row"><div class="col-md-2 col-md-offset-5"><img src="' + asset_url + '/images/spinner.gif" id="product_loader" /></div></div>');
		var ProductID  = $(this).val();
		if( ProductID == "" || ProductID == false ) {
			$('#product_loader').remove();
			$('#product_details').css('display', 'none');
			$('#product_image').attr('src', '');
			$('#product_name').html('');
			$('#product_desc').html('');
			$('#product_price').html('');
			$('#product_link').attr('href', '');
			$('#product_list_select_error').show(500);
			$('#product_list_select_error').html('Error! Select product again from the list!');
			return false;
		} else {
			var dataString = 'ProductID=' + ProductID;
			$.ajax({
				type: 'POST',
				url: ajax_url + '/get-product/',
				data: dataString,
				dataType: 'json',
				success: function(data) {
					if(data != "") {
						$('#product_loader').remove();
						$('#product_details').css('display', 'block');
						$('#product_image').attr('src', base_url + '/uploads/prd/' + data.Product_Image);
						$('#product_name').html(data.Product_Name);
						$('#product_desc').html(data.Product_Desc);
						$('#product_price').html('$' + data.Product_Price + '.00');
						$('#product_link').attr('href', data.Product_Link);
						$('#product_list_select_error').hide();
						$('#product_list_select_error').html('');
						return true;
					} else {
						$('#product_details').css('display', 'none');
						$('#product_list_select_error').show(500);
						$('#product_list_select_error').html('Error! Select product again from the list!');
						return false;
					}
				}
			});
		}
	});
	/* OnChange AJAX Function for Select Product */
	
	$('.training-next').on('click', function(){
		var nextStep = $(this).parent().siblings('.active').data('step');
		var step = nextStep - 1;
		if(step == 1) {
			var page_type 			= $('#page_type').val();
			var training_name 		= $('#training_name').val();
			var training_cat 		= $('#training_cat').val();
			var training_link_text 	= $('#training_link_text').val();
			var video_type			= $('input[name="video_type"]:checked').val();
			var intro_video 		= $('#intro_video').val();
			var intro_video_youtube = $('#intro_video_youtube').val();
			var alphabetPattern		= /^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]+$/;
			var videoExt   			= $('#intro_video').val().split('.').pop().toLowerCase();
			
			if(page_type == 'Insert') {
				if((training_name == "" || training_name == false) && (training_cat == "" || training_cat == false) && (training_link_text == "" || training_link_text == false) ) {
					$('#training_name_error').show(500);
					$('#training_name_error').html('Enter training name!');
					$('#training_cat_error').show(500);
					$('#training_cat_error').html('Select training category!');
					$('#training_link_text_error').show(500);
					$('#training_link_text_error').html('Enter Training Link text!');
					if ( video_type == 'custom' && (intro_video == "" || intro_video == false) ) {
						$('#intro_video_error').show(500);
						$('#intro_video_error').html('Upload intro video for training!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if ( video_type == 'youtube' && (intro_video_youtube == "" || intro_video_youtube == false) ) {
						$('#intro_video_error').show(500);
						$('#intro_video_error').html('Enter a youtube video link for training intro!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else {
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						return false;
					}
				} else {
					if(training_name == "" || training_name == false) {
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						$('#training_name_error').show(500);
						$('#training_name_error').html('Enter training name!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(training_cat == "" || training_cat == false) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						$('#training_cat_error').show(500);
						$('#training_cat_error').html('Select training category!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(training_link_text == "" || training_link_text == false) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Enter Training Link text!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(!alphabetPattern.test(training_link_text)) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Alphabets and Special characters only!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if( training_link_text.length > 20 ) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Too long, only 20 characters allowed!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if (video_type == 'custom' ) {
						if (intro_video == "" || intro_video == false) {
							$('#training_name_error').hide();
							$('#training_name_error').html('');
							$('#training_cat_error').hide();
							$('#training_cat_error').html('');
							$('#training_link_text_error').hide();
							$('#training_link_text_error').html('');
							$('#intro_video_error').show(500);
							$('#intro_video_error').html('Upload intro video for training!');
							$('.steps').find('li:first-child').trigger('click');
							return false;
						} else {
							$('#training_name_error').hide();
							$('#training_name_error').html('');
							$('#training_cat_error').hide();
							$('#training_cat_error').html('');
							$('#training_link_text_error').hide();
							$('#training_link_text_error').html('');
							$('#intro_video_error').hide();
							$('#intro_video_error').html('');
							return true;
						}
					} else if (video_type == 'youtube' ) {
						if (intro_video_youtube == "" || intro_video_youtube == false) {
							$('#training_name_error').hide();
							$('#training_name_error').html('');
							$('#training_cat_error').hide();
							$('#training_cat_error').html('');
							$('#training_link_text_error').hide();
							$('#training_link_text_error').html('');
							$('#intro_video_error').show(500);
							$('#intro_video_error').html('Enter a youtube video link for training intro!');
							$('.steps').find('li:first-child').trigger('click');
							return false;
						} else {
							$('#training_name_error').hide();
							$('#training_name_error').html('');
							$('#training_cat_error').hide();
							$('#training_cat_error').html('');
							$('#training_link_text_error').hide();
							$('#training_link_text_error').html('');
							$('#intro_video_error').hide();
							$('#intro_video_error').html('');
							return true;
						}
					} else if(($.inArray(videoExt, ['mp4' , 'm4p' , 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#intro_video_error').show(500);
						$('#intro_video_error').html('Unsupported file, only MP4|WebM|OGG|MOV!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#intro_video_error').hide();
						$('#intro_video_error').html('');
						return true;	
					}
				}
			} else {
				if((training_name == "" || training_name == false) && (training_cat == "" || training_cat == false) && (training_link_text == "" || training_link_text == false)) {
					$('#training_name_error').show(500);
					$('#training_name_error').html('Enter training name!');
					$('#training_cat_error').show(500);
					$('#training_cat_error').html('Select training category!');
					$('#training_link_text_error').show(500);
					$('#training_link_text_error').html('Enter Training Link text!');
					$('.steps').find('li:first-child').trigger('click');
					return false;
				} else {
					if(training_name == "" || training_name == false) {
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#training_name_error').show(500);
						$('#training_name_error').html('Enter training name!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(training_cat == "" || training_cat == false) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						$('#training_cat_error').show(500);
						$('#training_cat_error').html('Select training category!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(training_link_text == "" || training_link_text == false) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Enter Training Link text!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if(!alphabetPattern.test(training_link_text)) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Alphabets and Special characters only!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else if( training_link_text.length > 20 ) {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').show(500);
						$('#training_link_text_error').html('Too long, only 20 characters allowed!');
						$('.steps').find('li:first-child').trigger('click');
						return false;
					} else {
						$('#training_name_error').hide();
						$('#training_name_error').html('');
						$('#training_cat_error').hide();
						$('#training_cat_error').html('');
						$('#training_link_text_error').hide();
						$('#training_link_text_error').html('');
						return true;	
					}
				}
			}			
		}
		if(step == 2) {
			var about_training 		= $('#about_training').val();
			if(about_training == "" || about_training == false) {
				$('#about_training_error').show(500);
				$('#about_training_error').html('Enter description about training!');
				$('.steps').find('li:nth-child(2)').trigger('click');
				return false;
			} else {
				$('#about_training_error').hide();
				$('#about_training_error').html('');
				return true;
			}
		}
		if(step == 3) {
			var page_type 		= $('#page_type').val();
			var training_video_type = $('input[name="training_video_type"]:checked').val();
			var training_video 	= $('#training_video').val();
			var training_video_youtube 	= $('#training_video_youtube').val();
			//var training_thumb 	= $('#training_thumb').val();
			var videoExt   		= $('#training_video').val().split('.').pop().toLowerCase();
			//var thumbExt   		= $('#training_thumb').val().split('.').pop().toLowerCase();
			var visible_time	= $('#visible_time').val();
			
			if( page_type == 'Insert' ) {
				if((training_thumb == "" || training_thumb == false) && (visible_time == "" || visible_time == false)) {
					if ( training_video_type == 'custom' && (training_video == "" || training_video == false) ) {
						$('#training_video_error').show(500);
						$('#training_video_error').html('Select video for training!');
					} else if ( training_video_type == 'youtube' && (training_video_youtube == "" || training_video_youtube == false) ) {
						$('#training_video_error').show(500);
						$('#training_video_error').html('Enter a youtube video link for training!');
					} else {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
					}
					//$('#training_thumb_error').show(500);
					//$('#training_thumb_error').html('Select image for training thumbnail!');
					$('#visible_time_error').show(500);
					$('#visible_time_error').html('Select visible time from video preview!');
					$('.steps').find('li:nth-child(3)').trigger('click');
					return false;
				} else {
					if ( training_video_type == 'custom' ) {
						if(training_video == "" || training_video == false) {
							/*$('#training_thumb_error').hide();
							$('#training_thumb_error').html('');*/
							$('#visible_time_error').hide();
							$('#visible_time_error').html('');
							$('#training_video_error').show(500);
							$('#training_video_error').html('Upload training video for training!');
							$('.steps').find('li:nth-child(3)').trigger('click');
							return false;
						} else {
							$('#training_video_error').hide();
							$('#training_video_error').html('');
							/*$('#training_thumb_error').hide();
							$('#training_thumb_error').html('');*/
							$('#visible_time_error').hide();
							$('#visible_time_error').html('');
							return true;
						}
					} else if ( training_video_type == 'youtube' ) {
						if(training_video_youtube == "" || training_video_youtube == false) {
							/*$('#training_thumb_error').hide();
							$('#training_thumb_error').html('');*/
							$('#visible_time_error').hide();
							$('#visible_time_error').html('');
							$('#training_video_error').show(500);
							$('#training_video_error').html('Enter a youtube video link for training!');
							$('.steps').find('li:nth-child(3)').trigger('click');
							return false;
						} else {
							$('#training_video_error').hide();
							$('#training_video_error').html('');
							/*$('#training_thumb_error').hide();
							$('#training_thumb_error').html('');*/
							$('#visible_time_error').hide();
							$('#visible_time_error').html('');
							return true;
						}
					} else if(($.inArray(videoExt, ['mp4' , 'm4p' , 'm4v', 'webm', 'ogg', 'ogv', 'mov']) == -1)) {
						/*$('#training_thumb_error').hide();
						$('#training_thumb_error').html('');*/
						$('#visible_time_error').hide();
						$('#visible_time_error').html('');
						$('#training_video_error').show(500);
						$('#training_video_error').html('Unsupported file, only MP4|WebM|OGG|MOV!');
						$('.steps').find('li:nth-child(3)').trigger('click');
						return false;				
					/*} else if(training_thumb == "" || training_thumb == false) {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
						$('#visible_time_error').hide();
						$('#visible_time_error').html('');
						$('#training_thumb_error').show(500);
						$('#training_thumb_error').html('Upload training thumbnail for display training!');
						$('.steps').find('li:nth-child(3)').trigger('click');
						return false;
					} else if(($.inArray(thumbExt, ['jpg', 'jpeg' , 'png']) == -1)) {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
						$('#visible_time_error').hide();
						$('#visible_time_error').html('');
						$('#training_thumb_error').show(500);
						$('#training_thumb_error').html('Unsupported file, only JPG|PNG format!');
						$('.steps').find('li:nth-child(3)').trigger('click');
						return false;				
					} else if(training_thumb.length > 0) {
						var training_file	= $('#training_thumb')[0];
						var reader 			= new FileReader();
						reader.readAsDataURL(training_file.files[0]);
						reader.onload = function (e) {
							var image = new Image();
							image.src = e.target.result;
							image.onload = function () {
								var height = this.height;
								var width = this.width;
								if(width != 760 && height != 450) {
									$('#training_video_error').hide();
									$('#training_video_error').html('');
									$('#visible_time_error').hide();
									$('#visible_time_error').html('');
									$('#training_thumb_error').show(500);
									$('#training_thumb_error').html('Resolution must be 760x450');
									$('.steps').find('li:nth-child(3)').trigger('click');
									return false;
								}
							};
						};*/					
					} else if(visible_time == "" || visible_time == false) {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
						/*$('#training_thumb_error').hide();
						$('#training_thumb_error').html('');*/
						$('#visible_time_error').show(500);
						$('#visible_time_error').html('Select visible time from video preview!');
						$('.steps').find('li:nth-child(3)').trigger('click');
						return false;
					} else if( isNaN(visible_time) ) {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
						/*$('#training_thumb_error').hide();
						$('#training_thumb_error').html('');*/
						$('#visible_time_error').show(500);
						$('#visible_time_error').html('Time will only in numbers!');
						$('.steps').find('li:nth-child(3)').trigger('click');
						return false;
					} else {
						$('#training_video_error').hide();
						$('#training_video_error').html('');
						/*$('#training_thumb_error').hide();
						$('#training_thumb_error').html('');*/
						$('#visible_time_error').hide();
						$('#visible_time_error').html('');
						return true;
					}
				}
			} else {
				if(visible_time == "" || visible_time == false) {
					$('#visible_time_error').show(500);
					$('#visible_time_error').html('Select visible time from video preview!');
					$('.steps').find('li:nth-child(3)').trigger('click');
					return false;
				} else if( isNaN(visible_time) ) {
					$('#visible_time_error').show(500);
					$('#visible_time_error').html('Time will only in numbers!');
					$('.steps').find('li:nth-child(3)').trigger('click');
					return false;
				} else {
					$('#visible_time_error').hide();
					$('#visible_time_error').html('');
				}
			}
		}
		if(step == 4) {
			var training_tags		= $('#training_tags').val();
			var value 				= $('#training_tags').val().replace(" ", "");
    		var words 				= value.split(",");
			var similar_training	= $('#similar_training').val();
			var ProductCount 		= $('#products_count').val();
			
			if((training_tags == "" || training_tags == false) && (similar_training == "" || similar_training == false)) {
				$('#training_tags_error').show(500);
				$('#training_tags_error').html('Enter training tags!');
				$('#similar_training_error').show(500);
				$('#similar_training_error').html('Select option of display similar trainings!');
				$('.steps').find('li:nth-child(4)').trigger('click');
				return false;
			} else {
				if(training_tags == "" || training_tags == false) {
					$('#similar_training_error').hide();
					$('#similar_training_error').html('');
					$('#training_tags_error').show(500);
					$('#training_tags_error').html('Enter training tags!');
					$('.steps').find('li:nth-child(4)').trigger('click');
					return false;
				} else if(words.length > 3) {
					$('#similar_training_error').hide();
					$('#similar_training_error').html('');
					$('#training_tags_error').show(500);
					$('#training_tags_error').html('Hey! That\'s more than 3 tags!');
					$('.steps').find('li:nth-child(4)').trigger('click');
					return false;
				} else if(similar_training == "" || similar_training == false) {
					$('#training_tags_error').hide();
					$('#training_tags_error').html('');
					$('#similar_training_error').show(500);
					$('#similar_training_error').html('Select option of display similar trainings!');
					$('.steps').find('li:nth-child(4)').trigger('click');
					return false;
				} else {
					if( ProductCount == "No" ) {
						$('.final-step').remove();
						return false;
					} else if( ProductCount == "Yes" ) {
						return true;
					}
					$('#training_tags_error').hide();
					$('#training_tags_error').html('');
					$('#similar_training_error').hide();
					$('#similar_training_error').html('');
					return true;
				}
			}
		}
		if(step == 5) {
			var cta_text			= $('#cta_text').val();
			var product_list_select	= $('#product_list_select').val();
			var alphabetPattern		= /^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]+$/;
			
			if((cta_text == "" || cta_text == false) && (product_list_select == "" || product_list_select == false)) {
				$('#product_list_select_error').show(500);
				$('#product_list_select_error').html('Error! Select product from the list!');
				$('#cta_text_error').show(500);
				$('#cta_text_error').html('Enter a Call To Action text!');
				$('.steps').find('li:nth-child(5)').trigger('click');
				return false;
			} else {
				if(cta_text == "" || cta_text == false) {
					$('#product_list_select_error').hide();
					$('#product_list_select_error').html('');
					$('#cta_text_error').show(500);
					$('#cta_text_error').html('Enter a Call To Action text!');
					$('.steps').find('li:nth-child(5)').trigger('click');
					return false;
				} else if(!alphabetPattern.test(cta_text)) {
					$('#product_list_select_error').hide();
					$('#product_list_select_error').html('');
					$('#cta_text_error').show(500);
					$('#cta_text_error').html('Alphabets and Special characters only!');
					$('.steps').find('li:nth-child(5)').trigger('click');
					return false;
				} else if( cta_text.length > 20 ) {
					$('#product_list_select_error').hide();
					$('#product_list_select_error').html('');
					$('#cta_text_error').show(500);
					$('#cta_text_error').html('Too long, only 20 characters allowed!');
					$('.steps').find('li:nth-child(5)').trigger('click');
					return false;
				} else if(product_list_select == "" || product_list_select == false) {					
					$('#cta_text_error').hide();
					$('#cta_text_error').html('');
					$('#product_list_select_error').show(500);
					$('#product_list_select_error').html('Error! Select product from the list!');
					$('.steps').find('li:nth-child(5)').trigger('click');
					return false;
				} else {
					$('#product_list_select_error').hide();
					$('#product_list_select_error').html('');
					$('#cta_text_error').hide();
					$('#cta_text_error').html('');
					return true;
				}
			}
		}
	});
	/* Training Sctipts End */
});

// NProgress
if (typeof NProgress != 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}
$(function() {
	$('#page-content-wrapper').addClass('animated fadeIn');
});

function initSidebarMenu() {
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");
	});
	$("#menu-toggle-2").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled-2");
		$('#menu ul').hide();
	});
}

function initPagination() {
	var pageSize = 9;
	var pagesCount = $(".content").length;
	var currentPage = 1;
	var nav = '';
	var totalPages = Math.ceil(pagesCount / pageSize);
	if ( pagesCount > 0 ) {
		$(".pagination").prepend('<li class="pag_prev"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>');
		for ( var s = 0; s < totalPages; s++ ) {
			nav += '<li class="numeros"><a href="#">'+(s+1)+'</a></li>';
		}
		$(".pagination").append(nav);
		$(".pagination").append('<li class="pag_next"><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>');

		$(".numeros").first().addClass("active");
		showPage = function() {
			$(".content").hide().each(function(n) {
				if ( n >= pageSize * (currentPage - 1) && n < pageSize * currentPage )
					$(this).show();
			});
		}
		showPage();
	}
	
	$(".pagination li.numeros").click(function(e) {
		e.preventDefault();
		
		$(".pagination li").removeClass("active");
		$(this).addClass("active");
		currentPage = parseInt($(this).text());
		showPage();
	});
	
	$(".pagination li.pag_prev").click(function(e) {
		e.preventDefault();
		
		if($(this).next().is('.active')) return;
		$('.numeros.active').removeClass('active').prev().addClass('active');
		currentPage = currentPage > 1 ? (currentPage-1) : 1;
		showPage();
	});
	
	$(".pagination li.pag_next").click(function(e) {
		e.preventDefault();
		
		if($(this).prev().is('.active')) return;
		$('.numeros.active').removeClass('active').next().addClass('active');
		currentPage = currentPage < totalPages ? (currentPage+1) : totalPages;
		showPage();
	});
}

function initFlowPlayer() {
	if ( $('.flowplayer').length > 0 ) {
		var cuetime  = $('#CTA_Time').length>0 ? $('#CTA_Time').val() : 0;
		var videosrc = $('#Video_SRC').val();
		window.onload = function() {
			var feature = {
				cuepoints: [cuetime],
				sources: [
					{ type: "video/webm",  	 	src: videosrc },
					{ type: "video/mp4",   	 	src: videosrc },
					{ type: "video/flash", 	 	src: videosrc },
					{ type: "video/ogg", 	 	src: videosrc },
					{ type: "video/quicktime", 	src: videosrc },
				]
			},
			container = document.getElementById("training_player"),		  
			toggleUiElement = function(klass, show) {
				var divs = container.getElementsByTagName("div"), klasspat = new RegExp("(^| )" + klass + "( |$)"), i;		
				for(i=0;i<divs.length;i+=1) {
					if(klasspat.test(divs[i].className)) {
						divs[i].style.display = show ? "block" : "none";
						return;
					}
				}
			};
			
			flowplayer(container, {
				ratio: 0.56,
				clip: feature
			}).on("cuepoint", function (e, api, cuepoint) {
				if(cuepoint.time === feature.cuepoints[0]) {
					if( $('#CTA_Button').hasClass('hide') ) {
						$('#CTA_Button').removeClass('hide');
					}
				}
			}).on("progress", function(e, api) {			
				var trainingID		= $('#Training_ID').val();
				var trainerID		= $('#Trainer_ID').val();
				var categoryID		= $('#Category_ID').val();
				var memberID		= $('#Member_ID').val();
				var userType		= $('#User_Type').val();
				var lastSeekTime	= api.video.time;
				if( userType == 'viewer' ) {
					var dataString 		= 'trainingID=' + trainingID + '&trainerID=' + trainerID + '&categoryID=' + categoryID + '&memberID=' + memberID + '&lastSeekTime=' + lastSeekTime;
					$.ajax({
						type: "POST",
						url: ajax_url + "/ongoing-training/",
						async: dataString.async,
						data: dataString,
						success: function(data) {}
					});
				}
			}).on("finish", function(e, api) {
				var trainingID		= $('#Training_ID').val();
				var trainerID		= $('#Trainer_ID').val();
				var categoryID		= $('#Category_ID').val();
				var memberID		= $('#Member_ID').val();
				var userType		= $('#User_Type').val();
				var lastSeekTime	= api.video.time;
				if( userType == 'viewer' ) {
					var dataString 		= 'trainingID=' + trainingID + '&trainerID=' + trainerID + '&categoryID=' + categoryID + '&memberID=' + memberID + '&lastSeekTime=' + lastSeekTime;
					$.ajax({
						type: "POST",
						url: ajax_url + "/finish-training/",
						async: dataString.async,
						data: dataString,
						success: function(data) {}
					});
				}
			});
		}
	}
}
/* YouTube Seeking Code */
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('training_youtube_player', {
		height: '390',
		width: '640',
		videoId: $('#trainingVideoID').val(),
		events: {
            'onStateChange': onPlayerStateChange,
        }
	});
}

var timestamp = $('#CTA_Time').val();
var timer; 
function timestamp_reached() {
	if( $('#CTA_Button').hasClass('hide') ) {
		$('#CTA_Button').removeClass('hide');
	}
}
 
function timestamp_callback() {
	clearTimeout(timer);
		 
	current_time = player.getCurrentTime();
	remaining_time = timestamp - current_time;
	if (remaining_time > 0) {
		timer = setTimeout(timestamp_reached, remaining_time * 1000);
	}    
}

function onPlayerStateChange(evt) {
	var trainingID		= $('#Training_ID').val();
	var trainerID		= $('#Trainer_ID').val();
	var categoryID		= $('#Category_ID').val();
	var memberID		= $('#Member_ID').val();
	var userType		= $('#User_Type').val();
	var lastSeekTime	= player.getCurrentTime();
	var ctaTime			= $('#CTA_Time').val();
	
	if(evt.data == YT.PlayerState.PLAYING) {
		timestamp_callback();
	}
	
	if(evt.data != 0) {
		if( userType == 'viewer' ) {
			var dataString 		= 'trainingID=' + trainingID + '&trainerID=' + trainerID + '&categoryID=' + categoryID + '&memberID=' + memberID + '&lastSeekTime=' + lastSeekTime;
			$.ajax({
				type: "POST",
				url: ajax_url + "/ongoing-training/",
				async: dataString.async,
				data: dataString,
				success: function(data) {}
			});
		}	
	} else if(evt.data === 0) {
		if( userType == 'viewer' ) {
			var dataString 		= 'trainingID=' + trainingID + '&trainerID=' + trainerID + '&categoryID=' + categoryID + '&memberID=' + memberID + '&lastSeekTime=' + lastSeekTime;
			$.ajax({
				type: "POST",
				url: ajax_url + "/finish-training/",
				async: dataString.async,
				data: dataString,
				success: function(data) {}
			});
		}
	}
}
/* YouTube Seeking Code */

$(document).ready(function() {
	initSidebarMenu();
	initPagination();
	initFlowPlayer();
	$('.flowplayer').on('click', function() {
		$(this).css('background', 'none');
	});
});
$(document).ready(function() {
    //Socail Registration Process
    $('#ePrenticeRegister').bootstrapValidator({
        live: 'enabled',
        excluded: [':disabled'],
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your First Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your First name contain alphabet only'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
            lname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your Last Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your Last name contain alphabet only'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email Address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'The input is not a valid email address'
                    },
                    remote: {
                        type: 'POST',
                        url: ajax_url + '/check-email/',
                        message: 'The Email is already used'
                    }
                }
            }
        }   
    }).on('status.field.bv', function(e, data) {
        data.bv.disableSubmitButtons(false);
    }).on('success.form.bv', function(e, data) {
        e.preventDefault();

        var $form = $(e.target);
        HTMLFormElement.prototype.submit.call($form[0]);

    });
    // End Process

	//Registration Process
	$('#ePrenticeRegister').bootstrapValidator({
        live: 'enabled',
        excluded: [':disabled'],
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your First Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your First name contain alphabet only'
                    },
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
			lname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your Last Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your Last name contain alphabet only'
                    },
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email Address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'The input is not a valid email address'
                    },
                    remote: {
                        type: 'POST',
                        url: ajax_url + '/check-email/',
                        message: 'The Email is already used'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The Password must between 6 to 30 characters'
                    }
                }
            }
        }   
    }).on('status.field.bv', function(e, data) {
        data.bv.disableSubmitButtons(false);
    }).on('success.form.bv', function(e, data) {
		e.preventDefault();

		var $form = $(e.target);
		HTMLFormElement.prototype.submit.call($form[0]);

    });
	// End Process
	
	// Login Process
	$('#ePrenticeLogin').bootstrapValidator({
        live: 'enabled',
        excluded: [':disabled'],
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email Address is required and cannot be empty'
                    },
                    regexp: {
                        regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'The input is not a valid email address'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The Password is required and cannot be empty'
                    }
                }
            }
        }   
    }).on('status.field.bv', function(e, data) {
        data.bv.disableSubmitButtons(false);
    }).on('success.form.bv', function(e, data) {
		e.preventDefault();

		var $form = $(e.target);
		HTMLFormElement.prototype.submit.call($form[0]);

    });
	// End Process
	
	// Profile Process
	$('#ePrenticeProfile').bootstrapValidator({
        live: 'enabled',
        excluded: [':disabled'],
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your First Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your First name contain alphabet only'
                    },
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
			lname: {
                validators: {
                    notEmpty: {
                        message: 'Please Enter your Last Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your Last name contain alphabet only'
                    },
					stringLength: {
                        min: 2,
                        max: 30,
                        message: 'The name must between 2 to 30 characters'
                    }
                }
            },
            old_password: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The Password must between 6 to 30 characters'
                    },
					different: {
						field: 'new_password',
						message: 'The Old Password and New Password can\'t same'
					}
                }
            },
			new_password: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The Password must between 6 to 30 characters'
                    },
					different: {
						field: 'old_password',
						message: 'The Old Password and New Password can\'t same'
					}
                }
            },
			desig: {
                validators: {
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Your designation contain alphabet only'
                    }
                }
            },
			paypal_id: {
                validators: {
                    regexp: {
                        regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        message: 'The input is not a valid email address'
                    }
                }
            },
			facebook: {
                validators: {
                    uri: {
                        message: 'The URL is not valid'
                    }
                }
            },
			google: {
                validators: {
                    uri: {
                        message: 'The URL is not valid'
                    }
                }
            },
			twitter: {
                validators: {
                    uri: {
                        message: 'The URL is not valid'
                    }
                }
            },
			linkedin: {
                validators: {
                    uri: {
                        message: 'The URL is not valid'

                    }
                }
            }
        }   
    }).on('status.field.bv', function(e, data) {
        data.bv.disableSubmitButtons(false);
    }).on('success.form.bv', function(e, data) {
		var $form = $(e.target);
		HTMLFormElement.prototype.submit.call($form[0]);
    });
	// End Process
	
	// Experience Process
	$('#ePrenticeExperience').bootstrapValidator({
        live: 'enabled',
        excluded: [':disabled'],
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            domain: {
                validators: {
					regexp: {
                        regexp: /^[A-Za-z0-9\.\,\#\+\s]+$/i,
                        message: 'Domain can contain alphanumeric and special characters(+|#|.|,)'
                    },
					callback: {
                        message: 'The Domain details is not valid',
                        callback: function(value, validator, $field) {
							var res = value.charAt(0);
							if (value === '') {
                                return true;
                            }
							
                            if (value == ' ') {
                                return false;
                            }							
							
							if (res.search(/[A-Za-z]/) < 0) {
                                return {
                                    valid: false,
                                    message: 'Domain must starts with alphabet only'
                                }
                            }

                            return true;
                        }
					}
                }
            },
			projects: {
                validators: {
					regexp: {
                        regexp: /^[A-Za-z0-9\.\,\#\+\s]+$/i,
                        message: 'Projects can contain alphanumeric and special characters(+|#|.|,)'
                    },
					callback: {
                        message: 'The Projects details is not valid',
                        callback: function(value, validator, $field) {
							var res = value.charAt(0);
							if (value === '') {
                                return true;
                            }
							
                            if (value == ' ') {
                                return false;
                            }							
							
							if (res.search(/[A-Za-z]/) < 0) {
                                return {
                                    valid: false,
                                    message: 'Projects must starts with alphabet only'
                                }
                            }

                            return true;
                        }
					}
                }
            },
            experience: {
                validators: {
					regexp: {
                        regexp: /^[0-9]+$/i,
                        message: 'Your experience only number of months(e.g. 36)'
                    }
                }
            }
        }   
    }).on('status.field.bv', function(e, data) {
        data.bv.disableSubmitButtons(false);
    }).on('success.form.bv', function(e, data) {
		var $form = $(e.target);
		HTMLFormElement.prototype.submit.call($form[0]);
    });
	// End Process
});
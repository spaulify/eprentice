<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

function get_trainer_name($trainer_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = $trainer_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$trainer = $rs->getrows();
		return $trainer;
	}
}

function get_product($product_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "products` WHERE `Product_ID` = $product_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$product = $rs->getrows();
		return $product;
	}
}

function get_training($training_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_ID` = $training_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$training = $rs->getrows();
		return $training;
	}
}

function get_promotion_file($promotion_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "promotion_file` WHERE `Promotion_ID` = $promotion_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$promotion_file = $rs->getrows();
		return $promotion_file;
	}
}

function get_promotion_question($promotion_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Promotion_ID` = $promotion_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$promotion_questions = $rs->getrows();
		return $promotion_questions;
	}
}

function get_category_name($category_id) {
	global $conn;
	global $config;
	$query = "SELECT `Category_Name` FROM `" . $config['db_prefix'] . "category` WHERE `Category_ID` = $category_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$category = $rs->getrows();
		return $category;
	}
}

function get_ip_address() {
    $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                $ip = trim($ip);
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        }
    }

    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}

function validate_ip($ip) {
    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}

function dateDiff($ptime) {
	$etime = time() - $ptime;
	if ( $etime < 1 ) {
		return '0s';
	}

	$a = array(
		365 * 24 * 60 * 60  =>  ' years',
		 30 * 24 * 60 * 60  =>  ' months',
			  24 * 60 * 60  =>  ' days',
				   60 * 60  =>  ' hours',
						60  =>  ' minutes',
						 1  =>  ' seconds'
	);
	
	foreach ( $a as $secs => $str ) {
		$d = $etime / $secs;
		if ( $d >= 1 ) {
			$r = round($d);
			return $r . $str;
		}
	}
}

function check_payment($training_id, $trainer_id, $affiliator_id, $member_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "payments` WHERE `Training_ID` = $training_id AND `Trainer_ID` = $trainer_id AND `Affiliator_ID` = $affiliator_id AND `Member_ID` = $member_id LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$num = $rs->numrows();
	} else {
		$num = 0;
	}
	
	return $num;
}

function follow($Followee_ID, $Follower_ID) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "follow` WHERE `Followee_ID` = $Followee_ID AND `Follower_ID` = $Follower_ID LIMIT 0, 1";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num == 1 ) {
		return 'following';
	}
}

function remove_social_session() {
	// Unset Facebook Session
	unset($_SESSION['fb_login']);
	unset($_SESSION['fb_user']);
	//unset($_SESSION['fb_access_token']);
	
	// Unset Google Session	
	unset($_SESSION['gp_login']);
	unset($_SESSION['gp_user']);
	//unset($_SESSION['gp_access_token']);

	// Unset Twitter Session	
	unset($_SESSION['tw_login']);
	unset($_SESSION['tw_user']);
	//unset($_SESSION['tw_access_token']);
	//unset($_SESSION['tw_access_token_secret']);

	// Unset Linkedin Session	
	unset($_SESSION['li_login']);
	unset($_SESSION['li_user']);
	//unset($_SESSION['li_access_token']);
}

function insert_notification($User_ID, $Notification_Text, $Notification_Link, $Notification_Image) {
	global $conn;
	global $config;
	$conn->execute("INSERT INTO `" . $config['db_prefix'] . "notifications` (`User_ID`, `Notification_Text`, `Notification_Link`, `Notification_Image`, `Date_Time`) VALUES($User_ID, '$Notification_Text', '$Notification_Link', '$Notification_Image', '".date('Y-m-d H:i:s')."')");
}

function totalNotification($User_ID, $Status) {
	global $conn;
	global $config;
	$numquery = "SELECT * FROM `" . $config['db_prefix'] . "notifications` WHERE `User_ID` = $User_ID AND `Notification_Status` = '$Status'";
	$rs = $conn->execute($numquery);
	if( $rs ) {
		$totnum = $rs->numrows();
	} else {
		$totnum = 0;
	}
	return $totnum;
}

function active_page() {
	$segments = explode('/', $_SERVER['PHP_SELF']);
	$current_page = end($segments);
	return $current_page;
}

function modify_http($url) {
	$url = trim($url);	
	if ($url && strncasecmp($url,'http:', 5) != 0 && strncasecmp($url,'https:', 6) != 0) {
		$url = 'http://'.$url;
	}	
	return $url;
}

function get_training_status($training_id, $user_id) {
	global $conn;
	global $config;
	$numquery = "SELECT * FROM `" . $config['db_prefix'] . "ongoing_training` WHERE `Member_ID` = $user_id AND `Training_ID` = '$training_id' LIMIT 0, 1";
	$rs = $conn->execute($numquery);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$status = $rs->getrows();
		return $status;
	}
}

function get_training_favorite($training_id, $user_id) {
	global $conn;
	global $config;
	$numquery = "SELECT * FROM `" . $config['db_prefix'] . "favorite` WHERE `User_ID` = $user_id AND `Training_ID` = '$training_id' LIMIT 0, 1";
	$rs = $conn->execute($numquery);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$favorite_class = 'fa-heart';
	} else {
		$favorite_class = 'fa-heart-o';	
	}
	return $favorite_class;
}

function get_facebook_schedule($campaign_id, $trainer_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Campaign_ID` = $campaign_id AND `Trainer_ID` = $trainer_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$fbSchedule = $rs->getrows();
	} else {
		$fbSchedule = array();
	}
	return $fbSchedule;
}

function check_product($user_id, $product_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "payments` WHERE `Buyer_ID` = $user_id AND `Product_ID` = $product_id";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$status = 'Yes';
	} else {
		$status = 'No';
	}
	return $status;
}

function email_tracking($sender_id, $sender_email_address, $receiver_email_address, $email_product_id, $email_subject, $email_content, $email_product_url, $email_unique_id, $email_type, $email_status) {
	global $conn;
	global $config;
	$query = "INSERT INTO `" . $config['db_prefix'] . "email_tracking` (`Email_ID`, `Sender_ID`, `Sender_Email_Address`, `Receiver_Email_Address`, `Email_Product_ID`, `Email_Subject`, `Email_Content`, `Email_Product_URL`, `Email_Unique_TrackID`, `Email_Type`, `Email_Status`, `Email_Send_DateTime`) VALUES(NULL, $sender_id, '$sender_email_address', '$receiver_email_address', $email_product_id, '$email_subject', '$email_content', '$email_product_url', '$email_unique_id', '$email_type', '$email_status', '".date('Y-m-d')."')";
	$rs = $conn->execute($query);
	if ( $rs ) {
		return true;
	} else {
		return false;
	}
}

function get_video_info($videofile) {
	global $conn;
	global $config;
	$ffprobe_path  = $config['FFPROBE_PATH'];
	$ffmpeg_path   = $config['FFMPEG_PATH'];
	$ffprobe_cmd   = $ffprobe_path . " -v quiet -print_format json -show_format -show_streams " . $videofile . " 2>&1";
	ob_start();
	passthru($ffprobe_cmd);
	$ffmpeg_output = ob_get_contents();
	ob_end_clean();
	// if file not found just return null
	if(sizeof($ffmpeg_output) == null ) {
		return null;
	}
	$json = json_decode($ffmpeg_output,true);
	//Uncomment below if you want to debug the json output
	//echo "<pre>";
	//echo json_encode($json, JSON_PRETTY_PRINT);
	//echo "</pre>";
	/*$video_codec	= $json['streams'][0]['codec_name'];
	$width			= $json['streams'][0]['width'];
	$height			= $json['streams'][0]['height'];
	$r_frame_rate	= $json['streams'][0]['r_frame_rate'];
	$fr				= explode("/", $r_frame_rate);
	$r_frame_rate	= round(intval($fr[0])/intval($fr[1]),3);
	$video_bitrate	= round(intval($json['streams'][0]['bit_rate'])/1000, 0);*/
	$duration		= $json['streams'][0]['duration'];
	/*$audio_codec	= $json['streams'][1]['codec_name'];
	$sample_rate	= $json['streams'][1]['sample_rate'];
	$channels		= $json['streams'][1]['channels'];
	$bit_rate		= round($json['streams'][1]['bit_rate']/1000, 0);
	$resultset 		= array(
						'video_codec' 	=> $video_codec,
						'width' 		=> $width,
						'height' 		=> $height,
						'r_frame_rate'  => $r_frame_rate,
						'video_bitrate' => $video_bitrate,
						'duration'  	=> $duration,
						'audio_codec'  	=> $audio_codec,
						'sample_rate'  	=> $sample_rate,
						'channels'  	=> $channels,
						'bit_rate'  	=> $bit_rate
					);
	return $resultset;*/
	return $duration;
}

function get_email_tracking($email_unique_id) {
	global $conn;
	global $config;
	$query = "SELECT * FROM `" . $config['db_prefix'] . "email_tracking` WHERE `Email_Unique_TrackID` = '$email_unique_id' LIMIT 0, 1";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$emailInfo = $rs->getrows();
	} else {
		$emailInfo = array();
	}
	return $emailInfo;
}


function get_youtube_id_from_url($url) {
	if (stristr($url,'youtu.be/')) {
		preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); return $final_ID[4]; 
	} else {
		@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); return $IDD[5]; 
	}
}
<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-11-09 19:54:32
         compiled from "C:\xampp\htdocs\eprentice\templates\frontend\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:150895a0465205778a1-48144563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e82a6f722fc39a5c203c9513ef8d3d0b7c96f061' => 
    array (
      0 => 'C:\\xampp\\htdocs\\eprentice\\templates\\frontend\\header.tpl',
      1 => 1501489825,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '150895a0465205778a1-48144563',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'url' => 0,
    'base_url' => 0,
    'training' => 0,
    'asset_url' => 0,
    'page_title' => 0,
    'page_description' => 0,
    'Facebook_App_ID' => 0,
    'page_name' => 0,
    'ajax_url' => 0,
    'Stripe_Publication_Key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a046520800f10_23520305',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a046520800f10_23520305')) {function content_5a046520800f10_23520305($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
    <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable(explode("/",$_SERVER['REQUEST_URI']), null, 0);?>
    <?php if (in_array('training',$_smarty_tpl->tpl_vars['url']->value)) {?>
		<meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/uploads/trp/<?php echo $_smarty_tpl->tpl_vars['training']->value['Training_Thumb'];?>
" />
        <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['training']->value['Training_Name'];?>
" />
        <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['training']->value['Training_Desc'];?>
" />
        <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/training/<?php echo $_smarty_tpl->tpl_vars['training']->value['Training_URL'];?>
/" />
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/images/LightBlueLogo.png" />
        <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
" />
        <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['page_description']->value;?>
" />
        <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
" />
    <?php }?>
    <meta property="fb:app_id" content="<?php echo $_smarty_tpl->tpl_vars['Facebook_App_ID']->value;?>
" />
    <meta property="og:site_name" content="ePrentice" />
    <meta property="og:type" content="Website" />
    
    <title><?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
</title>

	<link href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/favicon.ico" rel="icon" />
	<link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/jquery.switchButton.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/nanoscroller.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/jquery.wizard.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/jquery.tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/fa-social.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/flowplayer-6.0.5/skin/functional.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/cropper.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/app.css" rel="stylesheet" type="text/css" />
    
    <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='dashboard.php') {?>
        <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/custom.css" rel="stylesheet" />
        <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/icheck/flat/green.css" rel="stylesheet" />
        <link href="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/css/floatexamples.css" rel="stylesheet" type="text/css" />
    <?php }?>
    
    <?php echo '<script'; ?>
 type="text/javascript">
	var base_url = '<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
'; var ajax_url = '<?php echo $_smarty_tpl->tpl_vars['ajax_url']->value;?>
'; var asset_url = '<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
'; var Stripe_Publication_Key = '<?php echo $_smarty_tpl->tpl_vars['Stripe_Publication_Key']->value;?>
';
   	<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery-2.2.1.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/nprogress.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery-ui.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery.tagsinput.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery.nanoscroller.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery.wizard.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/jquery.steps.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/ePrenticeApp.js"><?php echo '</script'; ?>
>

	<!--[if lt IE 9]>
    	<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['asset_url']->value;?>
/js/ie8-responsive-file-warning.js"><?php echo '</script'; ?>
>
    <![endif]-->
    
	<!--[if lt IE 9]>
		<?php echo '<script'; ?>
 type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
	<![endif]-->
</head>
<body <?php if ($_smarty_tpl->tpl_vars['page_name']->value=='dashboard.php') {?>class="nav-md"<?php }?>><?php }} ?>

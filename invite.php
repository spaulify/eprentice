<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$queryUser = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = $UID LIMIT 0, 1";
$rs = $conn->execute($queryUser);
if ( $rs ) {
	$user = $rs->getrows();
} else {
	$user = array();
}
$smarty->assign('user', $user[0]);

$success = '';$error = '';
if ( isset($_POST['share_submit']) ) {
	$share_url 	 = addslashes($_POST['share_url']);
	$share_email = addslashes($_POST['share_email']);
	$emails = explode(',', $share_email);
	foreach ( $emails as $email ) {
		if( $email == "" || $email == false ) {
			$error = "Email Address field cannot be blank!";
		} elseif( filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {
			$error = "Invalid Email Address inserted!";
		} else {
			$message = SMEmail::Invite_Friends($email, $share_url);
			$subject = 'Come Join ePrentice!';
			if(	eprenticeMail($email, '', $subject, $message) == "true" ) {
				$success = 'Invitation has been successfully shared by email.';	
			}
		}
	}
}

$smarty->assign('success', 			$success);
$smarty->assign('error', 			$error);
$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('invite.tpl');
$smarty->display('footer.tpl');
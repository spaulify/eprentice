<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

class SMEmail {

    public static function Verify_Email($email, $name, $link) {
		global $config;
        $body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" />
		<title>' . $config['site_name'] . '</title>
		</head>
		<body>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;line-height:24px;margin:0;padding:0;width:100%;font-size:17px;color:#373737;background:#f9f9f9">
			<tbody>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="bottom" style="border-collapse:collapse;padding:20px 16px 12px">
										<div style="text-align:center">
											<a target="_blank" style="color:#439fe0;font-weight:bold;text-decoration:none;word-break:break-word" href="#">
												<img style="outline:none;text-decoration:none;border:none;" src="' . $config['ASSET_URL'] . '/frontend/images/EmailHeader.png" alt="ePrentice" />
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="32" border="0" align="center" style="border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem">
							<tbody>
								<tr>
									<td width="100%" valign="top" style="border-collapse:collapse">
										<div style="max-width:600px;margin:0 auto">
											<div style="background:white;border-radius:0.5rem;margin-bottom:1rem">
												<h2 style="color:#1B75BC;line-height:30px;margin-bottom:12px;margin:0 0 12px">Hello ' . $name . '!</h2>
												<p style="font-size:18px;line-height:24px;margin:0 0 16px">Thank you for registering with ePrentice. You are almost done to create your training with ePrentice. Just click on Verify Email button below to verify your email address:</p>
												<div style="text-align:center;margin:2rem 0 1rem">
													<a target="_blank" align="center" style="color:white;font-weight:normal;text-decoration:none;word-break:break-word;display:inline-block;background:#1B75BC;border-bottom:2px solid #0c62a9;border-radius:4px;padding:14px 32px;letter-spacing:1px;font-size:20px;line-height:26px" href="' . $link . '">Verify Email</a>
												</div>
												<p style="font-size:12px;line-height:20px;margin:0 auto 1rem;color:#aaa;text-align:center;max-width:100%;word-break:break-word;margin-bottom:2rem">Or, copy and paste this link: <a style="color:#439fe0;font-weight:normal;text-decoration:none;word-break:break-word">' . $link . '</a></p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">If you\'re having trouble or need some help, <strong>reply to this email and we\'ll respond</strong>.</p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">Cheers,<br>The team ePrentice</p>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		</body>
		</html>';
		return $body;
    }
	
	public static function Password_Email($email, $name, $password, $login_link) {
		global $config;
        $body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" />
		<title>' . $config['site_name'] . '</title>
		</head>
		<body>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;line-height:24px;margin:0;padding:0;width:100%;font-size:17px;color:#373737;background:#f9f9f9">
			<tbody>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="bottom" style="border-collapse:collapse;padding:20px 16px 12px">
										<div style="text-align:center">
											<a target="_blank" style="color:#439fe0;font-weight:bold;text-decoration:none;word-break:break-word" href="#">
												<img style="outline:none;text-decoration:none;border:none;" src="' . $config['ASSET_URL'] . '/frontend/images/EmailHeader.png" alt="ePrentice" />
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="32" border="0" align="center" style="border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem">
							<tbody>
								<tr>
									<td width="100%" valign="top" style="border-collapse:collapse">
										<div style="max-width:600px;margin:0 auto">
											<div style="background:white;border-radius:0.5rem;margin-bottom:1rem">
												<h2 style="color:#1B75BC;line-height:30px;margin-bottom:12px;margin:0 0 12px">Hello ' . $name . '!</h2>
												<p style="font-size:18px;line-height:24px;margin:0 0 16px">Here is your ePrentice account login details:</p>
												<p style="font-size:12px;line-height:18px;margin:0 0 5px">Email: ' . $email . '</p>
												<p style="font-size:12px;line-height:18px;margin:0 0 5px">Password: ' . $password . '</p>
												<div style="text-align:center;margin:2rem 0 1rem">
													<a target="_blank" align="center" style="color:white;font-weight:normal;text-decoration:none;word-break:break-word;display:inline-block;background:#1B75BC;border-bottom:2px solid #0c62a9;border-radius:4px;padding:14px 32px;letter-spacing:1px;font-size:20px;line-height:26px" href="' . $login_link . '">Login</a>
												</div>
												<p style="font-size:12px;line-height:20px;margin:0 auto 1rem;color:#aaa;text-align:center;max-width:100%;word-break:break-word;margin-bottom:2rem">Or, copy and paste this link: <a style="color:#439fe0;font-weight:normal;text-decoration:none;word-break:break-word">' . $login_link . '</a></p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">If you\'re having trouble or need some help, <strong>reply to this email and we\'ll respond</strong>.</p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">Cheers,<br>The team ePrentice</p>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		</body>
		</html>';
		return $body;
    }
	
	public static function Share_Training_Email($email, $training_link) {
		global $config;
        $body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" />
		<title>' . $config['site_name'] . '</title>
		</head>
		<body>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;line-height:24px;margin:0;padding:0;width:100%;font-size:17px;color:#373737;background:#f9f9f9">
			<tbody>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="bottom" style="border-collapse:collapse;padding:20px 16px 12px">
										<div style="text-align:center">
											<a target="_blank" style="color:#439fe0;font-weight:bold;text-decoration:none;word-break:break-word" href="#">
												<img style="outline:none;text-decoration:none;border:none;" src="' . $config['ASSET_URL'] . '/frontend/images/EmailHeader.png" alt="ePrentice" />
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="32" border="0" align="center" style="border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem">
							<tbody>
								<tr>
									<td width="100%" valign="top" style="border-collapse:collapse">
										<div style="max-width:600px;margin:0 auto">
											<div style="background:white;border-radius:0.5rem;margin-bottom:1rem">
												<h2 style="color:#1B75BC;line-height:30px;margin-bottom:12px;margin:0 0 12px">Hello!</h2>
												<p style="font-size:18px;line-height:24px;margin:0 0 16px">This is a Training link shared from ePrentice. Click to watch the training:</p>
												<div style="text-align:center;margin:2rem 0 1rem">
													<a target="_blank" align="center" style="color:white;font-weight:normal;text-decoration:none;word-break:break-word;display:inline-block;background:#1B75BC;border-bottom:2px solid #0c62a9;border-radius:4px;padding:14px 32px;letter-spacing:1px;font-size:20px;line-height:26px" href="' . $training_link . '">Watch Now!</a>
												</div>
												<p style="font-size:12px;line-height:20px;margin:0 auto 1rem;color:#aaa;text-align:center;max-width:100%;word-break:break-word;margin-bottom:2rem">Or, copy and paste this link: <a style="color:#439fe0;font-weight:normal;text-decoration:none;word-break:break-word">' . $training_link . '</a></p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">If you\'re having trouble or need some help, <strong>reply to this email and we\'ll respond</strong>.</p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">Cheers,<br>The team ePrentice</p>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		</body>
		</html>';
		return $body;
    }
	
	public static function Invite_Friends($email, $share_url) {
		global $config;
        $body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" />
		<title>' . $config['site_name'] . '</title>
		</head>
		<body>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;line-height:24px;margin:0;padding:0;width:100%;font-size:17px;color:#373737;background:#f9f9f9">
			<tbody>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse">
							<tbody>
								<tr>
									<td valign="bottom" style="border-collapse:collapse;padding:20px 16px 12px">
										<div style="text-align:center">
											<a target="_blank" style="color:#439fe0;font-weight:bold;text-decoration:none;word-break:break-word" href="#">
												<img style="outline:none;text-decoration:none;border:none;" src="' . $config['ASSET_URL'] . '/frontend/images/EmailHeader.png" alt="ePrentice" />
											</a>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" style="border-collapse:collapse">
						<table cellspacing="0" cellpadding="32" border="0" align="center" style="border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem">
							<tbody>
								<tr>
									<td width="100%" valign="top" style="border-collapse:collapse">
										<div style="max-width:600px;margin:0 auto">
											<div style="background:white;border-radius:0.5rem;margin-bottom:1rem">
												<h2 style="color:#1B75BC;line-height:30px;margin-bottom:12px;margin:0 0 12px">Hello!</h2>
												<p style="font-size:18px;line-height:24px;margin:0 0 16px">This is an invitation for join with us in ePrentice. Click to join now with ePrentice:</p>
												<div style="text-align:center;margin:2rem 0 1rem">
													<a target="_blank" align="center" style="color:white;font-weight:normal;text-decoration:none;word-break:break-word;display:inline-block;background:#1B75BC;border-bottom:2px solid #0c62a9;border-radius:4px;padding:14px 32px;letter-spacing:1px;font-size:20px;line-height:26px" href="' . $share_url . '">Join Now!</a>
												</div>
												<p style="font-size:12px;line-height:20px;margin:0 auto 1rem;color:#aaa;text-align:center;max-width:100%;word-break:break-word;margin-bottom:2rem">Or, copy and paste this link: <a style="color:#439fe0;font-weight:normal;text-decoration:none;word-break:break-word">' . $share_url . '</a></p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">If you\'re having trouble or need some help, <strong>reply to this email and we\'ll respond</strong>.</p>
												<p style="font-size:17px;line-height:24px;margin:0 0 16px">Cheers,<br>The team ePrentice</p>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		</body>
		</html>';
		return $body;
    }
	
	public static function Payment_Invoice($email, $fname, $lname, $product, $amount, $date, $transaction, $product_link, $login_link) {
		global $config;
        $body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="format-detection" content="telephone=no" />
		<title>' . $config['site_name'] . '</title>
		</head>
		<body>
		<table cellspacing="0" cellpadding="0" border="0" width="600px" style="border-collapse:collapse;margin:0 auto;color:#3b4859;border:1px solid #ddd;font-family:Tahoma, Geneva, sans-serif;font-size:14px;line-height:1.35;">
		<tbody>
			<tr>
				<td valign="top" style="border-collapse:collapse;">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;border-bottom:1px solid #ddd;">
						<tbody>
							<tr>
								<td width="80%" valign="top" style="border-collapse:collapse">
									<a style="color:#41a5e1;text-decoration:none;" href="#"><img src="' . $config['ASSET_URL'] . '/frontend/images/EmailHeader.png" alt="ePrentice" width="250" /></a>
								</td>
								<td width="20%" valign="middle" align="right" style="border-collapse:collapse;font-size:22px"><strong>INVOICE</strong></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;border-bottom:1px solid #ddd;">
						<tbody>
							<tr>
								<td width="100%" valign="top" style="border-collapse:collapse" colspan="2">
									<p>Dear ' . $fname . ',</p>
									<p>Your payment for your online order placed on <a style="color:#41a5e1;text-decoration:none;" href="#">www.eprentice.com</a> on ' . $date . ' has been approved (order reference number: <strong>' . $transaction . '</strong>). Please note that "eprentice.com" will appear on your card statement, instead of ePrentice. To get further payment support for your purchase, please signup using your email address to ePrentice at <a style="color:#41a5e1;text-decoration:none;" href="' . $login_link . '">' . $login_link . '</a>.</p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;border-bottom:1px solid #ddd;line-height:1.5;">
						<tbody>
							<tr>
								<td width="50%" valign="top" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<p style="margin:0">Invoice Send To</p>
									<p style="margin:0"><strong>' . $fname . ' ' . $lname . '</strong></p>
									<p style="margin:0"><a style="color:#41a5e1;text-decoration:none;" href="mailto:' . $email . '">' . $email . '</a></p>
								</td>
								<td width="50%" valign="top" style="border-collapse:collapse">
									<p style="margin:0">Invoice Send From</p>
									<p style="margin:0"><strong>ePrentice LLC</strong></p>
									<p style="margin:0"><a style="color:#41a5e1;text-decoration:none;" href="mailto:jerry@eprentice.com">jerry@eprentice.com</a></p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;border-bottom:1px solid #ddd;line-height:1.5;">
						<tbody>
							<tr>
								<td width="33.33%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<p style="margin:0">Transaction No</p>
									<p style="margin:0"><strong>' . $transaction . '</strong></p>
								</td>
								<td width="33.33%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<p style="margin:0">Transaction Date</p>
									<p style="margin:0"><strong>' . date('F j, Y', strtotime($date)) . '</strong></p>
								</td>
								<td width="33.33%" valign="top" align="center" style="border-collapse:collapse">
									<p style="margin:0">Transaction Total</p>
									<p style="margin:0"><strong>$' . $amount . '.00</strong></p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;background-color:#67bffe;color:#fff;">
						<thead>
							<tr>
								<th width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #fff;">
									<strong>Description</strong>
								</th>
								<th width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #fff;">
									<strong>Price</strong>
								</th>
								<th width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #fff;">
									<strong>Quantity</strong>
								</th>
								<th width="25%" valign="top" align="center" style="border-collapse:collapse;">
									<strong>Price</strong>
								</th>
							</tr>
						</thead>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;background-color:#eff2f7;border-bottom:1px solid #ddd;">
						<tbody>
							<tr>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<strong>' . $product . '</strong>
								</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<strong>$' . $amount . '.00</strong>
								</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									<strong>1</strong>
								</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;">
									<strong>$' . $amount . '.00</strong>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;background-color:#eff2f7;">
						<tbody>
							<tr>
								<td width="50%" colspan="2" style="border-collapse:collapse;border-right:1px solid #ddd;">&nbsp;</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #ddd;">
									Sub Total
								</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;">
									<strong>$' . $amount . '.00</strong>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;background-color:#eff2f7;">
						<tbody>
							<tr>
								<td width="50%" colspan="2" style="border-collapse:collapse;border-right:1px solid #fff;">&nbsp;</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;border-right:1px solid #fff;background-color:#67bffe;color:#fff;">
									<strong>Total</strong>
								</td>
								<td width="25%" valign="top" align="center" style="border-collapse:collapse;background-color:#67bffe;color:#fff;">
									<strong>$' . $amount . '.00</strong>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="top" style="border-collapse:collapse">
					<table cellspacing="0" width="100%" cellpadding="15" border="0" align="center" style="border-collapse:collapse;border-bottom:1px solid #ddd;font-size:16px;line-height:1.5;">
						<tbody>
							<tr>
								<td width="100%" valign="top" align="center" style="border-collapse:collapse" colspan="2">
									<p style="margin-bottom:20px;">Payments should be made within 30 days with one of the options below, or you can enter any note here if necessary.</p>
									<p style="margin:0;"><strong>Payment Methods:</strong> Cheque, Paypal, WesternUnion, BACS</p>
									<p style="margin:0;"><strong>We accept:</strong> MasterCard, Visa, AmericanExpress</p>
									<p style="margin:0;"><strong>Paypal Email Address:</strong> <a style="color:#41a5e1;text-decoration:none;" href="#">eprenticepaypal@eprentice.com</a></p><br>
									<p style="margin:0;"><a style="color:#41a5e1;text-decoration:none;" target="_blank" href="' . $config['BASE_URL'] . '">' . $config['BASE_URL'] . '</a></p>
									<p style="margin:0;"><a style="color:#41a5e1;text-decoration:none;" href="mailto:sales@eprentice.com">sales@eprentice.com</a></p>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	</body>
	</html>';
		return $body;
    }

}
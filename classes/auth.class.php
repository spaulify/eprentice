<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

class SMAuth {

    public function not_loggedin() {
        global $config;

        $access = false;
        if (isset($_SESSION['UID']) && isset($_SESSION['UNAME']) && isset($_SESSION['USER']) && isset($_SESSION['UEMAIL']) && isset($_SESSION['UROLE'])) {
			if (!empty($_SESSION['UID']) && !empty($_SESSION['UNAME']) && !empty($_SESSION['USER']) && !empty($_SESSION['UEMAIL']) && !empty($_SESSION['UROLE'])) {
	            $access = true;
			}
        }

        if (!$access) {
            SMRedirect::go($config['BASE_URL'] . '/');
			return false;
        }
    }
	
	public function loggedin() {
        global $config;

        $access = false;
        if (isset($_SESSION['UID']) && isset($_SESSION['UNAME']) && isset($_SESSION['USER']) && isset($_SESSION['UEMAIL']) && isset($_SESSION['UROLE'])) {
			if (!empty($_SESSION['UID']) && !empty($_SESSION['UNAME']) && !empty($_SESSION['USER']) && !empty($_SESSION['UEMAIL']) && !empty($_SESSION['UROLE'])) {
	            $access = true;
			}
        }

        if ($access) {
			if ( $_SESSION['UROLE'] == 'trainer' ) {
	            SMRedirect::go($config['BASE_URL'] . '/dashboard/');
			} else {
				SMRedirect::go($config['BASE_URL'] . '/news/');
			}
			return false;
        }
    }
	
	public function get_user($user_id) {
		global $config, $conn;
		
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = $user_id LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) 
			return $rs->getrows();
	}

	public function not_loggedin_admin() {
        global $config;

        $access = false;
        if (isset($_SESSION['ADID']) && isset($_SESSION['ADNAME']) && isset($_SESSION['ADUSER']) && isset($_SESSION['ADEMAIL'])) {
			if (!empty($_SESSION['ADID']) && !empty($_SESSION['ADNAME']) && !empty($_SESSION['ADUSER']) && !empty($_SESSION['ADEMAIL'])) {
	            $access = true;
			}
        }

        if (!$access) {
            SMRedirect::go($config['ADMIN_URL'] . '/');
			return false;
        }
    }

    public function loggedin_admin() {
        global $config;

        $access = false;
        if (isset($_SESSION['ADID']) && isset($_SESSION['ADNAME']) && isset($_SESSION['ADUSER']) && isset($_SESSION['ADEMAIL'])) {
			if (!empty($_SESSION['ADID']) && !empty($_SESSION['ADNAME']) && !empty($_SESSION['ADUSER']) && !empty($_SESSION['ADEMAIL'])) {
	            $access = true;
			}
        }

        if ($access) {
			SMRedirect::go($config['ADMIN_URL'] . '/dashboard/');
			return false;
        }
    }

}
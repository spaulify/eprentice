<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

class SMRedirect {

    public static function go($url) {
        session_write_close();
        if (headers_sent()) {
            echo "<script>document.location.href='" . $url . "';</script>\n";
        } else {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $url);
        }
        die();
    }

}

?>
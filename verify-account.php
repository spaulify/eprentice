<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

$message = '';
if ( isset($_GET['param']) && $_GET['param'] != 'not-verified' ) {
	$param = explode('-', $_GET['param']);
	$code = $param[0];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Verify_Code` = '$code' LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs )
		$num = $rs->numrows();
	else
		$num = 0;
	if ( $num == 1 ) {
		$user = $rs->getrows();
		if ( $user[0]['User_Role'] == 'viewer' ) {
			if ( isset($param[1]) ) {
				$Training_ID = $param[1];
				$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_ID` = $Training_ID LIMIT 0, 1";
				$rs = $conn->execute($query);
				if ( $rs ) {
					$training = $rs->getrows();
				} else {
					$training = array();
				}
			}
		}
		if ( $user[0]['Profile_Verify'] == 'no' ) {
			$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Profile_Verify` = 'yes' WHERE `Verify_Code` = '$code'";
			$rs = $conn->execute($query);
			if ( $rs ) {
				if ( $user[0]['User_Role'] == 'viewer' ) {
					if ( isset($param[1]) ) {
						$login_link = $config['BASE_URL'] . '/login/?return=' . $config['BASE_URL'] . '/training/' . $training[0]['Training_URL'] . '/';
					} else {
						$login_link = $config['BASE_URL'] . '/login/';
					}
					
					$message = SMEmail::Password_Email($user[0]['Email_ID'], $user[0]['Full_Name'], $user[0]['Password'], $login_link);
					$subject = 'Your ePrentice account details';
					if(eprenticeMail($user[0]['Email_ID'], $user[0]['Full_Name'], $subject, $message)) {
						$password = sha1($user[0]['Password']);
						$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Password` = '$password' WHERE `Email_ID` = '" . $user[0]['Email_ID'] . "'";
						$rs = $conn->execute($query);
						if ( $user[0]['User_Role'] == 'viewer' ) {
							if ( isset($param[1]) ) {
								$message = 'Your email address has been successfully verified. Go to your email to get your login details. <br />Click to <a href="' . $config['BASE_URL'] . '/login/?return=' . $config['BASE_URL'] . '/training/' . $training[0]['Training_URL'] . '/">login</a> here.';
							} else {
								$message = 'Your email address has been successfully verified. Go to your email to get your login details. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
							}
						} else {
							$message = 'Your email address has been successfully verified. Go to your email to get your login details. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
						}
					}
				} else {
					if ( $user[0]['User_Role'] == 'viewer' ) {
						if ( isset($param[1]) ) {
							$message = 'Your email address has been successfully verified. <br />Click to <a href="' . $config['BASE_URL'] . '/login/?return=' . $config['BASE_URL'] . '/training/' . $training[0]['Training_URL'] . '/">login</a> here.';
						} else {
							$message = 'Your email address has been successfully verified. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
						}
					} else {
						$message = 'Your email address has been successfully verified. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
					}
				}
			}
		} else {
			if ( $user[0]['User_Role'] == 'viewer' ) {
				if ( isset($param[1]) ) {
					$message = 'You have already verified your email address. <br />Click to <a href="' . $config['BASE_URL'] . '/login/?return=' . $config['BASE_URL'] . '/training/' . $training[0]['Training_URL'] . '/">login</a> here.';
				} else {
					$message = 'You have already verified your email address. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
				}
			} else {
				$message = 'You have already verified your email address. <br />Click to <a href="' . $config['BASE_URL'] . '/login/">login</a> here.';
			}
		}
	} else {
		$message = 'This is not a valid verification link. <br />Click to go to the <a href="' . $config['BASE_URL'] . '/">home page</a>.';
	}	
} elseif ( isset($_GET['param']) && $_GET['param'] == 'not-verified' ) {
	$message = 'A verification link has been sent to your email. Please verify your email id to visit this link. <br /><small>Did not receive email? Don\'t worry, <a href="#">send it</a> again.';
}
$smarty->assign('message', $message);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('verify-account.tpl');
$smarty->display('footer.tpl');
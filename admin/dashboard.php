<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin_admin();

$ADID    = isset($_SESSION['ADID'])   ? $_SESSION['ADID']   : '';
$ADNAME  = isset($_SESSION['ADNAME']) ? $_SESSION['ADNAME'] : '';
$ADUSER  = isset($_SESSION['ADUSER'])  ? $_SESSION['ADUSER']  : '';
$ADEMAIL = isset($_SESSION['ADEMAIL']) ? $_SESSION['ADEMAIL'] : '';

/* Total Products Count */
$productQuery = "SELECT * FROM `" . $config['db_prefix'] . "products`";
$productrs = $conn->execute($productQuery);
if ( $productrs ) {
	$products = $productrs->numrows();
} else {
	$products = 0;
}
$smarty->assign('products', $products);
/* Total Products Count */

/* Total Trainings Count */
$trainingQuery = "SELECT * FROM `" . $config['db_prefix'] . "trainings`";
$trainingrs = $conn->execute($trainingQuery);
if ( $trainingrs ) {
	$trainings = $trainingrs->numrows();
} else {
	$trainings = 0;
}
$smarty->assign('trainings', $trainings);
/* Total Trainings Count */

/* Total Promotions Count */
$promotionQuery = "SELECT * FROM `" . $config['db_prefix'] . "promotions`";
$promotionrs = $conn->execute($promotionQuery);
if ( $promotionrs ) {
	$promotions = $promotionrs->numrows();
} else {
	$promotions = 0;
}
$smarty->assign('promotions', $promotions);
/* Total Promotions Count */

/* Email Click Rate */
$EmailClickArray = array();
$emailtrackSQL = "SELECT `Email_Send_Date` FROM `" . $config['db_prefix'] . "email_tracking` GROUP BY `Email_Send_Date` ORDER BY `Email_Send_Date` DESC LIMIT 0, 5";
$emailtrackRs  = $conn->execute($emailtrackSQL);
if( $emailtrackRs ) {
	$emailtrackRow   = $emailtrackRs->numrows();
	$emailtrackData = $emailtrackRs->getrows();
	for($i=0;$i<count($emailtrackData);$i++) {
		$emailcountSQL = "SELECT * FROM `" . $config['db_prefix'] . "email_tracking` WHERE `Email_Send_Date` = '".$emailtrackData[$i]['Email_Send_Date']."'";
		$emailcountRs  = $conn->execute($emailcountSQL);
		if( $emailcountRs ) {
			$emailcountData  = $emailcountRs->getrows();
			$totalEmailCount = count($emailcountData);
		}
		$emailreturnSQL = "SELECT * FROM `" . $config['db_prefix'] . "email_tracking` WHERE `Email_Send_Date` = '".$emailtrackData[$i]['Email_Send_Date']."' AND `Email_Status` = 'Return' AND `Email_Link_Status` = 'Deactive'";
		$emailreturnRs  = $conn->execute($emailreturnSQL);
		if( $emailreturnRs ) {
			$emailreturnData  = $emailreturnRs->getrows();
			$returnEmailCount = count($emailreturnData);
			$returnEmailPercent	= round((($returnEmailCount * 100) / $totalEmailCount), 0);
		}
		$EmailClickArray[$emailtrackData[$i]['Email_Send_Date']] = $returnEmailPercent;
	}
} else {
	$emailtrackRow   = 0;
	$emailtrackData = array();
}
$smarty->assign('EmailClickArray', 	$EmailClickArray);
/* Email Click Rate */

/* Impressions vs. Video Views vs. Sales - graph */
$impressions = array(); $videos = array(); $payments = array();
$Imp_Data = ''; $Vid_Data = ''; $Pmt_Data = ''; $count = 0;
$end_date = date('Y-m-d');
$date = date('Y-m-d', strtotime("-6 day", strtotime($end_date)));
while (strtotime($date) <= strtotime($end_date)) {
	$impSQL = "SELECT COUNT(*) AS `Graph_Count`, CAST(`Created_On` AS DATE) AS `Graph_Date` FROM `" . $config['db_prefix'] . "impressions` WHERE CAST(`Created_On` AS DATE) = '$date' GROUP BY CAST(`Created_On` AS DATE) LIMIT 0, 7";
	$imgRs = $conn->execute($impSQL);
	if ( $imgRs ) {
		$ImpRows = $imgRs->numrows();
	} else {
		$ImpRows = 0;
	}
	if ( $ImpRows > 0 ) {
		$impressions[$count] = $imgRs->getrows();
	} else {
		$impressions[$count][0]['Graph_Count'] = 0;
		$impressions[$count][0]['Graph_Date'] = $date;
	}
	
	$vidSQL = "SELECT COUNT(*) AS `Graph_Count`, CAST(`Last_Date_Time` AS DATE) AS `Graph_Date` FROM `" . $config['db_prefix'] . "ongoing_training` AND CAST(`Last_Date_Time` AS DATE) = '$date' GROUP BY CAST(`Last_Date_Time` AS DATE) LIMIT 0, 7";
	$vidRs = $conn->execute($vidSQL);
	if ( $vidRs ) {
		$VidRows = $vidRs->numrows();
	} else {
		$VidRows = 0;
	}
	if ( $VidRows > 0 ) {
		$videos[$count] = $vidRs->getrows();
	} else {
		$videos[$count][0]['Graph_Count'] = 0;
		$videos[$count][0]['Graph_Date'] = $date;
	}
	
	$pmtSQL = "SELECT COUNT(*) AS `Graph_Count`, CAST(`Purchased_ON` AS DATE) AS `Graph_Date` FROM `" . $config['db_prefix'] . "payments` WHERE CAST(`Purchased_ON` AS DATE) = '$date' GROUP BY CAST(`Purchased_ON` AS DATE) LIMIT 0, 7";
	$pmtRs = $conn->execute($pmtSQL);
	if ( $pmtRs ) {
		$PmtRows = $pmtRs->numrows();
	} else {
		$PmtRows = 0;
	}
	if ( $PmtRows > 0 ) {
		$payments[$count] = $pmtRs->getrows();
	} else {
		$payments[$count][0]['Graph_Count'] = 0;
		$payments[$count][0]['Graph_Date'] = $date;
	}
	$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	$count++;
}
for ( $i = 0; $i < $count; $i++ ) {
	$Imp_Data .= '[gd(' . date('Y, m, d', strtotime($impressions[$i][0]['Graph_Date'])) . '), ' . $impressions[$i][0]['Graph_Count'] . '],';
	$Vid_Data .= '[gd(' . date('Y, m, d', strtotime($videos[$i][0]['Graph_Date'])) . '), ' . $videos[$i][0]['Graph_Count'] . '],';
	$Pmt_Data .= '[gd(' . date('Y, m, d', strtotime($payments[$i][0]['Graph_Date'])) . '), ' . $payments[$i][0]['Graph_Count'] . '],';
}
$smarty->assign('Imp_Data', $Imp_Data);
$smarty->assign('Vid_Data', $Vid_Data);
$smarty->assign('Pmt_Data', $Pmt_Data);
/* Impressions vs. Video Views vs. Sales - graph */

/* Share Users Section */
$categorySQL = "SELECT * FROM `" . $config['db_prefix'] . "trainings`";
$categoryRs = $conn->execute($categorySQL);
if ( $categoryRs ) {
	$categories = $categoryRs->getrows();
} else {
	$categories = array();
}
$cataArray   		= array();
$cataDetails 		= array();
$categoryName		= array();
$categoryPercent 	= array();
$color 				= array();
for($c=0;$c<count($categories);$c++) {
	$cataArray[] = $categories[$c]['Training_Category'];
}
$totalcountSQL = "SELECT * FROM `" . $config['db_prefix'] . "ongoing_training`";
$totalcountRs  = $conn->execute($totalcountSQL);
$totalCount    = $totalcountRs->numrows();
for($c=0;$c<count($cataArray);$c++) {
	$ongoingSQL = "SELECT COUNT(`Member_ID`) AS `Member_Count` FROM `" . $config['db_prefix'] . "ongoing_training` AND `Training_CataID` = " . $cataArray[$c] . " LIMIT 0, 5";
	$ongoingRs 	= $conn->execute($ongoingSQL);
	if( $ongoingRs ) {
		$ongoingData = $ongoingRs->getrows();
		$color = array('blue', 'green', 'purple', 'aero', 'red');
		foreach($ongoingData as $key => $value) {
			$cataName = get_category_name($cataArray[$c]);
			$category 	   	= $cataName[0]['Category_Name'];
			$categoryCount  = $value['Member_Count'];
			$percentCount 	= round((($categoryCount * 100) / ($totalCount==0?1:$totalCount)), 0);
			$cataDetails[$category] = $percentCount;
		}
	}
}
arsort($cataDetails);	
foreach($cataDetails as $x => $x_value) {
	"Key=" . $x . ", Value=" . $x_value;
	$categoryName[] 	= '"' . $x . '"';
	$categoryPercent[] 	= $x_value;
}
$cata_name	  = implode(',', $categoryName);
$cata_perc	  = implode(', ', $categoryPercent);

$smarty->assign('cata_name', 	$cata_name);
$smarty->assign('cata_perc', 	$cata_perc);
$smarty->assign('totalCount', 	$totalCount);
$smarty->assign('cataDetails', 	$cataDetails);
$smarty->assign('categoryName', 	$categoryName);
$smarty->assign('categoryPercent', 	$categoryPercent);
$smarty->assign('color', 	$color);
/* Share Users Section */

/* Top Campaign Performance */
$emCount = 0;
$fbCount = 0;
$twCount = 0;
$liCount = 0;
$gpCount = 0;
$topcampSQL = "SELECT * FROM `" . $config['db_prefix'] . "traffic_campaign`";
$topcampRs 	= $conn->execute($topcampSQL);
if( $topcampRs ) {
	$topcampCount = $topcampRs->numrows();
	$topcampData  = $topcampRs->getrows();
} else {
	$topcampCount = 0;
	$topcampData  = array();
}
for($c=0;$c<count($topcampData);$c++) {
	if( $topcampData[$c]['Traffic_Source'] == 'Email' ) {
		$emCount = $emCount + 1;
	}
	if( $topcampData[$c]['Traffic_Source'] == 'Facebook' ) {
		$fbCount = $fbCount + 1;
	}
	if( $topcampData[$c]['Traffic_Source'] == 'Twitter' ) {
		$twCount = $twCount + 1;
	}
	if( $topcampData[$c]['Traffic_Source'] == 'Linkedin' ) {
		$liCount = $liCount + 1;
	}
	if( $topcampData[$c]['Traffic_Source'] == 'Google' ) {
		$gpCount = $gpCount + 1;
	}
}
$emPerc = round((($emCount * 100) / ($topcampCount ? $topcampCount : 1)), 0);
$fbPerc = round((($fbCount * 100) / ($topcampCount ? $topcampCount : 1)), 0);
$twPerc = round((($twCount * 100) / ($topcampCount ? $topcampCount : 1)), 0);
$liPerc = round((($liCount * 100) / ($topcampCount ? $topcampCount : 1)), 0);
$gpPerc = round((($gpCount * 100) / ($topcampCount ? $topcampCount : 1)), 0);

$smarty->assign('topcampCount', $topcampCount);
$smarty->assign('emPerc', 		$emPerc);
$smarty->assign('fbPerc', 		$fbPerc);
$smarty->assign('twPerc', 		$twPerc);
$smarty->assign('liPerc', 		$liPerc);
$smarty->assign('gpPerc', 		$gpPerc);
/* Top Campaign Performance */

/* Total Timing's Average Per Training */
$TotalTime		= 0;
$SeekTime 		= 0;
$TotalTraining  = 0;
$SeekAVGTiming	= 0;
$trainingVideoViewQuery = "SELECT * FROM `" . $config['db_prefix'] . "ongoing_training` AS `OT`, `" . $config['db_prefix'] . "trainings` AS `T` WHERE `OT`.`Training_ID` = `T`.`Training_ID`";
$trainingVideoRs = $conn->execute($trainingVideoViewQuery);
if ( $trainingVideoRs ) {
	$totalview 		= $trainingVideoRs->numrows();
	$ongoing 		= $trainingVideoRs->getrows();
	for($i=0;$i<count($ongoing);$i++) {			
		$videofilename 	 = $ongoing[$i]['Training_Video'];
		$videofilepath	 = $config['UPLOAD_DIR'] . '/trp/' . $videofilename;
		$TrainingTiming  = round(get_video_info($videofilepath), 0);
		$TotalTime 		+= $TrainingTiming;
		$SeekTime 		+= $ongoing[$i]['Last_Seek_Time'];
	}
	$SeekAVGTiming	= round(($SeekTime / ($totalview ? $totalview : 1)), 0);
	$TotalAVGTiming = round(($TotalTime / ($totalview ? $totalview : 1)), 0);
	if( $SeekAVGTiming >= 60 ) {
		$SeekAVGTime = round(($SeekAVGTiming / 60), 0) . ' <small>Min</small>';
	} elseif( $SeekAVGTiming >= 3600 ) {
		$SeekAVGTime = round(($SeekAVGTiming / (60 * 60)), 0) . ' <small>Hr</small>';
	} else {
		$SeekAVGTime = $SeekAVGTiming . ' <small>Sec</sub>';
	}
	if( $TotalAVGTiming >= 60 ) {
		$TotalAVGTime = round(($TotalAVGTiming / 60), 0) . ' <small>Min</small>';
	} elseif( $TotalAVGTiming >= 3600 ) {
		$TotalAVGTime = round(($TotalAVGTiming / (60 * 60)), 0) . ' <small>Hr</small>';
	} else {
		$TotalAVGTime = $TotalAVGTiming . ' <small>Sec</small>';
	}		
}
$trainingViewQuery =  "SELECT * FROM `" . $config['db_prefix'] . "hits` AS `H`, `" . $config['db_prefix'] . "trainings` AS `T` WHERE `H`.`Training_ID` = `T`.`Training_ID`";
$trainingViewRs = $conn->execute($trainingViewQuery);
if ( $trainingViewRs ) {
	$trainingView = $trainingViewRs->numrows();
} else {
	$trainingView = 0;
}
$totalTraining = $trainingView + $totalview;
$smarty->assign('SeekAVGTiming', 	$SeekAVGTiming);
$smarty->assign('TotalAVGTiming', 	$TotalAVGTiming);
$smarty->assign('trainingVideo', 	$totalview);
$smarty->assign('totalTraining', 	$totalTraining);
$smarty->assign('SeekAVGTime', 		$SeekAVGTime);
$smarty->assign('TotalAVGTime', 	$TotalAVGTime);
/* Total Timing's Average Per Training */

/* Email Tracking Functionality */
$EmailData  = array();
$ETSQL 		= "SELECT * FROM `" . $config['db_prefix'] . "email_tracking` ORDER BY `Email_ID` DESC LIMIT 0, 5";
$ETRs = $conn->execute($ETSQL);
if ( $ETRs ) {
	$EmailData	= $ETRs->getrows();
}
/* Email Tracking Functionality */

/* Last Product Purchase Functionality */
$LPSQL = "SELECT * FROM `" . $config['db_prefix'] . "payments` ORDER BY `Payment_ID` DESC LIMIT 0, 5";
$LPRs = $conn->execute($LPSQL);
if ( $LPRs ) {
	$LPData	= $LPRs->getrows();
}
/* Last Product Purchase Functionality */

/* Top Viewed Training Functionality */
$TVTSQL = "SELECT *, COUNT(*) AS `Total` FROM `" . $config['db_prefix'] . "hits` GROUP BY `Training_ID` ORDER BY `Total` DESC LIMIT 0, 7";
$TVTRs = $conn->execute($TVTSQL);
if ( $TVTRs ) {
	$TVTRow		= $TVTRs->numrows();
	if( $TVTRow > 0 ) {
		$TVTData	= $TVTRs->getrows();
	} else {
		$TVTData	= array();
	}
}
$smarty->assign('TVTData', $TVTData);
/* Top Viewed Training Functionality */

/* Today's Stats Functionality */
$TDTraSQL1 	= "SELECT * FROM `" . $config['db_prefix'] . "hits` WHERE `Visited_On` LIKE '".date('Y-m-d')."%'";
$TDTraRs1  	= $conn->execute($TDTraSQL1);
$TDTraRow1 	= $TDTraRs1->numrows();
$TDTraSQL2 	= "SELECT * FROM `" . $config['db_prefix'] . "ongoing_training` AS `OT`, `" . $config['db_prefix'] . "trainings` AS `T` WHERE `OT`.`Training_ID` = `T`.`Training_ID` AND `OT`.`Last_Date_Time` LIKE '".date('Y-m-d')."%'";
$TDTraRs2  	= $conn->execute($TDTraSQL2);
$TDTraRow2 	= $TDTraRs2->numrows();
$TodayTra  	= $TDTraRow1 + $TDTraRow2;
$smarty->assign('TodayTra', $TodayTra);
$TDPaySQL 	= "SELECT * FROM `" . $config['db_prefix'] . "payments` WHERE `Purchased_On` LIKE '".date('Y-m-d')."%'";
$TDPayRs	= $conn->execute($TDPaySQL);
$TDPayRow	= $TDPayRs->numrows();
$TDTotalAmount	= 0;
if( $TDPayRow > 0 ) {
	$TDPayData		= $TDPayRs->getrows();
	for($i=0;$i<count($TDPayData);$i++) {
		$TDAmount 	= $TDPayData[$i]['Purchase_Amount'];
		$TDTotalAmount += $TDAmount;
	}
} else {
	$TDTotalAmount = 0;
}
$smarty->assign('TDTotalAmount', $TDTotalAmount);
$smarty->assign('TDPayRow', $TDPayRow);
$TDEmailSQL	= "SELECT * FROM `" . $config['db_prefix'] . "email_tracking` WHERE `Email_Send_Date` = '".date('Y-m-d')."'";
$TDEmailRs 	= $conn->execute($TDEmailSQL);
$TDEmailRow	= $TDEmailRs->numrows();
$smarty->assign('TDEmailRow', $TDEmailRow);
/* Today's Stats Functionality */

/* Location Tracking Functionality */
$LocData = array();
$record  = array();
$IPSQL = "SELECT `IP_Address` FROM `" . $config['db_prefix'] . "hits`";
$IPRs = $conn->execute($IPSQL);
if ( $IPRs ) {
	$IPData	= $IPRs->getrows();
	foreach ( $IPData as $IP ) {
		$ip_address = $IP['IP_Address'];
		$record[] = geoip_record_by_addr($giCity, $ip_address);
	}
}	
$Countries = ''; $CountryCode = array(); $CountryList = array();
foreach ( $record as $key => $Loc ) {
	$CountryCode[$Loc->country_code][] = $Loc->country_name;
	$CountryCode[$Loc->country_code]['visit'] = count(array_filter(array_keys($CountryCode[$Loc->country_code]), 'is_numeric'));
}
$i = 0;
foreach ( $CountryCode as $key => $value ) {
	$Countries .= '"' . $key . '"' . ':' . $value['visit'] . ',';
	$CountryList[$i]['country'] = $value[0];
	$CountryList[$i]['visit'] = $value['visit'];
	$i++;
}
/* Location Tracking Functionality */

$smarty->assign('EmailData', 		$EmailData);
$smarty->assign('LPData', 			$LPData);
$smarty->assign('CountryList',		$CountryList);
$smarty->assign('Countries', 		$Countries);
$smarty->assign('Admin_ID', 		$ADID);
$smarty->assign('Admin_Name',   	$ADNAME);
$smarty->assign('Admin_Username',	$ADUSER);
$smarty->assign('Admin_Email',	    $ADEMAIL);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('dashboard.tpl');
$smarty->display('footer.tpl');
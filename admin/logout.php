<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';

unset($_SESSION['ADID']);
unset($_SESSION['ADNAME']);
unset($_SESSION['ADUSER']);
unset($_SESSION['ADEMAIL']);

remove_social_session();

session_write_close();
header('Location:' . $config['ADMIN_URL'] . '/');
die();
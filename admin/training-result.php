<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin_admin();

$ADID    = isset($_SESSION['ADID'])   ? $_SESSION['ADID']   : '';
$ADNAME  = isset($_SESSION['ADNAME']) ? $_SESSION['ADNAME'] : '';
$ADUSER  = isset($_SESSION['ADUSER'])  ? $_SESSION['ADUSER']  : '';
$ADEMAIL = isset($_SESSION['ADEMAIL']) ? $_SESSION['ADEMAIL'] : '';

$message = '';
if( isset($_POST['update_training']) ) {
	$training_id 	 = $_POST['training_id'];
	$training_status = $_POST['training_status'];
	$query = "UPDATE `" . $config['db_prefix'] . "trainings` SET `Training_Status` = '$training_status' WHERE `Training_ID` = $training_id";
	$rs = $conn->execute($query);
	if ( $rs ) {
		if( $training_status == 'Published' ) {
			$message = 'Training Published Successfully!';
		} else {
			$message = 'Training Unpublished Successfully!';	
		}
		SMRedirect::go($config['ADMIN_URL'] . '/training-programs/');
	}
}
$smarty->assign('message', $message);

$results = array();
$numbers = 0;
$search  = '';
if( isset($_POST['search_training']) ) {
	$search = addslashes($_POST['training_name']);
	$query  = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_Name` LIKE '%".$search."%'";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$numbers = $rs->numrows();
		$results = $rs->getrows();
	}
}
$smarty->assign('search',  $search);
$smarty->assign('numbers', $numbers);
$smarty->assign('results', $results);

$smarty->assign('Admin_ID', 		$ADID);
$smarty->assign('Admin_Name',   	$ADNAME);
$smarty->assign('Admin_Username',	$ADUSER);
$smarty->assign('Admin_Email',	    $ADEMAIL);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('training-result.tpl');
$smarty->display('footer.tpl');
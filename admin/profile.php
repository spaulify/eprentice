<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin_admin();

$ADID    = isset($_SESSION['ADID'])   ? $_SESSION['ADID']   : '';
$ADNAME  = isset($_SESSION['ADNAME']) ? $_SESSION['ADNAME'] : '';
$ADUSER  = isset($_SESSION['ADUSER'])  ? $_SESSION['ADUSER']  : '';
$ADEMAIL = isset($_SESSION['ADEMAIL']) ? $_SESSION['ADEMAIL'] : '';

$User_Name = $_GET['param'];
$UserQuery = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Username` = '$User_Name' LIMIT 0, 1";
$UserRS    = $conn->execute($UserQuery);
if ( $UserRS )
	$UserNum = $UserRS->numrows();
else
	$UserNum = 0;
if ( $UserNum == 1 ) {
	$Users = $UserRS->getrows();
	$User_ID = $Users[0]['User_ID'];
} else {
	$User_ID = $UID;
}
$user = $auth->get_user($User_ID);

$smarty->assign('user', 		  	$user[0]);
$smarty->assign('Admin_ID', 		$ADID);
$smarty->assign('Admin_Name',   	$ADNAME);
$smarty->assign('Admin_Username',	$ADUSER);
$smarty->assign('Admin_Email',	    $ADEMAIL);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('profile.tpl');
$smarty->display('footer.tpl');
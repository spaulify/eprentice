<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin_admin();

$ADID    = isset($_SESSION['ADID'])   ? $_SESSION['ADID']   : '';
$ADNAME  = isset($_SESSION['ADNAME']) ? $_SESSION['ADNAME'] : '';
$ADUSER  = isset($_SESSION['ADUSER'])  ? $_SESSION['ADUSER']  : '';
$ADEMAIL = isset($_SESSION['ADEMAIL']) ? $_SESSION['ADEMAIL'] : '';

$results = array();
$numbers = 0;
$search  = '';
if( isset($_POST['search_viewer']) ) {
	$search = addslashes($_POST['viewer_name']);
	$query  = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE (`First_Name` LIKE '%".$search."%' AND `User_Role` = 'viewer') OR (`Last_Name` LIKE '%".$search."%' AND `User_Role` = 'viewer')";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$numbers = $rs->numrows();
		$results = $rs->getrows();
	}
}
$smarty->assign('search',  $search);
$smarty->assign('numbers', $numbers);
$smarty->assign('results', $results);

$smarty->assign('Admin_ID', 		$ADID);
$smarty->assign('Admin_Name',   	$ADNAME);
$smarty->assign('Admin_Username',	$ADUSER);
$smarty->assign('Admin_Email',	    $ADEMAIL);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('member-result.tpl');
$smarty->display('footer.tpl');
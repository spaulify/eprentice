<?php 
define('_SMARTY_STARTED', true);
define('_ADMIN_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin_admin();

$username = '';
$error = '';
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $username = addslashes($_POST['username']);
    $password = addslashes($_POST['password']);
	
    if ( !empty($username) && !empty($password) ) {
		$sha1_password = sha1($password);
		$query = "SELECT * FROM `" . $config['db_prefix'] . "admin` WHERE `Admin_Username` = '$username' AND `Admin_Password` = '$sha1_password' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num == 1 ) {
			$user = $rs->getrows();
			if (isset($_POST['remember'])) {
				$year = time() + 365*86400;
				setcookie('remember_username', $username, $year);
				setcookie('remember_password', $password, $year);
			} elseif (!isset($_POST['remember'])) {
				if (isset($_COOKIE['remember_username']) && isset($_COOKIE['remember_password']) ) {
					$past = time() - 100;
					setcookie('remember_username', '', $past);
					setcookie('remember_password', '', $past);
				}
			}
			$_SESSION['ADID'] 	 = $user[0]['Admin_ID'];
			$_SESSION['ADNAME']  = $user[0]['Admin_Name'];
			$_SESSION['ADUSER']  = $user[0]['Admin_Username'];
			$_SESSION['ADEMAIL'] = $user[0]['Admin_Email'];
			if ( isset($_GET['return']) ) {
				SMRedirect::go($_GET['return']);
			} else {
				SMRedirect::go($config['ADMIN_URL'] . '/dashboard/');
			}
		} else {
			$error = 'You have entered an invalid username or password!';
		}
    } else {
		$error = 'Please enter your username and password!';
	}
}

if ( isset($_COOKIE['remember_username']) && isset($_COOKIE['remember_password']) ) {
	$remember_username = $_COOKIE['remember_username'];
	$remember_password = $_COOKIE['remember_password'];
} else {
	$remember_username = '';
	$remember_password = '';
}

$smarty->assign('remember_username', $remember_username);
$smarty->assign('remember_password', $remember_password);
$smarty->assign('username', $username);
$smarty->assign('error', $error);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('index.tpl');
$smarty->display('footer.tpl');

remove_social_session();
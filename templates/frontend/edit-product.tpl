<div id="page-content-wrapper">
	<div class="create-product-page">
        <div class="col-sm-12">
        	<h1>Edit Product</h1>
            <p class="subtitle">&nbsp;</p>
            <div class="row">
                <form method="post" action="{$ajax_url}/edit-product/" enctype="multipart/form-data" id="product_form" class="col-md-10 col-md-offset-1">
                    <div id="product_msg"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" name="page_type" id="page_type" value="Update" />
                                <label for="product_name">Product Name: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_name_error"></span>
                                <input class="form-control" type="text" id="product_name" name="product_name" value="{$product.Product_Name}" placeholder="Enter your product title." />
                            </div>
                            <div class="form-group">
                                <label for="product_link">Product Link: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product link."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_link_error"></span>
                                <input class="form-control" type="text" id="product_link" name="product_link" value="{$product.Product_Link}" placeholder="Enter your product link." />
                            </div>
                            <div class="form-group">
                                <label for="product_image">Product Image: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_image_error"></span>
                                <input class="form-control" type="file" id="product_image" name="product_image" accept="image/*" style="height:auto;" />
                                <input type="hidden" name="old_product_image" value="{$product.Product_Image}" />
                            </div>
                            <div class="form-group">
                                <label for="product_price">Product Price: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product price."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_price_error"></span><br />
                                <i class="fa fa-dollar symbol-dollar"></i>
                                <input class="form-control" type="text" id="product_price" name="product_price" value="{$product.Product_Price}" placeholder="Enter your product price." />
                            </div>
                        </div>
                        <div class="col-sm-6">                            
                            <div class="form-group">
                            	<label for="product_refund">Product Refund Policy URL: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your Refund Policy URL."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_refund_error"></span>
                                <input class="form-control" type="text" id="product_refund" name="product_refund" value="{$product.Product_Refund_URL}" placeholder="Enter your Refund Policy URL." />
                            </div>
                            <div class="form-group">
                                <label for="product_desc">Product Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_desc_error"></span>
                                <textarea class="form-control" id="product_desc" name="product_desc" rows="8" style="height:151px;" placeholder="Enter your product description within 500 characters max.">{$product.Product_Desc}</textarea>
                                <p class="help-block">
                                    <strong class="pull-right"><span id="product_desc_length"></span></strong>
                                </p>                    
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label for="product_submit">&nbsp;</label>
                            <input type="hidden" name="product_id" id="product_id" value="{$product.Product_ID}" />
                            <button class="btn btn-primary" id="product_submit" name="product_submit">Submit</button>
                        </div>
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
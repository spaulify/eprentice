<div id="page-content-wrapper">
	<div class="downline-members-page">
        <div class="col-sm-12">
            <h1>Following Trainers</h1>
            <div class="row">
                {if $trainers}
                    {section name=i loop=$trainers}
                    	{assign var=singleTrainer value=get_trainer_name($trainers[i].Followee_ID)}
                        {section name=n loop=$singleTrainer}                                            
                    		<div class="col-sm-3">
	                        <div class="member-rows">
                            	<a href="{$base_url}/profile/{$singleTrainer[n].Username}/">
                                	<img src="{$singleTrainer[n].Profile_Photo}" alt="{$singleTrainer[n].Full_Name}" class="img-circle img-responsive" height="150" width="150" />
                                </a>
                                <div class="caption">
                                	<h3>{$singleTrainer[n].Full_Name}</h3>
                                    <p>{$singleTrainer[n].Bio|truncate:120}</p>
                                    {if $singleTrainer[n].Facebook_URL || $singleTrainer[n].Google_URL || $singleTrainer[n].Twitter_URL || $singleTrainer[n].LinkedIn_URL}
                                        <p class="social-icons icon-circle">
                                            {if $singleTrainer[n].Facebook_URL}
                                                <a href="{$singleTrainer[n].Facebook_URL}" target="_blank"><i class="fa fa-facebook"></i></a> 
                                            {/if}
                                            {if $singleTrainer[n].Twitter_URL}
                                                <a href="{$singleTrainer[n].Twitter_URL}" target="_blank"><i class="fa fa-twitter"></i></a> 
                                            {/if}
                                            {if $singleTrainer[n].Google_URL}
                                                <a href="{$singleTrainer[n].Google_URL}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                                            {/if}
                                            {if $singleTrainer[n].LinkedIn_URL}
                                                <a href="{$singleTrainer[n].LinkedIn_URL}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                        {/section}
                    {/section}
                {else}
                	<div class="col-sm-12">
                        <p class="no-items">You have not following any trainers.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
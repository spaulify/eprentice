<div id="page-content-wrapper">
	<div class="create-product-page">
        <div class="col-sm-12">
        	<h1>Create Product</h1>
            <p class="subtitle">Just fill the below form to create your new product.</p>
            <div class="row">
                <form method="post" action="{$ajax_url}/new-product/" enctype="multipart/form-data" id="product_form" class="col-md-10 col-md-offset-1">
                    <div id="product_msg"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" name="page_type" id="page_type" value="Insert" />
                                <label for="product_name">Product Name: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_name_error"></span>
                                <input class="form-control" type="text" id="product_name" name="product_name" placeholder="Enter your product title." />
                            </div>
                            <div class="form-group">
                                <label for="product_link">Product Link: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product link."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_link_error"></span>
                                <input class="form-control" type="text" id="product_link" name="product_link" placeholder="Enter your product link." />
                            </div>
                            <div class="form-group">
                                <label for="product_image">Product Image: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_image_error"></span>
                                <input class="form-control" type="file" id="product_image" name="product_image" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group">
                                <label for="product_price">Product Price: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product price."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_price_error"></span><br />
                                <i class="fa fa-dollar symbol-dollar"></i>
                                <input class="form-control" type="text" id="product_price" name="product_price" placeholder="Enter your product price." />
                            </div>                
                        </div>
                        <div class="col-sm-6">                            
                            <div class="form-group">
                            	<label for="product_refund">Product Refund Policy URL: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your Refund Policy URL."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_refund_error"></span>
                                <input class="form-control" type="text" id="product_refund" name="product_refund" placeholder="Enter your Refund Policy URL." />
                            </div>
                            <div class="form-group">
                                <label for="product_desc">Product Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your product description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="product_desc_error"></span>
                                <textarea class="form-control" id="product_desc" name="product_desc" rows="8" placeholder="Enter your product description within 500 characters max."></textarea>
                                <p class="help-block">                                    
                                    <strong class="pull-right"><span id="product_desc_length"></span></strong>
                                </p>                    
                            </div>                                                       
                        </div>
                        <div class="col-sm-12"><hr /></div>
                        <div class="col-sm-12">
                            <button class="btn" id="product_submit" name="product_submit">Submit</button>
                        </div>
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="page-content-wrapper">
	<div class="social-campaign-page">
        <div class="col-sm-12">
            <h1>Social Media Campaign</h1>
            <div class="row">
            	<div class="col-sm-6">
					<div class="panel panel-default">
                    	<div class="panel-heading">
                        	<h3 class="panel-title">
                                <strong>Facebook Campaign</strong>
                                {if !empty($fbCampaign)}                                	
                                	<a href="#" class="btn-success btn-xs pull-right add_fb_campaign"><i class="fa fa-plus"></i> New Campaign</a>
                                {/if}
                            </h3>
                        </div>
                        <div class="panel-body">
                        	{if empty($fbCampaign)}
                                <div class="text-center">
                                    <a href="#" class="btn-custom add_fb_campaign"><i class="fa fa-plus"></i> New Campaign</a>
                                </div>
                            {else}
                                <table id="fb_campaign_list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center col-xs-5">Page Name</th>
                                            <th class="text-center col-xs-4">Added On</th>
                                            <th class="text-center col-xs-3">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	{section name=i loop=$fbCampaign}
                                            <tr>
                                                <td style="color:#0000FF;" class="col-xs-5">
                                                	<a href="#" class="fb_campage_id" id="{$fbCampaign[i].FB_CampPage_ID}" title="{$fbCampaign[i].FB_Campaign_Page_Name}">{$fbCampaign[i].FB_Campaign_Page_Name|truncate:35:'...':true}</a>
                                                </td>                                                
                                                <td class="text-center" class="col-xs-4">{$fbCampaign[i].Added_On|date_format:"%d %b, %Y at %H:%M"}</td>
                                                <td class="text-center" class="col-xs-3">
                                                	{assign var=fbSchedule value=get_facebook_schedule($fbCampaign[i].FB_CampPage_ID, $fbCampaign[i].Trainer_ID)}
                                                    {if !empty($fbSchedule)}
                                                    	{section name=n loop=$fbSchedule}
                                                    		<a href="#" class="btn-warning btn-xs fb_edit_schedule" id="{$fbSchedule[n].FB_Schedule_ID}" rel="{$fbCampaign[i].FB_CampPage_ID}">Edit</a>
                                                            <a href="#" class="btn-danger btn-xs fb_erase_schedule" id="{$fbSchedule[n].FB_Schedule_ID}" rel="{$fbCampaign[i].FB_CampPage_ID}">Delete</a>
                                                        {/section}
                                                    {else}
                                                    	<a href="#" class="btn-success btn-xs fb_add_schedule" rel="{$fbCampaign[i].FB_CampPage_ID}">Schedule</a>
                                                    {/if}
                                                </td>
                                            </tr>
                                            <div class="backdropFixed">
                                            	<div class="modal fade" id="fb_campaign_schedule_lists_{$fbCampaign[i].FB_CampPage_ID}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" style="width:740px;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title text-center">{$fbCampaign[i].FB_Campaign_Page_Name}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                {assign var=fbSchedule value=get_facebook_schedule($fbCampaign[i].FB_CampPage_ID, $fbCampaign[i].Trainer_ID)}
                                                                {if !empty($fbSchedule)}
                                                                    {section name=n loop=$fbSchedule}
                                                                        <div id="fb_status_msg_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                        <h3 class="text-center">{$fbSchedule[n].FB_Schedule_Title}</h3>
                                                                        <p>{$fbSchedule[n].FB_Schedule_Desc}</p>
                                                                        <div class="row">
                                                                            <div class="col-xs-6 text-center">
                                                                                <p><strong>Start Time: </strong>{$fbSchedule[n].FB_Schedule_Start|date_format:"%d %b, %Y at %H:%M"}</p>
                                                                            </div>
                                                                            <div class="col-xs-6 text-center">
                                                                                <p><strong>End Time: </strong>{$fbSchedule[n].FB_Schedule_End|date_format:"%d %b, %Y at %H:%M"}</p>
                                                                            </div>
                                                                            <!--<div class="col-xs-4">
                                                                                <p>
                                                                                    <strong>Status: </strong>
                                                                                    {if $fbSchedule[n].FB_Schedule_Status=='Active'}
                                                                                        <span class="text-success">{$fbSchedule[n].FB_Schedule_Status}</span>
                                                                                    {else}
                                                                                        <span class="text-danger">{$fbSchedule[n].FB_Schedule_Status}</span>
                                                                                    {/if}
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <p style="margin-top:-5px;">
                                                                                    {if $fbSchedule[n].FB_Schedule_Status=='Active'}
                                                                                        <input class="fb_campaign_status" id="fb_campaign_status_{$fbCampaign[i].FB_CampPage_ID}" value="Active" checked type="checkbox" />
                                                                                        <input type="hidden" name="fb_status_user_id" id="fb_status_user_id_{$fbCampaign[i].FB_CampPage_ID}" value="{$User_ID}" />
                                                                                        <input type="hidden" name="fb_status_schedule_id" id="fb_status_schedule_id_{$fbCampaign[i].FB_CampPage_ID}" value="{$fbSchedule[n].FB_Schedule_ID}" />
                                                                                    {elseif $fbSchedule[n].FB_Schedule_Status=='Deactive'}
                                                                                        <input class="fb_campaign_status" id="fb_campaign_status_{$fbCampaign[i].FB_CampPage_ID}" value="Deactive" type="checkbox" />
                                                                                        <input type="hidden" name="fb_status_user_id" id="fb_status_user_id_{$fbCampaign[i].FB_CampPage_ID}" value="{$User_ID}" />
                                                                                        <input type="hidden" name="fb_status_schedule_id" id="fb_status_schedule_id_{$fbCampaign[i].FB_CampPage_ID}" value="{$fbSchedule[n].FB_Schedule_ID}" />
                                                                                    {/if}
                                                                                </p>
                                                                            </div>-->
                                                                        </div>
                                                                    {/section}
                                                                {else}
                                                                    <h3 class="text-center">No Campaign Scheduled yet!</h3>
                                                                {/if}
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="backdropFixed">
                                            	<div class="modal fade" id="fb_campaign_schedule_edit_{$fbCampaign[i].FB_CampPage_ID}" data-rel="{$fbCampaign[i].FB_CampPage_ID}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Schedule Facebook Campaign</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    {assign var=fbSchedule value=get_facebook_schedule($fbCampaign[i].FB_CampPage_ID, $fbCampaign[i].Trainer_ID)}
                                                                    {if !empty($fbSchedule)}
                                                                        {section name=n loop=$fbSchedule}
                                                                            <div class="col-md-12">
                                                                                <div id="fb_schedule_msg_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_schedule_promo_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_schedule_title_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_schedule_start_date_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_schedule_end_date_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_schedule_desc_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_campaign_id_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                                <div id="fb_trainer_id_error_{$fbCampaign[i].FB_CampPage_ID}"></div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">                    
                                                                                    <label for="fb_schedule_promo">Select Promotion: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Promotion from the list for campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                                    <select class="form-control" id="fb_schedule_promo_{$fbCampaign[i].FB_CampPage_ID}" name="fb_schedule_promo">
                                                                                        <option {if $fbSchedule[n].FB_Schedule_Promotion_ID==''} selected="selected" {/if} value="">Select Promotions</option>
                                                                                        {section name=o loop=$promotions}
                                                                                            <option {if $fbSchedule[n].FB_Schedule_Promotion_ID==$promotions[o].Promotion_ID} selected="selected" {/if} value="{$promotions[o].Promotion_ID}">{$promotions[o].Promotion_Title}</option>
                                                                                        {/section}
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="fb_schedule_title_{$fbCampaign[i].FB_CampPage_ID}">Schedule Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter a suitable Title for the campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                                    <input class="form-control" type="text" id="fb_schedule_title_{$fbCampaign[i].FB_CampPage_ID}" name="fb_schedule_title" value="{$fbSchedule[n].FB_Schedule_Title}" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="fb_schedule_start_date_{$fbCampaign[i].FB_CampPage_ID}">Schedule Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select campaign start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                                    <div class="input-group date" id="fb_schedule_start_date_div_{$fbCampaign[i].FB_CampPage_ID}">
                                                                                        <input type="text" class="form-control" id="fb_schedule_start_date_{$fbCampaign[i].FB_CampPage_ID}" name="fb_schedule_start_date" value="{$fbSchedule[n].FB_Schedule_Start}" />
                                                                                        <span class="input-group-addon">
                                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="fb_schedule_end_date_{$fbCampaign[i].FB_CampPage_ID}">Schedule End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select campaign end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                                    <div class="input-group date" id="fb_schedule_end_date_div_{$fbCampaign[i].FB_CampPage_ID}">
                                                                                        <input type="text" class="form-control" id="fb_schedule_end_date_{$fbCampaign[i].FB_CampPage_ID}" name="fb_schedule_end_date" value="{$fbSchedule[n].FB_Schedule_End}" />
                                                                                        <span class="input-group-addon">
                                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="fb_schedule_desc_{$fbCampaign[i].FB_CampPage_ID}">Schedule Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter description for the campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                                    <textarea class="form-control" id="fb_schedule_desc_{$fbCampaign[i].FB_CampPage_ID}" name="fb_schedule_desc" rows="6">{$fbSchedule[n].FB_Schedule_Desc}</textarea>
                                                                                    <p class="help-block">
                                                                                        <strong class="pull-right"><span id="fb_schedule_desc_length_{$fbCampaign[i].FB_CampPage_ID}"></span></strong>
                                                                                    </p>                    
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="hidden" id="fb_campaign_id_{$fbCampaign[i].FB_CampPage_ID}" name="fb_campaign_id" value="" />
                                                                                <input type="hidden" id="fb_trainer_id_{$fbCampaign[i].FB_CampPage_ID}" name="fb_trainer_id" value="{$User_ID}" />
                                                                            </div>
                                                                        {/section}
                                                                    {/if}
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                <a href="#" class="btn btn-primary" id="fb_edit_schedule_{$fbCampaign[i].FB_CampPage_ID}">Save</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/section}
                                    </tbody>
                                </table>
                            {/if}
                        </div>
                    </div>
	            </div>
	            <div class="col-sm-6">
                	<div class="panel panel-default">
                    	<div class="panel-heading">
                        	<h3 class="panel-title">
                                <strong>Twitter Campaign</strong>
                                {if !empty($twSchedule)}
                                	<a href="#" class="btn-success btn-xs pull-right add_tw_campaign"><i class="fa fa-plus"></i> Schedule Tweet</a>
                                {/if}
                            </h3>
                        </div>
                        <div class="panel-body">
                        	{if empty($twSchedule)}
                                <div class="text-center">                            	
                                    <a href="#" class="btn-custom add_tw_campaign"><i class="fa fa-plus"></i> Schedule Tweet</a>                                
                                </div>
                            {else}
                            	<table id="tw_campaign_list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Tweets</th>
                                            <th class="text-center">Added On</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	{section name=i loop=$twSchedule}
                                            <tr>
                                                <td style="color:#0000FF;" class="col-xs-5">
                                                	<a href="#" class="tw_schedule_id" id="{$twSchedule[i].TW_Schedule_ID}" title="{$twSchedule[i].TW_Schedule_Desc}">{$twSchedule[i].TW_Schedule_Desc|truncate:30:'...':true}</a>
                                                </td>                                                
                                                <td class="text-center" class="col-xs-4">{$twSchedule[i].Created_On|date_format:"%d %b, %Y at %H:%M"}</td>
                                                <td class="text-center" class="col-xs-3">
                                                    <a href="#" class="btn-warning btn-xs tw_edit_schedule" id="{$twSchedule[i].TW_Schedule_ID}">Edit</a>
                                                    <a href="#" class="btn-danger btn-xs tw_erase_schedule" id="{$twSchedule[i].TW_Schedule_ID}">Delete</a>
                                                </td>
                                            </tr>
                                            <div class="backdropFixed">
                                            	<div class="modal fade" id="tw_schedule_lists_{$twSchedule[i].TW_Schedule_ID}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" style="width:740px;">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title text-center">Schedule Tweet on {$twSchedule[i].TW_Schedule_Start|date_format:"%d %b, %Y at %H:%M"}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div id="tw_status_msg_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                <p class="text-center">{$twSchedule[i].TW_Schedule_Desc}</p>
                                                                <div class="row">
                                                                    <div class="col-xs-6 text-center">
                                                                        <p><strong>Start Time: </strong>{$twSchedule[i].TW_Schedule_Start|date_format:"%d %b, %Y at %H:%M"}</p>
                                                                    </div>
                                                                    <div class="col-xs-6 text-center">
                                                                        <p><strong>End Time: </strong>{$twSchedule[i].TW_Schedule_End|date_format:"%d %b, %Y at %H:%M"}</p>
                                                                    </div>
                                                                    <!--<div class="col-xs-4">
                                                                        <p>
                                                                            <strong>Status: </strong>
                                                                            {if $twSchedule[i].TW_Schedule_Status=='Active'}
                                                                                <span class="text-success">{$twSchedule[i].TW_Schedule_Status}</span>
                                                                            {else}
                                                                                <span class="text-danger">{$twSchedule[i].TW_Schedule_Status}</span>
                                                                            {/if}
                                                                        </p>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        <p style="margin-top:-5px;">
                                                                            {if $twSchedule[i].TW_Schedule_Status=='Active'}
                                                                                <input class="tw_schedule_status" id="tw_schedule_status_{$twSchedule[i].TW_Schedule_ID}" value="Active" checked type="checkbox" />
                                                                                <input type="hidden" name="tw_status_user_id" id="tw_status_user_id_{$twSchedule[i].TW_Schedule_ID}" value="{$User_ID}" />
                                                                            {elseif $twSchedule[i].TW_Schedule_Status=='Deactive'}
                                                                                <input class="tw_schedule_status" id="tw_schedule_status_{$twSchedule[i].TW_Schedule_ID}" value="Deactive" type="checkbox" />
                                                                                <input type="hidden" name="tw_status_user_id" id="tw_status_user_id_{$twSchedule[i].TW_Schedule_ID}" value="{$User_ID}" />
                                                                            {/if}
                                                                        </p>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="backdropFixed">
                                            	<div class="modal fade" id="tw_campaign_schedule_edit_{$twSchedule[i].TW_Schedule_ID}" data-rel="{$twSchedule[i].TW_Schedule_ID}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Schedule Tweet for Campaign</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div id="tw_schedule_msg_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                        <div id="tw_schedule_start_date_error_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                        <div id="tw_schedule_end_date_error_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                        <div id="tw_schedule_desc_error_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                        <div id="tw_schedule_id_error_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                        <div id="tw_trainer_id_error_{$twSchedule[i].TW_Schedule_ID}"></div>
                                                                    </div>                                                                        
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="tw_schedule_desc_{$twSchedule[i].TW_Schedule_ID}">Tweet Content: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter a content for Tweet."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                            <textarea class="form-control" id="tw_schedule_desc_{$twSchedule[i].TW_Schedule_ID}" name="tw_schedule_desc" rows="6">{$twSchedule[i].TW_Schedule_Desc}</textarea>
                                                                            <p class="help-block">
                                                                                <strong class="pull-right"><span id="tw_schedule_desc_length_{$twSchedule[i].TW_Schedule_ID}"></span></strong>
                                                                            </p>                    
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="tw_schedule_start_date_{$twSchedule[i].TW_Schedule_ID}">Tweet Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select Tweet start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                            <div class="input-group date" id="tw_schedule_start_date_div_{$twSchedule[i].TW_Schedule_ID}">
                                                                                <input type="text" class="form-control" id="tw_schedule_start_date_{$twSchedule[i].TW_Schedule_ID}" name="tw_schedule_start_date" value="{$twSchedule[i].TW_Schedule_Start}" />
                                                                                <span class="input-group-addon">
                                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                        
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="tw_schedule_end_date_{$twSchedule[i].TW_Schedule_ID}">Tweet End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select Tweet end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                                                            <div class="input-group date" id="tw_schedule_end_date_div_{$twSchedule[i].TW_Schedule_ID}">
                                                                                <input type="text" class="form-control" id="tw_schedule_end_date_{$twSchedule[i].TW_Schedule_ID}" name="tw_schedule_end_date" value="{$twSchedule[i].TW_Schedule_End}" />
                                                                                <span class="input-group-addon">
                                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                                        
                                                                    <div class="form-group">
                                                                        <input type="hidden" id="tw_schedule_id_{$twSchedule[i].TW_Schedule_ID}" name="tw_schedule_id" value="" />
                                                                        <input type="hidden" id="tw_trainer_id_{$twSchedule[i].TW_Schedule_ID}" name="tw_trainer_id" value="{$User_ID}" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                <a href="#" class="btn btn-primary" id="tw_edit_schedule_{$twSchedule[i].TW_Schedule_ID}">Save</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/section}
                                    </tbody>
                                </table>
                            {/if}
                        </div>
                    </div>
	            </div>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div class="modal fade" id="fb_new_campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            {assign var=trainer value=get_trainer_name($User_ID)}
            {section name=n loop=$trainer}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Facebook Page List</h4>
                    </div>
                    <div class="modal-body">           
                        {if empty($trainer[n].FB_Access_Token)}
                            <a class="btn-custom btn-block btn-social btn-facebook text-center" href="{$fbLoginUrl}">
                                <span class="fa fa-facebook"></span> Login with Facebook
                            </a>
                        {else}
                            <p>List of Facebook Pages.</p>
                            {if empty($fbPages)}
                                <div class="form-group">
                                    <p>You don't have any facebook page yet!</p>
                                </div>
                            {else}
                                <div class="form-group">
                                    <div id="fb_campaign_msg"></div>
                                    <select class="form-control" id="fb_pages" name="fb_pages">
                                        {section name=i loop=$fbPages}
                                            <option id="{$fbPages[i]->name}" title="{$fbPages[i]->access_token}" value="{$fbPages[i]->id}">{$fbPages[i]->name}</option>
                                        {/section}
                                    </select>
                                </div>
                            {/if}
                        {/if}
                    </div>
                    {if !empty($trainer[n].FB_Access_Token) && !empty($fbPages)}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="#" class="btn btn-primary" id="fb_add_campaign">Add</a>
                        </div>
                    {/if}
                </div>
            {/section}
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div class="modal fade" id="fb_campaign_schedule_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="newFBCampForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Schedule Facebook Campaign</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 fb_error_block">
                                <div id="fb_schedule_msg"></div>
                                <div id="fb_schedule_promo_error"></div>
                                <div id="fb_schedule_title_error"></div>
                                <div id="fb_schedule_start_date_error"></div>
                                <div id="fb_schedule_end_date_error"></div>
                                <div id="fb_schedule_desc_error"></div>
                                <div id="fb_campaign_id_error"></div>
                                <div id="fb_trainer_id_error"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">                    
                                    <label for="fb_schedule_promo">Select Promotion: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Promotion from the list for campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                    <select class="form-control" id="fb_schedule_promo" name="fb_schedule_promo">
                                        <option value="">Select Promotions</option>
                                        {section name=i loop=$promotions}
                                            <option value="{$promotions[i].Promotion_ID}">{$promotions[i].Promotion_Title}</option>
                                        {/section}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fb_schedule_title">Schedule Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter a suitable Title for the campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                    <input class="form-control" type="text" id="fb_schedule_title" name="fb_schedule_title" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fb_schedule_start_date">Schedule Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select campaign start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                    <div class="input-group date" id="fb_schedule_start_date_div">
                                        <input type="text" class="form-control" name="fb_schedule_start_date" id="fb_schedule_start_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label for="fb_schedule_end_date">Schedule End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select campaign end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                    <div class="input-group date" id="fb_schedule_end_date_div">
                                        <input type="text" class="form-control" name="fb_schedule_end_date" id="fb_schedule_end_date" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="fb_schedule_desc">Schedule Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter description for the campaign."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                    <textarea class="form-control" id="fb_schedule_desc" name="fb_schedule_desc" rows="6"></textarea>
                                    <p class="help-block">
                                        <strong class="pull-right"><span id="fb_schedule_desc_length"></span></strong>
                                    </p>                    
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="fb_campaign_id" name="fb_campaign_id" value="" />
                                <input type="hidden" id="fb_trainer_id" name="fb_trainer_id" value="{$User_ID}" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="fb_save_schedule">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div class="modal fade" id="fb_erase_campaign_schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Removing Campaign Schedule</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="fb_erase_campaign_msg"></div>
                        <p>Are you sure want to do this?</p>
                        <p class="text-warning"><small>Are you sure want to remove this schedule of this campaign? After erase, it will not revert back.</small></p>
                        <input type="hidden" id="fb_erase_schedule_id" name="fb_erase_schedule_id" value="" />
                        <input type="hidden" id="fb_erase_campaign_id" name="fb_erase_campaign_id" value="" />
                        <input type="hidden" id="fb_erase_user_id" name="fb_erase_user_id" value="{$User_ID}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="#" class="btn btn-primary" id="fb_erase_schedule">Erase</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div class="modal fade" id="tw_new_campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="newTWCampForm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Schedule Tweet for Campaign</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                                    <div class="col-md-12 fb_error_block">
                                        <div id="tw_schedule_msg"></div>
                                        <div id="tw_schedule_desc_error"></div>
                                        <div id="tw_schedule_start_date_error"></div>
                                        <div id="tw_schedule_end_date_error"></div>
                                        <div id="tw_trainer_id_error"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="tw_schedule_desc">Tweet Content: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter a content for Tweet."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                            <textarea class="form-control" id="tw_schedule_desc" name="tw_schedule_desc" rows="6"></textarea>
                                            <p class="help-block">
                                                <strong class="pull-right"><span id="tw_schedule_desc_length"></span></strong>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tw_schedule_start_date">Tweet Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select Tweet start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                            <div class="input-group date" id="tw_schedule_start_date_div">
                                                <input type="text" class="form-control" name="tw_schedule_start_date" id="tw_schedule_start_date" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tw_schedule_end_date">Tweet End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select Tweet end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                            
                                            <div class="input-group date" id="tw_schedule_end_date_div">
                                                <input type="text" class="form-control" name="tw_schedule_end_date" id="tw_schedule_end_date" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" id="tw_trainer_id" name="tw_trainer_id" value="{$User_ID}" />
                                    </div>
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="tw_save_schedule">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div class="modal fade" id="tw_erase_schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Removing Scheduled Tweet</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="tw_erase_campaign_msg"></div>
                        <p>Are you sure want to do this?</p>
                        <p class="text-warning"><small>Are you sure want to remove this scheduled Tweet? After erase, it will not revert back.</small></p>
                        <input type="hidden" id="tw_erase_schedule_id" name="tw_erase_schedule_id" value="" />
                        <input type="hidden" id="tw_erase_user_id" name="tw_erase_user_id" value="{$User_ID}" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="#" class="btn btn-primary" id="tw_remove_schedule">Erase</a>
                </div>
            </div>
        </div>
    </div>
</div>


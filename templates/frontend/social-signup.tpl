<div class="wrapper">
	<form class="form-signin" method="post" action="" name="EprenticeRegisterSocial" id="ePrenticeRegisterSocial">
    	<div class="login-form">
            <h2 class="form-signin-heading">Sign Up to ePrentice</h2>
            {if $error}
                <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {$error}
                </div>
            {/if}
            <div class="form-group">
            	<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" value="{$fname}" />
            	<span id="fnameReg" title="{$fname}"></span>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" value="{$lname}" style="border-radius:0;" />
                <span id="lnameReg" title="{$lname}"></span>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{$email}" style="border-radius:0;" />
                <span id="emailReg" title="{$email}"></span>
            </div>            
            {if !$refer}
                <div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" name="role" id="role" {if $role!='viewer'} checked {/if} /> Join as a Trainer
                    </label>
                </div>
            {/if}
            <div class="form-group">
            	<button class="btn btn-lg btn-info btn-block" type="submit" name="submit" id="social_register">Register</button>
            </div>
        </div>
        <div class="register-now">
            <p>Already have an Account? <a class="btn btn-xs btn-success pull-right" href="{$base_url}/login/">Login</a></p>
        </div>
	</form>
</div>
<div id="page-content-wrapper">
	<div class="training-page">
    	<div class="col-sm-4">
            <div class="row">            	
                {assign var=trainer value=get_trainer_name($promotion.Trainer_ID)}
                {section name=n loop=$trainer}
                    <div class="profile-sec">
                    	<a href="{$base_url}/profile/{$trainer[n].Username}/">
	                        <img src="{$trainer[n].Profile_Photo}" class="img-circle" alt="{$trainer[n].Full_Name}" width="170" height="170" />
                        </a>
                        <h3>{$trainer[n].Full_Name}</h3>
                        <p class="small text-muted">{$trainer[n].Designation}</p>
                        <p>{$trainer[n].Bio}</p>
                        <p class="social-icons icon-circle">
                            {if !empty($trainer[n].Facebook_URL)}
                                <a href="{$trainer[n].Facebook_URL}" target="_blank"><i class="fa fa-facebook"></i></a>
                            {/if}
                            {if !empty($trainer[n].Twitter_URL)}
                                <a href="{$trainer[n].Twitter_URL}" target="_blank"><i class="fa fa-twitter"></i></a>
                            {/if}
                            {if !empty($trainer[n].Google_URL)}
                                <a href="{$trainer[n].Google_URL}" target="_blank"><i class="fa fa-google-plus"></i></a>
                            {/if}
                            {if !empty($trainer[n].LinkedIn_URL)}
                                <a href="{$trainer[n].LinkedIn_URL}" target="_blank"><i class="fa fa-linkedin"></i></a>
                            {/if}
                        </p>
                        {if $User_Role=='viewer'}
                        	{assign var=followdata value=follow($trainer[n].User_ID,$User_ID)}
                			{if $followdata=='following'}
                        		<p><a href="#" class="btn follow following followee-{$trainer[n].User_ID}">+ Unfollow</a></p>
                            {else}
                            	<p><a href="#" class="btn follow followee-{$trainer[n].User_ID}">+ Follow</a></p>
                            {/if}
                            <input type="hidden" name="Followee_ID" value="{$trainer[n].User_ID}" />
                            <input type="hidden" name="Follower_ID" value="{$User_ID}" />
                            <input type="hidden" name="User_Role" value="{$User_Role}" />
                            <div class="follow_msg"></div>
                        {/if}
                    </div>
                {/section}
            </div>
        </div>
        <div class="col-sm-8" style="border-left: 1px solid #ddd;">
            <div class="row">
                <div class="col-sm-12">
                	<div class="row">
                        {if $promotion.Promotion_Type=='Image'}
                            {assign var=promofile value=get_promotion_file($promotion.Promotion_ID)}
                            {if count($promofile)==1}
                                {section name=u loop=$promofile}
                                    <img src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}" class="img-responsive" alt="{$promofile[u].Promotion_Filename}" />
                                {/section}
                            {else}
                                <ul class="bxslider">
                                    {section name=u loop=$promofile}
                                        <li class="text-center">
                                            <img src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}" class="img-responsive" alt="{$promofile[u].Promotion_Filename}" width="100%" />
                                        </li>
                                    {/section}
                                </ul>
                            {/if}
                            <div class="clearfix"></div>
                        {elseif  $promotion.Promotion_Type=='Video'}
                        	{assign var=promofile value=get_promotion_file($promotion.Promotion_ID)}
                            {if count($promofile)==1}
                                {section name=u loop=$promofile}
                                    <div class="flowplayer is-poster" style="background:#F7F7F7 url({$base_url}/uploads/prm/{$promotion.Promotion_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56">
                                        <video>
                                            <source type="video/webm"  src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                            <source type="video/mp4"   src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                            <source type="video/flash" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                            <source type="video/ogg" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                            <source type="video/quicktime" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                        </video>
                                    </div>
                                {/section}
                            {else}
                                <ul class="bxslider">
                                    {section name=u loop=$promofile}
                                        <li class="text-center">
                                            <div class="flowplayer is-poster" style="background:#F7F7F7 url({$base_url}/uploads/prm/{$promotion.Promotion_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56">
                                                <video>
                                                    <source type="video/webm"  src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                    <source type="video/mp4"   src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                    <source type="video/flash" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                    <source type="video/ogg" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                    <source type="video/quicktime" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                </video>
                                            </div>
                                        </li>
                                    {/section}
                                </ul>
                            {/if}
                            <div class="clearfix"></div>
                        {else}
                            <!--<img src="{$asset_url}/images/question.jpg" alt="" class="img-responsive" />-->
                        {/if}
                    </div>
                </div>
                <div class="col-sm-12">
                    <h2>{$promotion.Promotion_Title}</h2><hr>
                    <h4><b>Description :</b></h4>
                    <p>{$promotion.Promotion_Desc}</p>
                    <p>
                        <b>Training:</b>
                        <i>
                            {$training.Training_Name}
                        </i>
                    </p>
                    <p>
                        <b>Trainer:</b>
                        <i>
                            {assign var=trainer value=get_trainer_name($promotion.Trainer_ID)}
                            {section name=n loop=$trainer}
                                {$trainer[n].Full_Name}
                            {/section}
                        </i>
                    </p>
                    <b>Share :</b>
                    {assign var=user value=get_trainer_name($promotion.Trainer_ID)}
                    {section name=u loop=$user}                                    
                        {if $user[u].PayPal_ID==''}
                            <a href="javascript:;" class="btn btn-primary" id="send_invite_fb">
                                <i class="fa fa-facebook fa-1x"></i>
                            </a>
                        {else}
                            <a href="javascript:fbShare('{$base_url}/promotion/{$promotion.Promotion_URL}/', 'ePrentice', 'Watch this Promotion {$promotion.Promotion_Title}!', '{$asset_url}/images/logo2.png', 520, 350)" target="_blank">
                                <i class="fa fa-facebook fa-1x"></i>
                            </a>
                        {/if}
                        {if $user[u].PayPal_ID==''}
                            <a href="javascript:;" class="btn btn-primary" id="send_invite_tw">
                                <i class="fa fa-twitter fa-1x"></i>
                            </a>
                        {else}
                            <a href="http://twitter.com/home?status=Currently Reading {$base_url}/promotion/{$promotion.Promotion_URL}" target="_blank" title="Click to share on Twitter">
                                <i class="fa fa-twitter fa-1x"></i>
                            </a>
                        {/if}
                        {if $user[u].PayPal_ID==''}
                            <a href="javascript:;" class="btn btn-primary" id="send_invite_li">
                                <i class="fa fa-linkedin fa-1x"></i>
                            </a>
                        {else}
                            <a href="http://www.linkedin.com/shareArticle?mini=true&url={$base_url}/promotion/{$promotion.Promotion_URL}&title={$promotion.Promotion_Title}&source={$base_url}/promotion/{$promotion.Promotion_URL}" target="_blank">
                                <i class="fa fa-linkedin fa-1x"></i>
                            </a>
                        {/if}
                        <div class="backdropFixed">
                        	<div id="paypal_id_check" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Alert</h4>                            
                                    </div>
                                    <div class="modal-body">
                                        <p>You didn't add your PayPal ID yet!</p>
                                        <p class="text-warning"><small>Would you like to add your PayPal ID now?</small></p>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="paypal_msg"></div>
                                        <form method="post" action="{$ajax_url}/update-email/">
                                            <div class="form-group">
                                                <label for="training_name">PayPal ID: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your PayPal ID."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <input type="text" name="paypal_id" id="paypal_id" class="form-control" placeholder="Enter PayPal ID" />
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" id="userID" name="userID" value="{$user[u].User_ID}" />
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-primary" id="paypal_save">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    {/section}
                    <br/><br/>                
                    {if empty($User_ID)}
                        <script type="text/javascript">
                        $(window).load(function(){
                            $('#myModal-3').modal('show');
                        });
                        </script>
                        <button id="video-btn" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#myModal-3">Register</button>
                        <div class="backdropFixed">
                        	<div class="modal fade" data-backdrop="static" id="myModal-3" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="" method="post">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Register for the Training</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="fname">First Name:</label>
                                                <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" />
                                            </div>
                                            <div class="form-group">
                                                <label for="lname">Last Name</label>
                                                <input type="text" class="form-control" id="lname" name="lname" placeholder="First Name" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email address</label>
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="pull-left">
                                                Already have an Account? <a href="{$base_url}/login/?return={$base_url}/training/{$training.Training_URL}/">Login</a>
                                            </span>
                                            <button type="submit" name="MemberSubmit" class="btn btn-default">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                    {/if}
                </div>
            </div>
            {if $promotion.Promotion_Type=='Question'}
                <div class="row">            	
                    <div class="col-sm-12">
                        <h2 class="text-center">Take a little survey for this Promotion</h2>
                    </div>
                    <div class="col-sm-12">
                        <div id="survey_msg"></div>
                    </div>
                    <form action="{$ajax_url}/survey-promotion/" method="post" id="survey_promotion" class="col-md-10 col-md-offset-1">
                        <input type="hidden" name="promotion_id" value="{$promotion.Promotion_ID}" />
                        {section name=i loop=$promotion_details}
                            <div class="questionnaire-set">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="question_id">{$promotion_details[i].Question}</label>
                                        <input type="hidden" name="question_count[]" value="{$promotion_details[i].Question_ID}" />
                                        <input type="hidden" name="question_id[]" value="{$promotion_details[i].Question_ID}" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="radio" class="survey_answers" name="survey_answer_{$promotion_details[i].Question_ID}" value="{$promotion_details[i].Answer_One}" />
                                        <label for="answer_one">{$promotion_details[i].Answer_One}</label>                        		
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="radio" class="survey_answers" name="survey_answer_{$promotion_details[i].Question_ID}" value="{$promotion_details[i].Answer_Two}" />
                                        <label for="answer_two">{$promotion_details[i].Answer_Two}</label>                        		
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="radio" class="survey_answers" name="survey_answer_{$promotion_details[i].Question_ID}" value="{$promotion_details[i].Answer_Three}" />
                                        <label for="answer_three">{$promotion_details[i].Answer_Three}</label>                        		
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="radio" class="survey_answers" name="survey_answer_{$promotion_details[i].Question_ID}" value="{$promotion_details[i].Answer_Four}" />
                                        <label for="answer_four">{$promotion_details[i].Answer_Four}</label>                        		
                                    </div>
                                </div>
                            </div>
                        {/section}
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="survey_submit">&nbsp;</label>
                                <button class="btn btn-primary" id="survey_submit" type="button" name="survey_submit">Submit</button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </form>
                </div>
            {/if}
        </div>
    </div>
</div>
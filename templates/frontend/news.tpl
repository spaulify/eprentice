<script type="text/javascript">
$(document).ready(function() {
	var track_load = 0;
	var loading  = false;
	var total_groups = '{$total_news}';
	$('#results').load(base_url + "/ajax/news-feed/", {
		'group_no' : track_load
	}, function() {
		track_load++;
	});
	
	$(window).scroll(function() {
		if ( $(window).scrollTop() + $(window).height() == $(document).height() ) {
			if ( track_load <= total_groups && loading == false ) {
				loading = true;
				$('.animation_image').show();
				$.post(base_url + "/ajax/news-feed/", {
					'group_no' : track_load
				}, function(data) {
					$("#results").append(data);
					$('.animation_image').hide();
					track_load++;
					loading = false; 
				}).fail(function(xhr, ajaxOptions, thrownError) {
					alert(thrownError);
					$('.animation_image').hide();
					loading = false;
				});
			}
		}
	});	
});
</script>

<div id="page-content-wrapper">
	<div class="news-feed-page">
    	<div id="first_login_error"></div>
        <div class="col-sm-8">
            <div class="row">
            	{if $promotions}
                    {section name=p loop=$promotions}
                        {if $promotions[p].Promotion_Type=='Image'}
                            <div class="training-video">
                            	<div class="video-desc">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            {assign var=trainer value=get_trainer_name($promotions[p].Trainer_ID)}
                                            {section name=u loop=$trainer}
                                                <a href="{$base_url}/profile/{$trainer[u].Username}/">
                                                    <img src="{$trainer[u].Profile_Photo}" class="img-circle" width="80" height="80" alt="{$trainer[u].Full_Name}" />
                                                    <p><strong>{$trainer[u].Full_Name}</strong></p>
                                                </a>
                                                {if $User_Role=='viewer'}
                                                    {assign var=followdata value=follow($trainer[u].User_ID,$User_ID)}
                                                    {if $followdata=='following'}
                                                        <p><a href="#" class="btn btn-block follow following followee-{$trainer[u].User_ID}">+ Unfollow</a></p>
                                                    {else}
                                                        <p><a href="#" class="btn btn-block follow followee-{$trainer[u].User_ID}">+ Follow</a></p>
                                                    {/if}
                                                    <input type="hidden" name="Followee_ID" value="{$trainer[u].User_ID}" />
                                                    <input type="hidden" name="Follower_ID" value="{$User_ID}" />
                                                    <input type="hidden" name="User_Role" value="{$User_Role}" />
                                                {/if}
                                            {/section}                                    
                                        </div>
                                        <div class="col-sm-10">
                                            <h3>{$promotions[p].Promotion_Title} 
                                                <small class="post-time">{$promotions[p].Created_On|date_format:"%d %b, %Y at %H:%M"}</small>
                                            </h3>
                                            <p>{$promotions[p].Promotion_Desc}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="video-wrap">
                                	<div class="row">
                                        <div class="col-sm-2">
                                        	{if $User_Role=='viewer'}
                                                <div class="follow_msg"></div>
                                            {/if}
                                        </div>
                                        <div class="col-sm-10">
                                        	<div class="content-cta">
                                                {assign var=promofile value=get_promotion_file($promotions[p].Promotion_ID)}
                                                {if count($promofile)==1}
                                                    {section name=u loop=$promofile}
                                                        <img src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}" class="img-responsive" alt="{$promofile[u].Promotion_Filename}" />
                                                    {/section}
                                                {else}
                                                    <ul class="bxslider">
                                                        {section name=u loop=$promofile}
                                                            <li class="text-center">
                                                                <img src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}" class="img-responsive" alt="{$promofile[u].Promotion_Filename}" width="100%" />
                                                            </li>
                                                        {/section}
                                                    </ul>
                                                {/if}
                                                <div class="form-action">
                                                	{assign var=training value=get_training($promotions[p].Training_ID)}
                                                    {section name=n loop=$training}
                                                       	{if $training[n].Training_Status=='Pending'}
                                                       		<span class="pull-left">This Training <strong>Coming Soon</strong>.</span>
                                                       		<span>Publish on: <strong>{$training[n].Training_Published_On|date_format:"%d %b, %Y at %H:%M"}</strong></span>
                                                       	{else}
															<span class="pull-left">Visit the Training to know more about <strong>{$promotions[p].Promotion_Title}</strong>.</span>
															<a href="{$base_url}/training/{$training[n].Training_URL}/" class="btn btn-success">Go to Training</a> 			
                                                   		{/if}
                                                    {/section}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        {elseif  $promotions[p].Promotion_Type=='Video'}
                            <div class="training-video">
                                <div class="video-desc">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            {assign var=trainer value=get_trainer_name($promotions[p].Trainer_ID)}
                                            {section name=u loop=$trainer}
                                                <a href="{$base_url}/profile/{$trainer[u].Username}/">
                                                    <img src="{$trainer[u].Profile_Photo}" class="img-circle" width="80" height="80" alt="{$trainer[u].Full_Name}" />
                                                    <p><strong>{$trainer[u].Full_Name}</strong></p>
                                                </a>
                                                {if $User_Role=='viewer'}
                                                    {assign var=followdata value=follow($trainer[u].User_ID,$User_ID)}
                                                    {if $followdata=='following'}
                                                        <p><a href="#" class="btn btn-block follow following followee-{$trainer[u].User_ID}">+ Unfollow</a></p>
                                                    {else}
                                                        <p><a href="#" class="btn btn-block follow followee-{$trainer[u].User_ID}">+ Follow</a></p>
                                                    {/if}
                                                    <input type="hidden" name="Followee_ID" value="{$trainer[u].User_ID}" />
                                                    <input type="hidden" name="Follower_ID" value="{$User_ID}" />
                                                    <input type="hidden" name="User_Role" value="{$User_Role}" />
                                                {/if}
                                            {/section}                                    
                                        </div>
                                        <div class="col-sm-10">
                                            <h3>{$promotions[p].Promotion_Title} 
                                                <small class="post-time">{$promotions[p].Created_On|date_format:"%d %b, %Y at %H:%M"}</small>
                                            </h3>
                                            <p>{$promotions[p].Promotion_Desc}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="video-wrap">
                                	<div class="row">
                                        <div class="col-sm-2">
                                        	{if $User_Role=='viewer'}
                                                <div class="follow_msg"></div>
                                            {/if}
                                        </div>
                                        <div class="col-sm-10">
                                        	<div class="content-cta">
                                                {assign var=promofile value=get_promotion_file($promotions[p].Promotion_ID)}
                                                {if count($promofile)==1}
                                                    {section name=u loop=$promofile}
                                                        <div class="flowplayer is-poster" style="background:#F7F7F7 url({$base_url}/uploads/prm/{$promotions[p].Promotion_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56">
                                                            <video>
                                                                <source type="video/webm"  src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                <source type="video/mp4"   src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                <source type="video/flash" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                <source type="video/ogg" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                <source type="video/quicktime" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                            </video>
                                                        </div>
                                                    {/section}
                                                {else}
                                                    <ul class="bxslider">
                                                        {section name=u loop=$promofile}
                                                            <li class="text-center">
                                                                <div class="flowplayer is-poster" style="background:#F7F7F7 url({$base_url}/uploads/prm/{$promotions[p].Promotion_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56">
                                                                    <video>
                                                                        <source type="video/webm"  src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                        <source type="video/mp4"   src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                        <source type="video/flash" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                        <source type="video/ogg" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                        <source type="video/quicktime" src="{$base_url}/uploads/prm/{$promofile[u].Promotion_Filename}">
                                                                    </video>
                                                                </div>
                                                            </li>
                                                        {/section}
                                                    </ul>
                                                {/if}
                                                <div class="form-action">
                                                    {assign var=training value=get_training($promotions[p].Training_ID)}
                                                    {section name=n loop=$training}
                                                       	{if $training[n].Training_Status=='Pending'}
                                                       		<span class="pull-left">This Training <strong>Coming Soon</strong>.</span>
                                                       		<span>Publish on: <strong>{$training[n].Training_Published_On|date_format:"%d %b, %Y at %H:%M"}</strong></span>
                                                       	{else}
															<span class="pull-left">Visit the Training to know more about <strong>{$promotions[p].Promotion_Title}</strong>.</span>
															<a href="{$base_url}/training/{$training[n].Training_URL}/" class="btn btn-success">Go to Training</a>
                                                   		{/if}
                                                    {/section}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        {elseif  $promotions[p].Promotion_Type=='Question'}
                            <div class="training-video">
                                <div class="video-desc">
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            {assign var=trainer value=get_trainer_name($promotions[p].Trainer_ID)}
                                            {section name=u loop=$trainer}
                                                <a href="{$base_url}/profile/{$trainer[u].Username}/">
                                                    <img src="{$trainer[u].Profile_Photo}" class="img-circle" width="80" height="80" alt="{$trainer[u].Full_Name}" />
                                                    <p><strong>{$trainer[u].Full_Name}</strong></p>
                                                </a>
                                                {if $User_Role=='viewer'}
                                                    {assign var=followdata value=follow($trainer[u].User_ID,$User_ID)}
                                                    {if $followdata=='following'}
                                                        <p><a href="#" class="btn btn-block follow following followee-{$trainer[u].User_ID}">+ Unfollow</a></p>
                                                    {else}
                                                        <p><a href="#" class="btn btn-block follow followee-{$trainer[u].User_ID}">+ Follow</a></p>
                                                    {/if}
                                                    <input type="hidden" name="Followee_ID" value="{$trainer[u].User_ID}" />
                                                    <input type="hidden" name="Follower_ID" value="{$User_ID}" />
                                                    <input type="hidden" name="User_Role" value="{$User_Role}" />
                                                {/if}
                                            {/section}                                    
                                        </div>
                                        <div class="col-sm-10">
                                            <h3>{$promotions[p].Promotion_Title} 
                                                <small class="post-time">{$promotions[p].Created_On|date_format:"%d %b, %Y at %H:%M"}</small>
                                            </h3>
                                            <p>{$promotions[p].Promotion_Desc}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="video-wrap">
                                	<div class="row">
                                    	<div class="col-sm-2">
                                        	{if $User_Role=='viewer'}
                                                <div class="follow_msg"></div>
                                            {/if}
                                        </div>
                                    	<div class="col-sm-10">
                                    		<div class="survey_msg"></div>
                                            <div class="col-sm-12">
                                            	<div class="row">
                                                    <div class="questionCard">
                                                        <form action="{$ajax_url}/survey-promotion/" method="post" id="survey_promotion" class="survey_promotion">
                                                            <input type="hidden" name="promotion_id" value="{$promotions[p].Promotion_ID}" />
                                                            {assign var=promoque value=get_promotion_question($promotions[p].Promotion_ID)}
                                                            {section name=i loop=$promoque}
                                                                <div class="questionnaire-set "><!--{if $smarty.section.i.iteration!=1}hide{/if}-->
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <p><strong>{$smarty.section.i.index+1}. {$promoque[i].Question}</strong></p>
                                                                            <input type="hidden" name="question_count[]" value="{$promoque[i].Question_ID}" />
                                                                            <input type="hidden" name="question_id[]" value="{$promoque[i].Question_ID}" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="answer-1-{$promoque[i].Question_ID}">
                                                                                <input type="radio" class="survey_answers" id="answer-1-{$promoque[i].Question_ID}" name="survey_answer_{$promoque[i].Question_ID}" value="{$promoque[i].Answer_One}" /><span>{$promoque[i].Answer_One}</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="answer-2-{$promoque[i].Question_ID}">
                                                                                <input type="radio" class="survey_answers" id="answer-2-{$promoque[i].Question_ID}" name="survey_answer_{$promoque[i].Question_ID}" value="{$promoque[i].Answer_Two}" /><span>{$promoque[i].Answer_Two}</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="answer-3-{$promoque[i].Question_ID}">
                                                                                <input type="radio" class="survey_answers" id="answer-3-{$promoque[i].Question_ID}" name="survey_answer_{$promoque[i].Question_ID}" value="{$promoque[i].Answer_Three}" /><span>{$promoque[i].Answer_Three}</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="answer-4-{$promoque[i].Question_ID}">
                                                                                <input type="radio" class="survey_answers" id="answer-4-{$promoque[i].Question_ID}" name="survey_answer_{$promoque[i].Question_ID}" value="{$promoque[i].Answer_Four}" /><span>{$promoque[i].Answer_Four}</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12"><br /></div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            {/section}
                                                        </form>
                                                        <div class="form-action">
                                                            {assign var=training value=get_training($promotions[p].Training_ID)}
                                                            {section name=n loop=$training}
																{if $training[n].Training_Status=='Pending'}
																	<span class="pull-left">This Training <strong>Coming Soon</strong>.</span>
                                                       				<span>Publish on: <strong>{$training[n].Training_Published_On|date_format:"%d %b, %Y at %H:%M"}</strong></span>
																{else}
																	<span class="pull-left">Visit the Training to know more about <strong>{$promotions[p].Promotion_Title}</strong>.</span>
																	<a href="{$base_url}/training/{$training[n].Training_URL}/" class="btn btn-success">Go to Training</a>
                                                           		{/if}
                                                            {/section}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/section}
                {else}
                	<div class="training-video" style="height:90vh">
                    	<div class="video-wrap">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                               		<p><br /><br /><br /><br /><br /><br /><br /><br />Currently, no news feed are available at that time.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
        </div>
        
        <div class="col-sm-4">
            <div class="row">
            	{if $trainer_id}
                    <div class="popular-trainers">
                        <h3>Popular Trainers</h3>
                        <ul>
                            {section name=i loop=$trainer_id}
                                {assign var=trainer value=get_trainer_name($trainer_id[i].Trainer_ID)}
                                {section name=n loop=$trainer}
                                    <li>
                                        <a href="{$base_url}/profile/{$trainer[n].Username}/">
                                            <img src="{$trainer[n].Profile_Photo}" class="img-circle" alt="{$trainer[n].Full_Name}" width="60" height="60" />
                                        </a>
                                    </li>
                                {/section}
                            {/section}
                        </ul>
                    </div>
                {else}
                	<div class="popular-trainers" style="height:30vh;">
                        <h3>Popular Trainers</h3>
                        <ul>
                            <li><br /><br />No Trainers are available in the Popular list.</li>
                        </ul>
                    </div>
                {/if}
                {if $training_id}
                    <div class="popular-trainings">
                        <h3>Popular Trainings</h3>
                        {section name=t loop=$training_id}
                            {assign var=training value=get_training($training_id[t].Training_ID)}
                            {section name=n loop=$training}
                                <div class="popular-video">
                                    <div class="text-center">
                                        <a href="{$base_url}/training/{$training[n].Training_URL}/">
                                            <img src="{$base_url}/uploads/trp/{$training[n].Training_Thumb}" alt="{$training[n].Training_Name}" class="img-responsive" />
                                        </a>
                                    </div>
                                    <h4><a href="{$base_url}/training/{$training[n].Training_URL}/">{$training[n].Training_Name}</a></h4>
                                    <p>Added {$training[n].Created_On|date_format:"%d %b, %Y at %H:%M"} from 
                                        {assign var=trainer value=get_trainer_name($training[n].Trainer_ID)}
                                        {section name=u loop=$trainer}
                                            {$trainer[u].Full_Name}
                                        {/section}...
                                    </p>
                                </div>
                            {/section}
                        {/section}
                    </div>
                {else}
                	<div class="popular-trainings" style="height:30vh;">
		                <h3>Popular Trainings</h3>
                        <div class="popular-video">
                            <div class="text-center"><br /><br />No Popular Trainings are available.</div>
                        </div>
	                </div>
                {/if}
                {if $products}
                    <div class="popular-communities">
                        <h3>Recent Products</h3>
                        {section name=d loop=$products}
                            <div class="communities">
                                <p>
                                    {assign var=product_url value=modify_http($products[d].Product_Link)}
                                    <a href="{$product_url}" target="_blank">
                                        <img src="{$base_url}/uploads/prd/{$products[d].Product_Image}" class="img-circle" alt="{$products[d].Product_Name}" width="60" height="60" /> 
                                        <strong>{$products[d].Product_Name}</strong><br>
                                    </a>
                                    <span>{$products[d].Product_Desc|truncate:30:"...":true}</span>
                                </p>
                            </div>
                        {/section}
                    </div>
                {else}
                	<div class="popular-communities" style="height:30vh;">
                        <h3>Recent Products</h3>
                        <div class="communities">
                            <p class="text-center"><br /><br />There are no products right now.</p>
                        </div>
                    </div>
                {/if}
	        </div>
		</div>
    </div>
</div>
{assign var=user value=get_trainer_name($User_ID)}
{section name=n loop=$user}
	<input type="hidden" name="login_first" id="login_first" value="{$user[n].Login_First}" />
    <input type="hidden" name="user_id" id="user_id" value="{$user[n].User_ID}" />
{/section}
<div class="backdropFixed">
	<div id="login_first_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Watch the video for how to use ePrentice!</h4>
            </div>
            <div class="modal-body">
                <video width="470" height="320" controls>
					<source src="{$base_url}/assets/frontend/images/login_first_video.mp4" type="video/mp4">
                </video> 
            </div>
        </div>
    </div>
</div>
</div>
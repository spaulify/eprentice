<div id="page-content-wrapper">
	<div class="training-list-page">
        <div class="col-sm-7">
            <h1>Training Programs</h1>
        </div>
        <div class="col-sm-5">
		    <div class="input-group">
				<input type="text" class="form-control" placeholder="Start Searching with Course Title or Subject" />
				<span class="input-group-btn">
					<button class="btn btn-danger" type="button">
                        <span class=" glyphicon glyphicon-search"></span>
                    </button>
				</span>
			</div>
        </div>

        <div class="col-md-12">
        	<div class="row">
            	{if $trainings}
                    {section name=t loop=$trainings}
                        <div class="col-sm-4 content">
                            <a href="{$base_url}/training/{$trainings[t].Training_URL}/">
                                <img src="{$base_url}/uploads/trp/{$trainings[t].Training_Thumb}" alt="{$trainings[t].Training_Name}" class="img-responsive" />
                            </a>
                            <div class="border-white">
                                <h3><a href="{$base_url}/training/{$trainings[t].Training_URL}/">{$trainings[t].Training_Name|truncate:25:"...":true}</a></h3>
                                <p>{$trainings[t].Training_Desc|truncate:100:"...":true}</p>
                                <div class="like">
                                    <a href="#" class="favorite">
                                    	{assign var=class value=get_training_favorite($trainings[t].Training_ID, $User_ID)}
                                        {if $class=='fa-heart'}
                                    		<i class="fa fa-heart fa-3x fav-logo"></i>
                                        {elseif $class='fa-heart-o'}
                                        	<i class="fa fa-heart-o fa-3x fav-logo"></i>
                                        {/if}
                                    </a>
                                    <input type="hidden" name="Training_ID" id="Training_ID" value="{$trainings[t].Training_ID}" />
                                    <input type="hidden" name="User_ID" id="User_ID" value="{$User_ID}" />
                                    <input type="hidden" name="User_Role" id="User_Role" value="{$User_Role}" />
                                    <a href="{$base_url}/training/{$trainings[t].Training_URL}/" class="btn">Watch</a>
                                </div>
                                <div class="favorite_msg"></div>
                            </div>
                        </div>
                    {/section}
                {else}
                	<div class="col-sm-12">
                        <p class="no-items">No trainings in the list.</p>
                    </div>
                {/if}
            </div>
            
            <div class="text-center">
				<ul class="pagination"></ul>
            </div>
        </div>
    </div>
</div>
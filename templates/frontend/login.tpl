<div class="wrapper">
	<form class="form-signin" method="post" action="" name="EprenticeLogin" id="ePrenticeLogin">
    	<div class="login-form">
            <h2 class="form-signin-heading">Sign In to ePrentice</h2>
            {if $error}
                <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {$error}
                </div>
            {/if}
            <div class="form-group">
	            <input type="email" class="form-control" name="email" id="email" placeholder="Email Address" value="{if $email} {$email} {else} {$remember_email} {/if}" />
            </div>
            <div class="form-group">
	            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="{$remember_pass}" />
            </div>
            <div class="form-group">
                <label class="checkbox">
                    <input type="checkbox" name="remember" id="remember" {if $remember_email && $remember_pass} checked="checked" {else} '' {/if}> Remember me
                </label>
            </div>
            <div class="form-group">
				<button class="btn btn-lg btn-info btn-block" type="submit" name="submit" id="login">Login</button>
            </div>
        </div>
		<div id="password" class="text-center">
			<a href="{$base_url}/forgot-password/" style="color:red;"> Forgot Password?</a>
		</div>
		<div class="social-acc">
			<div class="row">
            	<hr /><h4 class="form-signin-heading">Login with your Social Accounts</h4>
            	<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-facebook" href="{$fbLoginUrl}">
						<span class="fa fa-facebook"></span> Facebook
					</a>
				</div>
				<div class="col-sm-3"> 
					<a class="btn btn-block btn-social btn-google" href="{$gpLoginUrl}">
						<span class="fa fa-google"></span> Google
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-twitter" href="{$twLoginUrl}">
						<span class="fa fa-twitter"></span> Twitter
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-linkedin" href="{$liLoginUrl}">
						<span class="fa fa-linkedin"></span> Linkedin
					</a>
				</div><br /><br /><hr />
			</div>
		</div>
		<div class="register-now">
			<p>Don't have an Account? <a class="btn btn-xs btn-success pull-right" href="{$base_url}/register/">Register</a></p>
		</div>
	</form>
</div>
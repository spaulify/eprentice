<div id="page-content-wrapper">
	<div class="withdraw-payment-page">
    	<div class="row">
            <div class="payment-detail">
            	<h3 class="payment-detail-header">Payment Details</h3>
                <h4 class="payment-detail-header secure-trans">Secure Transaction <i class="fa fa-lock"></i> </h4>
                <hr class="horizontal-line"/>
                <p class="payment-sub">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                <div class="row">
                	<div class="col-sm-5 payment-info-header">
                    	<label class="payment-info">Pay To:</label> Name Here<br/>
                        <label class="payment-info">Payment For:</label> Video Tutorial<br/>
                        <label class="payment-info">Currency:</label> U.S Dollars ?<br/>
                        <label class="payment-info">Amount:</label> $20.00
                    </div>
                </div>
            </div>
            <hr class="horizontal-line-dotted"/>
            <p class="payment-sub">If you have never paid through PayPal, <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click Here</button></p>
            <hr class="horizontal-line-dotted"/>   
            <h3 class="paypal-detail-header">PayPal Login</h3>
            <p class="welcome">Welcome Back !</p>
            <form role="form">
            	<div class="row login-info-header">
                	<div class="col-sm-12">
                    	<div class="form-group">
                        	<label id="">Email Address:</label>
                            <div class="input-group col-sm-6">
                            	<input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                    	<div class="form-group">
                        	<label id="">Password:</label>
                            <div class="input-group col-sm-6">
                            	<input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                    	<div class="form-group">
                        	<div class="input-group col-sm-6 forgot-pass">
                            	<a href="">Forgot Password?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr class="horizontal-line"/>
            <div class="btn-continue">
	            <button class="btn btn-primary">Continue</button>
	        </div>
        </div>
    </div>
</div>

<!--<div class="backdropFixed">
    <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pay with Credit card or Debit card</h4>
                </div>
                <div class="modal-body">
                    <div id="payment-header">    
                        <div id="panel-default" class="panel panel-default credit-card-box">
                            <div  id="panel-heading" class="panel-heading display-table" >
                                <div  class="row display-tr">
                                    <h3 class="panel-title display-td">Pay Now</h3>
                                    <div class="display-td">
                                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form role="form" id="payment-form" method="post" action="" autocomplete="off">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="CardNumber">CARD NUMBER</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="CardNumber" id="CardNumber" placeholder="Card Number" required />
                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="CardExpiryMM">EXP. MONTH</label>
                                                <input type="text" class="form-control" name="CardExpiryMM" id="CardExpiryMM" placeholder="MM" required />
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="CardExpiryYY">EXP. YEAR</label>
                                                <input type="text" class="form-control" name="CardExpiryYY" id="CardExpiryYY" placeholder="YY" required />
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <label for="CardCVV">CVV CODE</label>
                                                <input type="text" class="form-control" name="CardCVV" id="CardCVV" placeholder="CVV" />
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="NameOnCard">NAME ON CARD</label>
                                                <input type="text" class="form-control" name="NameOnCard" id="NameOnCard" placeholder="CARD HOLDER NAME" required />
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <input type="hidden" name="fname" id="fname" value="{$User_First_Name}" />
                                            <input type="hidden" name="lname" id="lname" value="{$User_Last_Name}" />
                                            <button id="transaction-btn" class="btn btn-primary btn-lg btn-block" type="submit">Process Transaction Now</button>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="payment-errors">{$success}</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->
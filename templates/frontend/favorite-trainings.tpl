<div id="page-content-wrapper">
	<div class="favorite-trainings-page">
        <div class="col-md-12">
        	<h1>Favorite Trainings</h1>
        	<div class="row">
            	{if $trainings}
                    {section name=t loop=$trainings}
                        <div class="col-sm-4 content">
                            <a href="{$base_url}/training/{$trainings[t].Training_URL}/">
                                <img src="{$base_url}/uploads/trp/{$trainings[t].Training_Thumb}" alt="{$trainings[t].Training_Name}" class="img-responsive" />
                            </a>
                            <div class="border-white">
                                <h3><a href="{$base_url}/training/{$trainings[t].Training_URL}/">{$trainings[t].Training_Name|truncate:25:"...":true}</a></h3>
                                <p>{$trainings[t].Training_Desc|truncate:100:"...":true}</p>
                                <div class="like">
                                    <a href="#" class="favorite">
                                    	{assign var=class value=get_training_favorite($trainings[t].Training_ID, $User_ID)}
                                        {if $class=='fa-heart'}
                                    		<i class="fa fa-heart fa-3x fav-logo"></i>
                                        {elseif $class='fa-heart-o'}
                                        	<i class="fa fa-heart-o fa-3x fav-logo"></i>
                                        {/if}
                                    </a>
                                    <input type="hidden" name="Training_ID" id="Training_ID" value="{$trainings[t].Training_ID}" />
                                    <input type="hidden" name="User_ID" id="User_ID" value="{$User_ID}" />
                                    <input type="hidden" name="User_Role" id="User_Role" value="{$User_Role}" />
                                    {assign var=status value=get_training_status($trainings[t].Training_ID, $User_ID)}
                                    {section name=o loop=$status}
                                        {if $status[o].Training_Status=='Ongoing'}
                                            <span class="fav-ongoing-status">On-Going Training</span>
                                        {/if}
                                    {/section}
                                    <a href="{$base_url}/training/{$trainings[t].Training_URL}/" class="btn">Watch</a>
                                </div>
                                <div class="favorite_msg"></div>
                            </div>
                        </div>
                    {/section}
                {else}
                	<div class="col-sm-12">
                        <p class="no-items">You have no favorite trainings in the list.</p>
                    </div>
                {/if}
            </div>
            
            <div class="text-center">
				<ul class="pagination"></ul>
            </div>
        </div>
    </div>
</div>
<div id="page-content-wrapper">
	<div class="invite-friend-page">
    	{if $success}
            <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {$success}
            </div>
        {/if}
        {if $error}
            <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {$error}
            </div>
        {/if}
    	<div class="row">
            <div class="col-sm-6">
                <div class="invite-border">
                    <div class="img-logo">
                        <img src="{$asset_url}/images/fb.png" class="img-circle" alt="" width="80" height="80" />
                    </div>
                    <div class="invite-header text-center">
                        <h3>Share your unique ePrentice link with your Facebook friends to join ePrentice...</h3>
                        <h4>{$base_url}/aff/<strong>{$user.Ref_Code}</strong>/</h4>
                    </div>
                    <div class="col-mid-content">
						{if $user.PayPal_ID==''}                            
                            <a href="javascript:;" class="btn btn-primary" id="send_invite_fb">
                                <i class="fa fa-facebook"></i> Share
                            </a>
                        {else}
                        	<a href="javascript:fbShare('{$base_url}/aff/{$user.Ref_Code}/', 'ePrentice', 'Come Join ePrentice!', '{$asset_url}/images/logo2.png', 520, 350)" class="btn btn-primary">
                                <i class="fa fa-facebook"></i> Share
                            </a>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="invite-border">
                    <div class="img-logo">
                        <img src="{$asset_url}/images/linkedin.png" class="img-circle" alt="" width="80" height="80" />
                    </div>
                    <div class="invite-header text-center">
                        <h3>Share your unique ePrentice link with your Linkedin friends to join ePrentice...</h3>
                        <h4>{$base_url}/aff/<strong>{$user.Ref_Code}</strong>/</h4>
                    </div>
                    <div class="col-mid-content">
                        {if $user.PayPal_ID==''}                            
                            <a href="javascript:;" class="btn btn-primary" id="send_invite_li">
                                <i class="fa fa-linkedin"></i> Share
                            </a>
                        {else}
                        	<a class="btn btn-primary" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url={$base_url}/aff/{$user.Ref_Code}/&title=ePrentice&source={$base_url}/','ePrentice','height=320, width=300,location=no,scrollbars=yes,left='+(screen.width/2)-(300/2)+',top='+(screen.height/2)-(320/2));">
                                <i class="fa fa-linkedin"></i> Share
                            </a>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="invite-border">
                    <div class="img-logo">
                        <img src="{$asset_url}/images/envelop.png" class="img-circle" alt="" width="80" height="80" />
                    </div>
                    <div class="invite-header text-center">
                        <h3>Send Email invitation to your friends to join ePrentice...</h3>
                    </div>
                    <div class="col-mid-content">
                        {if $user.PayPal_ID==''}
                        	<h5>Missing Payment Information<br />Update before sending invitation</h5>
                            <a class="btn btn-primary" id="send_invite_email">Update</a>
                        {else}
                        	<form action="" method="post">
                                <input type="hidden" id="share_url" name="share_url" value="{$base_url}/{$user.Ref_Code}/" />
                                <input type="text" class="form-control" id="emails" name="share_email" placeholder="Enter your friends' email here" />
                                <span class="help-block">Enter multiple emails separated by comma (,).</span>
                                <button type="submit" name="share_submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                            </form>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div id="paypal_id_check" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Missing Paypal ID</h4>                            
            </div>
            <form method="post" action="{$ajax_url}/update-email/" onsubmit="return false;">
	            <div class="modal-body">
                    <h5>You didn't add your PayPal ID yet! Please enter your PayPal ID below.</h5>
                    <div class="form-group">
                    	<div id="paypal_msg"></div>
                        <input type="text" name="paypal_id" id="paypal_id" class="form-control" placeholder="Enter PayPal ID" />
                    </div>
                    <input type="hidden" id="userID" name="userID" value="{$user.User_ID}" />
	            </div>
    	        <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="paypal_save">Save</button>
	            </div>
            </form>
        </div>
    </div>
</div>
</div>
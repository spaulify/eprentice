<div id="page-content-wrapper">
	<div class="trainings-page">
        <div class="col-sm-12">
        	<h1>Your Trainings</h1>
            {if $message}
                <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {$message}
                </div>
            {/if}
            {if $error}
                <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {$error}
                </div>
            {/if}
            <div class="row">
                {if $trainings}
                    {section name=i loop=$trainings}
                        {assign var=category value=get_category_name($trainings[i].Training_Category)}
                        <div class="col-sm-12">
                            <div class="training-row btnwrap">
                                <div class="col-sm-4">
                                    <img src="{$base_url}/uploads/trp/{$trainings[i].Training_Thumb}" alt="{$trainings[i].Training_Name}" class="img-responsive" />
                                </div>
                                <div class="col-sm-8">
                                    <h2>
                                        {$trainings[i].Training_Name}
                                        <a style="margin:5px;" href="{$base_url}/edit-training/{$trainings[i].Training_ID}/" class="btn btn-xs btn-primary edit-btn pull-right">
                                            <span class="fa fa-edit"></span> Edit
                                        </a>
                                        {assign var=user value=get_trainer_name($trainings[i].Trainer_ID)}
                                        {section name=u loop=$user}
                                            {if $user[u].PayPal_ID==''}                            
                                                <a style="margin:5px;" class="btn btn-xs btn-info edit-btn pull-right" id="send_invite_email">
                                                    <i class="fa fa-envelope-o"></i> Share
                                                </a> 
                                            {else}
                                                <a style="margin:5px;" class="btn btn-xs btn-info edit-btn pull-right" data-toggle="modal" data-target="#EmailModal-{$trainings[i].Training_ID}">
                                                    <span class="fa fa-envelope"></span> Share
                                                </a>
                                            {/if}
                                        {/section}
                                    </h2>                                    
                                    <p>
                                        <b>Category:</b>
                                        <i>
                                            {assign var=category value=get_category_name($trainings[i].Training_Category)}
                                            {section name=n loop=$category}
                                                {$category[n].Category_Name}
                                            {/section}
                                        </i>
                                    </p>
                                    <p>
                                        <b>Trainer:</b>
                                        <i>
                                            {assign var=trainer value=get_trainer_name($trainings[i].Trainer_ID)}
                                            {section name=n loop=$trainer}
                                                {$trainer[n].Full_Name}
                                            {/section}
                                        </i>
                                    </p>
                                    <p>
                                        <b>Tags:</b>
                                        <i>{$trainings[i].Training_Tags}</i>
                                    </p>
                                    <p>
                                        <b>Status: {if $trainings[i].Training_Status=='Pending'}<a href="#" data-toggle="tooltip" data-placement="right" title="The training is yet to be approved. Please check back later."><small class="glyphicon glyphicon-question-sign"></small></a>{/if}</b>
                                        <i>{$trainings[i].Training_Status}</i>
                                    </p>
                                    <p><a class="btn btn-primary" href="{$base_url}/training/{$trainings[i].Training_URL}/">Watch</a></p>
                                </div>
                                <div class="clearfix"></div>
                                <div class="backdropFixed">
                                    <div class="modal fade" data-backdrop="static" id="EmailModal-{$trainings[i].Training_ID}" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Share Training</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="" method="post">
                                                        <div class="form-group">
                                                            <label for="emails">Share via Email:</label>
                                                            <textarea class="form-control" id="emails" name="emails"></textarea>
                                                            <p class="help-block">Enter multiple emails separated by comma (,).</p>
                                                            <input type="hidden" name="Training_URL" value="{$trainings[i].Training_URL_EM}" />
                                                            <input type="hidden" name="Training_ID" value="{$trainings[i].Training_ID}" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" name="EmailShareSubmit" class="btn btn-default">Submit</button>
                                                        </div>
                                                    </form>
                                                    <div class="form-group">
                                                        <p>OR</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message">Share via Social Media</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <a onclick="window.open('https://www.facebook.com/dialog/share?app_id={$Facebook_App_ID}&display=popup&href={$base_url}/training/{$trainings[i].Training_URL_FB}/', 'ePrentice - {$trainings[i].Training_Name}', 'height=640, width=600,location=no,scrollbars=yes');" class="social-share-btn"><i class="fa fa-facebook"></i></a>
                                                        <a onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url={$base_url}/training/{$trainings[i].Training_URL_LI}/&title=ePrentice&source={$base_url}/','ePrentice - {$trainings[i].Training_Name}','height=640, width=600,location=no,scrollbars=yes');" class="social-share-btn"><i class="fa fa-linkedin"></i></a>
                                                        <a onClick="window.open('https://twitter.com/intent/tweet?hashtags=ePrentice&original_referer=https://dev.twitter.com/web/tweet-button&text=Watch Training: {$trainings[i].Training_Name}&tw_p=tweetbutton&url={$base_url}/training/{$trainings[i].Training_URL_TW}/&via=ePrentice','ePrentice - {$trainings[i].Training_Name}','height=640, width=600,location=no,scrollbars=yes');" class="social-share-btn"><i class="fa fa-twitter"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    {/section}
                {else}
                    <div class="col-sm-12">
                        <p class="no-items">No training is available in the list.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div id="paypal_id_check" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Missing Paypal ID</h4>                            
            </div>
            <form method="post" action="{$ajax_url}/update-email/" onsubmit="return false;">
	            <div class="modal-body">
                    <h5>You didn't add your PayPal ID yet! Please enter your PayPal ID below.</h5>
                    <div class="form-group">
                    	<div id="paypal_msg"></div>
                        <input type="text" name="paypal_id" id="paypal_id" class="form-control" placeholder="Enter PayPal ID" />
                    </div>
                    <input type="hidden" id="userID" name="userID" value="{$User_ID}" />
	            </div>
    	        <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="paypal_save">Save</button>
	            </div>
            </form>
        </div>
    </div>
</div>
</div>
<div id="page-content-wrapper">
	<div class="downline-members-page">
        <div class="col-sm-12">
            <h1>Downline Members</h1>
            <div class="row">
                {if $members}
                    {section name=i loop=$members}
                    	<div class="col-sm-3">
	                        <div class="member-rows">
                                <img src="{$members[i].Profile_Photo}" alt="{$members[i].Full_Name}" class="img-circle img-responsive" />
                                <div class="caption">
                                	<h3>{$members[i].Full_Name}</h3>
                                    <p>{$members[i].Bio|truncate:120}</p>
                                </div>
                            </div>
                        </div>
                    {/section}
                {else}
                	<div class="col-sm-12">
                        <p class="no-items">You have no downline members.<br />Please <a href="{$base_url}/invite/">invite</a> your friends to join ePrentice.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
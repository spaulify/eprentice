<div id="page-content-wrapper">
	<div class="bulk-share-page">
        <div class="col-sm-12">
            <h1>Bulk Share Trainings</h1>
            <div class="row">
                <form method="post" action="{$ajax_url}/new-invitation/" enctype="multipart/form-data" id="send_invitation" class="col-md-10 col-md-offset-1">
                    <div id="invitation_msg"></div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="send_training">Training Name: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="send_training_error"></span>
                                <select class="form-control" id="send_training" name="send_training">
                                    <option value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="email_csv">Select CSV File: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a CSV File for Send Invitation for Training"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="email_csv_error"></span>
                                <input class="form-control" type="file" id="email_csv" name="email_csv" accept=".csv" style="height:auto;" />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="invitation_submit">&nbsp;</label><br/>
                                <button class="btn" id="invitation_submit" name="invitation_submit">Submit</button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
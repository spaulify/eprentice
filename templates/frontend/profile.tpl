<div id="page-content-wrapper">
	<div class="profile-page">
        <div class="col-sm-4">
            <div class="row">
                <div class="profile-sec">
                	{if $user['User_ID'] == $User_ID}
                        <small class="pull-right relative">
                            <button type="button" class="edit-btn-small" data-toggle="modal" data-target="#picModal"><i class="fa fa-edit"></i> Edit</button>
                        </small>
                    {/if}
                    <img src="{$user['Profile_Photo']}" class="img-circle" alt="{$user['Full_Name']}" width="170" height="170" />
                    <h3>{$user['Full_Name']}</h3>
                    <p class="small text-muted">
                        {if $user['Designation']} {$user['Designation']}<br /> {/if}
                    </p>
                    {if $user['Facebook_URL'] || $user['Google_URL'] || $user['Twitter_URL'] || $user['LinkedIn_URL']}
                        <p class="social-icons icon-circle">
                            {if $user['Facebook_URL']}
                                <a href="{$user['Facebook_URL']}" target="_blank"><i class="fa fa-facebook"></i></a> 
                            {/if}
                            {if $user['Twitter_URL']}
                                <a href="{$user['Twitter_URL']}" target="_blank"><i class="fa fa-twitter"></i></a> 
                            {/if}
                            {if $user['Google_URL']}
                                <a href="{$user['Google_URL']}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                            {/if}
                            {if $user['LinkedIn_URL']}
                                <a href="{$user['LinkedIn_URL']}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                            {/if}
                        </p>
                    {/if}
                </div>
            </div>
        </div>
        
        <div class="col-sm-8">
            <div class="row">
                <div class="trainer-profile">
                	{if $user['User_ID'] == $User_ID}
                        {if $error}
                            <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {$error}
                            </div>
                        {/if}
                        {if $success}
                            <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {$success}
                            </div>
                        {/if}
                    {/if}
                    <div class="profile-row">
                        <h2>
                            {if $user['User_ID'] == $User_ID}
                                About <span>Me</span>
                            {else}
                                Trainer <span>Bio</span>
                            {/if}
                            {if $user['User_ID'] == $User_ID}
                                <small class="pull-right relative">
                                    <button class="edit-btn-small" data-toggle="modal" data-target="#bioModal"><i class="fa fa-edit"></i> Edit</button>
                                </small>
                            {/if}
                        </h2>
                        <p>{if $user['Bio']} {$user['Bio']} {else} {if $user['User_ID'] == $User_ID} Enter something about yourself within maximum 500 characters. {else} No information found. {/if} {/if}</p>
                    </div>
                    <div class="profile-row">
                        <h2>
                            Areas of <span>Expertise</span> 
                            {if $user['User_ID'] == $User_ID}
                                <small class="pull-right relative">
                                    <button class="edit-btn-small" data-toggle="modal" data-target="#expModal"><i class="fa fa-edit"></i> Edit</button>
                                </small>
                            {/if}
                        </h2>
                        <p>
                            <i class="fa fa-css3"></i> 
                            <label>Domain:</label> 
                            {if $user['Domain']} {$user['Domain']} {else} {if $user['User_ID'] == $User_ID} Enter your specific domains. {else} Info not available. {/if} {/if}
                        </p>
                        <p>
                            <i class="fa fa-files-o"></i> 
                            <label>Projects:</label> 
                            {if $user['Projects']} {$user['Projects']} {else} {if $user['User_ID'] == $User_ID} Specify the projects you have done. {else} Info not available. {/if} {/if}
                        </p>
                        <p>
                            <i class="fa fa-briefcase"></i> 
                            <label>Experience:</label> 
                            {if $user['Experience']} {$user['Experience']} Month(s) {else} {if $user['User_ID'] == $User_ID} Enter your total experience in months. {else} Info not available. {/if} {/if}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{if $user['User_ID'] == $User_ID}
    <div class="modal fade" id="picModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="" enctype="multipart/form-data" name="EprenticeGeneralInfo" id="ePrenticeProfile">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-8">
                                    <label for="photo">Profile Photo:</label>
                                    <input class="form-control" name="photo" id="photo" type="file" onchange="displayPreview(this.files);" />
                                </div>
                                <div class="col-sm-3 text-center">
                                    <span id="preview">
                                        <img src="{$user['Profile_Photo']}" class="img-circle" alt="{$user['Full_Name']}" id="previewIMG" width="80" height="80" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row profile-row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="fname">First Name:</label>
                                    <input class="form-control" name="fname" id="fname" type="text" placeholder="First Name" value="{$user['First_Name']}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="lname">Last Name:</label>
                                    <input class="form-control" name="lname" id="lname" type="text" placeholder="Last Name" value="{$user['Last_Name']}" />
                                </div>
                            </div>
                        </div>
                        <div class="row profile-row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="old_password">Old Password:</label>
                                    <input class="form-control" name="old_password" id="old_password" type="password" placeholder="Old Password" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="new_password">New Password:</label>
                                    <input class="form-control" name="new_password" id="new_password" type="password" placeholder="New Password" />
                                </div>
                            </div>
                        </div>
                        <div class="row profile-row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="desig">Designation:</label>
                                    <input class="form-control" name="desig" id="desig" type="text" value="{$user['Designation']}" placeholder="Designation" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="paypal_id">PayPal ID:</label>
                                    <input class="form-control" name="paypal_id" id="paypal_id" type="text" value="{$user['PayPal_ID']}" placeholder="PayPal ID" />
                                </div>
                            </div>
                        </div>
                        <div class="row profile-row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="facebook" class="btn-block">
                                        Facebook URL:
                                        {if $user['Login_With_FB']=='yes'}
                                            <a class="btn-facebook pull-right" href="javascript:void(0);">
                                                <span class="fa fa-facebook"></span> Connected
                                            </a>
                                        {else}
                                            <a class="btn-facebook pull-right" href="{$fbLoginUrl}">
                                                <span class="fa fa-facebook"></span> Connect
                                            </a>
                                        {/if}
                                    </label>
                                    <input class="form-control" name="facebook" id="facebook" type="url" placeholder="Facebook URL" value="{$user['Facebook_URL']}" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="google" class="btn-block">
                                        Google URL: 
                                        {if $user['Login_With_GP']=='yes'}
                                            <a class="btn-google pull-right" href="javascript:void(0);">
                                                <span class="fa fa-google"></span> Connected
                                            </a>
                                        {else}    
                                            <a class="btn-google pull-right" href="{$gpLoginUrl}">
                                                <span class="fa fa-google"></span> Connect
                                            </a>
                                        {/if}
                                    </label>
                                    <input class="form-control" name="google" id="google" type="url" placeholder="Google URL" value="{$user['Google_URL']}" readonly />
                                </div>
                            </div>
                        </div>
                        <div class="row profile-row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="twitter" class="btn-block">
                                        Twitter URL: 
                                        {if $user['Login_With_TW']=='yes'}
                                            <a class="btn-twitter pull-right" href="javascript:void(0);">
                                                <span class="fa fa-twitter"></span> Connected
                                            </a>
                                        {else}    
                                            <a class="btn-twitter pull-right" href="{$twLoginUrl}">
                                                <span class="fa fa-twitter"></span> Connect
                                            </a>
                                        {/if}
                                    </label>
                                    <input class="form-control" name="twitter" id="twitter" type="url" placeholder="Twitter URL" value="{$user['Twitter_URL']}" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="linkedin" class="btn-block">
                                        LinkedIn URL: 
                                        {if $user['Login_With_LI']=='yes'}
                                            <a class="btn-linkedin pull-right" href="javascript:void(0);">
                                                <span class="fa fa-linkedin"></span> Connected
                                            </a>
                                        {else}
                                            <a class="btn-linkedin pull-right" href="{$liLoginUrl}">
                                                <span class="fa fa-linkedin"></span> Connect
                                            </a>
                                        {/if}
                                    </label>
                                    <input class="form-control" name="linkedin" id="linkedin" type="url" placeholder="LinkedIn URL" value="{$user['LinkedIn_URL']}" readonly />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="alert alert-danger alert-dismissible" id="gen-div" role="alert">
                                    <div id="gen-msg"></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" name="ac_submit" id="ac_submit" class="btn btn-block btn-primary">
                                    <span class="fa fa-save"></span> Update
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

	<div class="backdropFixed">
    	<div class="modal fade" id="bioModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="" name="EprenticeAboutInfo" novalidate>
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="form-group">
                            <label for="bio">About me:</label>
                            <textarea class="form-control" rows="8" placeholder="About me..." name="bio" id="bio">{$user['Bio']}</textarea>
                            <small class="help-block">Enter About me within max 500 characters.<strong class="pull-right"><span id="biolength"></span></strong></small>
                        </div>                                    
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-9">
                                <div style="padding-top:6px;padding-bottom:6px;text-align:left;" class="alert alert-danger alert-dismissible" id="bio-div" role="alert">
                                    <div id="bio-msg"></div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" name="bio_submit" id="bio_submit" class="btn btn-block btn-primary" ng-click="submit($event)">
                                    <span class="fa fa-save"></span> Update
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>

	<div class="backdropFixed">
    	<div class="modal fade" id="expModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="" id="ePrenticeExperience">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <div class="form-group" style="margin-top: 20px;">
                            <label for="domain">Domains:</label>
                            <input class="form-control" name="domain" id="domain" type="text" value="{$user['Domain']}" placeholder="Domains" />
                            <small class="help-block">Enter multiple domains separated by comma (,).</small>
                        </div>
                        <div class="form-group">
                            <label for="projects">Projects:</label>
                            <input class="form-control" name="projects" id="projects" type="text" value="{$user['Projects']}" placeholder="Projects" />
                            <small class="help-block">Enter multiple projects separated by comma (,).</small>
                        </div>
                        <div class="form-group">
                            <label for="experience">Experience: </label>
                            <input class="form-control" name="experience" id="experience" type="text" value="{$user['Experience']}" placeholder="Total Experience" />
                            <small class="help-block">Enter total experience you have in months (e.g. 36).</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-sm-9"></div>
                            <div class="col-sm-3">
                                <button type="submit" name="exp_submit" id="exp_submit" class="btn btn-block btn-primary"><span class="fa fa-save"></span> Update</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
{/if}
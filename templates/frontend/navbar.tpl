<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ep-main-nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{$base_url}/"><img src="{$asset_url}/images/logo.png" alt="ePrentice" height="50" /></a>
        </div>
        <div class="collapse navbar-collapse" id="ep-main-nav">
            <ul class="nav navbar-nav navbar-right">
            	<!--<li class="top-datetime">
                    <div id="DateTimeContainer"></div>
                </li>-->
            	{if !empty($User_ID)}
                    <li class="dropdown notifications">
                        <a href="#" class="dropdown-toggle notification-btn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <center id="notification-center">
                            {assign var=totalNotification value=totalNotification($User_ID, 'Unread')}
                            {if $totalNotification > 0}
                                <i class="notification-count">{$totalNotification}</i>
                            {elseif $totalNotification > 99}
                                <i class="notification-count">99+</i>
                            {/if}
                            </center>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="notification-heading">
                                <h4 class="menu-title">
                                    Notifications
                                    <small class="pull-right">
                                        <a href="#" title="Mark All as Read" id="mark_all_read" rel="{$User_ID}">
                                            <i class="fa fa-eye"></i> Mark All as Read
                                        </a> 
                                    </small>
                                </h4>
                            </li>
                            <li class="divider"></li>
                            <li class="notifications-wrapper">
                                <div class="nano">
                                    <div class="nano-content" id="notification-zone"></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li class="notification-footer">
                                <a href="{$base_url}/notifications/">See All <i class="fa fa-arrow-right"></i></a>
                            </li>
                        </ul>
                    </li>
                {/if}                
                <li>
                    <form class="navbar-form">
                        <input type="text" class="form-control" /><i class="fa fa-search"></i>
                    </form>
                </li>
                {if !empty($User_ID)}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>{$User_Full_Name}</span> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{$base_url}/profile/{$Username}/"><i class="fa fa-pencil-square-o"></i> My Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{$base_url}/logout/"><i class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </li>
                {/if}
            </ul>
        </div>
    </div>
</nav>

{if !empty($User_ID)}
	<div id="wrapper" class="toggled-2">
{/if}
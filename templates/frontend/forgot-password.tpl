<div class="wrapper">
	<form class="form-signin" method="post" action="" id="forgot_password">
    	<div class="login-form">
            <h2 class="form-signin-heading">Forgot Password!</h2>
            <div class="form-group" id="forgot_password_success"></div>
            <div class="form-group" id="emailaddr_error"></div>
            <div class="form-group">
            	<input type="email" class="form-control" name="emailaddr" id="emailaddr" placeholder="Email Address" autofocus />
            </div>  
            <div class="form-group" id="confemail_error"></div>          
            <div class="form-group">
            	<input type="email" class="form-control" name="confemail" id="confemail" placeholder="Confirm Email Address" autofocus />
            </div>            
            <div class="form-group">
				<button class="btn btn-lg btn-info btn-block" type="button" name="submit" id="reset">Reset</button>
            </div>
        </div>
		<div class="social-acc">
			<div class="row">
            	<hr /><h4 class="form-signin-heading">Login with your Social Accounts</h4>
            	<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-facebook" href="{$fbLoginUrl}">
						<span class="fa fa-facebook"></span> Facebook
					</a>
				</div>
				<div class="col-sm-3"> 
					<a class="btn btn-block btn-social btn-google" href="{$gpLoginUrl}">
						<span class="fa fa-google"></span> Google
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-twitter" href="{$twLoginUrl}">
						<span class="fa fa-twitter"></span> Twitter
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-linkedin" href="{$liLoginUrl}">
						<span class="fa fa-linkedin"></span> Linkedin
					</a>
				</div><br /><br /><hr />
			</div>
		</div>
        <div class="register-now">
            <p>Already have an Account? <a class="btn btn-xs btn-success pull-right" href="{$base_url}/login/">Login</a></p>
			<p>Don't have an Account? <a class="btn btn-xs btn-success pull-right" href="{$base_url}/register/">Register</a></p>
		</div>        
	</form>
</div>
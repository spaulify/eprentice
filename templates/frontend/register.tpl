<div class="wrapper">
	<form class="form-signin" method="post" action="" name="EprenticeRegister" id="ePrenticeRegister">
    	<div class="login-form">
            <h2 class="form-signin-heading">Sign Up to ePrentice</h2>
            {if $error}
                <div class="alert alert-danger alert-dismissible AlertMessage" role="alert">
                	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                	{$error}
                </div>
            {/if}
            <input type="hidden" name="logwith" value="{$logwith}" />
            <div class="form-group">
            	<input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" />
            	<span id="fnameReg" title="{$fname}"></span>
            </div>
            <div class="form-group">
            	<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" style="border-radius:0;" />
                <span id="lnameReg" title="{$lname}"></span>
            </div>
            <div class="form-group">
            	<input type="email" class="form-control" name="email" id="email" placeholder="Email Address" style="border-radius:0;" />
                <span id="emailReg" title="{$email}"></span>
            </div>
            <div class="form-group">
            	<input type="password" class="form-control" name="password" id="password" placeholder="Password" />
			</div>
            {if !$refer}
            	<div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" name="role" id="role" {if $role!='viewer'} checked {/if} /> Join as a Trainer
                    </label>
                </div>
            {/if}
            <div class="form-group">
            	<button class="btn btn-lg btn-info btn-block" type="submit" name="submit" id="register">Register</button>
            </div>
        </div>
        <div class="social-acc">
            <div class="row">
                <h4 class="form-signin-heading">Register with your Social Accounts</h4>
                <div class="col-sm-3">
					<a class="btn btn-block btn-social btn-facebook" href="{$fbLoginUrl}">
						<span class="fa fa-facebook"></span> Facebook
					</a>
				</div>
				<div class="col-sm-3"> 
					<a class="btn btn-block btn-social btn-google" href="{$gpLoginUrl}">
						<span class="fa fa-google"></span> Google
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-twitter" href="{$twLoginUrl}">
						<span class="fa fa-twitter"></span> Twitter
					</a>
				</div>
				<div class="col-sm-3">
					<a class="btn btn-block btn-social btn-linkedin" href="{$liLoginUrl}">
						<span class="fa fa-linkedin"></span> Linkedin
					</a>
				</div><br /><br /><hr />
            </div>
        </div>
        <div class="register-now">
            <p>Already have an Account? <a class="btn btn-xs btn-success pull-right" href="{$base_url}/login/">Login</a></p>
        </div>
	</form>
</div>
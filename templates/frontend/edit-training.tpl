<div id="crop-avatar">
    <div id="page-content-wrapper">
        <div class="create-training-page">
            <div class="col-sm-12">
                <h1>Edit Training</h1>
                <p class="subtitle"></p>
                <div class="row">
                    <form method="post" action="{$ajax_url}/edit-training/" enctype="multipart/form-data" id="training_form" class="col-md-10 col-md-offset-1">
                        <div id="training_msg"></div>
                        <div data-wizard-init>
                            <ul class="steps">
                                <li data-step="1">Introduction Video</li>
                                <li data-step="2">About Training</li>
                                <li data-step="3">Training Video</li>
                                <li data-step="4">Training Tags</li>
                                <li data-step="5">Product Settings</li>
                            </ul>
                            <div class="steps-content">
                                <div data-step="1">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="hidden" name="page_type" id="page_type" value="Update" />
                                                <label for="training_name">Training Name: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your training name."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_name_error"></span>
                                                <input class="form-control" type="text" id="training_name" name="training_name" value="{$training.Training_Name}" placeholder="Enter your training name." />
                                            </div>
                                            <div class="form-group">
                                                <label for="training_cat">Training Category: <a href="#" data-toggle="tooltip" data-placement="right" title="Select training category."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_cat_error"></span>
                                                <select class="form-control" id="training_cat" name="training_cat">
                                                    <option {if $training.Training_Category==''} selected {/if} value="">Select Category</option>
                                                    {section name=i loop=$categories}
                                                        <option {if $training.Training_Category==$categories[i].Category_ID} selected {/if} value="{$categories[i].Category_ID}">
                                                            {$categories[i].Category_Name}
                                                        </option>
                                                    {/section}
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="training_link_text">Pre-Training CTA Name: <a href="#" data-toggle="tooltip" data-placement="right" title="e.g. “Join Training”"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_link_text_error"></span>
                                                <input class="form-control" type="text" id="training_link_text" name="training_link_text"  value="{$training.Join_Training_Btn}" placeholder="e.g. “Join Training”" />
                                            </div>
                                            <div class="form-group">
                                                <label for="intro_video">Introduction Video: 
                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a MP4|WebM|OGG|MOV video with max length of 120 seconds or enter youtube video link.">
                                                        <small class="glyphicon glyphicon-info-sign"></small>
                                                    </a>
                                                </label>
                                                {if preg_match($YTRegex, $training.Intro_Video)}
                                                    <label class="checkbox-inline"><input type="radio" name="video_type" value="custom"> Custom Video</label>
                                                    <label class="checkbox-inline"><input type="radio" checked="checked" name="video_type" value="youtube"> YouTube Video</label>
                                                {else}
                                                    <label class="checkbox-inline"><input type="radio" checked="checked" name="video_type" value="custom"> Custom Video</label>
                                                    <label class="checkbox-inline"><input type="radio" name="video_type" value="youtube"> YouTube Video</label>
                                                {/if}
                                                <span class="pull-right error-msg" id="intro_video_error"></span>
                                                {if preg_match($YTRegex, $training.Intro_Video)}
                                                    <input class="form-control hide" type="file" id="intro_video" name="intro_video" accept="video/*" style="height:auto;" />
                                                    <input type="hidden" name="old_intro_video" />
                                                    <input class="form-control" type="text" id="intro_video_youtube" name="intro_video_youtube" placeholder="Enter YouTube Video Link" value="{$training.Intro_Video}" />
                                                {else}
                                                    <input class="form-control" type="file" id="intro_video" name="intro_video" accept="video/*" style="height:auto;" />
                                                    <input type="hidden" name="old_intro_video" value="{$training.Intro_Video}" />
                                                    <input class="form-control hide" type="text" id="intro_video_youtube" name="intro_video_youtube" placeholder="Enter YouTube Video Link" />
                                                {/if}
                                            </div>
                                        </div>
                                        <div class="col-sm-6 text-center">
                                            {if preg_match($YTRegex, $training.Intro_Video)}
                                                {assign var=videoID value=get_youtube_id_from_url($training.Intro_Video)}
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item hide" id="IntroVideoPlayer" controls></video>
                                                    <iframe class="embed-responsive-item" id="IntroVideoiFrame" src="https://www.youtube.com/embed/{$videoID}?showinfo=0" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            {else}
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" id="IntroVideoPlayer" controls>
                                                        <source src="{$base_url}/uploads/trp/{$training.Intro_Video}" type="video/mp4">
                                                        <source src="{$base_url}/uploads/trp/{$training.Intro_Video}" type="video/ogg">
                                                        <source src="{$base_url}/uploads/trp/{$training.Intro_Video}" type="video/webm">
                                                    </video>
                                                    <iframe class="embed-responsive-item hide" id="IntroVideoiFrame" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            {/if}
                                            <div id="IntroVideoMessage"></div>
                                        </div>
                                    </div>
                                </div>
                                <div data-step="2">
                                    <div class="form-group">
                                        <label for="training_headline">Training Headline: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training headline here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                        <span class="pull-right error-msg" id="training_headline_error"></span>
                                        <input type="text" class="form-control" name="training_headline" id="training_headline" value="{$training.Training_Headline}" placeholder="Enter training headline here." />
                                    </div> 
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="training_point_one">Training Bullet Point #1: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training point here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_point_one_error"></span>
                                                <input type="text" class="form-control" name="training_point_one" id="training_point_one" value="{$training.Training_Point_One}" placeholder="Enter training point here." />
                                            </div>
                                            <div class="form-group">
                                                <label for="training_point_two">Training Bullet Point #2: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training point here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_point_two_error"></span>
                                                <input type="text" class="form-control" name="training_point_two" id="training_point_two" value="{$training.Training_Point_Two}" placeholder="Enter training point here." />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">                                    
                                            <div class="form-group">
                                                <label for="training_point_three">Training Bullet Point #3: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training point here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_point_three_error"></span>
                                                <input type="text" class="form-control" name="training_point_three" id="training_point_three" value="{$training.Training_Point_Three}" placeholder="Enter training point here." />
                                            </div>
                                            <div class="form-group">
                                                <label for="training_point_four">Training Bullet Point #4: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training point here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_point_four_error"></span>
                                                <input type="text" class="form-control" name="training_point_four" id="training_point_four" value="{$training.Training_Point_Four}" placeholder="Enter training point here." />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="about_training">About Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter training description here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                        <span class="pull-right error-msg" id="about_training_error"></span>
                                        <small class="pull-right" id="about_counter"></small>
                                        <textarea class="form-control" rows="5" name="about_training" id="about_training" placeholder="Enter training description here.">{$training.Training_Desc}</textarea>
                                    </div>
                                </div>
                                <div data-step="3">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="training_video">Training Video: 
                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Supported video format: MP4, WebM, OGG, MOV or Enter a Youtube Video Link">
                                                        <small class="glyphicon glyphicon-info-sign"></small>
                                                    </a>
                                                </label>
                                                {if preg_match($YTRegex, $training.Training_Video)}
                                                    <label class="checkbox-inline"><input type="radio" name="training_video_type" value="custom"> Custom Video</label>
                                                    <label class="checkbox-inline"><input type="radio" checked="checked" name="training_video_type" value="youtube"> YouTube Video</label>
                                                {else}
                                                    <label class="checkbox-inline"><input type="radio" checked="checked" name="training_video_type" value="custom"> Custom Video</label>
                                                    <label class="checkbox-inline"><input type="radio" name="training_video_type" value="youtube"> YouTube Video</label>
                                                {/if}
                                                <span class="pull-right error-msg" id="training_video_error"></span>
                                                {if preg_match($YTRegex, $training.Training_Video)}
                                                    <input class="form-control hide" type="file" id="training_video" name="training_video" accept="video/*" style="height:auto;" />
                                                    <input type="hidden" name="old_training_video" />
                                                    <input class="form-control" type="text" id="training_video_youtube" name="training_video_youtube" placeholder="Enter YouTube Video Link" value="{$training.Training_Video}" />
                                                {else}
                                                    <input class="form-control" type="file" id="training_video" name="training_video" accept="video/*" style="height:auto;" />
                                                    <input type="hidden" name="old_training_video" value="{$training.Training_Video}" />
                                                    <input class="form-control hide" type="text" id="training_video_youtube" name="training_video_youtube" placeholder="Enter YouTube Video Link" />
                                                {/if}
                                            </div>
                                            <div class="form-group">
                                                <label for="training_thumb">Training Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Thumbnail Image should be maximum 800x450 and JPG or PNG format"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="training_thumb_error"></span>
                                                <input type="hidden" name="old_training_video_thumb" value="{$training.Training_Thumb}" />
                                                <!--<input class="form-control" type="file" id="training_thumb" name="training_thumb" accept="images/*" style="height:auto;" />-->
                                                <input class="form-control" type="hidden" id="training_thumb" name="training_thumb" value="" />
                                                <div class="avatar-view">
                                                    <a href="javascript:;" class="btn-thumb-upload">Click To Upload Thumbnail</a>
                                                </div>
                                                <!-- Loading state -->
                                                <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                                <!-- Loading state -->
                                            </div>
                                            <div class="form-group">
                                                <img height="93px" src="{$base_url}/uploads/trp/{$training.Training_Thumb}" alt="" id="TrainingThumbImage" />
                                            </div>
                                            <div class="form-group">
                                                <label for="visible_time">CTA Visible Time: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter or Seek your video to set the time in second."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                                <span class="pull-right error-msg" id="visible_time_error"></span>
                                                <input class="form-control" type="number" id="visible_time" name="visible_time" step="1" min="0" max="0" value="{$training.CTA_Time}" placeholder="Enter or Seek your video to set the time in second." />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 text-center">
                                            {if preg_match($YTRegex, $training.Training_Video)}
                                                {assign var=videoID value=get_youtube_id_from_url($training.Training_Video)}
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item hide" id="TrainingVideoPlayer" controls></video>
                                                    <iframe class="embed-responsive-item" id="TrainingVideoiFrame" src="https://www.youtube.com/embed/{$videoID}?showinfo=0" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            {else}
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" id="TrainingVideoPlayer" controls>
                                                        <source src="{$base_url}/uploads/trp/{$training.Training_Video}" type="video/mp4">
                                                        <source src="{$base_url}/uploads/trp/{$training.Training_Video}" type="video/ogg">
                                                        <source src="{$base_url}/uploads/trp/{$training.Training_Video}" type="video/webm">
                                                    </video>
                                                    <iframe class="embed-responsive-item hide" id="TrainingVideoiFrame" frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            {/if}
                                            <div id="TrainingVideoMessage"></div>
                                        </div>
                                    </div>
                                </div>
                                <div data-step="4">
                                    <div class="form-group">
                                        <label for="training_tags">Training Tags: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter multiple tags separated by comma (,). Only 3 tags allowed."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                        <span class="pull-right error-msg" id="training_tags_error"></span>
                                        <textarea class="form-control" rows="5" name="training_tags" id="training_tags" placeholder="Enter multiple tags separated by comma (,). Only 3 tags allowed.">{$training.Training_Tags}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="similar_training">Would you like to be an affiliate of ePrentice? <a href="#" data-toggle="tooltip" data-placement="right" title="Want to display similar trainings section in your training page?"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                        <span class="pull-right error-msg" id="similar_training_error"></span>
                                        <select name="similar_training" id="similar_training" class="form-control">
                                            <option value="">-Select-</option>
                                            <option {if $training.Similar_Section=='yes'} selected {/if} value="yes">Yes, I want to display</option>
                                            <option {if $training.Similar_Section=='no'} selected {/if} value="no">No, I don't want</option>
                                            <option {if $training.Similar_Section=='other'} selected {/if} value="other">I want to display other trainings</option>
                                        </select>
                                    </div>
                                </div>
                                <div data-step="5">
                                    {if $products!=""}
                                        <div class="form-group">
                                            <p>You can setup a CTA button under the training video whenever you want.</p>
                                            <label for="cta_text">Buy Button Text: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter CTA button name. Alphabets and Special characters only!"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                            <span class="pull-right error-msg" id="cta_text_error"></span>
                                            <input class="form-control" type="text" id="cta_text" name="cta_text" value="{$training.CTA_Text}" placeholder="Enter CTA button name." />
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" id="products_count" name="products_count" value="Yes" />
                                            <label for="product_list_select">Product List: <a href="#" data-toggle="tooltip" data-placement="right" title="Select product from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                            <span class="pull-right error-msg" id="product_list_select_error"></span>                    	                    	
                                            <select class="form-control" id="product_list_select" name="product_list_select" required>
                                                <option {if $training.Product_ID==''} selected {/if} value="">Select Product</option>
                                                {section name=i loop=$products}
                                                    <option {if $training.Product_ID==$products[i].Product_ID} selected {/if} value="{$products[i].Product_ID}">{$products[i].Product_Name}</option>
                                                {/section}
                                            </select>
                                        </div>
                                        <div class="form-group" id="product_wrapper" style="min-height:300px;">
                                            {assign var=products value=get_product($training.Product_ID)}
                                            {section name=i loop=$products}
                                                <div class="row btnwrap" id="product_details">
                                                    <div class="well">
                                                        <div class="col-sm-4">
                                                            <img id="product_image" alt="" class="img-responsive" src="{$base_url}/uploads/prd/{$products[i].Product_Image}" />
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <h2 id="product_name">{$products[i].Product_Name}</h2>
                                                            <p id="product_desc">{$products[i].Product_Desc}</p>
                                                            <p>
                                                                <b>Price:</b>
                                                                <span id="product_price">${$products[i].Product_Price}</span>
                                                            </p>
                                                            <p><a class="btn btn-primary" target="_blank" href="{$products[i].Product_Link}" id="product_link">View Product</a></p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            {/section}
                                        </div>
                                    {else}
                                        <div class="form-group">
                                            <input type="hidden" id="products_count" name="products_count" value="No" />
                                            <h2>You have no Product! <a href="{$base_url}/create-product/">Click</a> to add new Product before continue!</h2>
                                        </div>
                                    {/if}
                                    <div class="form-group">
										<label for="publish_datetime">Publish Date & Time: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
										<div class="input-group date" id="publish_datetime_div">
											<input type="text" class="form-control" id="publish_datetime" name="publish_datetime" value="{$training.Training_Published_On}" placeholder="Select publish date and time for the training." />
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>
										</div>
										<span class="pull-right error-msg" id="publish_datetime_error"></span>
									</div>
                                   <input type="hidden" name="old_publish_time" id="old_publish_time" value="{$training.Training_Published_On}" />
                                    <input type="hidden" name="training_id" id="training_id" value="{$training.Training_ID}" />
                                </div>
                                <div id="progressbox">
                                    <div id="progressbar"></div>
                                    <div id="statustxt">0%</div>
                                </div>
                            </div>
                        </div>
                    </form>                
                </div>
            </div>
        </div>
    </div>
    <!-- Cropping modal -->
    <div class="backdropFixed">
    	<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">    
            <div class="modal-content">
                <form class="avatar-form" action="{$ajax_url}/crop-image/" enctype="multipart/form-data" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" id="avatar-modal-label">Upload Training Thumbnail</h4>
                    </div>
                    <div class="modal-body">
                        <div class="avatar-body">
                            <!-- Upload image and data -->
                            <div class="avatar-upload">
                                <input type="hidden" class="avatar-src" name="avatar_src" />
                                <input type="hidden" class="avatar-data" name="avatar_data" />
                                <label for="avatarInput">Select File: <a href="#" data-toggle="tooltip" data-placement="right" title="Thumbnail Image should be maximum 800x450 and JPG or PNG format"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <input type="file" class="avatar-input" id="avatarInput" name="avatar_file" />
                            </div>
                            <!-- Crop and preview -->
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="avatar-wrapper"></div>
                                </div>
                                <div class="col-md-3">
                                    <div class="avatar-preview preview-lg"></div>
                                </div>
                            </div>                
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row avatar-btns">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                 </form>
            </div>
        </div>
    </div>
    </div>
    <!-- Cropping modal -->
</div>
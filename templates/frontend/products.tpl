<div id="page-content-wrapper">
	<div class="products-page">
        <div class="col-sm-12">
        	<h1>Your Products</h1>
            <div class="row">
                {if $products}
                    {section name=i loop=$products}
                    	<div class="col-sm-12">
                            <div class="product-row btnwrap">
                                <div class="col-sm-4">
                                    <img src="{$base_url}/uploads/prd/{$products[i].Product_Image}" alt="" class="img-responsive" />
                                </div>
                                <div class="col-sm-8">
                                    <h2>
                                        {$products[i].Product_Name}
                                        <a style="margin:5px;" href="{$base_url}/edit-product/{$products[i].Product_ID}/" class="btn btn-xs btn-primary edit-btn pull-right">
                                            <span class="fa fa-edit"></span> Edit
                                        </a>                                    
                                    </h2>
                                    <p>{$products[i].Product_Desc}</p>
                                    <p>
                                        <b>Trainer:</b>
                                        <i>
                                            {assign var=trainer value=get_trainer_name($products[i].Trainer_ID)}
                                            {section name=n loop=$trainer}
                                                {$trainer[n].Full_Name}
                                            {/section}
                                        </i>
                                    </p>
                                    <p>
                                        <b>Price:</b>
                                        <span>${$products[i].Product_Price}.00</span>
                                    </p>
                                    {assign var=product_url value=modify_http($products[i].Product_Link)}
                                    <p><a class="btn btn-primary" target="_blank" href="{$product_url}">View Product</a></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    {/section}
                {else}
                    <div class="col-sm-12">
                        <p class="no-items">No products is available in the list.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
<div id="page-content-wrapper">
	<div class="training-page">
        <div class="col-sm-4 hide">
            <div class="row">
                {assign var=trainer value=get_trainer_name($training.Trainer_ID)}
                {section name=n loop=$trainer}
                    <div class="profile-sec">
                    	<a href="{$base_url}/profile/{$trainer[n].Username}/">
	                        <img src="{$trainer[n].Profile_Photo}" class="img-circle" alt="{$trainer[n].Full_Name}" width="170" height="170" />
                        </a>
                        <h3>{$trainer[n].Full_Name}</h3>
                        <p class="small text-muted">{$trainer[n].Designation}</p>
                        <p>{$trainer[n].Bio}</p>
                        <p class="social-icons icon-circle">
                            {if !empty($trainer[n].Facebook_URL)}
                                <a href="{$trainer[n].Facebook_URL}" target="_blank"><i class="fa fa-facebook"></i></a>
                            {/if}
                            {if !empty($trainer[n].Twitter_URL)}
                                <a href="{$trainer[n].Twitter_URL}" target="_blank"><i class="fa fa-twitter"></i></a>
                            {/if}
                            {if !empty($trainer[n].Google_URL)}
                                <a href="{$trainer[n].Google_URL}" target="_blank"><i class="fa fa-google-plus"></i></a>
                            {/if}
                            {if !empty($trainer[n].LinkedIn_URL)}
                                <a href="{$trainer[n].LinkedIn_URL}" target="_blank"><i class="fa fa-linkedin"></i></a>
                            {/if}
                        </p>
                        {if $User_Role=='viewer'}
                        	{assign var=followdata value=follow($trainer[n].User_ID,$User_ID)}
                			{if $followdata=='following'}
                        		<p><a href="#" class="btn follow following followee-{$trainer[n].User_ID}">+ Unfollow</a></p>
                            {else}
                            	<p><a href="#" class="btn follow followee-{$trainer[n].User_ID}">+ Follow</a></p>
                            {/if}
                            <input type="hidden" name="Followee_ID" value="{$trainer[n].User_ID}" />
                            <input type="hidden" name="Follower_ID" value="{$User_ID}" />
                            <input type="hidden" name="User_Role" value="{$User_Role}" />
                            <div class="follow_msg"></div>
                        {/if}
                    </div>
                {/section}
            </div>
        </div>

        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <div class="training-video">
                    <div class="video-wrap">
                    	<div class="embed-responsive embed-responsive-16by9">
                            {if !empty($User_ID)}
                                {if preg_match($YTRegex, $training.Training_Video)}
                                    {assign var=videoID value=get_youtube_id_from_url($training.Training_Video)}
                                    <div class="embed-responsive-item" id="training_youtube_player"></div>
                                    <input type="hidden" id="trainingVideoID" value="{$videoID}" />
                                    <input type="hidden" id="CTA_Time" value="{$training.CTA_Time}" />
                                    <input type="hidden" id="Video_SRC" value="{$training.Training_Video}" />
                                    <input type="hidden" id="Training_ID" value="{$training.Training_ID}" />
                                    <input type="hidden" id="Trainer_ID" value="{$training.Trainer_ID}" />
                                    <input type="hidden" id="Category_ID" value="{$training.Training_Category}" />
                                    <input type="hidden" id="Member_ID" value="{$User_ID}" />
                                    <input type="hidden" id="User_Type" value="{$User_Role}" />
                                {else}
                                    <div class="embed-responsive-item flowplayer is-poster" id="training_player" style="background:#F7F7F7 url({$base_url}/uploads/trp/{$training.Training_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56"></div>
                                    <input type="hidden" id="CTA_Time" value="{$training.CTA_Time}" />
                                    <input type="hidden" id="Video_SRC" value="{$base_url}/uploads/trp/{$training.Training_Video}" />
                                    <input type="hidden" id="Training_ID" value="{$training.Training_ID}" />
                                    <input type="hidden" id="Trainer_ID" value="{$training.Trainer_ID}" />
                                    <input type="hidden" id="Category_ID" value="{$training.Training_Category}" />
                                    <input type="hidden" id="Member_ID" value="{$User_ID}" />
                                    <input type="hidden" id="User_Type" value="{$User_Role}" />
                                {/if}
                            {else}
                                {if preg_match($YTRegex, $training.Intro_Video)}
                                    {assign var=videoID value=get_youtube_id_from_url($training.Intro_Video)}
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{$videoID}?showinfo=0&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
                                {else}
                                    <div class="embed-responsive-item flowplayer is-poster" style="background:#F7F7F7 url({$base_url}/uploads/trp/{$training.Training_Thumb}) center center / cover;" data-swf="{$asset_url}/flowplayer-6.0.5/flowplayer.swf" data-key="$863732616083910" data-ratio="0.56">
                                        <video>
                                            <source type="video/webm"  src="{$base_url}/uploads/trp/{$training.Intro_Video}">
                                            <source type="video/mp4"   src="{$base_url}/uploads/trp/{$training.Intro_Video}">
                                            <source type="video/flash" src="{$base_url}/uploads/trp/{$training.Intro_Video}">
                                            <source type="video/ogg" src="{$base_url}/uploads/trp/{$training.Intro_Video}">
                                            <source type="video/quicktime" src="{$base_url}/uploads/trp/{$training.Intro_Video}">
                                        </video>
                                    </div>
                                {/if}
	                        {/if}
                        </div>
                    </div>
                    <div class="video-desc">
                        {if empty($User_ID)}
                            <h2>{$training.Training_Headline} <small class="post-time">{$training.Created_On|date_format:"%d %b, %Y at %H:%M"}</small></h2>
                            <ul class="list-unstyled">
                                <li><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;{$training.Training_Point_One}</li>
                                <li><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;{$training.Training_Point_Two}</li>
                                <li><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;{$training.Training_Point_Three}</li>
                                <li><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;{$training.Training_Point_Four}</li>
                            </ul>
                        {else}     
                            <h2>{$training.Training_Name} <small class="post-time">{$training.Created_On|date_format:"%d %b, %Y at %H:%M"}</small></h2>
                            <p>{$training.Training_Desc}</p>
                        {/if}
                    </div>
                    <div class="video-footer">
                        {if empty($User_ID)}
                            <script type="text/javascript">
                            $(window).load(function(){
                                $('#myModal-3').modal('show');
                            });
                            </script>
                            <button id="video-btn" class="btn" data-toggle="modal" data-target="#myModal-3">{$training.Join_Training_Btn}</button>
                            <div class="backdropFixed">
                            	<div class="modal fade" data-backdrop="static" id="myModal-3" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <form action="" method="post" class="form-horizontal" id="registerModalForm">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Register for the Training</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="fname" class="col-sm-3 control-label">First Name:</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="lname" class="col-sm-3 control-label">Last Name:</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control" id="lname" name="lname" placeholder="First Name" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="email" class="col-sm-3 control-label">Email ID:</label>
                                                        <div class="col-sm-9">
                                                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <span class="pull-left">
                                                        Already have an Account? <a href="{$base_url}/login/?return={$base_url}/training/{$training.Training_URL}/">Login</a>
                                                    </span>
                                                    <button type="submit" name="MemberSubmit" class="btn btn-default">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {else}
                            {assign var=prodCheck value=check_product($User_ID, $training.Product_ID)}
                            {if $prodCheck=='No' && $User_Role=='viewer'}
                                <button class="btn hide" id="CTA_Button" data-toggle="modal" data-target="#myModal">{$training.CTA_Text}</button>
                            {/if}
                            {if $prodCheck=='Yes'}
                                <a class="btn" href="{$training.Product_Link}" target="_blank">View Product</a>
                            {/if}
                        {/if}
                    </div>
                </div>
                <div id="payment_msg"></div>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
	<div id="myModal" class="modal fade payment-modal" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center">Pay with Credit card or Debit card</h4>
            </div>
            <div class="modal-body">
                <div id="payment-header demo-container">    
                    <div id="panel-default" class="panel panel-default credit-card-box">
                        <div id="panel-heading" class="panel-heading display-table" >
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">Pay Now</h3>
                                <div class="display-td">
                                    <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                </div>
                            </div>
                        </div>
                        <div class="panel-body form-container active">
                        	<div class="row">
                            	<div class="card-wrapper"></div><br/>
                            </div>
                            <form role="form" id="payment-form" method="post" action="" autocomplete="off">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="CardNumber">CARD NUMBER</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="number" id="CardNumber" placeholder="Card Number" required />
                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="CardExpiryYY">Expiration</label>
                                            <input type="text" class="form-control" name="expiry" id="CardExpiry" placeholder="MM/YY" required />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="CardCVV">CVV CODE</label>
                                            <input type="password" class="form-control" name="cvc" id="CardCVC" placeholder="CVC" required />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="NameOnCard">NAME ON CARD</label>
                                            <input type="text" class="form-control" name="name" id="NameOnCard" placeholder="CARD HOLDER NAME" required />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                    	{assign var=products value=get_product($training.Product_ID)}
                                        {section name=i loop=$products}                                            
                                            <input type="hidden" name="product_id" id="product_id" value="{$products[i].Product_ID}" />
                                            <input type="hidden" name="product_name" id="product_name" value="{$products[i].Product_Name}" />
                                            <input type="hidden" name="product_price" id="product_price" value="{$products[i].Product_Price}" />
                                            <input type="hidden" name="product_link" id="product_link" value="{$products[i].Product_Link}" />
                                        {/section}
                                        <input type="hidden" name="trainer_id" id="trainer_id" value="{$training.Trainer_ID}" />
                                        <input type="hidden" name="training_url" id="training_url" value="{$training.Training_URL}" />
                                        <input type="hidden" name="training_thumb" id="training_thumb" value="{$training.Training_Thumb}" />
                                        <button id="transaction-btn" name="payment_submit" class="btn btn-primary btn-lg btn-block" type="submit">Process Transaction Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="payment_error"></div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="page-content-wrapper">
	<div class="create-promotion-page">
        <div class="col-sm-12">
        	<h1>Create New Promotion</h1>
            <p class="subtitle">Just fill the below form to create your new promotion.</p>
            <div class="row">
                <form method="post" action="{$ajax_url}/new-promotion/" enctype="multipart/form-data" id="promotion_form" class="col-md-10 col-md-offset-1">
                    <div id="promotion_msg"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="page_type" id="page_type" value="Insert" />
                                <label for="promotion_type">Promotion Type: <a href="#" data-toggle="tooltip" data-placement="right" title="Select from the list of what type of promotion you want. e.g. 'Video Promotion', 'Questionnaire'."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_type_error"></span>
                                <select class="form-control" id="promotion_type" name="promotion_type">
                                    <option value="">Select Promotion Type</option>
                                    <option value="Image">Picture Promotion</option>
                                    <option value="Video">Video Promotion</option>
                                    <option value="Question">Questionnaire Promotion</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="ImageBlock" style="display:none;">
                        <div class="col-sm-12">
                            <h2 class="text-center">You have selected "Picture Promotion"</h2>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_img">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_img"></span>
                                <input class="form-control" type="text" id="promotion_title_img" name="promotion_title_img" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_img">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_img"></span>
                                <select class="form-control" id="training_id_img" name="training_id_img">
                                    <option value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_img">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_img"></span>
                                <input class="form-control promotion_thumb_img" type="file" id="promotion_thumb_img" name="promotion_thumb_img" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group promotion_img">
                                <label for="promotion_file_img">Promotion Images: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <a href="javascript:;" id="add_promotion_img" class="pull-right"><small class="glyphicon glyphicon-plus-sign"></small></a>
                                <span class="pull-right error-msg" id="promotion_file_error_img"></span>
                                <input class="form-control promotion_file_img" type="file" id="promotion_file_img" name="promotion_file_img[]" accept="image/*" style="height:auto;" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_img">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_img_div">
                                    <input type="text" class="form-control" id="promo_start_date_img" name="promo_start_date_img" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_img"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_img">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_img_div">
                                    <input type="text" class="form-control" id="promo_end_date_img" name="promo_end_date_img" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_img"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="promotion_desc_img">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_img" name="promotion_desc_img" rows="8" placeholder="Enter your promotion description within 500 characters max."></textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error_img"></span>
                                <p class="help-block">
                                    <strong class="pull-right"><span id="promo_desc_length_img"></span></strong>
                                </p>                    
                            </div>
                        </div>
                        <div class="col-sm-12"><hr /></div>
                        <div class="col-sm-12">
                            <div class="form-group">                            	
                                <button class="btn btn-primary" id="promotion_submit_img" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="VideoBlock" style="display:none;">
                        <div class="col-sm-12">
                            <h2 class="text-center">You have selected "Video Promotion"</h2>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_vid">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_vid"></span>
                                <input class="form-control" type="text" id="promotion_title_vid" name="promotion_title_vid" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_vid">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_vid"></span>
                                <select class="form-control" id="training_id_vid" name="training_id_vid">
                                    <option value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_vid">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_vid"></span>
                                <input class="form-control promotion_thumb_vid" type="file" id="promotion_thumb_vid" name="promotion_thumb_vid" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group promotion_vid">
                                <label for="promotion_file_vid">Promotion Videos: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a MP4|WebM|OGG|MOV video with max length of 120 seconds."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <a href="javascript:;" id="add_promotion_vid" class="pull-right"><small class="glyphicon glyphicon-plus-sign"></small></a>
                                <span class="pull-right error-msg" id="promotion_file_error_vid"></span>
                                <input class="form-control promotion_file_vid" type="file" id="promotion_file_vid" name="promotion_file_vid[]" accept="video/*" style="height:auto;" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_vid">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_vid_div">
                                    <input type="text" class="form-control" id="promo_start_date_vid" name="promo_start_date_vid" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_vid"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_vid">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_vid_div">
                                    <input type="text" class="form-control" id="promo_end_date_vid" name="promo_end_date_vid" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_vid"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="promotion_desc">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_vid" name="promotion_desc_vid" rows="8" placeholder="Enter your promotion description within 500 characters max."></textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error_vid"></span>
                                <p class="help-block">
                                    <strong class="pull-right"><span id="promo_desc_length_vid"></span></strong>
                                </p>                    
                            </div>
                        </div>
                        <div class="col-sm-12"><hr /></div>
                        <div class="col-sm-12">
                            <div class="form-group">                            	
                                <button class="btn btn-primary" id="promotion_submit_vid" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="QuestionBlock" style="display:none;">
                        <div class="col-sm-12">
                            <h2 class="text-center">You have selected "Questionnaire Promotion"</h2>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_que">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_que"></span>
                                <input class="form-control" type="text" id="promotion_title_que" name="promotion_title_que" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_que">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_que"></span>
                                <select class="form-control" id="training_id_que" name="training_id_que">
                                    <option value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_que">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_que"></span>
                                <input class="form-control promotion_thumb_que" type="file" id="promotion_thumb_que" name="promotion_thumb_que" accept="image/*" style="height:auto;" />
                            </div>                
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_que">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_que_div">
                                    <input type="text" class="form-control" id="promo_start_date_que" name="promo_start_date_que" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_que"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_que">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_que_div">
                                    <input type="text" class="form-control" id="promo_end_date_que" name="promo_end_date_que" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_que"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="promotion_desc_que">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_que" name="promotion_desc_que" rows="4" placeholder="Enter your promotion description within 500 characters max."></textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error_que"></span>
                                <p class="help-block">
                                    <strong class="pull-right"><span id="promo_desc_length_que"></span></strong>
                                </p>                    
                            </div>                
                        </div>
                        <div class="col-sm-12"><hr /></div>
                        <div class="question_set">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="promotion_question">Question: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your question here."><small class="glyphicon glyphicon-info-sign"></small></a>
                                    </label>
                                    <a href="javascript:;" id="add_promotion_que" class="pull-right">
                                        <small class="glyphicon glyphicon-plus-sign"></small>
                                    </a>
                                    <span class="pull-right error-msg" id="promotion_question_error"></span>
                                    <input class="form-control" type="text" id="promotion_question" name="promotion_question[]" placeholder="Enter your question here." />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="promotion_answer_one">Answer #1: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                    
                                    <input class="form-control" type="text" id="promotion_answer_one" name="promotion_answer_one[]" placeholder="Enter your Answer." />
                                    <span class="pull-right error-msg" id="promotion_answer_one_error"></span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="promotion_marks_one">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                    <input class="form-control" type="number" id="promotion_marks_one" name="promotion_marks_one[]" placeholder="25" />
                                    <span class="pull-right error-msg" id="promotion_marks_one_error"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="promotion_answer_two">Answer #2: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                    <input class="form-control" type="text" id="promotion_answer_two" name="promotion_answer_two[]" placeholder="Enter your Answer." />
                                    <span class="pull-right error-msg" id="promotion_answer_two_error"></span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="promotion_marks_two">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                    <input class="form-control" type="number" id="promotion_marks_two" name="promotion_marks_two[]" placeholder="25" />
                                    <span class="pull-right error-msg" id="promotion_marks_two_error"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="promotion_answer_three">Answer #3: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                    <input class="form-control" type="text" id="promotion_answer_three" name="promotion_answer_three[]" placeholder="Enter your Answer." />
                                    <span class="pull-right error-msg" id="promotion_answer_three_error"></span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="promotion_marks_three">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                    <input class="form-control" type="number" id="promotion_marks_three" name="promotion_marks_three[]" placeholder="25" />
                                    <span class="pull-right error-msg" id="promotion_marks_three_error"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="promotion_answer_four">Answer #4: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                    
                                    <input class="form-control" type="text" id="promotion_answer_four" name="promotion_answer_four[]" placeholder="Enter your Answer." />
                                    <span class="pull-right error-msg" id="promotion_answer_four_error"></span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="promotion_marks_four">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>                                    
                                    <input class="form-control" type="number" id="promotion_marks_four" name="promotion_marks_four[]" placeholder="25" />
                                    <span class="pull-right error-msg" id="promotion_marks_four_error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12"><hr /></div>
                        <div class="col-sm-12">
                            <div class="form-group">                            	
                                <button class="btn btn-primary" id="promotion_submit_que" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </div><br /><br /><br />
                </form>
            </div>
        </div>
    </div>
</div>
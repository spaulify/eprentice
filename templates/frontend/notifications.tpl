<div id="page-content-wrapper">
	<div class="notifications-page">
        <div class="col-sm-12">
            <h1>Notifications</h1>
            <div class="row">
            	{if $notifications!=''}                    
                    <div class="col-sm-12">
                    	<div class="full-notification">
                    		{section name=i loop=$notifications}                        	
                            	{if (strpos($notifications[i].Notification_Link, 'eprentice')===false)}
                                    <a href="{$notifications[i].Notification_Link}" target="_blank" rel="{$notifications[i].Notification_ID}" class="notification-link">
                                {else}
                                	<a href="{$notifications[i].Notification_Link}" rel="{$notifications[i].Notification_ID}" class="notification-link">
                                {/if}
                                    <div class="notification-item {$notifications[i].Notification_Status}">
                                        <div class="notification-image col-sm-1">
                                            <img src="{$notifications[i].Notification_Image}" alt="" width="80" />
                                        </div>
                                        <div class="notification-text col-sm-11">
                                            <h4 class="item-title">{$notifications[i].Notification_Text}</h4>
                                            {assign var=datetime value=dateDiff(strtotime($notifications[i].Date_Time))}                                        
                                            <p class="item-info"><i class="fa fa-clock-o"></i> {$datetime} ago</p>                                   
                                        </div>
                                    </div>
                                </a>                            
                        	{/section}
                        </div>
                    </div>
                {else}
                	<div class="col-sm-12">
                    	<div class="full-notification col-sm-12">
                            <div class="notification-item no-item">
                                <h4 class="item-title text-center">Currently, you have no notifications.</h4>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
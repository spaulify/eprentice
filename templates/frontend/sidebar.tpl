<div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
    	{assign var=current_page value=active_page()}
        {if $User_Role=='trainer'}
        	<li {if $current_page=='dashboard.php'}class="active"{/if}>
            	<a href="{$base_url}/dashboard/" data-parent="#menu">
                	<i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li {if $current_page=='products.php' || $current_page=='create-product.php'}class="active"{/if}>
            	<a href="#ProductMenu" data-toggle="collapse" data-parent="#menu">
                	<i class="fa fa-envelope-o"></i> <span>Products</span> <small class="fa fa-caret-down"></small>
                </a>
                <div class="collapse" id="ProductMenu">
                    <a href="{$base_url}/products/" {if $current_page=='products.php'}class="active"{/if}>
                    	<i class="fa fa-envelope-o"></i> <span>All Products</span>
                    </a>
                    <a href="{$base_url}/create-product/" {if $current_page=='create-product.php'}class="active"{/if}>
                    	<i class="fa fa-newspaper-o"></i> <span>Create Product</span>
                    </a>
                </div>
            </li>

            <li {if $current_page=='trainings.php' || $current_page=='create-training.php'}class="active"{/if}>
            	<a href="#TrainingMenu" data-toggle="collapse" data-parent="#menu">
                	<i class="fa fa-users"></i> <span>Trainings</span> <small class="fa fa-caret-down"></small>
                </a>
                <div class="collapse" id="TrainingMenu">
                    <a href="{$base_url}/trainings/" {if $current_page=='trainings.php'}class="active"{/if}>
                        <i class="fa fa-users"></i> <span>All Trainings</span>
                    </a>
                    <a href="{$base_url}/create-training/" {if $current_page=='create-training.php'}class="active"{/if}>
                        <i class="fa fa-file-photo-o"></i> <span>Create Training</span>
                    </a>
                </div>
            </li>
            
            <li {if $current_page=='send-invitation.php'}class="active"{/if}>
            	<a href="{$base_url}/send-invitation/">
                	<i class="fa fa-file-video-o"></i> <span>Bulk Invitation</span>
                </a>
            </li>
            
            <li {if $current_page=='promotions.php' || $current_page=='create-promotion.php'}class="active"{/if}>
            	<a href="#PromotionMenu" data-toggle="collapse" data-parent="#menu">
                	<i class="fa fa-music"></i> <span>Promotions</span> <small class="fa fa-caret-down"></small>
                </a>
                <div class="collapse" id="PromotionMenu">
                    <a href="{$base_url}/promotions/" {if $current_page=='promotions.php'}class="active"{/if}>
                    	<i class="fa fa-music"></i> <span>All Promotions</span>
                    </a>
                    <a href="{$base_url}/create-promotion/" {if $current_page=='create-promotion.php'}class="active"{/if}>
                    	<i class="fa fa-cogs"></i> <span>Create Promotion</span>
                    </a>
                </div>
            </li>
            
            <li {if $current_page=='social-campaign.php'}class="active"{/if}>
            	<a href="{$base_url}/social-campaign/">
                	<i class="fa fa-share-square-o"></i> <span>Social Campaign</span>
                </a>
            </li>
        {else}
        	<li {if $current_page=='news.php'}class="active"{/if}>
            	<a href="{$base_url}/news/">
                	<i class="fa fa-rss"></i> <span>News Feeds</span>
                </a>
            </li>
            
        	<li {if $current_page=='training-programs.php'}class="active"{/if}>
            	<a href="{$base_url}/training-programs/">
                	<i class="fa fa-list"></i> <span>Training Programs</span>
                </a>
            </li>
            
            <li {if $current_page=='set-preferences.php'}class="active"{/if}>
            	<a href="{$base_url}/set-preferences/">
                	<i class="fa fa-cogs"></i> <span>Set Preferences</span>
                </a>
            </li>
            
            <li {if $current_page=='ongoing-trainings.php'}class="active"{/if}>
            	<a href="{$base_url}/ongoing-trainings/">
                	<i class="fa fa-pause"></i> <span>On-Going Trainings</span>
                </a>
            </li>
            
            <li {if $current_page=='favorite-trainings.php'}class="active"{/if}>
            	<a href="{$base_url}/favorite-trainings/">
                	<i class="fa fa-heart"></i> <span>Favorite Trainings</span>
                </a>
            </li>
            
            <li {if $current_page=='following-trainers.php'}class="active"{/if}>
            	<a href="{$base_url}/following-trainers/">
                	<i class="fa fa-hand-o-right"></i> <span>Following Trainers</span>
                </a>
            </li>
            
            <li {if $current_page=='invite.php'}class="active"{/if}>
            	<a href="{$base_url}/invite/">
                	<i class="fa fa-facebook-official"></i> <span>Invite Friends</span>
                </a>
            </li>
            
            <li {if $current_page=='downline-members.php'}class="active"{/if}>
            	<a href="{$base_url}/downline-members/">
                	<i class="fa fa-user"></i> <span>Downline Members</span>
                </a>
            </li>
            
            <li {if $current_page=='withdraw-payment.php'}class="active"{/if}>
            	<a href="{$base_url}/withdraw-payment/">
                	<i class="fa fa-money"></i> <span>Withdraw Payment</span>
                </a>
            </li>
        {/if}
    </ul>
</div>
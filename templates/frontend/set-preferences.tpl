<div id="page-content-wrapper">
	<div class="set-preferences-page">
        <div class="col-sm-12">
            <h1>Set Preferences</h1>
            <div class="row">
                <form action="" id="set_preference_form" class="col-sm-6 col-sm-offset-3">
                    <div id="preferences_msg"></div>
                    <div class="row">
                    	<div class="col-sm-12">
                        	<h4 class="text-center">Choose Categories You Are Interested In</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <ul class="list-group">
                                            {section name=i loop=$categories start=0 max=$firstStep}
                                                <li class="list-group-item">
                                                    {$categories[i].Category_Name}
                                                    <div class="material-switch pull-right">
                                                        <input class="user_cat" id="user_cat_{$categories[i].Category_ID}" name="user_cat[]" type="checkbox" value="{$categories[i].Category_ID}" {if $categories[i].Category_ID|in_array:$pref_cat} checked="checked" {/if} />
                                                        <label for="user_cat_{$categories[i].Category_ID}" class="label-info"></label>
                                                    </div>
                                                </li>
                                            {/section}
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <ul class="list-group">
                                            {section name=i loop=$categories start=$firstStep max=$secondStep}
                                                <li class="list-group-item">
                                                    {$categories[i].Category_Name}
                                                    <div class="material-switch pull-right">
                                                        <input class="user_cat" id="user_cat_{$categories[i].Category_ID}" name="user_cat[]" type="checkbox" value="{$categories[i].Category_ID}" {if $categories[i].Category_ID|in_array:$pref_cat} checked="checked" {/if} />
                                                        <label for="user_cat_{$categories[i].Category_ID}" class="label-info"></label>
                                                    </div>
                                                </li>
                                            {/section}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <div class="form-group">
                                <label for="invitation_submit">&nbsp;</label><br/>
                                <input type="hidden" name="user_id" id="user_id" value="{$User_ID}" />
                                <a class="btn btn-success" type="button" id="set_preference" href="#">Submit</a>
                            </div>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="page-content-wrapper">
	<div class="create-promotion-page">
        <div class="col-sm-12">
        	<h1>Edit Promotion</h1>
            <p class="subtitle">&nbsp;</p>
            <div class="row">
                <form method="post" action="{$ajax_url}/edit-promotion/" enctype="multipart/form-data" id="promotion_form" class="col-md-10 col-md-offset-1">
                    <div id="promotion_msg"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="page_type" id="page_type" value="Update" />
                                <!--label for="promotion_type">Promotion Type: <a href="#" data-toggle="tooltip" data-placement="right" title="Select from the list of what type of promotion you want. e.g. 'Video Promotion', 'Questionnaire'."><small class="glyphicon glyphicon-info-sign"></small></a>
                                </label-->
                                <!--span class="pull-right error-msg" id="promotion_type_error"></span-->
                                <input type="hidden" value="{$promotion.Promotion_Type}" id="promotion_type" name="promotion_type">
                                <!--select class="form-control" id="promotion_type" name="promotion_type">
                                    <option {if $promotion.Promotion_Type==''} selected="selected" {/if} value="">Select Promotion Type</option>
                                    <option {if $promotion.Promotion_Type=='Image'} selected="selected" {/if} value="Image">Picture Promotion</option>
                                    <option {if $promotion.Promotion_Type=='Video'} selected="selected" {/if} value="Video">Video Promotion</option>
                                    <option {if $promotion.Promotion_Type=='Question'} selected="selected" {/if} value="Question">Questionnaire Promotion</option>
                                </select-->
                            </div>
                        </div>
                    </div>        
                    <div class="row" id="ImageBlock" {if $promotion.Promotion_Type=='Image'} style="display:block;" {else} style="display:none;" {/if}>
                        <!--div class="col-sm-12">
                            <h2 class="text-center">You have selected "Picture Promotion"</h2>
                        </div-->
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_img">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_img"></span>
                                <input class="form-control" type="text" id="promotion_title_img" name="promotion_title_img" value="{$promotion.Promotion_Title}" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_img">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_img"></span>
                                <select class="form-control" id="training_id_img" name="training_id_img">
                                    <option {if $promotion.Training_ID==''} selected="selected" {/if} value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option {if $promotion.Training_ID==$trainings[i].Training_ID} selected="selected" {/if} value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_img">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_img"></span>
                                <input type="hidden" name="old_promotion_thumb_img" value="{$promotion.Promotion_Thumb}" />
                                <input class="form-control promotion_thumb_img" type="file" id="promotion_thumb_img" name="promotion_thumb_img" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group promotion_img">
                                <label for="promotion_file_img">Promotion Images: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <a href="javascript:;" id="add_promotion_img" class="pull-right"><small class="glyphicon glyphicon-plus-sign"></small></a>
                                <span class="pull-right error-msg" id="promotion_file_error_img"></span>
                                <input class="form-control" type="file" id="promotion_file_img" name="promotion_file_img[]" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group">
                                <label for="promotion_submit_img">&nbsp;</label>
                                <input type="hidden" name="promotion_id" id="promotion_id" value="{$promotion.Promotion_ID}" />
                                <button class="btn btn-primary" id="promotion_submit_img" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_img">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_img_div">
                                    <input type="text" class="form-control" id="promo_start_date_img" name="promo_start_date_img" value="{$promotion.Promotion_Start}" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_img"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_img">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_img_div">
                                    <input type="text" class="form-control" id="promo_end_date_img" name="promo_end_date_img" value="{$promotion.Promotion_End}" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_img"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">            	
                            <div class="form-group">
                                <label for="promotion_desc_img">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_img" name="promotion_desc_img" rows="6" placeholder="Enter your promotion description within 500 characters max.">{$promotion.Promotion_Desc}</textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error"></span>
                                <div class="help-block">
                                    <strong class="pull-right">
                                        <span id="promo_desc_length_img"></span>
                                    </strong>
                                    <div class="clearfix"></div>
                                </div>                    
                            </div>
                            <div class="form-group old-promotion-file">
                            	<div class="row">
                                    {section name=f loop=$file}
                                        <div class="col-sm-4 promo-file-wrapper relative" id="file_{$file[f].File_ID}">
                                            <a class="remove_promo_file" href="javascript:;" rel="{$file[f].File_ID}" name="{$file[f].Promotion_Filename}">
                                                <small class="glyphicon glyphicon-minus-sign pull-left"></small>
                                            </a>
                                            <img src="{$base_url}/uploads/prm/{$file[f].Promotion_Filename}" class="img-responsive" width="100%" />
                                            <input type="hidden" name="old_promotion_file_img[]" value="{$file[f].Promotion_Filename}" />
                                            <input type="hidden" name="old_promotion_file_id[]" value="{$file[f].File_ID}" />
                                        </div>
                                    {/section}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="VideoBlock" {if $promotion.Promotion_Type=='Video'} style="display:block;" {else} style="display:none;" {/if}>
                        <!--div class="col-sm-12">
                            <h2 class="text-center">You have selected "Video Promotion"</h2>
                        </div-->
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_vid">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_vid"></span>
                                <input class="form-control" type="text" id="promotion_title_vid" name="promotion_title_vid" value="{$promotion.Promotion_Title}" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_vid">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_vid"></span>
                                <select class="form-control" id="training_id_vid" name="training_id_vid">
                                    <option {if $promotion.Training_ID==''} selected="selected" {/if} value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option {if $promotion.Training_ID==$trainings[i].Training_ID} selected="selected" {/if} value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_vid">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_vid"></span>
                                <input type="hidden" name="old_promotion_thumb_vid" value="{$promotion.Promotion_Thumb}" />
                                <input class="form-control promotion_thumb_vid" type="file" id="promotion_thumb_vid" name="promotion_thumb_vid" accept="image/*" style="height:auto;" />
                            </div>
                            <div class="form-group promotion_vid">
                                <label for="promotion_file_vid">Promotion Videos: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a MP4|WebM|OGG|MOV video with max length of 120 seconds."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <a href="javascript:;" id="add_promotion_vid" class="pull-right"><small class="glyphicon glyphicon-plus-sign"></small></a>
                                <span class="pull-right error-msg" id="promotion_file_error_vid"></span>
                                <input class="form-control" type="file" id="promotion_file_vid" name="promotion_file_vid[]" accept="video/*" style="height:auto;" />
                                <input type="hidden" name="old_promotion_file_vid" value="{$file.Promotion_Filename}" />
                            </div>
                            <div class="form-group old-promotion-file">
                                {section name=f loop=$file}
                                    <div class="promo-file-wrapper" id="file_{$file[f].File_ID}">
                                        <a class="remove_promo_file" href="javascript:;" rel="{$file[f].File_ID}" name="{$file[f].Promotion_Filename}">
                                            <small class="glyphicon glyphicon-minus-sign pull-right"></small>
                                        </a>
                                        <video width="100%" height="100%" controls class="pull-left col-offset-2 col-sm-12">
                                            <source src="{$base_url}/uploads/prm/{$file[f].Promotion_Filename}" type="video/mp4">
                                            <source src="{$base_url}/uploads/prm/{$file[f].Promotion_Filename}" type="video/ogg">
                                            <source src="{$base_url}/uploads/prm/{$file[f].Promotion_Filename}" type="video/webm">
                                            <source src="{$base_url}/uploads/prm/{$file[f].Promotion_Filename}" type="video/quicktime">
                                        </video>
                                        <input type="hidden" name="old_promotion_file_vid[]" value="{$file[f].Promotion_Filename}" />
                                        <input type="hidden" name="old_promotion_file_id[]" value="{$file[f].File_ID}" />
                                    </div>
                                {/section}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_vid">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_vid_div">
                                    <input type="text" class="form-control" id="promo_start_date_vid" name="promo_start_date_vid" value="{$promotion.Promotion_Start}" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_vid"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_vid">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_vid_div">
                                    <input type="text" class="form-control" id="promo_end_date_vid" name="promo_end_date_vid" value="{$promotion.Promotion_End}" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_vid"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">            	
                            <div class="form-group">
                                <label for="promotion_desc">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_vid" name="promotion_desc_vid" rows="6" placeholder="Enter your promotion description within 500 characters max.">{$promotion.Promotion_Desc}</textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error_vid"></span>
                                <div class="help-block">
                                    <strong class="pull-right">
                                        <span id="promo_desc_length_vid"></span>
                                    </strong>
                                    <div class="clearfix"></div>
                                </div>                    
                            </div>
                            <div class="form-group">
                                <label for="promotion_submit_vid">&nbsp;</label>
                                <input type="hidden" name="promotion_id" id="promotion_id" value="{$promotion.Promotion_ID}" />
                                <button class="btn btn-primary" id="promotion_submit_vid" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="QuestionBlock" {if $promotion.Promotion_Type=='Question'} style="display:block;" {else} style="display:none;" {/if}>
                        <!--div class="col-sm-12">
                            <h2 class="text-center">You have selected "Questionnaire Promotion"</h2>
                        </div-->
                        <div class="col-sm-6">
                            <div class="form-group">                    
                                <label for="promotion_title_que">Promotion Title: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion title."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_title_error_que"></span>
                                <input class="form-control" type="text" id="promotion_title_que" name="promotion_title_que" value="{$promotion.Promotion_Title}" placeholder="Enter your promotion title." />
                            </div>
                            <div class="form-group">                    
                                <label for="training_id_que">Select Training: <a href="#" data-toggle="tooltip" data-placement="right" title="Select Training Name from the list."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="training_id_error_que"></span>
                                <select class="form-control" id="training_id_que" name="training_id_que">
                                    <option {if $promotion.Training_ID==''} selected="selected" {/if} value="">Select Training</option>
                                    {section name=i loop=$trainings}
                                        <option {if $promotion.Training_ID==$trainings[i].Training_ID} selected="selected" {/if} value="{$trainings[i].Training_ID}">{$trainings[i].Training_Name}</option>
                                    {/section}
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="promotion_thumb_que">Promotion Thumbnail: <a href="#" data-toggle="tooltip" data-placement="right" title="Upload a JPG|PNG|GIF|BMP image with max size of 2MB for Promotion Thumbnail."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                <span class="pull-right error-msg" id="promotion_thumb_error_que"></span>
                                <input type="hidden" name="old_promotion_thumb_que" value="{$promotion.Promotion_Thumb}" />
                                <input class="form-control promotion_thumb_que" type="file" id="promotion_thumb_que" name="promotion_thumb_que" accept="image/*" style="height:auto;" />
                            </div>                
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_start_date_que">Promotion Start Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion start date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_start_date_que_div">
                                    <input type="text" class="form-control" id="promo_start_date_que" name="promo_start_date_que" value="{$promotion.Promotion_Start}" placeholder="Select promotion start date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_start_date_error_que"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="promo_end_date_que">Promotion End Date: <a href="#" data-toggle="tooltip" data-placement="top" title="Select promotion end date."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <div class="input-group date" id="promo_end_date_que_div">
                                    <input type="text" class="form-control" id="promo_end_date_que" name="promo_end_date_que" value="{$promotion.Promotion_End}" placeholder="Select promotion end date." />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                <span class="pull-right error-msg" id="promo_end_date_error_que"></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="promotion_desc_que">Promotion Description: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your promotion description within 500 characters max."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                
                                <textarea class="form-control" id="promotion_desc_que" name="promotion_desc_que" rows="4" placeholder="Enter your promotion description within 500 characters max.">{$promotion.Promotion_Desc}</textarea>
                                <span class="pull-left error-msg" id="promotion_desc_error_que"></span>
                                <div class="help-block">
                                    <strong class="pull-right">
                                        <span id="promo_desc_length_que"></span>
                                    </strong>
                                    <div class="clearfix"></div>
                                </div>
                            </div>                
                        </div>
                        <div class="col-sm-12">
                            <a href="javascript:;" id="add_promotion_que" class="pull-right">
                                <small class="glyphicon glyphicon-plus-sign"></small>
                            </a>
                        </div>
                        <div class="col-sm-12"><br><br></div>
                        <div class="question_set">
                            {section name=q loop=$question}
                                <div class="existing_question" id="question_{$question[q].Question_ID}">
                                    <div class="col-sm-12">
                                        <a class="remove_promo_question" rel="{$question[q].Question_ID}" href="javascript:;">
                                            <small class="glyphicon glyphicon-minus-sign pull-right"></small>
                                        </a>
                                        <div class="form-group">
                                            <input type="hidden" name="promotion_question_id[]" id="promotion_question_id" value="{$question[q].Question_ID}" />
                                            <label for="promotion_question">Question: <a href="#" data-toggle="tooltip" data-placement="right" title="Enter your question here."><small class="glyphicon glyphicon-info-sign"></small></a></label>
                                            <span class="pull-right error-msg" id="promotion_question_error"></span>
                                            <input class="form-control" type="text" id="promotion_question" name="promotion_question[]" value="{$question[q].Question}" placeholder="Enter your question here." />
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="promotion_answer_one">Answer #1: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="text" id="promotion_answer_one" name="promotion_answer_one[]" value="{$question[q].Answer_One}" placeholder="Enter your Answer." />
                                            <span class="pull-right error-msg" id="promotion_answer_one_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="promotion_marks_one">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="number" id="promotion_marks_one" name="promotion_marks_one[]" value="{$question[q].Answer_One_Marks}" placeholder="25" />
                                            <span class="pull-right error-msg" id="promotion_marks_one_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="promotion_answer_two">Answer #2: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="text" id="promotion_answer_two" name="promotion_answer_two[]" value="{$question[q].Answer_Two}" placeholder="Enter your Answer." />
                                            <span class="pull-right error-msg" id="promotion_answer_two_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="promotion_marks_two">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="number" id="promotion_marks_two" name="promotion_marks_two[]" value="{$question[q].Answer_Two_Marks}" placeholder="25" />
                                            <span class="pull-right error-msg" id="promotion_marks_two_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="promotion_answer_three">Answer #3: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="text" id="promotion_answer_three" name="promotion_answer_three[]" value="{$question[q].Answer_Three}" placeholder="Enter your Answer." />
                                            <span class="pull-right error-msg" id="promotion_answer_three_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="promotion_marks_three">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="number" id="promotion_marks_three" name="promotion_marks_three[]" value="{$question[q].Answer_Three_Marks}" placeholder="25" />
                                            <span class="pull-right error-msg" id="promotion_marks_three_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="promotion_answer_four">Answer #4: <a href="#" data-toggle="tooltip" data-placement="top" title="Enter your Answer."><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="text" id="promotion_answer_four" name="promotion_answer_four[]" value="{$question[q].Answer_Four}" placeholder="Enter your Answer." />
                                            <span class="pull-right error-msg" id="promotion_answer_four_error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label for="promotion_marks_four">Marks: <a href="#" data-toggle="tooltip" data-placement="top" title="e.g. '25'"><small class="glyphicon glyphicon-info-sign"></small></a></label>                                            
                                            <input class="form-control" type="number" id="promotion_marks_four" name="promotion_marks_four[]" value="{$question[q].Answer_Four_Marks}" placeholder="25" />
                                            <span class="pull-right error-msg" id="promotion_marks_four_error"></span>
                                        </div>
                                    </div>
                                </div>
                            {/section}
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="promotion_submit_que">&nbsp;</label>
                                <input type="hidden" name="promotion_id" id="promotion_id" value="{$promotion.Promotion_ID}" />                    
                                <button class="btn btn-primary" id="promotion_submit_que" name="promotion_submit">Submit</button>
                                <button class="btn btn-primary" type="reset">Cancel</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="progressbox">
                                <div id="progressbar"></div>
                                <div id="statustxt">0%</div>
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
<div class="backdropFixed">
	<div id="promotion_file_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Alert</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this File?</p>
                <p class="text-warning"><small>It will delete this File.</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary promotion_file_remove">Delete</button>
            </div>
        </div>
    </div>
</div>
</div>
<div class="backdropFixed">
	<div id="promotion_question_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Alert</h4>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this Question?</p>
                <p class="text-warning"><small>It will delete this Question.</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary promotion_question_remove">Delete</button>
            </div>
        </div>
    </div>
</div>
</div>
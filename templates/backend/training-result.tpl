<div id="page-content-wrapper">
	<div class="trainings-page">
        <div class="col-sm-10 pull-right">
        	{if $results}
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Search Trainings List</h1>
                    </div>
                    <div class="col-sm-12">
                    	{if $numbers}
                            <h4 class="pull-left">Total Search Results: <strong>{$numbers}</strong></h4>
                        {/if}
                        <div class="pull-right">
                            <form class="navbar-form" method="post" action="{$admin_url}/training-result/">
                                <input type="text" class="form-control" name="training_name" value="{if $search}{$search}{/if}" />
                                <button type="submit" class="btn btn-primary" name="search_training">SEARCH</button>
                            </form>
                        </div>
                    </div>                
                    <div class="col-sm-12">
                        {if $message}
                            <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {$message}
                            </div>
                        {/if}
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
                <div class="row">
                    {if $results}
                        {section name=i loop=$results}
                            {assign var=category value=get_category_name($results[i].Training_Category)}
                            <div class="col-sm-12">
                                <form action="" method="post">
                                    <input type="hidden" name="training_id" value="{$results[i].Training_ID}" />
                                    <div class="training-row btnwrap">
                                        <div class="col-sm-4">
                                            <img src="{$base_url}/uploads/trp/{$results[i].Training_Thumb}" alt="{$results[i].Training_Name}" class="img-responsive" />
                                        </div>
                                        <div class="col-sm-8">
                                            <h2>
                                                {$results[i].Training_Name}
                                                <a style="margin:5px;" href="{$base_url}/edit-training/{$results[i].Training_ID}/" class="btn btn-xs btn-primary edit-btn pull-right">
                                                    <span class="fa fa-edit"></span> Edit
                                                </a>
                                                {assign var=user value=get_trainer_name($results[i].Trainer_ID)}
                                            </h2>
                                            <p>
                                                <b>Category:</b>
                                                <i>
                                                    {assign var=category value=get_category_name($results[i].Training_Category)}
                                                    {section name=n loop=$category}
                                                        {$category[n].Category_Name}
                                                    {/section}
                                                </i>
                                            </p>
                                            <p>
                                                <b>Trainer:</b>
                                                <i>
                                                    {assign var=trainer value=get_trainer_name($results[i].Trainer_ID)}
                                                    {section name=n loop=$trainer}
                                                        {$trainer[n].Full_Name}
                                                    {/section}
                                                </i>
                                            </p>
                                            <p>
                                                <b>Tags:</b>
                                                <i>{$results[i].Training_Tags}</i>
                                            </p>
                                            <p>
                                                <b>Status:</b>
                                                <i>{$results[i].Training_Status}</i>
                                            </p>
                                            <p>
                                                {if $results[i].Training_Status=='Pending'}
                                                    <input type="hidden" name="training_status" value="Published" />
                                                    <button type="submit" name="update_training" class="btn btn-success">Publish</button>
                                                {/if}
                                                {if $results[i].Training_Status=='Published'}
                                                    <input type="hidden" name="training_status" value="Pending" />
                                                    <button type="submit" name="update_training" class="btn btn-warning">Unpublish</button>
                                                {/if}
                                                <a class="btn btn-danger" href="#">Delete</a>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        {/section}
                    {else}
                        <div class="col-sm-12">
                            <p class="no-items">No training is available in the list.</p>
                        </div>
                    {/if}
                </div>
            {else}
            	<div class="row">
                    <div class="col-sm-12">
                        <h1>Search Trainings List</h1>
                    </div>
                    <div class="col-sm-12">
                    	<h4 class="pull-left">Total Search Results: <strong>0</strong></h4>
                        <div class="pull-right">
                            <form class="navbar-form" method="post" action="{$admin_url}/training-result/">
                                <input type="text" class="form-control" name="training_name" value="{if $search}{$search}{/if}" />
                                <button type="submit" class="btn btn-primary" name="search_training">SEARCH</button>
                            </form>
                        </div>
                    </div>                
                    <div class="col-sm-12">
                        {if $message}
                            <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {$message}
                            </div>
                        {/if}
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="no-items">Sorry, no trainig(s) found!</p>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
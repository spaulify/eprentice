<div id="page-content-wrapper">
	<div class="dashboard-page">
        <div class="col-sm-10 pull-right">
            <div class="row tile_count">
	            <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                    <div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top"><i class="fa fa-user"></i> Total Products</span>
                        <div class="count text-center">{$products}</div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                    <div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top"><i class="fa fa-clock-o"></i> Total Trainings</span>
                        <div class="count text-center">{$trainings}</div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                    <div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top"><i class="fa fa-user"></i> Total Promotions</span>
                        <div class="count green text-center">{$promotions}</div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                    <div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top"><i class="fa fa-user"></i> Training Page View</span>
                        <div class="count text-center">{$totalTraining}</div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                    <div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top text-center"><i class="fa fa-user"></i> Training Video View</span>
                        <div class="count text-center">{$trainingVideo}</div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                	<div class="left"></div>
                    <div class="right text-center">
                    	<span class="count_top"><i class="fa fa-user"></i> Average Watchtime</span>
                        <div class="count text-center">{$SeekAVGTime}</div>
                    </div>
                </div>
            </div>
			<div class="row">
            	<div class="col-md-12 col-sm-12 col-xs-12">
                	<div class="dashboard_graph">
                    	<div class="row x_title">
                        	<div class="col-md-12">
                            	<h3>Traffic Source</h3>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        	<div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                            <div style="width: 100%;">
                            	<div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 bg-white">
                        	<div class="x_title">
                            	<h2>Top Campaign Performance</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-6">
                            	<div>
                                	<p>Email</p>
                                    <div class="">
                                    	<div class="progress progress_sm" style="width: 76%;">
	                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{$emPerc}"></div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                	<p>Facebook</p>
                                    <div class="">
                                    	<div class="progress progress_sm" style="width: 76%;">
	                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{$fbPerc}"></div>
                                        </div>
                                    </div>
                                </div>
								<div>
									<p>Twitter</p>
									<div class="">
										<div class="progress progress_sm" style="width: 76%;">
											<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{$twPerc}"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-6">
								<div>
									<p>Linkedin</p>
									<div class="">
										<div class="progress progress_sm" style="width: 76%;">
											<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{$liPerc}"></div>
										</div>
									</div>
								</div>
								<div>
									<p>Google</p>
									<div class="">
										<div class="progress progress_sm" style="width: 76%;">
											<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="{$gpPerc}"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div> <br />
            <div class="row">
            	<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel tile fixed_height_320">
						<div class="x_title">
							<h2>Email Click Rates</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
                        	{if $EmailClickArray}
								<h4>Email Click Rates of Last 5 Days</h4>                            
                                {foreach from=$EmailClickArray key=d item=p name=emailloop}
                                    <div class="widget_summary">
                                        <div class="w_left w_25">
                                            <span>{$d|date_format:"%d %b, %Y"}</span>
                                        </div>
                                        <div class="w_center w_55">
                                            <div class="progress">
                                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="{$p}" aria-valuemin="0" aria-valuemax="100" style="width: {$p}%;">
                                                    <span class="sr-only">{$p}% Complete</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w_right w_20">
                                            <span>{$p}%</span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                {/foreach}
                            {else}
                            	<p class="alert alert-warning">No Email clicks yet.</p>
                            {/if}
						</div>
					</div>
				</div>				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel tile fixed_height_320 overflow_hidden">
						<div class="x_title">
							<h2>Share of User (%)</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
                        	{if $cataDetails}
                                <table class="" style="width:100%">
                                    <tr>
                                        <th style="width:37%;">
                                            <p>Top 5</p>
                                        </th>
                                        <th>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <p class="text-center">Category</p>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                <p class="text-right">View(%)</p>
                                            </div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                                        </td>
                                        <td>
                                            <table class="tile_info">                                        	
                                                {foreach from=$cataDetails key=c item=p name=cataloop}
                                                    {assign var="counter" value=$smarty.foreach.cataloop.iteration}
                                                    <tr>
                                                        {if $counter == 1}
                                                            <td><p><i class="fa fa-square" style="color:#BDC3C7;"></i>{$c} </p></td>
                                                            <td>{$p}</td>
                                                        {/if}
                                                        {if $counter == 2}
                                                            <td><p><i class="fa fa-square" style="color:#9B59B6;"></i>{$c} </p></td>
                                                            <td>{$p}</td>
                                                        {/if}
                                                        {if $counter == 3}
                                                            <td><p><i class="fa fa-square" style="color:#455C73;"></i>{$c} </p></td>
                                                            <td>{$p}</td>
                                                        {/if}
                                                        {if $counter == 4}
                                                            <td><p><i class="fa fa-square" style="color:#26B99A;"></i>{$c} </p></td>
                                                            <td>{$p}</td>
                                                        {/if}
                                                        {if $counter == 5}
                                                            <td><p><i class="fa fa-square" style="color:#3498DB;"></i>{$c} </p></td>
                                                            <td>{$p}</td>
                                                        {/if}
                                                    </tr>
                                                {/foreach}                                            
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            {else}
                                <p class="alert alert-warning">No shares yet.</p>
                            {/if}
						</div>
					</div>
				</div>				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel tile fixed_height_320">
						<div class="x_title">
							<h2>Top viewed Trainings</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
                        	{if !empty($TVTData)}
                                <div class="dashboard-widget-content">
                                    <ul class="quick-list">                                	
                                        {section name=t loop=$TVTData}
                                            {assign var=training value=get_training($TVTData[t].Training_ID)}
                                            {section name=n loop=$training}
                                                <li><i class="fa fa-bar-chart"></i><a href="{$base_url}/training/{$training[n].Training_URL}/">{$training[n].Training_Name}</a></li>
                                            {/section}
                                        {/section}
                                    </ul>
                                    <div class="sidebar-widget">
                                        <h4>Average Training (Seconds)</h4>                                    
                                        <canvas width="150" height="80" id="TrainingGauge" class="" style="width: 160px; height: 100px;"></canvas>
                                        <div class="goal-wrapper">
                                            <span id="gauge-text" class="gauge-value pull-left">{$SeekAVGTiming}</span>
                                            <span id="goal-text" class="goal-value pull-right">{$TotalAVGTiming}</span>
                                        </div>                                    
                                    </div>
                                </div>
                            {else}
                                <p class="alert alert-warning">No training viewed yet.</p>
                            {/if}
						</div>
					</div>
				</div>
			</div>			
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="x_panel">
						<div class="x_title">
							<h2>Recent Activities</h2>
							<div class="clearfix"></div>
						</div>
						<div class="x_content">
                        	{if !empty($EmailData)}
                                <div class="dashboard-widget-content">
                                    <ul class="list-unstyled timeline widget">                                	
                                        {section name=n loop=$EmailData}
                                            <li>
                                                <div class="block">
                                                    <div class="block_content">
                                                        <h2 class="title"><a href="#" class="EmailData" id="{$EmailData[n].Email_ID}">{$EmailData[n].Email_Subject}</a></h2>
                                                        <div class="byline">
                                                            <span>Send time: </span>
                                                            <a>{$EmailData[n].Email_Send_Date|date_format:"%d %b, %Y"}</a> <br />
                                                            <span>Send to: </span>
                                                            <a href="mailto:{$EmailData[n].Receiver_Email_Address}">
                                                                {$EmailData[n].Receiver_Email_Address}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <div id="EmailDetails_{$EmailData[n].Email_ID}" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h3 class="modal-title text-center">{$EmailData[n].Email_Subject}</h3>
                                                            <p>
                                                                <span>Send to: </span>
                                                                <a href="mailto:{$EmailData[n].Receiver_Email_Address}">
                                                                    {$EmailData[n].Receiver_Email_Address}
                                                                </a>
                                                            </p>
                                                            <p>
                                                                <span>Reply to: </span>
                                                                <a href="mailto:{$EmailData[n].Receiver_Email_Address}">
                                                                    {$EmailData[n].Receiver_Email_Address}
                                                                </a>
                                                            </p>
                                                            <p>
                                                                <span>Send time: </span>
                                                                <a href="#" class="EmailData" id="{$EmailData[n].Email_ID}">
                                                                    {$EmailData[n].Email_Send_Date|date_format:"%d %b, %Y"}
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div class="modal-body">
                                                            {$EmailData[n].Email_Content|unescape:'html'}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/section}
                                    </ul>
                                </div>
                            {else}
                                <p class="alert alert-warning title">No Recent activities.</p>
                            {/if}
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Visitors location</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="dashboard-widget-content">
										<div class="col-md-4 hidden-small">
											<table class="countries_list">
                                            	<thead>
                                                	<tr>
                                                    	<th>Country</th>
                                                        <th class="text-right">Visit</th>
                                                    </tr>
                                                </thead>
												<tbody>
                                                	{if $CountryList}
                                                        {section name=i loop=$CountryList}
                                                            <tr>
                                                                <td>{$CountryList[i].country}</td>
                                                                <td class="fs15 fw700 text-right">{$CountryList[i].visit}</td>
                                                            </tr>
                                                        {/section}
                                                    {else}
                                                    	<tr class="alert alert-warning">
                                                            <td align="center" colspan="2">No visitors yet.</td>
                                                        </tr>
                                                    {/if}
												</tbody>
											</table>
										</div>
										<div id="world-map-gdp" class="col-md-8 col-sm-12 col-xs-12" style="height:230px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Last Product Puchases</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
                                	{if $LPData}
                                        <div class="">
                                            <ul class="to_do">                                        	
                                                {section name=i loop=$LPData}
                                                    <li>
                                                        <p>{$LPData[i].Product_Name} <span class="pull-right"><strong>${$LPData[i].Purchase_Amount}</strong> <small>({$LPData[i].Purchased_ON|date_format:"%d %b, %Y at %H:%M"})</small></span></p>
                                                    </li>
                                                {/section}
                                            </ul>
                                        </div>
                                    {else}
                                        <p class="alert alert-warning">No product sold yet.</p>
                                    {/if}
								</div>
							</div>
						</div>						
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Today's Stats</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="row weather-days">
										<div class="col-sm-4">
											<div class="daily-weather">
												<h2 class="day"><i class="fa fa-eye" aria-hidden="true"></i> Total View</h2>
												<h3 class="text-center">{$TodayTra}</h3>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="daily-weather">
												<h2 class="day"><i class="fa fa-usd" aria-hidden="true"></i> Total Sold</h2>
												<h3 class="text-center">${$TDTotalAmount}</h3>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="daily-weather">
												<h2 class="day"><i class="fa fa-envelope" aria-hidden="true"></i> Email Sent</h2>
												<h3 class="text-center">{$TDEmailRow}</h3>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
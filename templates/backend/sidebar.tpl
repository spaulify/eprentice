<div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
    	{assign var=current_page value=active_page()}
        <li {if $current_page=='dashboard.php'}class="active"{/if}>
            <a href="{$admin_url}/dashboard/" data-parent="#menu">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        
        <li {if $current_page=='payment.php'}class="active"{/if}>
            <a href="{$admin_url}/payment/">
                <i class="fa fa-dollar"></i> <span>Payment</span>
            </a>
        </li>
        
        <li {if $current_page=='trainers.php'}class="active"{/if}>
            <a href="{$admin_url}/trainers/">
                <i class="fa fa-file-video-o"></i> <span>Trainers</span>
            </a>
        </li>
        
        <li {if $current_page=='training-programs.php'}class="active"{/if}>
            <a href="{$admin_url}/training-programs/">
                <i class="fa fa-file-video-o"></i> <span>Training Programs</span>
            </a>
        </li>
        
        <li {if $current_page=='members.php'}class="active"{/if}>
            <a href="{$admin_url}/members/">
                <i class="fa fa-share-square-o"></i> <span>Members</span>
            </a>
        </li>
    </ul>
</div>
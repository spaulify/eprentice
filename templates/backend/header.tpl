<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	
    {assign var=url value="/"|explode:$smarty.server.REQUEST_URI}
    {if in_array('training', $url)}
		<meta property="og:image" content="{$base_url}/uploads/trp/{$training.Training_Thumb}" />
        <meta property="og:title" content="{$training.Training_Name}" />
        <meta property="og:description" content="{$training.Training_Desc}" />
        <meta property="og:url" content="{$base_url}/training/{$training.Training_URL}/" />
    {else}
        <meta property="og:image" content="{$asset_url}/images/LightBlueLogo.png" />
        <meta property="og:title" content="{$page_title}" />
        <meta property="og:description" content="{$page_description}" />
        <meta property="og:url" content="{$base_url}" />
    {/if}
    <meta property="fb:app_id" content="{$Facebook_App_ID}" />
    <meta property="og:site_name" content="ePrentice" />
    <meta property="og:type" content="Website" />
    
    <title>{$page_title}</title>

	<link href="{$base_url}/favicon.ico" rel="icon" />
	<link href="{$asset_url}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/jquery.switchButton.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/nanoscroller.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/jquery.wizard.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/jquery.tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/fa-social.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/flowplayer-6.0.5/skin/functional.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/cropper.min.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/main.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/style.css" rel="stylesheet" type="text/css" />
    <link href="{$asset_url}/css/app.css" rel="stylesheet" type="text/css" />
    
    {if $page_name=='dashboard.php'}
        <link href="{$asset_url}/css/custom.css" rel="stylesheet" />
        <link href="{$asset_url}/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />
        <link href="{$asset_url}/css/icheck/flat/green.css" rel="stylesheet" />
        <link href="{$asset_url}/css/floatexamples.css" rel="stylesheet" type="text/css" />
    {/if}
    
    <script type="text/javascript">
	var base_url = '{$base_url}'; var ajax_url = '{$ajax_url}'; var asset_url = '{$asset_url}'; var Stripe_Publication_Key = '{$Stripe_Publication_Key}';
   	</script>
    <script type="text/javascript" src="{$asset_url}/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/nprogress.js"></script>
	<script type="text/javascript" src="{$asset_url}/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{$asset_url}/js/jquery.tagsinput.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.nanoscroller.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.wizard.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.steps.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/angular.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/angular-messages.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/ePrenticeApp.js"></script>

	<!--[if lt IE 9]>
    	<script type="text/javascript" src="{$asset_url}/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    
	<!--[if lt IE 9]>
		<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body {if $page_name=='dashboard.php'}class="nav-md"{/if}>
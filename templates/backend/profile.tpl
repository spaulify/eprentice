<div id="page-content-wrapper">
	<div class="profile-page">
        <div class="col-sm-7 pull-right">
            <div class="row">
                <div class="trainer-profile">
                    <div class="profile-row">
                        <h2>About <span>Me</span></h2>
                        <p>{if $user['Bio']} {$user['Bio']} {else} Enter something about yourself within maximum 500 characters. {/if}</p>
                    </div>
                    <div class="profile-row">
                        <h2>Areas of <span>Expertise</span></h2>
                        <p>
                            <i class="fa fa-css3"></i> 
                            <label>Domain:</label> 
                            {if $user['Domain']} {$user['Domain']} {else} Enter your specific domains. {/if}
                        </p>
                        <p>
                            <i class="fa fa-files-o"></i> 
                            <label>Projects:</label> 
                            {if $user['Projects']} {$user['Projects']} {else} Specify the projects you have done. {/if}
                        </p>
                        <p>
                            <i class="fa fa-briefcase"></i> 
                            <label>Experience:</label> 
                            {if $user['Experience']} {$user['Experience']} Month(s){else} Enter your total experience in months. {/if}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-3 pull-right">
            <div class="row">
                <div class="profile-sec">
                    <img src="{$user['Profile_Photo']}" class="img-circle" alt="{$user['Full_Name']}" width="170" height="170" />
                    <h3>{$user['Full_Name']}</h3>
                    <p class="small text-muted">
                        {if $user['Designation']} {$user['Designation']}<br /> {/if}
                    </p>
                    {if $user['Facebook_URL'] || $user['Google_URL'] || $user['Twitter_URL'] || $user['LinkedIn_URL']}
                        <p class="social-icons icon-circle">
                            {if $user['Facebook_URL']}
                                <a href="{$user['Facebook_URL']}" target="_blank"><i class="fa fa-facebook"></i></a> 
                            {/if}
                            {if $user['Twitter_URL']}
                                <a href="{$user['Twitter_URL']}" target="_blank"><i class="fa fa-twitter"></i></a> 
                            {/if}
                            {if $user['Google_URL']}
                                <a href="{$user['Google_URL']}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                            {/if}
                            {if $user['LinkedIn_URL']}
                                <a href="{$user['LinkedIn_URL']}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                            {/if}
                        </p>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
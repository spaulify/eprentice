<div class="wrapper">
	<form class="form-signin" method="post" action="">
    	<div class="login-form">
            <h2 class="form-signin-heading">Signin to ePrentice Administrator</h2>
            {if $error}
                <div class="alert alert-danger alert-dismissible" role="alert">{$error}</div>
            {/if}
            <input type="text" class="form-control" name="username" placeholder="Username" value="{if $username}{$username}{else}{$remember_username}{/if}" required autofocus="autofocus" /><br/>
            <input type="password" class="form-control" name="password" placeholder="Password" value="{$remember_password}" required /><br/>
            <label class="checkbox">
				<input type="checkbox" name="remember" {if $remember_username && $remember_password} checked="checked" {else} '' {/if}> Remember me
			</label>
			<button class="btn btn-lg btn-info btn-block" type="submit" name="submit">Login</button>
        </div>
		<div class="text-center">
			<a href="#" style="color:red;"> Forgot Password?</a>
		</div>
	</form>
</div>
<div id="page-content-wrapper">
	<div class="downline-members-page">
        <div class="col-sm-10 pull-right">
            <div class="row">
                <div class="col-sm-12">
                    <h1>All Members List</h1>
                </div>
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form class="navbar-form" method="post" action="{$admin_url}/member-result/">
                            <input type="text" class="form-control" name="viewer_name" />
                            <button type="submit" class="btn btn-primary" name="search_viewer">SEARCH</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">&nbsp;</div>
            </div>
            <div class="row">
                {if $members}
                    {section name=i loop=$members}
                        <div class="col-sm-3 profile-card">
                            <div class="member-rows">
                                <a href="{$admin_url}/profile/{$members[i].Username}/">
                                    <img src="{$members[i].Profile_Photo}" alt="{$members[i].Full_Name}" class="img-circle img-responsive" height="150" width="150" />
                                </a>
                                <div class="caption">
                                    <h3>{$members[i].Full_Name}</h3>
                                    <p>{$members[i].Bio|truncate:120}</p>
                                    {if $members[i]['Facebook_URL'] || $members[i]['Google_URL'] || $members[i]['Twitter_URL'] || $members[i]['LinkedIn_URL']}
                                        <p class="social-icons icon-circle">
                                            {if $members[i]['Facebook_URL']}
                                                <a href="{$members[i]['Facebook_URL']}" target="_blank"><i class="fa fa-facebook"></i></a> 
                                            {/if}
                                            {if $members[i]['Twitter_URL']}
                                                <a href="{$members[i]['Twitter_URL']}" target="_blank"><i class="fa fa-twitter"></i></a> 
                                            {/if}
                                            {if $members[i]['Google_URL']}
                                                <a href="{$members[i]['Google_URL']}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                                            {/if}
                                            {if $members[i]['LinkedIn_URL']}
                                                <a href="{$members[i]['LinkedIn_URL']}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    {/section}
                {else}
                    <div class="col-sm-12">
                        <p class="no-items">There is no Members to show for the list.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
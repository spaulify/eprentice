<div id="page-content-wrapper">
	<div class="trainings-page">
        <div class="col-sm-10 pull-right">
        	<div class="row">
            	<div class="col-sm-12">
                	<h1>All Trainings By Trainers</h1>
                </div>
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form class="navbar-form" method="post" action="{$admin_url}/training-result/">
                            <input type="text" class="form-control" name="training_name" />
                            <button type="submit" class="btn btn-primary" name="search_training">SEARCH</button>
                        </form>
                    </div>
                </div>                
                <div class="col-sm-12">
                    {if $message}
                        <div class="alert alert-success alert-dismissible AlertMessage" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {$message}
                        </div>
                    {/if}
                </div>
                <div class="col-sm-12">&nbsp;</div>
            </div>
            <div class="row">
                {if $trainings}
                    {section name=i loop=$trainings}
                        {assign var=category value=get_category_name($trainings[i].Training_Category)}
                        <div class="col-sm-12">
                        	<input type="hidden" name="training_id" value="{$trainings[i].Training_ID}" />
                            <div class="training-row btnwrap">
                                <div class="col-sm-4">
                                    <img src="{$base_url}/uploads/trp/{$trainings[i].Training_Thumb}" alt="{$trainings[i].Training_Name}" class="img-responsive" />
                                </div>
                                <div class="col-sm-8">
                                    <h2>
                                        {$trainings[i].Training_Name}
                                        <!--a style="margin:5px;" href="{$admin_url}/edit-training/{$trainings[i].Training_ID}/" class="btn btn-xs btn-primary edit-btn pull-right">
                                            <span class="fa fa-edit"></span> Edit
                                        </a-->
                                        {assign var=user value=get_trainer_name($trainings[i].Trainer_ID)}
                                    </h2>
                                    <p>
                                        <b>Category:</b>
                                        <i>
                                            {assign var=category value=get_category_name($trainings[i].Training_Category)}
                                            {section name=n loop=$category}
                                                {$category[n].Category_Name}
                                            {/section}
                                        </i>
                                    </p>
                                    <p>
                                        <b>Trainer:</b>
                                        <i>
                                            {assign var=trainer value=get_trainer_name($trainings[i].Trainer_ID)}
                                            {section name=n loop=$trainer}
                                                {$trainer[n].Full_Name}
                                            {/section}
                                        </i>
                                    </p>
                                    <p>
                                        <b>Tags:</b>
                                        <i>{$trainings[i].Training_Tags}</i>
                                    </p>
                                    <p>
                                        <b>Status:</b>
                                        <i>{$trainings[i].Training_Status}</i>
                                    </p>
                                    <p>
                                    	{if $trainings[i].Training_Status=='Pending'}
                                            <a href="#" data-status="Published" name="Publish" id="{$trainings[i].Training_ID}" rel="{$trainings[i].Trainer_ID}" class="btn btn-success admin_update_training">Publish</a>
                                        {/if}
                                        {if $trainings[i].Training_Status=='Published'}
                                            <a href="#" data-status="Pending" name="Unpublish" id="{$trainings[i].Training_ID}" rel="{$trainings[i].Trainer_ID}" class="btn btn-warning admin_update_training">Unpublish</a>
                                        {/if}
                                        <a class="btn btn-danger admin_remove_training" href="#" id="{$trainings[i].Training_ID}" rel="{$trainings[i].Trainer_ID}">Delete</a>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    {/section}
                {else}
                    <div class="col-sm-12">
                        <p class="no-items">No training is available in the list.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
    <div class="modal fade" id="admin_training_status_modal" tabindex="-1" role="dialog" aria-labelledby="admin_training_status_modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span class="stat"></span> Training</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="training_status_msg"></div>
                        <p>Are you sure want to do this?</p>
                        <p class="text-warning"><small>Are you sure want to <strong class="stat"></strong> this training? You can revert back the task again if you want.</small></p>
                        <input type="hidden" id="stat_training_id" name="stat_training_id" value="" />
                        <input type="hidden" id="stat_trainer_id" name="stat_trainer_id" value="" />
                        <input type="hidden" id="stat_training_status" name="stat_training_status" value="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="#" class="btn btn-primary" id="status_update"><span class="stat"></span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="backdropFixed">
    <div class="modal fade" id="admin_remove_training_modal" tabindex="-1" role="dialog" aria-labelledby="admin_remove_training_modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Removing Training</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="training_erase_msg"></div>
                        <p>Are you sure want to do this?</p>
                        <p class="text-warning"><small>Are you sure want to <strong>Remove</strong> this training? After erase, it will not revert back.</small></p>
                        <input type="hidden" id="training_id" name="training_id" value="" />
                        <input type="hidden" id="trainer_id" name="trainer_id" value="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="#" class="btn btn-primary" id="remove_training">Erase</a>
                </div>
            </div>
        </div>
    </div>
</div>
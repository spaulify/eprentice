<div id="page-content-wrapper">
	<div class="downline-members-page">
        <div class="col-sm-10 pull-right">
            <div class="row">
                <div class="col-sm-12">
                    <h1>All Trainers List</h1>
                </div>
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form class="navbar-form" method="post" action="{$admin_url}/trainer-result/">
                            <input type="text" class="form-control" name="trainer_name" />
                            <button type="submit" class="btn btn-primary" name="search_trainer">SEARCH</button>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">&nbsp;</div>
            </div>
            <div class="row">
                {if $trainers}
                    {section name=i loop=$trainers}
                        <div class="col-sm-3 profile-card">
                            <div class="member-rows">
                                <a href="{$admin_url}/profile/{$trainers[i].Username}/">
                                    <img src="{$trainers[i].Profile_Photo}" alt="{$trainers[i].Full_Name}" class="img-circle img-responsive" height="150" width="150" />
                                </a>
                                <div class="caption">
                                    <h3>{$trainers[i].Full_Name}</h3>
                                    <p>{$trainers[i].Bio|truncate:120}</p>
                                    {if $trainers[i]['Facebook_URL'] || $trainers[i]['Google_URL'] || $trainers[i]['Twitter_URL'] || $trainers[i]['LinkedIn_URL']}
                                        <p class="social-icons icon-circle">
                                            {if $trainers[i]['Facebook_URL']}
                                                <a href="{$trainers[i]['Facebook_URL']}" target="_blank"><i class="fa fa-facebook"></i></a> 
                                            {/if}
                                            {if $trainers[i]['Twitter_URL']}
                                                <a href="{$trainers[i]['Twitter_URL']}" target="_blank"><i class="fa fa-twitter"></i></a> 
                                            {/if}
                                            {if $trainers[i]['Google_URL']}
                                                <a href="{$trainers[i]['Google_URL']}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                                            {/if}
                                            {if $trainers[i]['LinkedIn_URL']}
                                                <a href="{$trainers[i]['LinkedIn_URL']}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    {/section}
                {else}
                    <div class="col-sm-12">
                        <p class="no-items">There is no Trainers to show for the list.</p>
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>
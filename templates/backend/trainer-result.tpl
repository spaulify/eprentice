<div id="page-content-wrapper">
	<div class="downline-members-page">
        <div class="col-sm-10 pull-right">
            {if $results}
            	<div class="row">
                    <div class="col-sm-12">
                        <h1>Search Trainer Results</h1>
                    </div>
                    <div class="col-sm-12">
                        {if $numbers}
                            <h4 class="pull-left">Total Search Results: <strong>{$numbers}</strong></h4>
                        {/if}
                        <div class="pull-right">
                            <form class="navbar-form" method="post" action="{$admin_url}/trainer-result/">
                                <input type="text" class="form-control" name="trainer_name" value="{if $search}{$search}{/if}" />
                                <button type="submit" class="btn btn-primary" name="search_trainer">SEARCH</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
            	<div class="row">
                    {section name=i loop=$results}
                        <div class="col-sm-3 profile-card">
                            <div class="member-rows">
                                <a href="{$admin_url}/profile/{$results[i].Username}/">
                                    <img src="{$results[i].Profile_Photo}" alt="{$results[i].Full_Name}" class="img-circle img-responsive" height="150" width="150" />
                                </a>
                                <div class="caption">
                                    <h3>{$results[i].Full_Name}</h3>
                                    <p>{$results[i].Bio|truncate:120}</p>
                                    {if $results[i]['Facebook_URL'] || $results[i]['Google_URL'] || $results[i]['Twitter_URL'] || $results[i]['LinkedIn_URL']}
                                        <p class="social-icons icon-circle">
                                            {if $results[i]['Facebook_URL']}
                                                <a href="{$results[i]['Facebook_URL']}" target="_blank"><i class="fa fa-facebook"></i></a> 
                                            {/if}
                                            {if $results[i]['Twitter_URL']}
                                                <a href="{$results[i]['Twitter_URL']}" target="_blank"><i class="fa fa-twitter"></i></a> 
                                            {/if}
                                            {if $results[i]['Google_URL']}
                                                <a href="{$results[i]['Google_URL']}" target="_blank"><i class="fa fa-google-plus"></i></a> 
                                            {/if}
                                            {if $results[i]['LinkedIn_URL']}
                                                <a href="{$results[i]['LinkedIn_URL']}" target="_blank"><i class="fa fa-linkedin"></i></a> 
                                            {/if}
                                        </p>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    {/section}
                </div>
            {else}
            	<div class="row">
                    <div class="col-sm-12">
                        <h1>Search Trainer Results</h1>
                    </div>
                    <div class="col-sm-12">
                        <h4 class="pull-left">Total Search Results: <strong>0</strong></h4>
                        <div class="pull-right">
                            <form class="navbar-form" method="post" action="{$admin_url}/trainer-result/">
                                <input type="text" class="form-control" name="trainer_name" value="{if $search}{$search}{/if}" />
                                <button type="submit" class="btn btn-primary" name="search_trainer">SEARCH</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-12">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <p class="no-items">Sorry, no trainer(s) found!</p>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
	{if !empty($User_ID)}
	    </div>
    {/if}
    
	<script type="text/javascript" src="{$asset_url}/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{$asset_url}/js/moment-with-locales.js"></script>
	<script type="text/javascript" src="{$asset_url}/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.switchButton.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.form.js"></script>
	<script type="text/javascript" src="{$asset_url}/js/lib/js/card.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/flowplayer-6.0.5/flowplayer.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/video.preview.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/pace/pace.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="{$asset_url}/js/cropper.min.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/main.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/scripts.js"></script>
    <script type="text/javascript" src="{$asset_url}/js/app.js"></script>
    
    {if $page_name=='dashboard.php'}
		<script type="text/javascript" src="{$asset_url}/js/gauge/gauge.min.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/icheck/icheck.min.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/moment/moment.min.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/datepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/chartjs/chart.min.js"></script>
        <!--[if lte IE 8]>
            <script type="text/javascript" src="{$asset_url}/js/excanvas.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.pie.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.orderBars.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/date.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.spline.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.stack.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/curvedLines.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/flot/jquery.flot.resize.js"></script>
        
        <script type="text/javascript" src="{$asset_url}/js/maps/jquery-jvectormap-2.0.3.min.js"></script>
        <script type="text/javascript">var gdpData = { {$Countries} }</script>
        <script type="text/javascript" src="{$asset_url}/js/maps/jquery-jvectormap-world-mill-en.js"></script>
        <script type="text/javascript" src="{$asset_url}/js/maps/jquery-jvectormap-us-aea-en.js"></script>
        <script src="{$asset_url}/js/custom.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            var data1 = [{$Imp_Data}];
            var data2 = [{$Vid_Data}];
            var data3 = [{$Pmt_Data}];
            $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                data1, data2, data3
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)", "rgba(52, 41, 182, 0.38)"],
                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                },
                yaxis: {
                    ticks: 8,
                    tickColor: "rgba(51, 51, 51, 0.06)",
					min: 0,
                },
                tooltip: false
            });
        
            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });

        Chart.defaults.global.legend = {
            enabled: false
        };
        var data = {
            labels: [
                {$cata_name}
            ],
            datasets: [{
                data: [{$cata_perc}],
                backgroundColor: [
                    "#BDC3C7",
                    "#9B59B6",
                    "#455C73",
                    "#26B99A",
                    "#3498DB"
                ],
                hoverBackgroundColor: [
                    "#CFD4D8",
                    "#B370CF",
                    "#34495E",
                    "#36CAAB",
                    "#49A9EA"
                ]
            }]
        };
        var canvasDoughnut = new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: data
        });
        </script>
  	{/if}
</body>
</html>
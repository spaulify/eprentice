<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$Promotion_ID = $_GET['param'];
$query = "SELECT * FROM `" . $config['db_prefix'] . "promotions` WHERE `Promotion_ID` = $Promotion_ID LIMIT 0, 1";
$rs = $conn->execute($query);
if ( $rs ) {
	$promotion = $rs->getrows();
} else {
	$promotion = array();
}
$smarty->assign('promotion', $promotion[0]);

$query1 = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Trainer_ID` = $UID ORDER BY `Training_ID` DESC";
$rs1 = $conn->execute($query1);
if ( $rs1 ) {
	$trainings = $rs1->getrows();
} else {
	$trainings = array();
}
$smarty->assign('trainings', $trainings);

$query2 = "SELECT * FROM `" . $config['db_prefix'] . "promotion_file` WHERE `Promotion_ID` = $Promotion_ID";
$rs2 = $conn->execute($query2);
if ( $rs2 ) {
	$file = $rs2->getrows();
} else {
	$file = array();
}
$smarty->assign('file', $file);

$query3 = "SELECT * FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Promotion_ID` = $Promotion_ID";
$rs3 = $conn->execute($query3);
if ( $rs3 ) {
	$question = $rs3->getrows();
} else {
	$question = array();
}
$smarty->assign('question', $question);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('edit-promotion.tpl');
$smarty->display('footer.tpl');
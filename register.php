<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

$error = ''; 

$logwith = 'yes';  		// Login with Email
$role 	 = 'viewer';
$fname 	 = '';
$lname 	 = '';
$email 	 = '';

$refer 	 = false;
if ( isset($_SESSION['Ref_Code']) ) {
	$refer = true;
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	$logwith 	= addslashes($_POST['logwith']);
	$fname 		= addslashes($_POST['fname']);
	$lname 		= addslashes($_POST['lname']);
	$email 		= addslashes($_POST['email']);
	$password 	= isset($_POST['password']) ? addslashes($_POST['password']) : '';
	$role 		= isset($_POST['role']) ? 'trainer' : 'viewer';
	$full_name 	= $fname . ' '. $lname;
	
	$username	= strtolower($fname . '.'. $lname);
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Username` = '$username' LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs )
		$userCount = $rs->numrows();
	else 
		$userCount = 0;
	if ( $userCount > 0 ) {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users`";
		$rs = $conn->execute($query);
		if ( $rs )
			$userRows = $rs->numrows() + 1;
		else
			$userRows = 1;
		$username = $username . '.' . $userRows;
	}
	
	$photo 	 	= $config['ASSET_URL'] . "/frontend/images/profile.jpg";
	$reflength	= 7;
	$refchars 	= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$refcode 	= substr( str_shuffle( $refchars ), 0, $reflength );
	
	if ( isset($_SESSION['Ref_Code']) ) {
		$Ses_Ref_Code = $_SESSION['Ref_Code'];
		$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Code` = '$Ses_Ref_Code' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$training = $rs->getrows();
			$Ref_Trainer_ID = $training[0]['User_ID'];
		} else {
			$Ref_Trainer_ID = 0;
		}
	} else {
		$Ref_Trainer_ID = 0;
	}
	
	if ( filter_var($email, FILTER_VALIDATE_EMAIL) ) {
		if ( !empty($fname) && !empty($lname) && !empty($email) && !empty($password) ) {
			if ( $role == 'trainer' ) {
				$password = sha1($password);
			}
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
			$rs = $conn->execute($query);
			if ( $rs )
				$num = $rs->numrows();
			else
				$num = 0;
			if ( $num == 0 ) {
				$verify_code = substr(md5(uniqid(rand(), true)), 16, 16);
				$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_Email`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `Email_ID`, `Password`, `Profile_Photo`, `Verify_Code`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', '$Ref_Trainer_ID', '$logwith', '$role', '$fname', '$lname', '$full_name', '$username', '$email', '$password', '$photo', '$verify_code', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
				$rs = $conn->execute($query);
				if ( $rs ) {
					$link = $config['BASE_URL'] . '/verify-account/' . $verify_code . '/?verify=Email&setup=true';
					$message = SMEmail::Verify_Email($email, $full_name, $link);
					$subject = 'Verify your ePrentice account';
					if(eprenticeMail($email, $full_name, $subject, $message) == "true") {
						if ( $role == 'viewer' ) {
							$Notification_Text = "<b>" . $full_name . "</b>, a member you had invited has joined, ePrentice.";
							$Notification_Link = "#";
							$Notification_Image = $photo;
							insert_notification($Ref_Trainer_ID, $Notification_Text, $Notification_Link, $Notification_Image);
						}
						SMRedirect::go($config['BASE_URL'] . '/verify-account/not-verified/');
					} else {
						$error = 'Sorry! We are unable to send the verify mail. <br /><small>Don\'t worry, <a href="#">sent it</a> again.</small>';
					}
				} else {
					$error = 'Something went wrong! Please try again.';
				}
			} else {
				$error = 'Your email is already exists!';
			}
		} else {
			$error = 'All fields are required!';
		}
	}
}

$smarty->assign('refer', 	$refer);
$smarty->assign('logwith', 	$logwith);
$smarty->assign('fname', 	$fname);
$smarty->assign('lname', 	$lname);
$smarty->assign('email', 	$email);
$smarty->assign('role', 	$role);
$smarty->assign('error', 	$error);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('register.tpl');
$smarty->display('footer.tpl');
remove_social_session();
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$promoquery = "SELECT * FROM `" . $config['db_prefix'] . "promotions` WHERE `Trainer_ID` = $UID";
$promors = $conn->execute($promoquery);
if ( $promors ) {
	$promotions = $promors->getrows();
} else {
	$promotions = array();
}
$smarty->assign('promotions', $promotions);

$fbquery = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign` WHERE `Trainer_ID` = $UID";
$fbrs = $conn->execute($fbquery);
if ( $fbrs ) {
	$fbCampaign = $fbrs->getrows();
} else {
	$fbCampaign = array();
}
$smarty->assign('fbCampaign', $fbCampaign);

$twquery = "SELECT * FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` WHERE `Trainer_ID` = $UID";
$twrs = $conn->execute($twquery);
if ( $twrs ) {
	$twSchedule = $twrs->getrows();
} else {
	$twSchedule = array();
}
$smarty->assign('twSchedule', $twSchedule);

$UserDetails 	 = get_trainer_name($UID);
if( !empty($UserDetails[0]['FB_Access_Token']) ) {
	unset($_SESSION['SocialLoginFB']);
	$FB_Access_Token = $UserDetails[0]['FB_Access_Token'];
	$graph_url = "https://graph.facebook.com/me/accounts?type=page&access_token=" . $FB_Access_Token;
	$pages = json_decode(file_get_contents($graph_url));
	$fbPages = $pages->data;
} else {
	$_SESSION['SocialLoginFB'] = true;
	$fbPages = array();
}
$smarty->assign('fbPages', $fbPages);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('social-campaign.tpl');
$smarty->display('footer.tpl');
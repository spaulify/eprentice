<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

require $config['BASE_DIR'] . '/config/facebook/facebook.php';

$config['fb_appid']  		= '801997639945791'; 					// Test Server App ID
$config['fb_secret']    	= 'c547c93cecf6a760b59a9ba6c6ce6d57';	// Test Server App Secret
/*$config['fb_appid']   		= '1026526520775306';					// Local Server App ID
$config['fb_secret']    	= '462082a92213f816268dccda4742a1be';*/	// Local Server App Secret
$config['fb_redirect']  	= $config['BASE_URL'] . '/process/?fbTrue=true';
$config['fb_scope']			= 'email,manage_pages,publish_actions,user_friends,read_insights,user_photos,public_profile,publish_pages'; // ,read_stream,user_groups

$fbLoginUrl = 'https://www.facebook.com/dialog/oauth?client_id=' . $config['fb_appid'] . '&redirect_uri=' . $config['fb_redirect'] . '&scope=' . $config['fb_scope'];
$smarty->assign('fbLoginUrl', $fbLoginUrl);
$smarty->assign('Facebook_App_ID', $config['fb_appid']);

$facebook = new Facebook(array(
	'appId'  	 => $config['fb_appid'],
	'secret' 	 => $config['fb_secret'],
	'fileUpload' => true,
	'cookie' 	 => true
));
<?php 
if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

require $config['BASE_DIR'] . '/config/geo/geoipcity.inc';

$giCity = geoip_open($config['BASE_DIR'] . '/config/geo/GeoLiteCity.dat', GEOIP_STANDARD);
$smarty->assign('giCity', $giCity);
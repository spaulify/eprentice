<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

if ( function_exists('date_default_timezone_set') )
   date_default_timezone_set('Asia/Kolkata');

require dirname(__FILE__) . '/debug.php';
require dirname(__FILE__) . '/config.paths.php';
require dirname(__FILE__) . '/config.db.php';
require dirname(__FILE__) . '/config.local.php';
require dirname(__FILE__) . '/config.seo.php';

require $config['BASE_DIR']. '/classes/redirect.class.php';
require $config['BASE_DIR'] . '/config/security.php';
require $config['BASE_DIR'] . '/config/smarty/libs/Smarty.class.php';
require $config['BASE_DIR'] . '/config/adodb/adodb.inc.php';
require $config['BASE_DIR'] . '/config/dbconn.php';
require $config['BASE_DIR'] . '/config/stripe/vendor/autoload.php';

if (!defined('_CONSOLE')) {
    require $config['BASE_DIR'] . '/config/sessions.php';
}

disableRegisterGlobals();

require $config['BASE_DIR'] . '/config/smarty.php';
require $config['BASE_DIR'] . '/config/config.phpmailer.php';
require $config['BASE_DIR'] . '/classes/email.class.php';
require $config['BASE_DIR'] . '/classes/cropimage.class.php';
require $config['BASE_DIR'] . '/config/config.facebook.php';
require $config['BASE_DIR'] . '/config/config.google.php';
require $config['BASE_DIR'] . '/config/config.twitter.php';
require $config['BASE_DIR'] . '/config/config.linkedin.php';
require $config['BASE_DIR'] . '/config/config.geoip.php';
require $config['BASE_DIR'] . '/config/language.php';
require $config['BASE_DIR'] . '/include/functions.php';
<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

require $config['BASE_DIR'] . '/config/google/autoload.php';

$config['gp_client_id']   	= '244804543444-9ost396b9sgi8irib1t9v03mv6u1opc3.apps.googleusercontent.com';
$config['gp_client_secret'] = 'tKXSXgxC_d0k1Mg-PYTJ7YL9';
$config['gp_redirect_uri']	= $config['BASE_URL'] . '/process/?gpTrue=true';

$client = new Google_Client();
$client->setClientId($config['gp_client_id']);
$client->setClientSecret($config['gp_client_secret']);
$client->setRedirectUri($config['gp_redirect_uri']);
$client->addScope("email");
$client->addScope("profile");

$service = new Google_Service_Oauth2($client);

if (isset($_SESSION['gp_access_token']) && $_SESSION['gp_access_token']) {
	$client->setAccessToken($_SESSION['gp_access_token']);
}

$gpLoginUrl = $client->createAuthUrl();
$smarty->assign('gpLoginUrl', $gpLoginUrl);
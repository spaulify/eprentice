<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$server = $_SERVER['HTTP_HOST'];
$config = array();
if ( $server == 'localhost' ) {
	$config['BASE_URL']   	= 'http://localhost/eprentice';
	$config['ADMIN_URL']   	= 'http://localhost/eprentice/admin';
	$config['FFMPEG_PATH'] 	= "C:\\ffmpeg\\bin\\ffmpeg";
	$config['FFPROBE_PATH'] = "C:\\ffmpeg\\bin\\ffprobe";
} else {
	$config['BASE_URL']   	= 'http://' . $_SERVER['HTTP_HOST'];
	$config['ADMIN_URL']   	= 'http://' . $_SERVER['HTTP_HOST'] . '/admin';
	$config['FFMPEG_PATH'] 	=  "/opt/ffmpeg/ffmpeg";
	$config['FFPROBE_PATH'] =  "/opt/ffmpeg/ffprobe";
}
$config['BASE_DIR']   	= dirname(dirname(__FILE__));
$config['ADMIN_DIR']   	= $config['BASE_DIR'] . '/admin';
$config['RELATIVE']   	= '';
$config['ASSET_URL']  	= $config['BASE_URL'] . '/assets';
$config['ASSET_DIR']  	= $config['BASE_DIR'] . '/assets';
$config['UPLOAD_URL'] 	= $config['BASE_URL'] . '/uploads';
$config['UPLOAD_DIR'] 	= $config['BASE_DIR'] . '/uploads';
<?php 

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

if ( ! isset($_SESSION['lang']) ) {
	$_SESSION['lang'] = 'ro';
}

$language = array();
$lang = $_SESSION['lang'];

// Sample Example
/*$language['en']['massage'] = 'ePrentice in en';
$language['de']['massage'] = 'ePrentice in de';
$smarty->assign('lang_massage', $language[$lang]['massage']); */
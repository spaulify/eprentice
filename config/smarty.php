<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$tpl_dir = 'frontend';
if (defined('_ADMIN_STARTED')) {
    $tpl_dir = 'backend';
}

$smarty 				= new Smarty();
$smarty->compile_check 	= true;
$smarty->debugging 		= false;
$smarty->cache_dir		= $config['BASE_DIR'] . '/cache/' . $tpl_dir;
$smarty->template_dir 	= $config['BASE_DIR'] . '/templates/' . $tpl_dir;
$smarty->compile_dir 	= $config['BASE_DIR'] . '/cache/' . $tpl_dir;
foreach ($config as $key => $value) {
    $smarty->assign($key, $value);
}

$smarty->assign('base_url', 	$config['BASE_URL']);
$smarty->assign('base_dir', 	$config['BASE_DIR']);
$smarty->assign('admin_url', 	$config['ADMIN_URL']);
$smarty->assign('admin_dir', 	$config['ADMIN_DIR']);
$smarty->assign('relative', 	$config['RELATIVE']);
$smarty->assign('relative_tpl', $config['RELATIVE'] . '/templates/' . $tpl_dir);
$smarty->assign('asset_url', 	$config['ASSET_URL'] . '/' . $tpl_dir);
$smarty->assign('ajax_url', 	$config['BASE_URL'] . '/ajax');

$smarty->assign('Stripe_Publication_Key', 	$config['Stripe_Publication_Key']);
$smarty->assign('Stripe_Secret_Key', 		$config['Stripe_Secret_Key']);

$page = explode('/', $_SERVER['PHP_SELF']);
$smarty->assign('page_name', end($page));

$YTRegex = '~
    ^(?:https?://)?              # Optional protocol
     (?:www\.)?                  # Optional subdomain
     (?:youtube\.com|youtu\.be)  # Mandatory domain name
     /watch\?v=([^&]+)           # URI with video id as capture group 1
     ~x';
$smarty->assign('YTRegex', $YTRegex);
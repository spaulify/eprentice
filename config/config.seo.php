<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$seo 								= array();

// HOME Page
$seo['home_title'] 					= $config['site_name'];
$seo['home_keywords'] 				= '';
$seo['home_desc'] 					= '';
$seo['home_author'] 				= '';
<?php 

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$config['li_client_id']   	= '75bavm0s0c7i7g';
$config['li_client_secret'] = 'Yw8BhXsqseoe16vl';
$config['li_redirect_uri']	= $config['BASE_URL'] . '/process/?liTrue=true&code=LinkedinLogin';
$config['li_scope']			= 'r_basicprofile r_emailaddress';

require $config['BASE_DIR'] . '/config/linkedin/http.php';
require $config['BASE_DIR'] . '/config/linkedin/oauth_client.php';

$liLoginUrl = $config['BASE_URL'] . '/process/?liTrue=true&code=signin';
$smarty->assign('liLoginUrl', $liLoginUrl);
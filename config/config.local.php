<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$config['site_name'] 		= 'ePrentice';
$config['site_title'] 		= 'ePrentice';

// For Test
$config['Stripe_Publication_Key'] 	= 'pk_test_qVZCZvasGHT2vnMFo7cpmghx'; 
$config['Stripe_Secret_Key'] 		= 'sk_test_dMZy9XaoeK7dA8CwwSlAWNEG'; 

// For Live
//$config['Stripe_Publication_Key'] 	= 'sk_live_H9yUeuTSlwgS8wzlGpjC3s5h';
//$config['Stripe_Secret_Key'] 		= 'pk_live_4hp226DIyiwvJQtW6fCQNtXO';

$config['admin_name'] 		= '';
$config['admin_pass'] 		= '';
$config['noreply_email'] 	= 'jerry@eprentice.com';
$config['admin_email'] 		= 'jerry@eprentice.com';

$config['session_lifetime'] = '1440';
$config['session_driver'] 	= 'files';
$config['gzip_encoding'] 	= '1';
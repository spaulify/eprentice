<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$config['db_type']   = 'mysqli';
$config['db_host']   = 'localhost';
$config['db_user']   = 'eprentice';
$config['db_pass']   = 'eprentice';
$config['db_name']   = 'eprentice';
$config['db_prefix'] = 'ep_';
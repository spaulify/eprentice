<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

ini_set('session.name', 'ePrentice');
ini_set('session.use_cookies', 1);
ini_set('session.use_only_cookies', 0);
ini_set('session.gc_maxlifetime', intval($config['session_lifetime']));
if ($config['session_driver'] == 'database') {
    require $config['BASE_DIR'] . '/classes/session.class.php';
    ini_set('session.save_handler', 'user');
    session_set_save_handler(array('Session', 'open'), array('Session', 'close'), array('Session', 'read'), array('Session', 'write'), array('Session', 'destroy'), array('Session', 'gc'));
} else {
    ini_set('session.save_handler', 'files');
    ini_set('session.save_path', $config['BASE_DIR'] . '/tmp/sessions');
}
session_start();
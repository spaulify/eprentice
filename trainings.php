<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$message = '';$error = '';

$query = "SELECT * FROM `" . $config['db_prefix'] . "category` ORDER BY `Category_ID` ASC";
$rs = $conn->execute($query);
if ( $rs ) {
	$categories = $rs->getrows();
} else {
	$categories = array();
}
$smarty->assign('categories', $categories);

if ( isset($_POST['EmailShareSubmit']) ) {
	$emails = addslashes($_POST['emails']);
	$training_url = $_POST['Training_URL'];
	$training_id  = $_POST['Training_ID'];
	$emails = explode(',', $emails);
	foreach ( $emails as $email ) {
		if( $email == "" || $email == false ) {
			$error = "Email Address field cannot be blank!";
		} elseif( filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {
			$error = "Invalid Email Address inserted!";
		} else {
			$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_URL` = '$training_url' LIMIT 0, 1";
			$rs = $conn->execute($query);
			if ( $rs ) {
				$training = $rs->getrows();
			} else {
				$training = array();
			}
			$training_link = $config['BASE_URL'] . '/training/' . $training_url . '/';
			$email_unique_id		= substr(md5(uniqid(rand(), true)), 8, 8);
			$training_unique_link 	= $training_link . $email_unique_id . '/';
			$body = SMEmail::Share_Training_Email($email, $training_unique_link);
			$subject = 'Watch the training';
			if(eprenticeMail($email, '', $subject, $body) == "true") {
				$email_type	  = 'Training';
				$email_status = 'Go';
				email_tracking($UID, $EMAIL, $email, $training_id, $subject, addslashes(htmlspecialchars($body)), $training_url, $email_unique_id, $email_type, $email_status);			
				$query = "INSERT INTO `" . $config['db_prefix'] . "share_training`(`Share_ID`, `Trainer_ID`, `Training_ID`, `Share_Via`, `Share_Email`, `Share_Time`) VALUES (NULL, $UID, $training_id, 'email', '$email', '".date('Y-m-d H:i:s')."')";
				$rs = $conn->execute($query);
				$message = 'Training has been successfully shared by email.';
			}
		}
	}
}

$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Trainer_ID` = $UID ORDER BY `Training_ID` DESC";
$rs = $conn->execute($query);
if ( $rs ) {
	$trainings = $rs->getrows();
} else {
	$trainings = array();
}
$smarty->assign('trainings', $trainings);

$smarty->assign('message', 			$message);
$smarty->assign('error', 			$error);
$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('trainings.tpl');
$smarty->display('footer.tpl');
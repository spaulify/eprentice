<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';

unset($_SESSION['UID']);
unset($_SESSION['UFNAME']);
unset($_SESSION['ULNAME']);	
unset($_SESSION['UNAME']);
unset($_SESSION['USER']);
unset($_SESSION['UEMAIL']);
unset($_SESSION['UROLE']);

remove_social_session();

session_write_close();
header('Location:' . $config['BASE_URL'] . '/');
die();
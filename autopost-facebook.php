<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();

$query = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign` AS `FC`, `" . $config['db_prefix'] . "facebook_campaign_schedule` AS `FCS`, `" . $config['db_prefix'] . "promotions` AS `PROMO`, `" . $config['db_prefix'] . "users` AS `USER` WHERE `FCS`.`FB_Campaign_ID` = `FC`.`FB_CampPage_ID` AND `FCS`.`FB_Schedule_Promotion_ID` = `PROMO`.`Promotion_ID` AND `FCS`.`Trainer_ID` = `USER`.`User_ID`";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$fbSchedule = $rs->getrows();
} else {
	$fbSchedule = array();
}

for($i=0;$i<count($fbSchedule);$i++) {

	$UploadDirectory 	= $config['UPLOAD_URL'] . "/prm/";
	$TW_Schedule_Status	= '';
	$Current_DateTime 	= date('Y-m-d H:i:s');
	$FB_Schedule_ID		= $fbSchedule[$i]['FB_Schedule_ID'];
	$FB_Schedule_Start	= $fbSchedule[$i]['FB_Schedule_Start'];
	$FB_Schedule_End	= $fbSchedule[$i]['FB_Schedule_End'];
	$FB_Schedule_Status	= $fbSchedule[$i]['FB_Schedule_Status'];
	$page_id			= $fbSchedule[$i]['FB_Campaign_Page_ID'];
	$FB_Trainer_ID		= $fbSchedule[$i]['Trainer_ID'];
	$fbToken			= $fbSchedule[$i]['FB_Access_Token'];	
	$CampaignTitle		= $fbSchedule[$i]['FB_Schedule_Title'];
	$CampaignDesc		= $fbSchedule[$i]['FB_Schedule_Desc'];
	$CampaignImage		= $fbSchedule[$i]['Promotion_Thumb'];
	$CampaignLink		= $config['BASE_URL'] . '/promotion/' . $fbSchedule[$i]['Promotion_URL'] . '/';

	if( $FB_Schedule_Status == 'Active' && (strtotime($FB_Schedule_Start) <= strtotime($Current_DateTime)) ) {
		try {
			$fbUserID		= $fbSchedule[$i]['Facebook_ID'];
			$facebook->setAccessToken($fbToken);
			$pages 			= $facebook->api('/me/accounts');
			$fbFilepath		= $UploadDirectory.$CampaignImage;
			$fbMessage		= $CampaignTitle . "\n" . $CampaignDesc . "\n" . $CampaignLink;
			$facebook->setFileUploadSupport(true);
			$body = array(
				'message'	=> $fbMessage,
				'url'		=> $fbFilepath,
			);
			foreach($pages['data'] as $fbPageID) {
				if($fbPageID['id'] == $page_id) {
					$body['access_token'] = $fbPageID['access_token'];
					break;
				}
			}
			$FBPostResult = $facebook->api("$page_id/photos", "post", $body);
			/* Update Query for Deactive FB Campaign */
			if( strtotime($FB_Schedule_End) <= strtotime($Current_DateTime) ) {
				$updateQuery = "UPDATE `" . $config['db_prefix'] . "facebook_campaign_schedule` SET `FB_Schedule_Status` = 'Deactive' WHERE `FB_Schedule_ID` = $FB_Schedule_ID AND `Trainer_ID` = $FB_Trainer_ID";
				$rs = $conn->execute($updateQuery);
			}
	
		}
		catch(Exception $e) {
			echo 'Exception:', $e->getMessage(),"\n";
		}
	}
}
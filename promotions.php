<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$message = '';$error = '';

if ( isset($_POST['EmailShareSubmit']) ) {
	$emails = addslashes($_POST['emails']);
	$promotion_url = $_POST['Promotion_URL'];
	$promotion_id  = $_POST['Promotion_ID'];
	$emails = explode(',', $emails);
	foreach ( $emails as $email ) {
		if( $email == "" || $email == false ) {
			$error = "Email Address field cannot be blank!";
		} elseif( filter_var($email, FILTER_VALIDATE_EMAIL) === false ) {
			$error = "Invalid Email Address inserted!";
		} else {
			$query = "SELECT * FROM `" . $config['db_prefix'] . "promotions` WHERE `Promotion_URL` = '$promotion_url' LIMIT 0, 1";
			$rs = $conn->execute($query);
			if ( $rs ) {
				$promotion = $rs->getrows();
			} else {
				$promotion = array();
			}
			$promotion_link = $config['BASE_URL'] . '/promotion/' . $promotion_url . '/';
			$email_unique_id		= substr(md5(uniqid(rand(), true)), 8, 8);
			$promotion_unique_link 	= $promotion_link . $email_unique_id . '/';
			$body = SMEmail::Share_Training_Email($email, $promotion_unique_link);
			$subject = 'Watch the promotion';
			if (eprenticeMail($email, '', $subject, $body) == "true") {
				$query = "INSERT INTO `" . $config['db_prefix'] . "share_promotion`(`Share_ID`, `Trainer_ID`, `Promotion_ID`, `Share_Via`, `Share_Email`, `Share_Time`) VALUES (NULL, $UID, " . $promotion[0]['Promotion_ID'] . ", 'email', '$email', '".date('Y-m-d H:i:s')."')";
				$rs = $conn->execute($query);
				$email_type	  = 'Promotion';
				$email_status = 'Go';
				email_tracking($UID, $EMAIL, $email, $promotion_id, $subject, addslashes(htmlspecialchars($body)), $promotion_url, $email_unique_id, $email_type, $email_status);			
				$message = 'Promotion has been successfully shared by email.';
			}
		}
	}
}

$query = "SELECT `promo`.`Promotion_ID`, `promo`.`Trainer_ID`, `promo`.`Training_ID`, `promo`.`Promotion_Type`, `promo`.`Promotion_Title`, `promo`.`Promotion_Thumb`, `promo`.`Promotion_Desc`, `promo`.`Promotion_URL`, `promo`.`Promotion_File`, `promo`.`Promotion_Question`, `promo`.`Shared`, `promo`.`Created_On`, `promofile`.`File_ID`, `promofile`.`Promotion_Filename`, `promofile`.`Created_On`, `promoque`.`Question_ID`, `promoque`.`Question`, `promoque`.`Answer_One`, `promoque`.`Answer_One_Marks`, `promoque`.`Answer_Two`, `promoque`.`Answer_Two_Marks`, `promoque`.`Answer_Three`, `promoque`.`Answer_Three_Marks`, `promoque`.`Answer_Four`, `promoque`.`Answer_Four_Marks`, `promoque`.`Created_On` FROM `" . $config['db_prefix'] . "promotions` AS `promo` LEFT JOIN `" . $config['db_prefix'] . "promotion_questions` AS `promoque` ON (`promo`.`Promotion_Type` = 'Question' AND `promoque`.`Promotion_ID` = `promo`.`Promotion_ID`) LEFT JOIN `" . $config['db_prefix'] . "promotion_file` AS `promofile` ON ((`promo`.`Promotion_Type` = 'Image' OR `promo`.`Promotion_Type` = 'Video') AND `promofile`.`Promotion_ID` = `promo`.`Promotion_ID`) WHERE `promo`.`Trainer_ID` = $UID GROUP BY `promo`.`Promotion_ID` ORDER BY  `promo`.`Promotion_ID` DESC";
$rs = $conn->execute($query);
if ( $rs ) {
	$promotions = $rs->getrows();
} else {
	$promotions = array();
}
$smarty->assign('promotions', $promotions);

$smarty->assign('message', 			$message);
$smarty->assign('error', 			$error);
$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('promotions.tpl');
$smarty->display('footer.tpl');
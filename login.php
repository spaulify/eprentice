<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

$email = '';
$error = '';
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $email = addslashes($_POST['email']);
    $password = addslashes($_POST['password']);
	
    if ( !empty($email) && !empty($password) ) {
		$sha1_password = sha1($password);
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' AND `Password` = '$sha1_password' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num == 1 ) {
			$user = $rs->getrows();
			$verify = $user[0]['Profile_Verify'];
			if ( $verify == 'yes' ) {
				if (isset($_POST['remember'])) {
					$year = time() + 365*86400;
					setcookie('remember_email', $email, $year);
					setcookie('remember_pass', $password, $year);
				} elseif (!isset($_POST['remember'])) {
					if (isset($_COOKIE['remember_email']) && isset($_COOKIE['remember_pass']) ) {
						$past = time() - 100;
						setcookie('remember_email', '', $past);
						setcookie('remember_pass', '', $past);
					}
				}
				$_SESSION['UID'] 	 = $user[0]['User_ID'];
				$_SESSION['UFNAME']  = $user[0]['First_Name'];
				$_SESSION['ULNAME']  = $user[0]['Last_Name'];
				$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
				$_SESSION['USER'] 	 = $user[0]['Username'];
				$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
				$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
				if ( isset($_GET['return']) ) {
					SMRedirect::go($_GET['return']);
				} else {
					if ( $user[0]['User_Role'] == 'trainer' ) {
						SMRedirect::go($config['BASE_URL'] . '/dashboard/');
					} else {
						SMRedirect::go($config['BASE_URL'] . '/news/');
					}
				}
			} else {
				$error = 'Your account is not verified! <br /><small>Lost your verify email? Don\'t worry, <a href="#">send it</a> again.</small>';
			}
		} else {
			$error = 'You have entered an invalid username or password!';
		}
    } else {
		$error = 'Please enter your email and password!';
	}
}

if ( isset($_COOKIE['remember_email']) && isset($_COOKIE['remember_pass']) ) {
	$remember_email = $_COOKIE['remember_email'];
	$remember_pass = $_COOKIE['remember_pass'];
} else {
	$remember_email = '';
	$remember_pass = '';
}

$smarty->assign('remember_email', $remember_email);
$smarty->assign('remember_pass', $remember_pass);
$smarty->assign('email', $email);
$smarty->assign('error', $error);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('login.tpl');
$smarty->display('footer.tpl');
remove_social_session();
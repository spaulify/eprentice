<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$query = "SELECT * FROM `" . $config['db_prefix'] . "category` ORDER BY `Category_ID` ASC";
$rs = $conn->execute($query);
if ( $rs ) {
	$categories = $rs->getrows();
} else {
	$categories = array();
}
$firstStep 	= (count($categories) % 2 == 1)? (count($categories) / 2) + 0.5 : count($categories) / 2;
$secondStep = count($categories) / 2;
$smarty->assign('categories', $categories);
$smarty->assign('firstStep',  $firstStep);
$smarty->assign('secondStep', $secondStep);

$query1 = "SELECT * FROM `" . $config['db_prefix'] . "preferences` WHERE `Pref_User_ID` = $UID LIMIT 0, 1";
$rs1 = $conn->execute($query1);
if ( $rs1 ) {
	$preferences = $rs1->getrows();
} else {
	$preferences = array();
}

if( !empty($preferences) ) {
	$pref_cat = explode(',', $preferences[0]['Pref_Category']);
} else {
	$pref_cat = array();
}
$smarty->assign('pref_cat', $pref_cat);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('set-preferences.tpl');
$smarty->display('footer.tpl');
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$Promotion_URL = $_GET['param'];
$query = "SELECT * FROM `" . $config['db_prefix'] . "promotions` WHERE `Promotion_URL` = '$Promotion_URL' LIMIT 0, 1";
$rs = $conn->execute($query);
if ( $rs ) {
	$promotion = $rs->getrows();
} else {
	$promotion = array();
}
$smarty->assign('promotion', $promotion[0]);

$Promotion_ID 	= $promotion[0]['Promotion_ID'];
$Promotion_Type = $promotion[0]['Promotion_Type'];
$Training_ID 	= $promotion[0]['Training_ID'];
$Trainer_ID 	= $promotion[0]['Trainer_ID'];

if( $Promotion_Type == 'Image' || $Promotion_Type == 'Video') {
	$query2 = "SELECT * FROM `" . $config['db_prefix'] . "promotion_file` WHERE `Promotion_ID` = $Promotion_ID AND `Promotion_Type` = '$Promotion_Type'";
	$rs2 = $conn->execute($query2);
	if ( $rs2 ) {
		$promotion_details = $rs2->getrows();
	} else {
		$promotion_details = array();
	}
	$smarty->assign('promotion_details', $promotion_details);
} elseif( $Promotion_Type == 'Question' ) {
	$query2 = "SELECT * FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Promotion_ID` = $Promotion_ID AND `Promotion_Type` = '$Promotion_Type'";
	$rs2 = $conn->execute($query2);
	if ( $rs2 ) {
		$promotion_details = $rs2->getrows();
	} else {
		$promotion_details = array();
	}
	$smarty->assign('promotion_details', $promotion_details);
}
$query3 = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_ID` = $Training_ID AND `Trainer_ID` = $Trainer_ID";
$rs3 = $conn->execute($query3);
if ( $rs3 ) {
	$training = $rs3->getrows();
} else {
	$training = array();
}
$smarty->assign('training', $training[0]);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$promotion[0]['Promotion_Title'] . ' - ' . $seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
if ( $UID ) {
	$smarty->display('navbar.tpl');
    $smarty->display('sidebar.tpl');
}
$smarty->display('promotion.tpl');
$smarty->display('footer.tpl');
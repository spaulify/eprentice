ePrentice

This is a website in PHP based Smarty framework, developed by Wisely Online Services Private Limited.

Setup

1. To installing ePrentice, unpacking it to your website root directory.
2. In the sql directory, you can find the db script. Create a database and import this script into that database.
3. Change database connectivity credentials with your own in config/config.db.php.
4. Change the $config['BASE_URL'] with your host name in config/config.paths.php.
5. All done :).
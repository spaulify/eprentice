<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$Notification_ID = $_POST["Notification_ID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($Notification_ID) ) {
		$queryNoti = "SELECT * FROM `" . $config['db_prefix'] . "notifications` WHERE `Notification_ID` = $Notification_ID";
		$rs = $conn->execute($queryNoti);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num > 0 ) {
			$query = "UPDATE `" . $config['db_prefix'] . "notifications` SET `Notification_Status` = 'Read' WHERE `Notification_ID` = $Notification_ID";
			$rs = $conn->execute($query);
			if( $rs )
				echo 'Read';
			else
				echo 'Unread';
		}
	}
}
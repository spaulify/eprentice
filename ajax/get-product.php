<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if( isset($_POST) ) {
		
	$trainer_id = $_SESSION['UID'];
	$product_id = $_POST['ProductID'];
	
	if ( !empty($trainer_id) && !empty($product_id) ) {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "products` WHERE `Trainer_ID` = $trainer_id AND `Product_ID` = $product_id LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$ProductDetails = $rs->getrows();
			echo json_encode($ProductDetails[0]);
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
		}
	} else {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Product not found.</div>');
	}
} else {
	die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
}
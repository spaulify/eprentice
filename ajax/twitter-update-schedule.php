<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$tw_schedule_desc		= addslashes($_POST['tw_schedule_desc']);
	$tw_schedule_start_date	= addslashes($_POST['tw_schedule_start_date']);
	$tw_schedule_end_date	= addslashes($_POST['tw_schedule_end_date']);
	$tw_trainer_id			= addslashes($_POST['tw_trainer_id']);
	$tw_schedule_id			= addslashes($_POST['tw_schedule_id']);
	$tw_schedule_status 	= '';
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	$checkQuery = "SELECT * FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` WHERE `TW_Schedule_Status` = 'Active' AND `Trainer_ID` = $tw_trainer_id";
	$checkRs = $conn->execute($checkQuery);
	$row = $checkRs->numrows();
	if( $row > 0 ) {
		$tw_schedule_status = 'Deactive';
	} else {
		$current_datetime = date('Y-m-d H:i:s');
		if( (strtotime($tw_schedule_start_date) <= strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) >= strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Active';
		} else if( (strtotime($tw_schedule_start_date) <= strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) < strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Deactive';
		} else if( (strtotime($tw_schedule_start_date) < strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) <= strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Active';
		} elseif( strtotime($tw_schedule_start_date) > strtotime($current_datetime) ) {
			$tw_schedule_status = 'Deactive';
		}
	}
	if ( !empty($tw_schedule_desc) && !empty($tw_schedule_start_date) && !empty($tw_schedule_end_date) && !empty($tw_schedule_id) && !empty($tw_trainer_id) ) {
		$selectQuery = "SELECT * FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` WHERE `TW_Schedule_ID` = $tw_schedule_id AND `Trainer_ID` = $tw_trainer_id";
		$selectRs = $conn->execute($selectQuery);
		$row = $selectRs->numrows();
		if( $row > 0 ) {
			$query = "UPDATE `" . $config['db_prefix'] . "twitter_campaign_schedule` SET `TW_Schedule_Desc` = '$tw_schedule_desc', `TW_Schedule_Start` = '$tw_schedule_start_date', `TW_Schedule_End` = '$tw_schedule_end_date', `TW_Schedule_Status` = '$tw_schedule_status' WHERE `TW_Schedule_ID` = $tw_schedule_id AND `Trainer_ID` = $tw_trainer_id";
		} else {
			echo 0;
		}
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
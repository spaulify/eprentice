<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_REQUEST) ) {
		
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	\Stripe\Stripe::setApiKey($config['Stripe_Secret_Key']);
	
	$UID  				= isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
	$NAME 				= isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
	$EMAIL 				= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
	$UFNAME 			= isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
	$ULNAME 			= isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';
	
	$seller_id 			= $_POST['trainer_id'];
	$product_id 		= $_POST['product_id'];
	$product_name 		= $_POST['product_name'];
	$product_price 		= $_POST['product_price'];
	$product_cent 		= ($product_price*100);
	$product_link 		= $_POST['product_link'];
	$training_url		= $_POST['training_url'];
	$training_thumb		= $_POST['training_thumb'];
	$stripeToken		= $_POST['stripeToken'];
	$login_link			= $config['BASE_URL'] . '/login/';
	$Notification_Text 	= '<strong>'.$UFNAME.' '.$ULNAME."</strong> purchased your product <strong>".$product_name."</strong> on <strong>$".$product_price."</strong>.";
	$Notification_Link	= $config['BASE_URL'] . '/training/' . $training_url . '/';
	$Notification_Image	= $config['BASE_URL'] . '/uploads/trp/' . $training_thumb;
	
	try {
		if (!isset($stripeToken))
			echo 0;
			
		// Create a Customer
		$customer = \Stripe\Customer::create(array(
			"source" 		=> $stripeToken,
			"description" 	=> $NAME,
			"email" 		=> $EMAIL,
		));
		
		// Create a Payment
		$payment = \Stripe\Charge::create(array(
			"amount" 	=> $product_cent,	// Amount is in cent
			"currency" 	=> "usd",
			"customer" 	=> $customer->id,	// Charge the Customer instead of the card
			"metadata" 	=> array("Product ID" => $seller_id, "Product Name" => $product_name, "Product Price" => '$'.$product_price)
		));
		
		$payment_array = $payment->__toArray(true);
		if ( $payment_array['status'] == 'succeeded' ) {
			$query = "INSERT INTO `" . $config['db_prefix'] . "payments`(`Stripe_Transaction_ID`, `Seller_ID`, `Buyer_ID`, `Buyer_First_Name`, `Buyer_Last_Name`, `Product_ID`, `Product_Name`, `Purchase_Amount`, `Purchased_ON`) VALUES ('" . $payment_array['balance_transaction'] . "', $seller_id, $UID, '$UFNAME', '$ULNAME', $product_id, '$product_name', $product_price, '" . date('Y-m-d H:i:s', $payment_array['created']) . "')";
			$rs = $conn->execute($query);
			if ( $rs ) {
				$message = SMEmail::Payment_Invoice($EMAIL, $UFNAME, $ULNAME, $product_name, $product_price, date('Y-m-d H:i:s', $payment_array['created']), $payment_array['balance_transaction'], $product_link, $login_link);
				$subject = 'Thankyou for Purchasing the Product: ' . $product_name;
				if( eprenticeMail($EMAIL, $UFNAME . ' ' . $ULNAME, $subject, $message) == "true") {
					insert_notification($seller_id, $Notification_Text, $Notification_Link, $Notification_Image);
					echo 1;
				}
			}
		}
	} catch (Exception $e) {
		echo $e->getMessage();
	}
	
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$scheduleID 	= $_POST["scheduleID"];
	$userID 		= $_POST["userID"];
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($scheduleID) && !empty($userID) ) {
		$query = "DELETE FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` WHERE `TW_Schedule_ID` = $scheduleID AND `Trainer_ID` = $userID";
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
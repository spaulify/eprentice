<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$Followee_ID 	= $_POST["Followee_ID"];
	$Follower_ID 	= $_POST["Follower_ID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($Followee_ID) && !empty($Follower_ID) ) {
		
		$queryFollow = "SELECT * FROM `" . $config['db_prefix'] . "follow` WHERE `Followee_ID` = $Followee_ID AND `Follower_ID` = $Follower_ID LIMIT 0, 1";
		$rs = $conn->execute($queryFollow);
		if( $rs ) {
			$num = $rs->numrows();
		} else {
			$num = 0;
		}
		if ( $num > 0 ) {
			$noti = $rs->getrows();
			$notiID = $noti[0]['Notification_ID'];
			$query = "DELETE FROM `" . $config['db_prefix'] . "follow` WHERE `Followee_ID` = $Followee_ID AND `Follower_ID` = $Follower_ID";
			$rs = $conn->execute($query);
			$query1 = "DELETE FROM `" . $config['db_prefix'] . "notifications` WHERE `Notification_ID` = $notiID AND `User_ID` = $Followee_ID";
			$rs1 = $conn->execute($query1);
			if ( $rs && $rs1 ) {
				echo 'Follow';
			} else {
				echo 0;
			}
		} else {
			$Follower_Name = get_trainer_name($Follower_ID);
			$Notification_Text = '<strong>' . $Follower_Name[0]['Full_Name'] . "</strong> is now following you.";
			$Notification_Link = $config['BASE_URL'] . '/profile/' . $Follower_Name[0]['Username'] . '/';
			$Notification_Image = $Follower_Name[0]['Profile_Photo'];
         	insert_notification($Followee_ID, $Notification_Text, $Notification_Link, $Notification_Image);
			$lqID  = "SELECT LAST_INSERT_ID() AS `LAST_NOTIFICATION_ID`";
			$rqID  = $conn->execute($lqID);
			$genID = $rqID->getrows();
			$Notification_ID = $genID[0]['LAST_NOTIFICATION_ID'];
			$query = "INSERT INTO `" . $config['db_prefix'] . "follow` (`Notification_ID`, `Followee_ID`, `Follower_ID`, `Date_Time`) VALUES($Notification_ID, $Followee_ID, $Follower_ID, '".date('Y-m-d H:i:s')."')";
			$rs = $conn->execute($query);
			if ( $rs ) {
				echo 'Unfollow';
			} else {
				echo 0;
			}
		}
	}
}
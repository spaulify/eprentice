<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$UploadDirectory    = $config['UPLOAD_DIR'] . '/trp/';
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$trainer_id 	 		= $_SESSION['UID'];
	$training_name 			= addslashes($_POST['training_name']);
	$training_cat 	 		= addslashes($_POST['training_cat']);
	$training_link_text 	= addslashes($_POST['training_link_text']);	
	$training_headline  	= addslashes($_POST['training_headline']);
	$training_point_one  	= addslashes($_POST['training_point_one']);
	$training_point_two  	= addslashes($_POST['training_point_two']);
	$training_point_three  	= addslashes($_POST['training_point_three']);
	$training_point_four	= addslashes($_POST['training_point_four']);	
	$about_training  		= addslashes($_POST['about_training']);
	$training_url 	 		= substr(md5(uniqid(rand(), true)), 8, 8);	
	$training_url_em 		= 'em_' . $training_url;
	$training_url_fb 		= 'fb_' . $training_url;
	$training_url_tw 		= 'tw_' . $training_url;
	$training_url_li 		= 'li_' . $training_url;
	$training_url_gp 		= 'gp_' . $training_url;
	$training_tags 	 		= addslashes($_POST['training_tags']);
	$similar_section 		= addslashes($_POST['similar_training']);
	$visible_time 	 		= addslashes($_POST['visible_time']);
	$cta_text				= addslashes($_POST['cta_text']);
	$publish_datetime		= addslashes($_POST['publish_datetime']);
	$product_id				= addslashes($_POST['product_list_select']);
	$training_thumb	 		= addslashes($_POST['training_thumb']);
	$randoms 				= rand(0, 9999999999);
	
	$video_type 			= addslashes($_POST['video_type']);
	$training_video_type 	= addslashes($_POST['training_video_type']);
	
	 if( empty($training_thumb) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Training Thumbnail.</div>');
	} elseif( empty($visible_time) || !is_numeric($visible_time) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Visible Time.</div>');
	} elseif( $cta_text == "" || $cta_text == false ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Buy Button Text.</div>');
	} elseif( !preg_match("/^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]*$/", $cta_text) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Buy Button Text allows Alphabets and Special characters only.</div>');
	} elseif( !preg_match("/^[a-zA-Z]*$/", substr($cta_text, 0,1)) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Buy Button Text should start with Alphabets only.</div>');
	} elseif( empty($publish_datetime) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Publish Date & Time Required.</div>');
	} elseif( empty($product_id) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! No Product found.</div>');
	} elseif( !empty($product_id) ) {
		if ( $video_type == 'custom' ) {
			switch(strtolower($_FILES['intro_video']['type'])) {
				case 'video/mp4':
				case 'video/webm':
				case 'video/ogg':
				case 'video/quicktime':
					break;
				default:
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Intro Video File!</div>');
			}
			$IntroFile_Name         = strtolower($_FILES['intro_video']['name']);
			$IntroFile_Ext          = substr($IntroFile_Name, strrpos($IntroFile_Name, '.'));
			$IntroRandom_Number     = 'intro-' . $randoms;
			$IntroNewFileName       = $IntroRandom_Number.$IntroFile_Ext;
		} else {
			$IntroNewFileName		= addslashes($_POST['intro_video_youtube']);
		}
		
		if ( $training_video_type == 'custom' ) {
			switch(strtolower($_FILES['training_video']['type'])) {
				case 'video/mp4':
				case 'video/webm':
				case 'video/ogg':
				case 'video/quicktime':
					break;
				default:
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Training Video File!</div>');
			}
			$TrainingFile_Name          = strtolower($_FILES['training_video']['name']);
			$TrainingFile_Ext           = substr($TrainingFile_Name, strrpos($TrainingFile_Name, '.'));
			$TrainingRandom_Number      = 'training-' . $randoms;
			$TrainingNewFileName        = $TrainingRandom_Number.$TrainingFile_Ext;
		} else {
			$TrainingNewFileName		= addslashes($_POST['training_video_youtube']);
		}
		
		/*switch(strtolower($_FILES['training_thumb']['type'])) {
			case 'image/jpg':
			case 'image/jpeg':
			case 'image/png':
				break;
			default:
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Training Thumbnail File!</div>');
		}
		$ThumbnailFile_Name         = strtolower($_FILES['training_thumb']['name']);
		$ThumbnailFile_Ext          = substr($ThumbnailFile_Name, strrpos($ThumbnailFile_Name, '.'));
		$ThumbnailRandom_Number     = 'trainingThumb' . $randoms;
		$ThumbnailNewFileName       = $ThumbnailRandom_Number.$ThumbnailFile_Ext;
		if ( move_uploaded_file($_FILES['training_thumb']['tmp_name'], $UploadDirectory.$ThumbnailNewFileName) ) {
			// Old Concept of Upload THumbnail without Cropping
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Video is not uploaded.</div>');
		}
		*/
		
		if( $video_type == 'custom' ) {
			move_uploaded_file($_FILES['intro_video']['tmp_name'], $UploadDirectory.$IntroNewFileName);
		}
		
		if ( $training_video_type == 'custom' ) {
			move_uploaded_file($_FILES['training_video']['tmp_name'], $UploadDirectory.$TrainingNewFileName);
		}
				
		$query = "INSERT INTO `" . $config['db_prefix'] . "trainings`(`Training_ID`, `Trainer_ID`, `Product_ID`, `Training_Name`, `Training_Category`, `Training_Headline`, `Training_Point_One`, `Training_Point_Two`, `Training_Point_Three`, `Training_Point_Four`, `Training_Desc`, `Training_URL`, `Training_URL_EM`, `Training_URL_FB`, `Training_URL_TW`, `Training_URL_LI`, `Training_URL_GP`, `Training_Video`, `Join_Training_Btn`, `Intro_Video`, `Training_Tags`, `Similar_Section`, `CTA_Text`, `CTA_Time`, `Training_Thumb`, `Shared`, `Training_Published_On`, `Created_On`) VALUES (NULL, $trainer_id, $product_id, '$training_name', '$training_cat', '$training_headline', '$training_point_one', '$training_point_two', '$training_point_three', '$training_point_four', '$about_training', '$training_url', '$training_url_em', '$training_url_fb', '$training_url_tw', '$training_url_li', '$training_url_gp', '$TrainingNewFileName', '$training_link_text', '$IntroNewFileName', '$training_tags', '$similar_section', '$cta_text', '$visible_time', '$training_thumb', '', '$publish_datetime', '".date('Y-m-d H:i:s')."')";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Trainer_ID` = $trainer_id";
			$rs = $conn->execute($query);
			$Ref_Users = $rs->getrows();
			for ( $i = 0; $i < count($Ref_Users); $i++ ) {
				$Notification_Text = "Trainer <b>" . $_SESSION['UFNAME'] . "</b> posted a new training <b>$training_name</b>.";
				$Notification_Image = $config['ASSET_URL'] . '/frontend/images/question.jpg';
				insert_notification($Ref_Users[$i]['User_ID'], $Notification_Text, $training_url, $Notification_Image);
			}
			die('Success');
		} else {
			//unlink($UploadDirectory.$training_thumb);
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
		}		
	}
} else {
	die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
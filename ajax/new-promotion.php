<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$UploadDirectory    = $config['UPLOAD_DIR'] . '/prm/';

	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$MovePromoFile 		= false;
	$trainer_id 	 	= $_SESSION['UID'];
	$promotion_type		= addslashes($_POST['promotion_type']);
	
	if(empty($promotion_type)) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Select Promotion Type first before Submit!</div>');
	} elseif($promotion_type == "Image") {
		$training_id_img		= addslashes($_POST['training_id_img']);
		$promotion_title_img	= addslashes($_POST['promotion_title_img']);
		$promotion_url 	 		= substr(md5(uniqid(rand(), true)), 8, 8);
		$promotion_desc_img		= addslashes($_POST['promotion_desc_img']);
		$promo_start_date_img	= addslashes($_POST['promo_start_date_img']);
		$promo_end_date_img		= addslashes($_POST['promo_end_date_img']);
		
		$promotion_thumb_img	= $_FILES['promotion_thumb_img']['name'];
		$randoms 				= rand(0, 9999999999);
		$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_img']['name']);
		$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
		$PromoThumb_Number		= 'promo-thumb-' . $randoms;
		$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
		
		if ( !empty($training_id_img) && !empty($promotion_title_img) && !empty($promotion_desc_img) && !empty($promo_start_date_img) && !empty($promo_end_date_img) && !empty($promotion_thumb_img) ) {
			
			if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
				switch(strtolower($_FILES['promotion_thumb_img']['type'])) {
					case 'image/jpg':
					case 'image/jpeg':
					case 'image/png':
					case 'image/gif':
					case 'image/bmp':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
						break;
				}
			}
			move_uploaded_file($_FILES['promotion_thumb_img']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
			
			$queryIMG = "INSERT INTO `" . $config['db_prefix'] . "promotions` (`Promotion_ID`, `Trainer_ID`, `Training_ID`, `Promotion_Type`, `Promotion_Thumb`, `Promotion_Title`, `Promotion_Desc`, `Promotion_URL`, `Promotion_File`, `Promotion_Question`, `Promotion_Start`, `Promotion_End`, `Shared`, `Created_On`) VALUES (NULL, $trainer_id, $training_id_img, '$promotion_type', '$PromoThumbNewName', '$promotion_title_img', '$promotion_desc_img', '$promotion_url', 'Yes', 'No', '$promo_start_date_img', '$promo_end_date_img', '', '".date('Y-m-d H:i:s')."')";
			$rsIMG = $conn->execute($queryIMG);
			$lqID  = "SELECT LAST_INSERT_ID() AS `LAST_PROMOTION_ID`";
			$rqID  = $conn->execute($lqID);
			$genID = $rqID->getrows();
			$Promotion_ID 		= $genID[0]['LAST_PROMOTION_ID'];
			$Supported_Image 	= array('gif','jpg','jpeg','png','bmp');
			$Restrict_Condition = array("False");
			$CheckFileIMG 		= array();
			
			if( !empty($_FILES['promotion_file_img']['name']) ) {
				foreach($_FILES['promotion_file_img']['name'] as $file => $name) {
					$randoms 			= rand(0, 9999999999);
					$PromoFile_Name		= strtolower($_FILES['promotion_file_img']['name'][$file]);
					$PromoFile_Ext		= strtolower(pathinfo($PromoFile_Name, PATHINFO_EXTENSION));
					$PromoFile_Number	= 'promo-' . $randoms;
					$PromoFileNewName	= $PromoFile_Number.'.'.$PromoFile_Ext;

					if ( !in_array($PromoFile_Ext, $Supported_Image) ) {
						array_push($CheckFileIMG, "False");						
					} else {
						array_push($CheckFileIMG, "True");
					}
					if( in_array("False", $CheckFileIMG) ) {
						$conn->execute("SET FOREIGN_KEY_CHECKS=0");
						unlink($UploadDirectory.$PromoThumbNewName);
						$DELqueryIMG = "DELETE FROM `" . $config['db_prefix'] . "promotions` WHERE `Promotion_ID` = $Promotion_ID";
						$DELrsIMG = $conn->execute($DELqueryIMG);
						$conn->execute("SET FOREIGN_KEY_CHECKS=1");
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Image File!</div>');
					} else {
						$conn->execute("SET FOREIGN_KEY_CHECKS=1");
						$MovePromoFile	= move_uploaded_file($_FILES['promotion_file_img']['tmp_name'][$file], $UploadDirectory.$PromoFileNewName);
						if ( $MovePromoFile ) {
							$queryFILE = "INSERT INTO `" . $config['db_prefix'] . "promotion_file` (`File_ID`, `Promotion_ID`, `Promotion_Type`, `Promotion_Filename`, `Created_On`) VALUES (NULL, $Promotion_ID, '$promotion_type', '$PromoFileNewName', '".date('Y-m-d H:i:s')."')";
							$rsFILE = $conn->execute($queryFILE);
						} else {
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Image is not uploaded.</div>');
						}
					}
				}
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
			}
			if ( $rsFILE ) {
				$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Trainer_ID` = $trainer_id";
				$rs = $conn->execute($query);
				$Ref_Users = $rs->getrows();
				for ( $i = 0; $i < count($Ref_Users); $i++ ) {
					$Notification_Text = "Trainer <b>" . $_SESSION['UFNAME'] . "</b> published a new promotion <b>$promotion_title_img</b>.";
					$Promotion_Link = $config['BASE_URL'] . '/promotion/' . $promotion_url . '/';
					$Notification_Image = $config['UPLOAD_URL'] . '/prm/' . $PromoFileNewName;
					insert_notification($Ref_Users[$i]['User_ID'], $Notification_Text, $Promotion_Link, $Notification_Image);
				}
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
		}
	} elseif($promotion_type == "Video") {
		$training_id_vid		= addslashes($_POST['training_id_vid']);
		$promotion_title_vid	= addslashes($_POST['promotion_title_vid']);
		$promotion_url 	 		= substr(md5(uniqid(rand(), true)), 8, 8);
		$promotion_desc_vid		= addslashes($_POST['promotion_desc_vid']);
		$promo_start_date_vid	= addslashes($_POST['promo_start_date_vid']);
		$promo_end_date_vid		= addslashes($_POST['promo_end_date_vid']);
		
		$promotion_thumb_vid	= $_FILES['promotion_thumb_vid']['name'];
		$randoms 				= rand(0, 9999999999);
		$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_vid']['name']);
		$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
		$PromoThumb_Number		= 'promo-thumb-' . $randoms;
		$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
		
		if ( !empty($training_id_vid) && !empty($promotion_title_vid) && !empty($promotion_desc_vid) && !empty($promo_start_date_vid) && !empty($promo_end_date_vid) && !empty($promotion_thumb_vid) ) {
			
			if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
				switch(strtolower($_FILES['promotion_thumb_vid']['type'])) {
					case 'image/jpg':
					case 'image/jpeg':
					case 'image/png':
					case 'image/gif':
					case 'image/bmp':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
						break;
				}
			}
			move_uploaded_file($_FILES['promotion_thumb_vid']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
			
			$queryIMG = "INSERT INTO `" . $config['db_prefix'] . "promotions`(`Promotion_ID`, `Trainer_ID`, `Training_ID`, `Promotion_Type`, `Promotion_Thumb`, `Promotion_Title`, `Promotion_Desc`, `Promotion_URL`, `Promotion_File`, `Promotion_Question`, `Promotion_Start`, `Promotion_End`, `Shared`, `Created_On`) VALUES (NULL, $trainer_id, $training_id_vid, '$promotion_type', '$PromoThumbNewName', '$promotion_title_vid', '$promotion_desc_vid', '$promotion_url', 'Yes', 'No', '$promo_start_date_vid', '$promo_end_date_vid', '', '".date('Y-m-d H:i:s')."')";
			$rsVID = $conn->execute($queryIMG);
			$lqID  = "SELECT LAST_INSERT_ID() AS `LAST_PROMOTION_ID`";
			$rqID  = $conn->execute($lqID);
			$genID = $rqID->getrows();			
			$Promotion_ID = $genID[0]['LAST_PROMOTION_ID'];
			$Supported_Video 	= array('mp4','webm','ogg','m4v','avi');
			$Restrict_Condition = array("False");
			$CheckFileVID 		= array();
			
			if( !empty($_FILES['promotion_file_vid']['name']) ) {
				foreach($_FILES['promotion_file_vid']['name'] as $file => $name) {
					$randoms 			= rand(0, 9999999999);
					$PromoFile_Name		= strtolower($_FILES['promotion_file_vid']['name'][$file]);
					$PromoFile_Ext		= strtolower(pathinfo($PromoFile_Name, PATHINFO_EXTENSION));
					$PromoFile_Number	= 'promo-' . $randoms;
					$PromoFileNewName	= $PromoFile_Number.'.'.$PromoFile_Ext;
				
					if ( !in_array($PromoFile_Ext, $Supported_Video) ) {
						array_push($CheckFileVID, "False");						
					} else {
						array_push($CheckFileVID, "True");
					}
					if( in_array("False", $CheckFileVID) ) {
						$conn->execute("SET FOREIGN_KEY_CHECKS=0");
						unlink($UploadDirectory.$PromoThumbNewName);
						$DELqueryVID = "DELETE FROM `" . $config['db_prefix'] . "promotions` WHERE `Promotion_ID` = $Promotion_ID";
						$DELrsVID = $conn->execute($DELqueryVID);
						$conn->execute("SET FOREIGN_KEY_CHECKS=1");
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Video File!</div>');
					} else {
						$conn->execute("SET FOREIGN_KEY_CHECKS=1");
						$MovePromoFile	= move_uploaded_file($_FILES['promotion_file_vid']['tmp_name'][$file], $UploadDirectory.$PromoFileNewName);
						if ( $MovePromoFile ) {			
							$queryFILE = "INSERT INTO `" . $config['db_prefix'] . "promotion_file`(`File_ID`, `Promotion_ID`, `Promotion_Type`, `Promotion_Filename`, `Created_On`) VALUES (NULL, $Promotion_ID, '$promotion_type', '$PromoFileNewName', '".date('Y-m-d H:i:s')."')";
							$rsFILE = $conn->execute($queryFILE);
						} else {
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Video is not uploaded.</div>');
						}
					}					
				}
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
			}	
			if ( $rsFILE ) {
				$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Trainer_ID` = $trainer_id";
				$rs = $conn->execute($query);
				$Ref_Users = $rs->getrows();
				for ( $i = 0; $i < count($Ref_Users); $i++ ) {
					$Notification_Text = "Trainer <b>" . $_SESSION['UFNAME'] . "</b> published a new promotion <b>$promotion_title_vid</b>.";
					$Promotion_Link = $config['BASE_URL'] . '/promotion/' . $promotion_url . '/';
					$Notification_Image = $config['UPLOAD_URL'] . '/prm/' . $PromoFileNewName;
					insert_notification($Ref_Users[$i]['User_ID'], $Notification_Text, $Promotion_Link, $Notification_Image);
				}
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
		}
	} elseif($promotion_type == "Question") {
		$training_id_que		= addslashes($_POST['training_id_que']);
		$promotion_title_que	= addslashes($_POST['promotion_title_que']);
		$promotion_url 	 		= substr(md5(uniqid(rand(), true)), 8, 8);
		$promotion_desc_que		= addslashes($_POST['promotion_desc_que']);
		$promo_start_date_que	= addslashes($_POST['promo_start_date_que']);
		$promo_end_date_que		= addslashes($_POST['promo_end_date_que']);
		$randoms 				= rand(0, 9999999999);
		$promotion_question		= $_POST['promotion_question'];
		$promotion_answer_one	= $_POST['promotion_answer_one'];
		$promotion_marks_one	= $_POST['promotion_marks_one'];
		$promotion_answer_two	= $_POST['promotion_answer_two'];
		$promotion_marks_two	= $_POST['promotion_marks_two'];
		$promotion_answer_three	= $_POST['promotion_answer_three'];
		$promotion_marks_three	= $_POST['promotion_marks_three'];
		$promotion_answer_four	= $_POST['promotion_answer_four'];
		$promotion_marks_four	= $_POST['promotion_marks_four'];
		
		$promotion_thumb_que	= $_FILES['promotion_thumb_que']['name'];
		$randoms 				= rand(0, 9999999999);
		$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_que']['name']);
		$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
		$PromoThumb_Number		= 'promo-thumb-' . $randoms;
		$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
		
		if ( !empty($training_id_que) && !empty($promotion_title_que) && !empty($promotion_desc_que) && !empty($promo_start_date_que) && !empty($promotion_thumb_que) && !empty($promo_end_date_que) && !empty($promotion_question) && !empty($promotion_answer_one) && !empty($promotion_marks_one) && !empty($promotion_answer_two) && !empty($promotion_marks_two) && !empty($promotion_answer_three) && !empty($promotion_marks_three) && !empty($promotion_answer_four) && !empty($promotion_marks_four) ) {
			
			if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
				switch(strtolower($_FILES['promotion_thumb_que']['type'])) {
					case 'image/jpg':
					case 'image/jpeg':
					case 'image/png':
					case 'image/gif':
					case 'image/bmp':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
						break;
				}
			}
			move_uploaded_file($_FILES['promotion_thumb_que']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
			
			$queryQUE = "INSERT INTO `" . $config['db_prefix'] . "promotions`(`Promotion_ID`, `Trainer_ID`, `Training_ID`, `Promotion_Type`, `Promotion_Thumb`, `Promotion_Title`, `Promotion_Desc`, `Promotion_URL`, `Promotion_File`, `Promotion_Question`, `Promotion_Start`, `Promotion_End`, `Shared`, `Created_On`) VALUES (NULL, $trainer_id, $training_id_que, '$promotion_type', '$PromoThumbNewName', '$promotion_title_que', '$promotion_desc_que', '$promotion_url', 'No', 'Yes', '$promo_start_date_que', '$promo_end_date_que', '', '".date('Y-m-d H:i:s')."')";
			$rsQUE = $conn->execute($queryQUE);
			$lqQZ  = "SELECT LAST_INSERT_ID() AS `LAST_PROMOTION_ID`";
			$rqQZ  = $conn->execute($lqQZ);
			$genQZ = $rqQZ->getrows();			
			$Promotion_ID = $genQZ[0]['LAST_PROMOTION_ID'];
			
			for($q=0;$q<count($promotion_question);$q++) {
				if ( !is_numeric($promotion_marks_one[$q]) || !is_numeric($promotion_marks_two[$q]) || !is_numeric($promotion_marks_three[$q]) || !is_numeric($promotion_marks_four[$q]) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Marks should be in number!</div>');
				} elseif( !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_question[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_one[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_two[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_three[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_four[$q]) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Question / Answers allow alphabets, numbers and specialchar(?|.) only!</div>');
				} elseif( !preg_match( '/^[A-Za-z]+$/i', substr($promotion_question[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_one[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_two[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_three[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_four[$q], 0, 1)) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Question / Answers should starts with alphabets only!</div>');
				} else {
					$queryQZ = "INSERT INTO `" . $config['db_prefix'] . "promotion_questions`(`Question_ID`, `Promotion_ID`, `Promotion_Type`, `Question`, `Answer_One`, `Answer_One_Marks`, `Answer_Two`, `Answer_Two_Marks`, `Answer_Three`, `Answer_Three_Marks`, `Answer_Four`, `Answer_Four_Marks`, `Created_On`) VALUES (NULL, $Promotion_ID, '$promotion_type', '".addslashes($promotion_question[$q])."', '".addslashes($promotion_answer_one[$q])."', ".addslashes($promotion_marks_one[$q]).", '".addslashes($promotion_answer_two[$q])."', ".addslashes($promotion_marks_two[$q]).", '".addslashes($promotion_answer_three[$q])."', ".addslashes($promotion_marks_three[$q]).", '".addslashes($promotion_answer_four[$q])."', ".addslashes($promotion_marks_four[$q]).", '".date('Y-m-d H:i:s')."')";
					$rsQZ = $conn->execute($queryQZ);
				}
			}
			if ( $rsQZ) {
				$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Trainer_ID` = $trainer_id";
				$rs = $conn->execute($query);
				$Ref_Users = $rs->getrows();
				for ( $i = 0; $i < count($Ref_Users); $i++ ) {
					$Notification_Text = "Trainer <b>" . $_SESSION['UFNAME'] . "</b> published a new promotion <b>$promotion_title_que</b>.";
					$Promotion_Link = $config['BASE_URL'] . '/promotion/' . $promotion_url . '/';
					$Notification_Image = $config['ASSET_URL'] . '/frontend/images/question.jpg';
					insert_notification($Ref_Users[$i]['User_ID'], $Notification_Text, $Promotion_Link, $Notification_Image);
				}
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		}
	}
} else {
	die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$userID 	= $_POST["userID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($userID) ) {
		$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_First` = 'No' WHERE `User_ID` = $userID";
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$UploadDirectory    = $config['UPLOAD_DIR'] . '/trp/';
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$UploadIntroVideo 		= false;
	$UploadTrainingVideo 	= false;
	//$UploadTrainingThumb 	= false;
	
	$training_id 	 		= addslashes($_POST['training_id']);
	$trainer_id 	 		= $_SESSION['UID'];
	$training_name 	 		= addslashes($_POST['training_name']);
	$training_cat 	 		= addslashes($_POST['training_cat']);
	$training_link_text 	= addslashes($_POST['training_link_text']);	
	$training_headline  	= addslashes($_POST['training_headline']);
	$training_point_one  	= addslashes($_POST['training_point_one']);
	$training_point_two  	= addslashes($_POST['training_point_two']);
	$training_point_three  	= addslashes($_POST['training_point_three']);
	$training_point_four	= addslashes($_POST['training_point_four']);
	$about_training  		= addslashes($_POST['about_training']);
	$training_tags 	 		= addslashes($_POST['training_tags']);
	$similar_section 		= addslashes($_POST['similar_training']);
	$visible_time 	 		= addslashes($_POST['visible_time']);
	$cta_text 		 		= addslashes($_POST['cta_text']);
	$publish_datetime		= addslashes($_POST['publish_datetime']);
	$product_id				= addslashes($_POST['product_list_select']);
	$OldIntroFile    		= addslashes($_POST['old_intro_video']);
	$OldTrainingFile 		= addslashes($_POST['old_training_video']);
	$OldTrainingThumb 		= addslashes($_POST['old_training_video_thumb']);
	$training_thumb	 		= addslashes($_POST['training_thumb']);
	$randoms 				= rand(0, 9999999999);
	
	$video_type 			= addslashes($_POST['video_type']);
	$training_video_type 	= addslashes($_POST['training_video_type']);
	
	if( empty($OldTrainingThumb) && empty($training_thumb) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Training Thumbnail.</div>');
	} elseif( empty($visible_time) || !is_numeric($visible_time) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Visible Time.</div>');
	} elseif( $cta_text == "" || $cta_text == false ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Check Buy Button Text.</div>');
	} elseif( !preg_match("/^[a-zA-Z\s_~\-!@#\$%\^&\*\(\)]*$/", $cta_text) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Buy Button Text allows Alphabets and Special characters only.</div>');
	} elseif( !preg_match("/^[a-zA-Z]*$/", substr($cta_text, 0,1)) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Buy Button Text should start with Alphabets only.</div>');
	} elseif( empty($product_id) ) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! No Product found.</div>');
	} elseif( !empty($product_id) ) {
		if ( $video_type == 'custom' ) {
			if( !empty($_FILES['intro_video']['name']) ) {
				switch(strtolower($_FILES['intro_video']['type'])) {
					case 'video/mp4':
					case 'video/webm':
					case 'video/ogg':
					case 'video/quicktime':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Intro Video File!</div>');
				}
				$IntroFile_Name          = strtolower($_FILES['intro_video']['name']);
				$IntroFile_Ext           = substr($IntroFile_Name, strrpos($IntroFile_Name, '.'));
				$IntroRandom_Number      = 'intro-' . $randoms;		
				$IntroNewFileName        = $IntroRandom_Number.$IntroFile_Ext;
				$UploadIntroVideo		 = move_uploaded_file($_FILES['intro_video']['tmp_name'], $UploadDirectory.$IntroNewFileName);
				unlink($UploadDirectory.$_POST['old_intro_video']);
			} else {
				$IntroNewFileName        = $OldIntroFile;
				$UploadIntroVideo		 = true;
			}
		} else {
			$IntroNewFileName		= addslashes($_POST['intro_video_youtube']);
			$UploadIntroVideo		 = true;
		}
		
		if ( $training_video_type == 'custom' ) {
			if( !empty($_FILES['training_video']['name']) ) {
				switch(strtolower($_FILES['training_video']['type'])) {
					case 'video/mp4':
					case 'video/webm':
					case 'video/ogg':
					case 'video/quicktime':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Training Video File!</div>');
				}
				$TrainingFile_Name          = strtolower($_FILES['training_video']['name']);
				$TrainingFile_Ext           = substr($TrainingFile_Name, strrpos($TrainingFile_Name, '.'));
				$TrainingRandom_Number      = 'training-' . $randoms;		
				$TrainingNewFileName        = $TrainingRandom_Number.$TrainingFile_Ext;
				$UploadTrainingVideo		= move_uploaded_file($_FILES['training_video']['tmp_name'], $UploadDirectory.$TrainingNewFileName);
				unlink($UploadDirectory.$_POST['old_training_video']);
			} else {
				$TrainingNewFileName        = $OldTrainingFile;
				$UploadTrainingVideo		= true;
			}
		} else {
			$TrainingNewFileName		= addslashes($_POST['training_video_youtube']);
			$UploadTrainingVideo		= true;
		}
		
		if( !empty($training_thumb) ) {
			/*$_FILES['training_thumb']['name']
			switch(strtolower($_FILES['training_thumb']['type'])) {
				case 'image/jpg':
				case 'image/jpeg':
				case 'image/png':
					break;
				default:
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Training Thumbnail File!</div>');
			}
			$ThumbnailFile_Name         = strtolower($_FILES['training_thumb']['name']);
			$ThumbnailFile_Ext          = substr($ThumbnailFile_Name, strrpos($ThumbnailFile_Name, '.'));
			$ThumbnailRandom_Number     = 'trainingThumb' . $randoms;
			$TrainingNewThumb       	= $ThumbnailRandom_Number.$ThumbnailFile_Ext;
			$UploadTrainingThumb		= move_uploaded_file($_FILES['training_thumb']['tmp_name'], $UploadDirectory.$TrainingNewThumb);
			&& $UploadTrainingThumb == true */
			$TrainingNewThumb			= $training_thumb;
			unlink($UploadDirectory.$_POST['old_training_video_thumb']);
		} else {
			$TrainingNewThumb			= $OldTrainingThumb;
		}	
		
		if( $UploadIntroVideo == true && $UploadTrainingVideo == true ) {
			$query = "UPDATE `" . $config['db_prefix'] . "trainings` SET `Product_ID` = $product_id, `Training_Name` = '$training_name', `Training_Category` = '$training_cat', `Training_Headline` = '$training_headline', `Training_Point_One` = '$training_point_one', `Training_Point_Two` = '$training_point_two', `Training_Point_Three` = '$training_point_three', `Training_Point_Four` = '$training_point_four', `Training_Desc` = '$about_training', `Training_Video` = '$TrainingNewFileName', `Join_Training_Btn` = '$training_link_text', `Intro_Video` = '$IntroNewFileName', `Training_Tags` = '$training_tags', `Similar_Section` = '$similar_section', `CTA_Text` = '$cta_text', `CTA_Time` = '$visible_time', `Training_Thumb` = '$TrainingNewThumb', `Training_Status` = 'Pending', `Training_Published_On` = '$publish_datetime' WHERE `Training_ID` = $training_id AND `Trainer_ID` = $trainer_id";
			$rs = $conn->execute($query);
			if ( $rs ) {
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Video is not uploaded.</div>');
		}
	}
} else {
	die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
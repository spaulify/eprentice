<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$userCAT 	= substr($_POST['userCAT'], 0, -1);
	$userID 	= $_POST["userID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}

	if( !empty($userCAT) && !empty($userID) ) {
		$searchQuery = "SELECT * FROM `" . $config['db_prefix'] . "preferences` WHERE `Pref_User_ID` = $userID LIMIT 0, 1";
		$searchrs = $conn->execute($searchQuery);
		if( $searchrs ) {
			$searchrow = $searchrs->numrows();
		} else {
			$searchrow = 0;	
		}
		if( $searchrow > 0 ) {
			$query = "UPDATE `" . $config['db_prefix'] . "preferences` SET `Pref_Category` = '$userCAT' WHERE `Pref_User_ID` = $userID";
		} else {
			$query = "INSERT INTO `" . $config['db_prefix'] . "preferences` (`Preference_ID`, `Pref_User_ID`, `Pref_Category`, `Added_On`) VALUES(NULL, $userID, '$userCAT', '".date('Y-m-d H:i:s')."')";	
		}
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	} else {
		echo 0;
	}
}
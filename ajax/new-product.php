<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$UploadDirectory    = $config['UPLOAD_DIR'] . '/prd/';

	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$MoveProductFile 	= false;
	$trainer_id 	 	= $_SESSION['UID'];
	$product_name 		= addslashes($_POST['product_name']);
	$product_link 		= addslashes($_POST['product_link']);
	$product_price 		= addslashes($_POST['product_price']);
	$product_refund		= addslashes($_POST['product_refund']);
	$product_desc 		= addslashes($_POST['product_desc']);
	$randoms 			= rand(0, 9999999999);
	
	if ( !empty($_FILES['product_image']['name']) && !empty($product_name) && !empty($product_link) && !empty($product_price) && !empty($product_refund) && !empty($product_desc) ) {
		if( !preg_match("/[a-zA-Z\s]/", $product_name) ) {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Product name should be in alphabet.</div>');
		} else if( (!is_numeric($product_price)) ) {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Product price should be in number.</div>');
		} elseif( strlen($product_desc) > 500 ) {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Product description should be in 500 characters.</div>');
		} else {
			$ProductFile_Name	= strtolower($_FILES['product_image']['name']);
			$ProductFile_Ext	= substr($ProductFile_Name, strrpos($ProductFile_Name, '.'));
			$ProductFile_Number	= 'product-' . $randoms;
			$ProductFileNewName	= $ProductFile_Number.$ProductFile_Ext;
			
			if ( $ProductFile_Ext == '.jpg' || $ProductFile_Ext == '.jpeg' || $ProductFile_Ext == '.png' || $ProductFile_Ext == '.gif' || $ProductFile_Ext == '.bmp' ) {
				switch(strtolower($_FILES['product_image']['type'])) {
					case 'image/jpg':
					case 'image/jpeg':
					case 'image/png':
					case 'image/gif':
					case 'image/bmp':
						break;
					default:
						die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Product Image File!</div>');
						break;
				}
			}
			$MoveProductFile	= move_uploaded_file($_FILES['product_image']['tmp_name'], $UploadDirectory.$ProductFileNewName);
		}
	} else {
		die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
	}
	
	if ( $MoveProductFile ) {
		$query = "INSERT INTO `" . $config['db_prefix'] . "products`(`Product_ID`, `Trainer_ID`, `Product_Name`, `Product_Desc`, `Product_Price`, `Product_Link`, `Product_Refund_URL`, `Product_Image`, `Product_Create_DateTime`) VALUES (NULL, $trainer_id, '$product_name', '$product_desc', '$product_price', '$product_link', '$product_refund', '$ProductFileNewName', '".date('Y-m-d H:i:s')."')";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Trainer_ID` = $trainer_id";
			$rs = $conn->execute($query);
			$Ref_Users = $rs->getrows();
			for ( $i = 0; $i < count($Ref_Users); $i++ ) {
				$Notification_Text = "Trainer <b>" . $_SESSION['UFNAME'] . "</b> created a new product <b>$product_name</b>.";
				$Notification_Image = $config['UPLOAD_URL'] . '/prd/' . $ProductFileNewName;
				insert_notification($Ref_Users[$i]['User_ID'], $Notification_Text, $product_link, $Notification_Image);
			}
			die('Success');
		} else {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
		}
	} else {
		die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Product Image is not uploaded.</div>');
	}
} else {
	die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$MovePromoFile 	= false;
	$trainer_id  	= $_SESSION['UID'];
	$trainer_email	= $_SESSION['UEMAIL'];
	$training_id	= addslashes($_POST['send_training']);
	$query 			= "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_ID` = " . $training_id;
	$rs 			= $conn->execute($query);
	$training 		= $rs->getrows();
	$training_url	= $training[0]['Training_URL_EM'];
	$training_link 	= $config['BASE_URL'] . '/training/' . $training_url . '/';
	$randoms 		= rand(0, 9999999999);
	$num			= 0;
	
	if ( !empty($_FILES['email_csv']['name']) && !empty($training_id) ) {
		$CSVFile_Name	= strtolower($_FILES['email_csv']['name']);
		$CSVFile_Ext	= substr($CSVFile_Name, strrpos($CSVFile_Name, '.'));
		$CSVFile_Number	= 'csv-' . $randoms;
		$CSVFileNewName	= $CSVFile_Number.$CSVFile_Ext;
		
		if ( $CSVFile_Ext == '.csv' ) {
			switch(strtolower($_FILES['email_csv']['type'])) {
				case 'text/csv':
				case 'application/vnd.ms-excel':
					break;
				default:
					die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported CSV File!</div>');
					break;
			}
		}
	} else {
		die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
	}
		
	$CSVFileName = $_FILES['email_csv']['tmp_name'];
	$CSVFileSize = filesize($CSVFileName);
	if(($CSVHandle = fopen($CSVFileName, "r")) === FALSE) {
		die('Error opening file');
	}
	
	$CSVCodes = array();
	while ($EmailRow = fgetcsv($CSVHandle, $CSVFileSize, ",")) {
		$CSVCodes[] = $EmailRow;
	}

	foreach($CSVCodes as $CSVCode){
		if ( !filter_var($CSVCode[0], FILTER_VALIDATE_EMAIL) ) {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! File Contains Invalid Email Address.</div>');
		}
		$curDate = date('Y-m-d');
		$checkSQL = "SELECT * FROM `" . $config['db_prefix'] . "share_training` WHERE `Trainer_ID` = $trainer_id AND `Training_ID` = $training_id AND `Share_Via` = 'email' AND `Share_Email` = '".$CSVCode[0]."' AND `Share_Time` LIKE '".$curDate."%' LIMIT 0,1";
		$cs = $conn->execute($checkSQL);
		$num = $cs->numrows();
		if( $num == 0 ) {
			$email_unique_id		= substr(md5(uniqid(rand(), true)), 8, 8);
			$training_unique_link 	= $training_link . $email_unique_id . '/';
			$message = SMEmail::Share_Training_Email($CSVCode[0], $training_unique_link);
			$subject = 'Watch the training';
			if (eprenticeMail($CSVCode[0], '', $subject, $message)) {
				$email_type	  = 'Training';
				$email_status = 'Go';
				email_tracking($trainer_id, $trainer_email, $CSVCode[0], $training_id, $subject, addslashes(htmlspecialchars($message)), $training_url, $email_unique_id, $email_type, $email_status);
				$query = "INSERT INTO `" . $config['db_prefix'] . "share_training`(`Share_ID`, `Trainer_ID`, `Training_ID`, `Share_Via`, `Share_Email`, `Share_Time`) VALUES (NULL, $trainer_id, $training_id, 'email', '" . $CSVCode[0] . "', '".date('Y-m-d H:i:s')."')";
				$rs = $conn->execute($query);
			}
		}
	}
	die('<div class="alert alert-success alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Training has been successfully shared by email.</div>');

} else {
	die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
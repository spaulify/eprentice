<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$fb_schedule_promo		= addslashes($_POST['fb_schedule_promo']);
	$fb_schedule_title		= addslashes($_POST['fb_schedule_title']);
	$fb_schedule_start_date	= addslashes($_POST['fb_schedule_start_date']);
	$fb_schedule_end_date	= addslashes($_POST['fb_schedule_end_date']);
	$fb_schedule_desc		= addslashes($_POST['fb_schedule_desc']);
	$fb_campaign_id			= addslashes($_POST['fb_campaign_id']);
	$fb_trainer_id			= addslashes($_POST['fb_trainer_id']);
	$fb_schedule_status 	= 'Active';
	$Query_Status			= '';
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	/*$checkQuery = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Schedule_Status` = 'Active' AND `Trainer_ID` = $fb_trainer_id";
	$checkRs = $conn->execute($checkQuery);
	$row = $checkRs->numrows();
	if( $row > 0 ) {
		$fb_schedule_status = 'Deactive';
	} else {
		$current_datetime = date('Y-m-d H:i:s');
		if( (strtotime($fb_schedule_start_date) <= strtotime($current_datetime) ) && (strtotime($fb_schedule_end_date) >= strtotime($current_datetime)) ) {
			$fb_schedule_status = 'Active';
		} else if( (strtotime($fb_schedule_start_date) <= strtotime($current_datetime)) && (strtotime($fb_schedule_end_date) < strtotime($current_datetime)) ) {
			$fb_schedule_status = 'Deactive';
		} else if( (strtotime($fb_schedule_start_date) < strtotime($current_datetime)) && (strtotime($fb_schedule_end_date) <= strtotime($current_datetime)) ) {
			$fb_schedule_status = 'Active';
		} elseif( strtotime($fb_schedule_start_date) > strtotime($current_datetime) ) {
			$fb_schedule_status = 'Deactive';
		}
	}*/
	if ( !empty($fb_schedule_promo) && !empty($fb_schedule_title) && !empty($fb_schedule_start_date) && !empty($fb_schedule_end_date) && !empty($fb_schedule_desc) && !empty($fb_campaign_id) && !empty($fb_trainer_id) ) {
		$selectQuery = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Campaign_ID` = $fb_campaign_id AND `Trainer_ID` = $fb_trainer_id";
		$selectRs = $conn->execute($selectQuery);
		$row = $selectRs->numrows();
		if( $row > 0 ) {
			$query = "UPDATE `" . $config['db_prefix'] . "facebook_campaign_schedule` SET `FB_Schedule_Title` = '$fb_schedule_title', `FB_Schedule_Desc` = '$fb_schedule_desc', `FB_Schedule_Promotion_ID` = $fb_schedule_promo, `FB_Schedule_Start` = '$fb_schedule_start_date', `FB_Schedule_End` = '$fb_schedule_end_date', `FB_Schedule_Status` = '$fb_schedule_status' WHERE `FB_Campaign_ID` = $fb_campaign_id AND `Trainer_ID` = $fb_trainer_id";
			$Query_Status = 1;
		} else {
			$query = "INSERT INTO `" . $config['db_prefix'] . "facebook_campaign_schedule` (`FB_Schedule_ID`, `FB_Campaign_ID`, `Trainer_ID`, `FB_Schedule_Title`, `FB_Schedule_Desc`, `FB_Schedule_Promotion_ID`, `FB_Schedule_Start`, `FB_Schedule_End`, `FB_Schedule_Status`, `Created_On`) VALUES(NULL, $fb_campaign_id, $fb_trainer_id, '$fb_schedule_title', '$fb_schedule_desc', $fb_schedule_promo, '$fb_schedule_start_date', '$fb_schedule_end_date', '$fb_schedule_status', '".date('Y-m-d H:i:s')."')";
			$Query_Status = 2;
		}
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo $Query_Status;
		} else {
			echo 0;
		}
	}
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$scheduleID	= addslashes($_POST['scheduleID']);
	$userID		= addslashes($_POST['userID']);
	$fbStatus	= addslashes($_POST['fbStatus']);
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if( !empty($scheduleID) && !empty($userID) && !empty($fbStatus) ) {
		if( $fbStatus == 'Active' ) {
			$checkQuery = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Schedule_Status` = 'Active' AND `Trainer_ID` = $userID LIMIT 0, 1";
			$checkRs = $conn->execute($checkQuery);
			$row = $checkRs->numrows();
			if( $row > 0 ) {
				$val = $checkRs->getrows();
				$activeID  = $val[0]['FB_Schedule_ID'];
				$upQuery  = "UPDATE `" . $config['db_prefix'] . "facebook_campaign_schedule` SET `FB_Schedule_Status` = 'Deactive' WHERE `FB_Schedule_ID` = $activeID AND `Trainer_ID` = $userID";
				$upRS 	  = $conn->execute($upQuery);
			}
			$checkQuery2 = "SELECT * FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Schedule_ID` = $scheduleID AND `Trainer_ID` = $userID LIMIT 0, 1";
			$checkRs2 = $conn->execute($checkQuery2);
			$row2 = $checkRs2->numrows();
			$val2 = $checkRs2->getrows();
			$Current_DateTime 	= date('Y-m-d H:i:s');
			$FB_Schedule_Start	= $val2[0]['FB_Schedule_Start'];
			$FB_Schedule_End	= $val2[0]['FB_Schedule_End'];
			if( (strtotime($FB_Schedule_Start) >= strtotime($Current_DateTime)) && (strtotime($FB_Schedule_End) >= strtotime($Current_DateTime)) ) {
				$upQuery2 = "UPDATE `" . $config['db_prefix'] . "facebook_campaign_schedule` SET `FB_Schedule_Status` = 'Active' WHERE `FB_Schedule_ID` = $scheduleID AND `Trainer_ID` = $userID";
				$upRS2 	  = $conn->execute($upQuery2);
				if( $upRS2 ) {
					echo 1;
				}
			} else {
				echo 2;
			}
		} elseif( $fbStatus == 'Deactive' ) {
			$upQuery3 = "UPDATE `" . $config['db_prefix'] . "facebook_campaign_schedule` SET `FB_Schedule_Status` = 'Deactive' WHERE `FB_Schedule_ID` = $scheduleID AND `Trainer_ID` = $userID";
			$upRS3 	  = $conn->execute($upQuery3);
			if( $upRS3 ) {
				echo 1;
			} else {
				echo 0;
			}	
		}
	} else {
		echo 0;	
	}
		
}
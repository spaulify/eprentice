<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$User_ID = $_POST["User_ID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($User_ID) ) {
		$queryNoti = "SELECT * FROM `" . $config['db_prefix'] . "notifications` WHERE `User_ID` = $User_ID";
		$rs = $conn->execute($queryNoti);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num > 0 ) {
			for($i=0;$i<count($num);$i++) {
				$query = "UPDATE `" . $config['db_prefix'] . "notifications` SET `Notification_Status` = 'Read' WHERE `User_ID` = " . $User_ID[$i];
				$rs = $conn->execute($query);
			}
			if( $rs )
				echo 'Read';
			else
				echo 'Unread';
		}
	}
}
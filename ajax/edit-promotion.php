<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$UploadDirectory    = $config['UPLOAD_DIR'] . '/prm/';

	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	$MovePromoFile 		= false;
	$UploadThumbFile	= false;
	$trainer_id 	 	= $_SESSION['UID'];
	$promotion_id 		= addslashes($_POST['promotion_id']);
	$promotion_type		= addslashes($_POST['promotion_type']);
	
	if(empty($promotion_type)) {
		die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Select Promotion Type first before Submit!</div>');
	} elseif($promotion_type == "Image") {
		$training_id_img		 = addslashes($_POST['training_id_img']);
		$promotion_title_img	 = addslashes($_POST['promotion_title_img']);
		$promotion_desc_img		 = addslashes($_POST['promotion_desc_img']);
		$promo_start_date_img	 = addslashes($_POST['promo_start_date_img']);
		$promo_end_date_img		 = addslashes($_POST['promo_end_date_img']);
		$old_promotion_file_id	 = isset($_POST['old_promotion_file_id']) ? $_POST['old_promotion_file_id'] : '';
		$old_promotion_file_img	 = isset($_POST['old_promotion_file_img']) ? $_POST['old_promotion_file_img'] : '';		
		$old_promotion_thumb_img = addslashes($_POST['old_promotion_thumb_img']);
		
		if ( !empty($training_id_img) && !empty($promotion_title_img) && !empty($promotion_desc_img) && !empty($promo_start_date_img) && !empty($promo_end_date_img) ) {

			if( !empty($_FILES['promotion_thumb_img']['name']) ) {			
				$promotion_thumb_img	= $_FILES['promotion_thumb_img']['name'];
				$randoms 				= rand(0, 9999999999);
				$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_img']['name']);
				$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
				$PromoThumb_Number		= 'promo-thumb-' . $randoms;
				$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
				if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
					switch(strtolower($_FILES['promotion_thumb_img']['type'])) {
						case 'image/jpg':
						case 'image/jpeg':
						case 'image/png':
						case 'image/gif':
						case 'image/bmp':
							break;
						default:
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
							break;
					}
				}
				$UploadThumbFile = move_uploaded_file($_FILES['promotion_thumb_img']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
				unlink($UploadDirectory.$old_promotion_thumb_img);
			} else {
				$PromoThumbNewName 	= $old_promotion_thumb_img;
				$UploadThumbFile 	= true;	
			}				
			
			$queryIMG = "UPDATE `" . $config['db_prefix'] . "promotions` SET `Training_ID` = $training_id_img, `Promotion_Type` = '$promotion_type', `Promotion_Thumb` = '$PromoThumbNewName', `Promotion_Title` = '$promotion_title_img', `Promotion_Desc` = '$promotion_desc_img', `Promotion_File` = 'Yes', `Promotion_Question` = 'No', `Promotion_Start` = '$promo_start_date_img', `Promotion_End` = '$promo_end_date_img' WHERE `Promotion_ID` = $promotion_id";
			$rsIMG = $conn->execute($queryIMG);
			$Supported_Image 	= array('gif','jpg','jpeg','png','bmp');
			$Restrict_Condition = array("False");
			$CheckFileIMG 		= array();
			
			if ( !empty($_FILES['promotion_file_img']['name']) ) {
				foreach($_FILES['promotion_file_img']['name'] as $file => $name) {
					$randoms 			= rand(0, 9999999999);
					$PromoFile_Name		= strtolower($_FILES['promotion_file_img']['name'][$file]);
					$PromoFile_Ext		= strtolower(pathinfo($PromoFile_Name, PATHINFO_EXTENSION));
					$PromoFile_Number	= 'promo-' . $randoms;
					$PromoFileNewName	= $PromoFile_Number.'.'.$PromoFile_Ext;
					
					if ( !in_array($PromoFile_Ext, $Supported_Image) ) {
						array_push($CheckFileIMG, "False");						
					} else {
						array_push($CheckFileIMG, "True");
					}
					if( in_array("False", $CheckFileIMG) ) {
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Image File!</div>');
					} else {
						$MovePromoFile	= move_uploaded_file($_FILES['promotion_file_img']['tmp_name'][$file], $UploadDirectory.$PromoFileNewName);
						if ( $MovePromoFile ) {			
							$queryFILE = "INSERT INTO `" . $config['db_prefix'] . "promotion_file` (`File_ID`, `Promotion_ID`, `Promotion_Type`, `Promotion_Filename`, `Created_On`) VALUES (NULL, $promotion_id, '$promotion_type', '$PromoFileNewName', '".date('Y-m-d H:i:s')."')";
							$rsFILE = $conn->execute($queryFILE);
						} else {
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Image is not uploaded.</div>');
						}
					}					
				}
			} else {
				for($p=0;$p<count($old_promotion_file_img);$p++) {
					$PromoFileNewName = $old_promotion_file_img[$p];
					$PromotionID	  = $old_promotion_file_id[$p];
					$MovePromoFile	  = true;
					if ( $MovePromoFile ) {			
						$queryFILE = "UPDATE `" . $config['db_prefix'] . "promotion_file` SET `Promotion_Type`  = '$promotion_type', `Promotion_Filename` = '$PromoFileNewName' WHERE `File_ID` = $PromotionID AND `Promotion_ID` = $promotion_id";
						$rsFILE = $conn->execute($queryFILE);
					} else {
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Image is not uploaded.</div>');
					}
				}
			}
			if ( $rsFILE ) {
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
		}
	} elseif($promotion_type == "Video") {
		$training_id_vid		 = addslashes($_POST['training_id_vid']);
		$promotion_title_vid	 = addslashes($_POST['promotion_title_vid']);
		$promotion_desc_vid		 = addslashes($_POST['promotion_desc_vid']);
		$promo_start_date_vid	 = addslashes($_POST['promo_start_date_vid']);
		$promo_end_date_vid		 = addslashes($_POST['promo_end_date_vid']);
		$old_promotion_file_id	 = isset($_POST['old_promotion_file_id']) ? $_POST['old_promotion_file_id'] : '';
		$old_promotion_file_vid	 = isset($_POST['old_promotion_file_vid']) ? $_POST['old_promotion_file_vid'] : '';
		$old_promotion_thumb_vid = addslashes($_POST['old_promotion_thumb_vid']);
		
		if ( !empty($training_id_vid) && !empty($promotion_title_vid) && !empty($promotion_desc_vid) && !empty($promo_start_date_vid) && !empty($promo_end_date_vid) ) {
			
			if( !empty($_FILES['promotion_thumb_vid']['name']) ) {			
				$promotion_thumb_img	= $_FILES['promotion_thumb_vid']['name'];
				$randoms 				= rand(0, 9999999999);
				$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_vid']['name']);
				$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
				$PromoThumb_Number		= 'promo-thumb-' . $randoms;
				$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
				if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
					switch(strtolower($_FILES['promotion_thumb_vid']['type'])) {
						case 'image/jpg':
						case 'image/jpeg':
						case 'image/png':
						case 'image/gif':
						case 'image/bmp':
							break;
						default:
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
							break;
					}
				}
				$UploadThumbFile = move_uploaded_file($_FILES['promotion_thumb_vid']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
				unlink($UploadDirectory.$old_promotion_thumb_vid);
			} else {
				$PromoThumbNewName 	= $old_promotion_thumb_vid;
				$UploadThumbFile 	= true;	
			}
			
			$queryVID = "UPDATE `" . $config['db_prefix'] . "promotions` SET `Training_ID` = $training_id_vid, `Promotion_Type` = '$promotion_type', `Promotion_Thumb` = '$PromoThumbNewName', `Promotion_Title` = '$promotion_title_vid', `Promotion_Desc` = '$promotion_desc_vid', `Promotion_File` = 'Yes', `Promotion_Question` = 'No', `Promotion_Start` = '$promo_start_date_vid', `Promotion_End` = '$promo_end_date_vid' WHERE `Promotion_ID` = $promotion_id";
			$rsVID = $conn->execute($queryVID);
			$Supported_Video 	= array('mp4','webm','ogg','m4v','avi');
			$Restrict_Condition = array("False");
			$CheckFileVID 		= array();
			
			if ( !empty($_FILES['promotion_file_vid']['name']) ) {
				foreach($_FILES['promotion_file_vid']['name'] as $file => $name) {
					$randoms 			= rand(0, 9999999999);
					$PromoFile_Name		= strtolower($_FILES['promotion_file_vid']['name'][$file]);
					$PromoFile_Ext		= strtolower(pathinfo($PromoFile_Name, PATHINFO_EXTENSION));
					$PromoFile_Number	= 'promo-' . $randoms;
					$PromoFileNewName	= $PromoFile_Number.'.'.$PromoFile_Ext;
					
					if ( !in_array($PromoFile_Ext, $Supported_Video) ) {
						array_push($CheckFileVID, "False");						
					} else {
						array_push($CheckFileVID, "True");
					}
					if( in_array("False", $CheckFileVID) ) {
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Video File!</div>');
					} else {
						$MovePromoFile	= move_uploaded_file($_FILES['promotion_file_vid']['tmp_name'][$file], $UploadDirectory.$PromoFileNewName);
						if ( $MovePromoFile ) {
							$queryFILE = "INSERT INTO `" . $config['db_prefix'] . "promotion_file`(`File_ID`, `Promotion_ID`, `Promotion_Type`, `Promotion_Filename`, `Created_On`) VALUES (NULL, $promotion_id, '$promotion_type', '$PromoFileNewName', '".date('Y-m-d H:i:s')."')";
							$rsFILE = $conn->execute($queryFILE);
						} else {
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Video is not uploaded.</div>');
						}
					}					
				}
			} else {
				for($p=0;$p<count($old_promotion_file_vid);$p++) {
					$PromoFileNewName = $old_promotion_file_vid[$p];
					$PromotionID	  = $old_promotion_file_id[$p];
					$MovePromoFile	  = true;
					if ( $MovePromoFile ) {
						$queryFILE = "UPDATE `" . $config['db_prefix'] . "promotion_file` SET `Promotion_Type`  = '$promotion_type', `Promotion_Filename` = '$PromoFileNewName' WHERE `File_ID` = $PromotionID AND `Promotion_ID` = $promotion_id";
						$rsFILE = $conn->execute($queryFILE);
					} else {
						die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Video is not uploaded.</div>');
					}
				}
			}
			if ( $rsFILE ) {
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		} else {
			die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
		}
	} elseif($promotion_type == "Question") {
		$promotion_question_id	 = $_POST['promotion_question_id'];
		$training_id_que		 = addslashes($_POST['training_id_que']);
		$promotion_title_que	 = addslashes($_POST['promotion_title_que']);
		$promotion_desc_que		 = addslashes($_POST['promotion_desc_que']);
		$promo_start_date_que	 = addslashes($_POST['promo_start_date_que']);
		$promo_end_date_que		 = addslashes($_POST['promo_end_date_que']);
		$promotion_question		 = $_POST['promotion_question'];
		$promotion_answer_one	 = $_POST['promotion_answer_one'];
		$promotion_marks_one	 = $_POST['promotion_marks_one'];
		$promotion_answer_two	 = $_POST['promotion_answer_two'];
		$promotion_marks_two	 = $_POST['promotion_marks_two'];
		$promotion_answer_three	 = $_POST['promotion_answer_three'];
		$promotion_marks_three	 = $_POST['promotion_marks_three'];
		$promotion_answer_four	 = $_POST['promotion_answer_four'];
		$promotion_marks_four	 = $_POST['promotion_marks_four'];
		$old_promotion_thumb_que = addslashes($_POST['old_promotion_thumb_que']);

		if ( !empty($training_id_que) && !empty($promotion_title_que) && !empty($promotion_desc_que) && !empty($promo_start_date_que) && !empty($promo_end_date_que) && !empty($promotion_question) && !empty($promotion_answer_one) && !empty($promotion_marks_one) && !empty($promotion_answer_two) && !empty($promotion_marks_two) && !empty($promotion_answer_three) && !empty($promotion_marks_three) && !empty($promotion_answer_four) && !empty($promotion_marks_four) ) {
			
			if( !empty($_FILES['promotion_thumb_que']['name']) ) {			
				$promotion_thumb_img	= $_FILES['promotion_thumb_que']['name'];
				$randoms 				= rand(0, 9999999999);
				$PromoThumb_Name		= strtolower($_FILES['promotion_thumb_que']['name']);
				$PromoThumb_Ext			= substr($PromoThumb_Name, strrpos($PromoThumb_Name, '.'));
				$PromoThumb_Number		= 'promo-thumb-' . $randoms;
				$PromoThumbNewName		= $PromoThumb_Number.$PromoThumb_Ext;
				if ( $PromoThumb_Ext == '.jpg' || $PromoThumb_Ext == '.jpeg' || $PromoThumb_Ext == '.png' || $PromoThumb_Ext == '.gif' || $PromoThumb_Ext == '.bmp' ) {
					switch(strtolower($_FILES['promotion_thumb_que']['type'])) {
						case 'image/jpg':
						case 'image/jpeg':
						case 'image/png':
						case 'image/gif':
						case 'image/bmp':
							break;
						default:
							die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Unsupported Promotion Thumbnail File!</div>');
							break;
					}
				}
				$UploadThumbFile = move_uploaded_file($_FILES['promotion_thumb_que']['tmp_name'], $UploadDirectory.$PromoThumbNewName);
				unlink($UploadDirectory.$old_promotion_thumb_que);
			} else {
				$PromoThumbNewName 	= $old_promotion_thumb_que;
				$UploadThumbFile 	= true;	
			}
			
			$queryQUE = "UPDATE `" . $config['db_prefix'] . "promotions` SET `Training_ID` = $training_id_que, `Promotion_Type` = '$promotion_type', `Promotion_Thumb` = '$PromoThumbNewName', `Promotion_Title` = '$promotion_title_que', `Promotion_Desc` = '$promotion_desc_que', `Promotion_File` = 'No', `Promotion_Question` = 'Yes', `Promotion_Start` = '$promo_start_date_que', `Promotion_End` = '$promo_end_date_que' WHERE `Promotion_ID` = $promotion_id";
			$rsQUE = $conn->execute($queryQUE);
		
			for($q=0;$q<count($promotion_question);$q++) {
				if ( !is_numeric($promotion_marks_one[$q]) || !is_numeric($promotion_marks_two[$q]) || !is_numeric($promotion_marks_three[$q]) || !is_numeric($promotion_marks_four[$q]) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Marks should be in number!</div>');
				} elseif( !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_question[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_one[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_two[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_three[$q]) || !preg_match( '/^[A-Za-z0-9\?\.\\s]+$/i', $promotion_answer_four[$q]) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Question / Answers allow alphabets, numbers and specialchar(?|.) only!</div>');
				} elseif( !preg_match( '/^[A-Za-z]+$/i', substr($promotion_question[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_one[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_two[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_three[$q], 0, 1)) || !preg_match( '/^[A-Za-z]+$/i', substr($promotion_answer_four[$q], 0, 1)) ) {
					die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Question / Answers should starts with alphabets only!</div>');
				} else {
					if( $promotion_question_id[$q] == "" ) {
						$queryQZ = "INSERT INTO `" . $config['db_prefix'] . "promotion_questions`(`Question_ID`, `Promotion_ID`, `Promotion_Type`, `Question`, `Answer_One`, `Answer_One_Marks`, `Answer_Two`, `Answer_Two_Marks`, `Answer_Three`, `Answer_Three_Marks`, `Answer_Four`, `Answer_Four_Marks`, `Created_On`) VALUES (NULL, $promotion_id, '$promotion_type', '".addslashes($promotion_question[$q])."', '".addslashes($promotion_answer_one[$q])."', ".addslashes($promotion_marks_one[$q]).", '".addslashes($promotion_answer_two[$q])."', ".addslashes($promotion_marks_two[$q]).", '".addslashes($promotion_answer_three[$q])."', ".addslashes($promotion_marks_three[$q]).", '".addslashes($promotion_answer_four[$q])."', ".addslashes($promotion_marks_four[$q]).", '".date('Y-m-d H:i:s')."')";
						$rsQZ = $conn->execute($queryQZ);	
					} else {
						$queryQZ = "UPDATE `" . $config['db_prefix'] . "promotion_questions` SET `Promotion_Type` = '$promotion_type', `Question` = '".addslashes($promotion_question[$q])."', `Answer_One` = '".addslashes($promotion_answer_one[$q])."', `Answer_One_Marks` = ".addslashes($promotion_marks_one[$q]).", `Answer_Two` = '".addslashes($promotion_answer_two[$q])."', `Answer_Two_Marks` = ".addslashes($promotion_marks_two[$q]).", `Answer_Three` = '".addslashes($promotion_answer_three[$q])."', `Answer_Three_Marks` = ".addslashes($promotion_marks_three[$q]).", `Answer_Four` = '".addslashes($promotion_answer_four[$q])."', `Answer_Four_Marks` = ".addslashes($promotion_marks_four[$q])." WHERE `Question_ID` = ".$promotion_question_id[$q]." AND `Promotion_ID` = $promotion_id";
						$rsQZ = $conn->execute($queryQZ);
					}
				}
			}
			if ( $rsQZ ) {
				die('Success');
			} else {
				die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Something went wrong, try again.</div>');
			}
		}
	}
} else {
	die('<div class="alert alert-danger alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}

	$ipaddress 		= get_ip_address();
	$promotion_id	= addslashes($_POST['promotion_id']);
	$question_count	= $_POST['question_count'];
	$question_id	= $_POST['question_id'];
	$number			= 0;
	
	for($c=0;$c<count($question_count);$c++) {
		
		$question 		= $question_id[$c];
		$survey_answer	= isset($_POST['survey_answer_'.$question]) ? $_POST['survey_answer_'.$question] : '';
			
		if ( !empty($survey_answer) ) {
			$queryQUE 	= "SELECT * FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Question_ID` = " . $question_id[$c] . " AND `Promotion_ID` = " . $promotion_id;
			$rsQUE 		= $conn->execute($queryQUE);
			$survey 	= $rsQUE->getrows();
			$queryMARKS = "SELECT CASE WHEN `Answer_One` = '$survey_answer' THEN 'Answer_One' WHEN `Answer_Two` = '$survey_answer' THEN 'Answer_Two' WHEN `Answer_Three` = '$survey_answer' THEN 'Answer_Three' WHEN `Answer_Four` = '$survey_answer' THEN 'Answer_Four' ELSE 'Not found' END AS Column_Name FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Question_ID` = " . $survey[0]['Question_ID'] . " AND `Promotion_ID` = " . $survey[0]['Promotion_ID'];
			$rsMARKS	= $conn->execute($queryMARKS);
			$marks 		= $rsMARKS->getrows();
			$marks_col	= $marks[0]['Column_Name'] . '_Marks';		
			$queryRES	= "SELECT `$marks_col` FROM `" . $config['db_prefix'] . "promotion_questions` WHERE `Question_ID` = " . $survey[0]['Question_ID'] . " AND `Promotion_ID` = " . $survey[0]['Promotion_ID'];
			$rsRES		= $conn->execute($queryRES);
			$result 	= $rsRES->getrows();
			$number		+= $result[0][$marks_col];
		} else {
			die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Error! Field(s) cannot be blank.</div>');
		}
	}
	if( $number ) {
		die('<div class="alert alert-success alert-dismissable AlertMessage" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Thank you for taking the Survey. Your marks is: <strong>'.$number.'</strong></div>');
	}
} else {
	die('<div class="alert alert-danger alert-dismissable AlertMessage" role="alert" style="margin-bottom:0;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Something wrong with upload! Is "upload_max_filesize" set correctly?</div>');
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$Training_ID 	= $_POST["Training_ID"];
	$User_ID 		= $_POST["User_ID"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($Training_ID) && !empty($User_ID) ) {
		
		$queryFavorite = "SELECT * FROM `" . $config['db_prefix'] . "favorite` WHERE `Training_ID` = $Training_ID AND `User_ID` = $User_ID LIMIT 0, 1";
		$rs = $conn->execute($queryFavorite);
		if( $rs ) {
			$num = $rs->numrows();
		} else {
			$num = 0;
		}
		if ( $num > 0 ) {
			$noti = $rs->getrows();
			$query = "DELETE FROM `" . $config['db_prefix'] . "favorite` WHERE `Training_ID` = $Training_ID AND `User_ID` = $User_ID";
			$rs = $conn->execute($query);
			if ( $rs ) {
				echo 'fa-heart-o';
			} else {
				echo 0;
			}
		} else {
			$query = "INSERT INTO `" . $config['db_prefix'] . "favorite` (`Favorite_ID`, `User_ID`, `Training_ID`, `Created_On`) VALUES(NULL, $User_ID, $Training_ID, '".date('Y-m-d H:i:s')."')";
			$rs = $conn->execute($query);
			if ( $rs ) {
				echo 'fa-heart';
			} else {
				echo 0;
			}
		}
	}
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$items_per_group = 5;
if ( isset($_POST) ) {
	$group_number = filter_var($_POST["group_no"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
	if ( !is_numeric($group_number) ) {
		header('HTTP/1.1 500 Invalid number!');
		exit();
	}

	$position = ($group_number * $items_per_group);
	$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` ORDER BY `Training_ID` DESC LIMIT $position, $items_per_group";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$trainings = $rs->getrows();
	} else {
		$trainings = array();
	}
	
	echo '<div class="col-md-12">';
	foreach ( $trainings as $training ) {
		$trainer = get_trainer_name($training['Trainer_ID']);
		$category = get_category_name($training['Training_Category']);
		$query1 = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = ".$training['Trainer_ID']." LIMIT 0, 1";
		$rs1 = $conn->execute($query1);
		if ( $rs1 ) {
			$user = $rs1->getrows();
		} else {
			$user = array();
		}
		echo '<div class="row">';
		echo '<div class="col-xs-2">';
		echo '<img src="' . $user[0]['Profile_Photo'] . '" class="img-responsive img-circle" width="50" height="20">';
		echo '</div>';
		echo '<div class="col-sm-10">';
		echo '<h5><a href="#">' . $trainer[0]['Full_Name'] . '</a> published a new training in <a href="#">' . $category[0]['Category_Name'] . '</a>.</h5>';
		echo '<p class="text-mute small"><span class="fa fa-clock-o"></span> ' . $training['Created_On'] . '</p>';
		echo '<h4><a href="' . $config['BASE_URL'] . '/training/' . $training['Training_URL'] . '/"><strong>' . $training['Training_Name'] . '</strong></a></h4>';
		echo '<video width="100%" height="100%" controls>';
		echo '<source src="' . $config['BASE_URL'] . '/uploads/trp/' . $training['Training_Video'] . '" type="video/mp4">';
		echo '<source src="' . $config['BASE_URL'] . '/uploads/trp/' . $training['Training_Video'] . '" type="video/ogg">';
		echo '<source src="' . $config['BASE_URL'] . '/uploads/trp/' . $training['Training_Video'] . '" type="video/webm">';
		echo '</video>';
		echo '<p>' . (strlen($training['Training_Desc']) > 100 ? substr($training['Training_Desc'], 0, 100) . '... <a href="' . $config['BASE_URL'] . '/training/' . $training['Training_URL'] . '/">Read more</a>' : $training['Training_Desc']) . '</p>';
		echo '<!--<a href="#"><i class="fa fa-heart">&nbsp; (3)Likes</i></a>';
		echo '<a href="#"><i class="fa fa-commenting-o">&nbsp; (10)Comments</i></a>';
		echo '<a href="#"><i class="fa fa-clock-o">&nbsp; 4 days ago</i></a>-->';
		echo '<div class="well" id="wellcomment">';
		echo '<img src="' . $config['ASSET_URL'] . '/frontend/images/comm-img-pro.png" class="img-responsive">';
		echo '<textarea name="comment" id="comment" ></textarea>';
		echo '</div><hr/>';
		echo '</div>';
		echo '</div>';
	}
	echo '</div>';
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if( isset($_POST) ) {
	
	$crop = new CropAvatar(
		isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
		isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
		isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
	);
	
	$response = array(
		'state'  => 200,
		'message' => $crop -> getMsg(),
		'result' => $crop -> getResult()
	);
	
	echo json_encode($response);
}
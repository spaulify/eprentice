<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$Promotion_File = $_POST["fileName"];
	$File_ID 		= $_POST["fileID"];
	$FileDirectory	= $config['UPLOAD_DIR'] . '/prm/';
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($Promotion_File) && !empty($File_ID) ) {
		$query = "DELETE FROM `" . $config['db_prefix'] . "promotion_file` WHERE `File_ID` = $File_ID AND `Promotion_Filename` = '$Promotion_File'";
		$rs = $conn->execute($query);
		if ( $rs ) {
			unlink($FileDirectory.$Promotion_File);
			echo 1;
		} else {
			echo 0;
		}
	}
}
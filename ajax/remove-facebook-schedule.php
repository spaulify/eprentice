<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$scheduleID 	= $_POST["scheduleID"];
	$campID			= $_POST["campID"];
	$userID 		= $_POST["userID"];
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($scheduleID) && !empty($campID) && !empty($userID) ) {
		$query = "DELETE FROM `" . $config['db_prefix'] . "facebook_campaign_schedule` WHERE `FB_Schedule_ID` = $scheduleID AND `FB_Campaign_ID` = $campID AND `Trainer_ID` = $userID";
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
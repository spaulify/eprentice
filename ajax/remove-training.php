<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin_admin();

if ( isset($_POST) ) {
	$trainingID = $_POST["trainingID"];
	$trainerID	= $_POST["trainerID"];
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	if ( !empty($trainingID) && !empty($trainerID) ) {
		$conn->execute("SET FOREIGN_KEY_CHECKS=0");
		$query = "DELETE FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_ID` = $trainingID AND `Trainer_ID` = $trainerID";		
		$rs = $conn->execute($query);
		if ( $rs ) {
			$conn->execute("SET FOREIGN_KEY_CHECKS=1");
			echo 1;
		} else {
			echo 0;
		}
	}
}
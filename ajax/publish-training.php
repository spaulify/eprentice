<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$date = date('Y-m-d H:i:s');
$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings`";
$rs = $conn->execute($query);
$num = $rs->numrows();
if( $num > 0 ) {
	$trainings = $rs->getrows();
	for($t=0;$t<count($trainings);$t++) {
		if( strtotime($date) >= strtotime($trainings[$t]['Training_Published_On']) ) {
			$queryUp = "UPDATE `" . $config['db_prefix'] . "trainings` SET `Training_Status` = 'Published' WHERE `Training_ID` = ".$trainings[$t]['Training_ID'];
			$rs = $conn->execute($queryUp);
		} else {
			$queryUp = "UPDATE `" . $config['db_prefix'] . "trainings` SET `Training_Status` = 'Pending' WHERE `Training_ID` = ".$trainings[$t]['Training_ID'];
			$rs = $conn->execute($queryUp);
		}
	}
}
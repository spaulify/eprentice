<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$Trainer_ID = filter_var($_POST["trainer"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
	$Member_ID = filter_var($_POST["member"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
	if ( !is_numeric($Trainer_ID) || !is_numeric($Member_ID) ) {
		header('HTTP/1.1 500 Invalid number!');
		exit();
	}
	
	$query = "SELECT `Ref_Trainer_ID` FROM `" . $config['db_prefix'] . "users` WHERE `User_ID` = $Member_ID LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$user = $rs->getrows();
	} else {
		$user = array();
	}
	$Affiliator_ID = $user[0]['Ref_Trainer_ID'];
	
	if ( $Affiliator_ID != $Trainer_ID ) {
		$query = "INSERT INTO `" . $config['db_prefix'] . "impressions`(`Impression_ID`, `Affiliator_ID`, `Aff_Trainer_ID`, `Member_ID`, `Created_On`) VALUES (NULL, $Affiliator_ID, $Trainer_ID, $Member_ID, '".date('Y-m-d H:i:s')."')";
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
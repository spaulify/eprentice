<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	
	for($i=0;$i<count($_FILES["promotion_file_img"]);$i++) {
		$fileName 	= $_FILES["promotion_file_img"][$i]["name"];
		$fileType 	= $_FILES["promotion_file_img"][$i]["type"];
		$fileSize 	= $_FILES["promotion_file_img"][$i]["size"];
	}
	
	if( !empty($fileName) ) {
		$file = array($fileName, $fileType, $fileSize);
		print_r($file);
	} else {
		echo 0;
	}
}
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$User_ID = $_SESSION['UID'];
$target = '';
$query = "SELECT * FROM `" . $config['db_prefix'] . "notifications` WHERE `User_ID` = $User_ID ORDER BY `Notification_ID` DESC LIMIT 0, 20";
$rs = $conn->execute($query);
if ( $rs )
	$num = $rs->numrows();
else
	$num = 0;
if ( $num > 0 ) {
	$notifications = $rs->getrows();
	for ( $i = 0; $i < count($notifications); $i++ ) {
		if ( strpos($notifications[$i]['Notification_Link'], 'eprentice') === false ) {
			$target = '__blank';
		}
		echo '<a href="' . $notifications[$i]['Notification_Link'] . '" target="' . $target . '" rel="' . $notifications[$i]['Notification_ID'] .'" class="notification-link">';
			echo '<div class="notification-item ' . $notifications[$i]['Notification_Status'] . '">';
				echo '<div class="notification-image">';
					echo '<img src="' . $notifications[$i]['Notification_Image'] . '" alt="" />';
				echo '</div>';
				echo '<div class="notification-text">';
					echo '<h4 class="item-title">' . $notifications[$i]['Notification_Text'] . '</h4>';
					echo '<p class="item-info"><i class="fa fa-clock-o"></i> ' . dateDiff(strtotime($notifications[$i]['Date_Time'])) . ' ago</p>';
				echo '</div>';
			echo '</div>';
		echo '</a>';
	}
} else {
	echo '<a>';
		echo '<div class="notification-item no-item">';
			echo '<h4 class="item-title">Currently, you have no notifications.</h4>';
		echo '</div>';
	echo '</a>';
}
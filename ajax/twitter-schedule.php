<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if ( isset($_POST) ) {
	$tw_schedule_desc		= addslashes($_POST['tw_schedule_desc']);
	$tw_schedule_start_date	= addslashes($_POST['tw_schedule_start_date']);
	$tw_schedule_end_date	= addslashes($_POST['tw_schedule_end_date']);
	$tw_trainer_id			= addslashes($_POST['tw_trainer_id']);
	$tw_schedule_status 	= 'Active';
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	/*$checkQuery = "SELECT * FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` WHERE `TW_Schedule_Status` = 'Active' AND `Trainer_ID` = $tw_trainer_id";
	$checkRs = $conn->execute($checkQuery);
	$row = $checkRs->numrows();
	if( $row > 0 ) {
		$tw_schedule_status = 'Deactive';
	} else {
		$current_datetime = date('Y-m-d H:i:s');
		if( (strtotime($tw_schedule_start_date) <= strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) >= strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Active';
		} else if( (strtotime($tw_schedule_start_date) <= strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) < strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Deactive';
		} else if( (strtotime($tw_schedule_start_date) < strtotime($current_datetime)) && (strtotime($tw_schedule_end_date) <= strtotime($current_datetime)) ) {
			$tw_schedule_status = 'Active';
		} elseif( strtotime($tw_schedule_start_date) > strtotime($current_datetime) ) {
			$tw_schedule_status = 'Deactive';
		}
	}*/
	if ( !empty($tw_schedule_desc) && !empty($tw_schedule_start_date) && !empty($tw_schedule_end_date) && !empty($tw_trainer_id) ) {
		$query = "INSERT INTO `" . $config['db_prefix'] . "twitter_campaign_schedule` (`TW_Schedule_ID`, `Trainer_ID`, `TW_Schedule_Desc`, `TW_Schedule_Start`, `TW_Schedule_End`, `TW_Schedule_Status`, `Created_On`) VALUES(NULL, $tw_trainer_id, '$tw_schedule_desc', '$tw_schedule_start_date', '$tw_schedule_end_date', '$tw_schedule_status', '".date('Y-m-d H:i:s')."')";
		$rs = $conn->execute($query);
		if ( $rs ) {
			echo 1;
		} else {
			echo 0;
		}
	}
}
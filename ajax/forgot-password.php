<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

if( isset($_POST) ) {
	$emailaddr 	= $_POST["emailaddr"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$emailaddr' LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$user = $rs->getrows();
	} else {
		$user = array();
	}
	if( !empty($user) ) {
		$userID    = $user[0]['User_ID'];
		$full_name = $user[0]['Full_Name'];
		$length	= 8;
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
		$password = substr( str_shuffle( $chars ), 0, $length );
		$login_link = $config['BASE_URL'] . '/login/';
		$update = "UPDATE `" . $config['db_prefix'] . "users` SET `Password` = '".sha1($password)."' WHERE `Email_ID` = '$emailaddr' AND `User_ID` = " . $userID;
		$rs = $conn->execute($update);
		$message = SMEmail::Password_Email($emailaddr, $full_name, $password, $login_link);
		$subject = 'Your ePrentice login password has been changed!';
		if(eprenticeMail($emailaddr, $full_name, $subject, $message) == "true") {
			echo 1;	
		} else {
			echo 0;
		}
	} else {
		echo 0;
	}
}
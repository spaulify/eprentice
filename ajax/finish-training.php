<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

if( isset($_POST) ) {
	$trainingID 	= $_POST["trainingID"];
	$trainerID 		= $_POST["trainerID"];
	$categoryID		= $_POST["categoryID"];
	$memberID 		= $_POST["memberID"];
	$lastSeekTime	= $_POST["lastSeekTime"];
	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}
	$query = "SELECT * FROM `" . $config['db_prefix'] . "ongoing_training` WHERE `Training_ID` = $trainingID AND `Member_ID` = $memberID LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$ongoing = $rs->getrows();
	} else {
		$ongoing = array();
	}
	if( empty($ongoing) ) {
		$insert = "INSERT INTO `" . $config['db_prefix'] . "ongoing_training` (`Ongoing_ID`, `Training_ID`, `Trainer_ID`, `Training_CataID`, `Member_ID`, `Last_Seek_Time`, `Training_Status`, `Last_Date_Time`) VALUES (NULL, $trainingID, $trainerID, $categoryID, $memberID, $lastSeekTime, 'Finish', '".date('Y-m-d H:i:s')."')";
		$rs = $conn->execute($insert);
	} else {
		$update = "UPDATE `" . $config['db_prefix'] . "ongoing_training` SET `Training_ID` = $trainingID, `Trainer_ID` = $trainerID, `Training_CataID` = $categoryID, `Member_ID` = $memberID, `Last_Seek_Time` = $lastSeekTime, `Training_Status` = 'Finish', `Last_Date_Time` = '".date('Y-m-d H:i:s')."' WHERE `Ongoing_ID` = " . $ongoing[0]['Ongoing_ID'];
		$rs = $conn->execute($update);
	}
}
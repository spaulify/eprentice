<?php 
define('_SMARTY_STARTED', true);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();

if ( isset($_POST) ) {	
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		die();
	}	
	$valid 	 = true;
	$UserSQL = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '" . $_REQUEST['email'] . "'";
	$rs = $conn->execute($UserSQL);
	if ( $rs ) {
		if ( $rs->numrows() > 0 ) {
			$valid   = false;
		}
	}
	echo json_encode(
		$valid ? array('valid' => $valid) : array('valid' => $valid)
	);
}
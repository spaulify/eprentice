<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$query = "SELECT * FROM `" . $config['db_prefix'] . "trainings` WHERE `Training_Status` = 'Publish' ORDER BY `Training_ID` DESC";
$rs = $conn->execute($query);
if ( $rs ) {
	$num = $rs->numrows();
} else {
	$num = array();
}
$smarty->assign('total_news', $num);

$query1 = "SELECT DISTINCT(`Trainer_ID`) FROM `" . $config['db_prefix'] . "hits` LIMIT 0, 12";
$rs1 = $conn->execute($query1);
if ( $rs1 ) {
	$trainer_id = $rs1->getrows();
} else {
	$trainer_id = array();
}
$smarty->assign('trainer_id', $trainer_id);

$query2 = "SELECT DISTINCT(`Training_ID`) FROM `" . $config['db_prefix'] . "hits` LIMIT 0, 3";
$rs2 = $conn->execute($query2);
if ( $rs2 ) {
	$training_id = $rs2->getrows();
} else {
	$training_id = array();
}
$smarty->assign('training_id', $training_id);

$query3 = "SELECT * FROM `" . $config['db_prefix'] . "promotions` ORDER BY `Promotion_ID` DESC";
$rs3 = $conn->execute($query3);
if ( $rs3 ) {
	$promotions = $rs3->getrows();
} else {
	$promotions = array();
}
$smarty->assign('promotions', $promotions);

$query4 = "SELECT * FROM `" . $config['db_prefix'] . "products` ORDER BY `Product_ID` DESC LIMIT 0, 5";
$rs4 = $conn->execute($query4);
if ( $rs4 ) {
	$products = $rs4->getrows();
} else {
	$products = array();
}
$smarty->assign('products', $products);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('news.tpl');
$smarty->display('footer.tpl');
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->loggedin();

$error = ''; 
$role  = 'viewer';
$refer 	 = false;
if ( isset($_SESSION['Ref_Code']) ) {
	$refer = true;
}

if ( isset($_SESSION['fb_login']) ) {
	$fb_user 		= unserialize($_SESSION['fb_user']);
	$logwith_fb		= 'yes';
	$facebook_id 	= $fb_user->id;
	$fb_access_token = $_SESSION['fb_access_token'];
	$fname 			= $fb_user->first_name;
	$lname 			= $fb_user->last_name;
	$email 			= $fb_user->email;
	$photo 			= 'http://graph.facebook.com/' . $fb_user->id . '/picture?type=large';
	$facebook_url	= 'https://www.facebook.com/' . $fb_user->id;
	$verify 		= 'yes';
} elseif ( isset($_SESSION['gp_login']) ) {
	$gp_user 		= unserialize($_SESSION['gp_user']);
	$logwith_gp		= 'yes';
	$google_id 		= $gp_user->id;
	$gp_access_token = $_SESSION['gp_access_token'];
	$fname 			= $gp_user->givenName;
	$lname 			= $gp_user->familyName;
	$email 			= $gp_user->email;
	$photo 			= isset($gp_user->picture) ? $gp_user->picture : $config['ASSET_URL'] . "/frontend/images/profile.jpg";
	$google_url		= isset($gp_user->link) ? $gp_user->link : '';
	$verify 		= 'yes';
} elseif ( isset($_SESSION['tw_login']) ) {
	$tw_user 		= unserialize($_SESSION['tw_user']);
	$name 			= explode(" ", $tw_user->name);
	$logwith_tw		= 'yes';
	$twitter_id 	= $tw_user->id;
	$tw_access_token = $_SESSION['tw_access_token'];
	$fname 			= isset($name[0]) ? $name[0] : '';
	$lname 			= isset($name[1]) ? $name[1] : '';
	$screen_name	= $tw_user->screen_name;
	$email 			= $tw_user->email;
	$photo 			= isset($tw_user->profile_image_url) ? $tw_user->profile_image_url : $config['ASSET_URL'] . "/frontend/images/profile.jpg";
	$twitter_url	= 'https://www.twitter.com/' . $tw_user->screen_name;
	$verify 		= 'yes';
} elseif ( isset($_SESSION['li_login']) ) {
	$li_user 		= unserialize($_SESSION['li_user']);
	$logwith_li		= 'yes';
	$linkedin_id	= $li_user->id;
	$li_access_token = $_SESSION['li_access_token'];
	$fname 			= $li_user->firstName;
	$lname 			= $li_user->lastName;
	$email 			= $li_user->emailAddress;
	$photo 			= isset($li_user->pictureUrl) ? $li_user->pictureUrl : $config['ASSET_URL'] . "/frontend/images/profile.jpg";
	$linkedin_url	= isset($li_user->publicProfileUrl) ? $li_user->publicProfileUrl : '';
	$verify 		= 'yes';
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	$fname 		= addslashes($_POST['fname']);
	$lname 		= addslashes($_POST['lname']);
	$email 		= addslashes($_POST['email']);
	$role 		= isset($_POST['role']) ? 'trainer' : 'viewer';
	$full_name 	= $fname . ' '. $lname;
	
	$username	= strtolower($fname . '.'. $lname);
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Username` = '$username' LIMIT 0, 1";
	$rs = $conn->execute($query);
	if ( $rs )
		$userCount = $rs->numrows();
	else 
		$userCount = 0;
	if ( $userCount > 0 ) {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users`";
		$rs = $conn->execute($query);
		if ( $rs )
			$userRows = $rs->numrows() + 1;
		else
			$userRows = 1;
		$username = $username . '.' . $userRows;
	}
	
	$reflength	= 7;
	$refchars 	= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$refcode 	= substr( str_shuffle( $refchars ), 0, $reflength );
	
	if ( isset($_SESSION['Ref_Code']) ) {
		$Ses_Ref_Code = $_SESSION['Ref_Code'];
		$query = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Ref_Code` = '$Ses_Ref_Code' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$training = $rs->getrows();
			$Ref_Trainer_ID = $training[0]['User_ID'];
		} else {
			$Ref_Trainer_ID = 0;
		}
	} else {
		$Ref_Trainer_ID = 0;
	}
	
	if( filter_var($email, FILTER_VALIDATE_EMAIL) ) {
		if ( !empty($fname) && !empty($lname) && !empty($email) ) {
			$password = '';
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
			$rs = $conn->execute($query);
			if ( $rs )
				$num = $rs->numrows();
			else
				$num = 0;
			if ( $num == 0 ) {
				$verify_code = '';
				if( $_SESSION['fb_login'] ) {
					$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_FB`, `Facebook_ID`, `FB_Access_Token`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `Email_ID`, `Password`, `Profile_Photo`, `Facebook_URL`, `Profile_Verify`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', '$Ref_Trainer_ID', '$logwith_fb', '$facebook_id', '$fb_access_token', '$role', '$fname', '$lname', '$full_name', '$username', '$email', '$password', '$photo', '$facebook_url', '$verify', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
				} elseif( $_SESSION['gp_login'] ) {
					$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_GP`, `Google_ID`, `GP_Access_Token`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `Email_ID`, `Password`, `Profile_Photo`, `Google_URL`, `Profile_Verify`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', '$Ref_Trainer_ID', '$logwith_gp', '$google_id', '$gp_access_token', '$role', '$fname', '$lname', '$full_name', '$username', '$email', '$password', '$photo', '$google_url', '$verify', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
				} elseif( $_SESSION['tw_login'] ) {
					$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_TW`, `Twitter_ID`, `TW_Access_Token`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `TW_Screen_Name`, `Email_ID`, `Password`, `Profile_Photo`, `Twitter_URL`, `Profile_Verify`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', '$Ref_Trainer_ID', '$logwith_tw', '$twitter_id', '$tw_access_token', '$role', '$fname', '$lname', '$full_name', '$username', '$screen_name', '$email', '$password', '$photo','$twitter_url', '$verify', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
				} elseif( $_SESSION['li_login'] ) {
					$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`Ref_Code`, `Ref_Trainer_ID`, `Login_With_LI`, `LinkedIn_ID`, `LI_Access_Token`, `User_Role`, `First_Name`, `Last_Name`, `Full_Name`, `Username`, `Email_ID`, `Password`, `Profile_Photo`, `LinkedIn_URL`, `Profile_Verify`, `Joining_Date`, `Expiration_Date`) VALUES ('$refcode', '$Ref_Trainer_ID', '$logwith_li', '$linkedin_id', '$li_access_token', '$role', '$fname', '$lname', '$full_name', '$username', '$email', '$password', '$photo', '$linkedin_url', '$verify', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s', strtotime('+30 days', time()))."')";
				}
				$rs = $conn->execute($query);
				if( $rs) {					
					$login_link = $config['BASE_URL'] . '/login/';
					$length	= 8;
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
					$password = substr( str_shuffle( $chars ), 0, $length );
					$message = SMEmail::Password_Email($email, $full_name, $password, $login_link);
					$subject = 'Your ePrentice account details';
					if (eprenticeMail($email, $full_name, $subject, $message) == "true") {
						$pass = sha1($password);
						$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Password` = '$pass' WHERE `Email_ID` = '" . $email . "'";
						$rs = $conn->execute($query);
						if ( $rs ) {
							$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
							$rs = $conn->execute($query);
							if ( $rs )
								$num = $rs->numrows();
							else
								$num = 0;
							if ( $num == 1 ) {
								$user = $rs->getrows();
								$_SESSION['UID'] 	 = $user[0]['User_ID'];
								$_SESSION['UFNAME']  = $user[0]['First_Name'];
								$_SESSION['ULNAME']  = $user[0]['Last_Name'];
								$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
								$_SESSION['USER'] 	 = $user[0]['Username'];
								$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
								$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
								if ( $role == 'viewer' ) {
									$Notification_Text = "<b>" . $user[0]['Full_Name'] . "</b>, a member you had invited has joined, ePrentice.";
									$Notification_Link = "#";
									$Notification_Image = $user[0]['Profile_Photo'];
									insert_notification($Ref_Trainer_ID, $Notification_Text, $Notification_Link, $Notification_Image);
								}
								SMRedirect::go($config['BASE_URL'] . '/dashboard/');
							}
						} else {
							$error = 'Something went wrong! Please try again.';
						}
					}
				}
			} else {
				$error = 'You are already registered with us!';
			}
		} else {
			$error = 'All fields are required!';
		}
	}
}

$smarty->assign('refer', 	$refer);
$smarty->assign('fname', 	$fname);
$smarty->assign('lname', 	$lname);
$smarty->assign('email', 	$email);
$smarty->assign('role', 	$role);
$smarty->assign('error', 	$error);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('social-signup.tpl');
$smarty->display('footer.tpl');
//unset($_SESSION['access_token']);
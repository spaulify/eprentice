<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$success = '';
/*if ( isset($_POST) ) {
	\Stripe\Stripe::setApiKey($config['Stripe_Secret_Key']);
	$error = ''; $success = '';
	try {
		if (!isset($_POST['stripeToken']))
			throw new Exception("The Stripe Token was not generated correctly");
			
		// Create a Customer
		$customer = \Stripe\Customer::create(array(
			"source" 		=> $_POST['stripeToken'],
			"description" 	=> $NAME,
			"email" 		=> $EMAIL,
		));
		
		// Create a Payment
		$payment = \Stripe\Charge::create(array(
			"amount" 	=> 5000,			// Amount is in cent
			"currency" 	=> "usd",
			"customer" 	=> $customer->id,	// Charge the Customer instead of the card
			"metadata" 	=> array("Product ID" => "1", "Product Name" => "EngageWise")
		));
		
		$payment_array = $payment->__toArray(true);
		if ( $payment_array['status'] == 'succeeded' ) {
			$query = "INSERT INTO `" . $config['db_prefix'] . "payments`(`Stripe_Transaction_ID`, `Seller_ID`, `Buyer_ID`, `Buyer_First_Name`, `Buyer_Last_Name`, `Product_ID`, `Product_Name`, `Purchase_Amount`, `Purchased_ON`) VALUES ('" . $payment_array['balance_transaction'] . "', 5, $UID, '$UFNAME', '$ULNAME', 1, 'EngageWise', 5000, '" . date('Y-m-d H:i:s', $payment_array['created']) . "')";
			$rs = $conn->execute($query);
			if ( $rs ) {
				SMEmail::Payment_Email($EMAIL, $NAME, 'EngageWise', 5000, date('Y-m-d H:i:s', $payment_array['created']), $payment_array['balance_transaction'], 'http://engagewise.com');
				$success = 'Your payment was successful.';
			}
		}
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}*/


$smarty->assign('success', 			$success);
$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('withdraw-payment.tpl');
$smarty->display('footer.tpl');
<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
$image = new SMImageConv();
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->not_loggedin();

$UID  = isset($_SESSION['UID'])   ? $_SESSION['UID']   : '';
$ROLE = isset($_SESSION['UROLE']) ? $_SESSION['UROLE'] : '';
$NAME = isset($_SESSION['UNAME']) ? $_SESSION['UNAME'] : '';
$USER = isset($_SESSION['USER'])  ? $_SESSION['USER']  : '';
$EMAIL 	= isset($_SESSION['UEMAIL']) ? $_SESSION['UEMAIL'] : '';
$UFNAME = isset($_SESSION['UFNAME']) ? $_SESSION['UFNAME'] : '';
$ULNAME = isset($_SESSION['ULNAME']) ? $_SESSION['ULNAME'] : '';

$error = ''; $success = ''; $flag = 0;
if ( isset($_POST['bio_submit']) ) {
	$bio = addslashes($_POST['bio']);
	$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Bio` = '$bio' WHERE `User_ID` = $UID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$success = 'About me have been successfully updated.';
	} else {
		$error = 'Sorry! We are unable to update, try again.';
	}
}

if ( isset($_POST['exp_submit']) ) {
	$domain = addslashes($_POST['domain']);
	$projects = addslashes($_POST['projects']);
	$experience = addslashes($_POST['experience']);

	$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Domain` = '$domain', `Projects` = '$projects', `Experience` = '$experience' WHERE `User_ID` = $UID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$success = 'Your Areas of Expertise have been successfully updated.';
	} else {
		$error = 'Sorry! We are unable to update, try again.';
	}
}

if ( isset($_POST['ac_submit']) ) {
	$fname = addslashes($_POST['fname']);
	$lname = addslashes($_POST['lname']);
	$desig = addslashes($_POST['desig']);
	$paypal_id = addslashes($_POST['paypal_id']);
	$facebook = addslashes($_POST['facebook']);
	$google = addslashes($_POST['google']);
	$twitter = addslashes($_POST['twitter']);
	$linkedin = addslashes($_POST['linkedin']);
	$oldpass  = isset($_POST['old_password']) ? addslashes($_POST['old_password']) : '';
	$newpass  = isset($_POST['new_password']) ? addslashes($_POST['new_password']) : '';
	
	$full_name = $fname . ' ' . $lname;
	$email	   = $_SESSION['UEMAIL'];

	if ( empty($fname) || empty($lname) ) {
		$fname = $_SESSION['UFNAME'];
		$lname = $_SESSION['ULNAME'];
		$full_name = $_SESSION['UNAME'];
	} else {
		$_SESSION['UFNAME'] = $fname;
		$_SESSION['ULNAME'] = $lname;
		$_SESSION['UNAME']  = $fname . ' ' . $lname;
	}
	
	if ( empty($paypal_id) ) {
		$flag = 0;
	} elseif ( !filter_var($paypal_id, FILTER_VALIDATE_EMAIL) === false && !empty($paypal_id) ) {
		$flag = 0;
	} else {
		$flag = 1;
	}
	
	if( $flag == 0 ) {
		if( !empty($oldpass) && !empty($newpass) ) {
			$old_pass = sha1($oldpass);
			$new_pass = sha1($newpass);
			$passquery = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Password` = '$old_pass' AND `User_ID` = $UID LIMIT 0, 1";
			$rs = $conn->execute($passquery);
			if ( $rs )
				$num = $rs->numrows();
			else
				$num = 0;
			if ( $num == 1  && strlen($newpass) > 5) {
				$query = "UPDATE `" . $config['db_prefix'] . "users` SET `First_Name` = '$fname', `Last_Name` = '$lname', `Full_Name` = '$full_name', `Password` = '$new_pass', `Designation` = '$desig', `PayPal_ID` = '$paypal_id', `Facebook_URL` = '$facebook', `Google_URL` = '$google', `Twitter_URL` = '$twitter', `LinkedIn_URL` = '$linkedin' WHERE `User_ID` = $UID";
				$rs = $conn->execute($query);
				if ( $rs ) {
					$login_link = $config['BASE_URL'] . '/login/';
					$message = SMEmail::Password_Email($email, $full_name, $newpass, $login_link);
					$subject = 'Your ePrentice account details';
					if( eprenticeMail($email, $full_name, $subject, $message) == "true" ) {
						$success = 'Your Account and Password have been successfully updated.';
					}
				} else {
					$error = 'Sorry! We are unable to update, try again.';
				}
			} else {
				$error = 'Something went wrong! Password mismatch or too short!';
			}
		} else {
			$query = "UPDATE `" . $config['db_prefix'] . "users` SET `First_Name` = '$fname', `Last_Name` = '$lname', `Full_Name` = '$full_name', `Designation` = '$desig', `PayPal_ID` = '$paypal_id', `Facebook_URL` = '$facebook', `Google_URL` = '$google', `Twitter_URL` = '$twitter', `LinkedIn_URL` = '$linkedin' WHERE `User_ID` = $UID";
			$rs = $conn->execute($query);
			if ( $rs ) {
				$success = 'Your Account have been successfully updated.';
			} else {
				$error = 'Sorry! We are unable to update, try again.';
			}
		}
	} else {
		$error = 'Invalid PayPal Email Address!';
	}
	
	if ( isset($_FILES['photo']) ) {
		$dp = NULL;
		$time = time();
		$photo = $_FILES['photo'];
		if ($photo['tmp_name'] != '') {
			if (is_uploaded_file($photo['tmp_name']) && getimagesize($photo['tmp_name'])) {
				$ext = strtolower(substr($photo['name'], strrpos($photo['name'], '.') + 1));
				if (!check_image($photo['tmp_name'], $ext)) {
					continue;
				}
				$photo_name = $time . '_' . rand(100, 999) . '.jpg';
				$src = $photo['tmp_name'];
				
				$dst = $config['UPLOAD_DIR'] . '/dp/' . $photo_name;
				$image->process($src, $dst, 'SAME', 0, 0);
				$image->resize(true, true);

				$dst = $config['UPLOAD_DIR'] . '/dp/thumb-' . $photo_name;
				$image->process($src, $dst, 'EXACT', 240, 240);
				$image->resize(true, true);
	
				$dp = $config['UPLOAD_URL'] . '/dp/thumb-' . $photo_name;
			}
		}
		if ( $dp ) {
			$query = "UPDATE `" . $config['db_prefix'] . "users` SET `Profile_Photo` = '$dp' WHERE `User_ID` = $UID";
			$rsDP = $conn->execute($query);
			if ( $rsDP ) {
				$success = 'Your Account have been successfully updated.';
			} else {
				$error = 'Sorry! We are unable to update, try again.';
			}
		}
	}
}

$User_Name = $_GET['param'];
$UserQuery = "SELECT `User_ID` FROM `" . $config['db_prefix'] . "users` WHERE `Username` = '$User_Name' LIMIT 0, 1";
$UserRS    = $conn->execute($UserQuery);
if ( $UserRS )
	$UserNum = $UserRS->numrows();
else
	$UserNum = 0;
if ( $UserNum == 1 ) {
	$Users = $UserRS->getrows();
	$User_ID = $Users[0]['User_ID'];
} else {
	$User_ID = $UID;
}
$user = $auth->get_user($User_ID);

$smarty->assign('user', $user[0]);
$smarty->assign('error', $error);
$smarty->assign('success', $success);

$smarty->assign('User_ID', 			$UID);
$smarty->assign('User_Role', 		$ROLE);
$smarty->assign('User_Full_Name',	$NAME);
$smarty->assign('User_Email',		$EMAIL);
$smarty->assign('User_First_Name',	$UFNAME);
$smarty->assign('User_Last_Name',	$ULNAME);
$smarty->assign('Username',			$USER);
$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('navbar.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('profile.tpl');
$smarty->display('footer.tpl');
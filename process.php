<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
$USERNAME = isset($_SESSION['USER']) ? $_SESSION['USER'] : '';

if ( isset($_GET['fbTrue']) && isset($_GET['code']) ) {
	$token_url = "https://graph.facebook.com/oauth/access_token?client_id=" . $config['fb_appid'] . "&redirect_uri=" . urlencode($config['fb_redirect']) . "&client_secret=" . $config['fb_secret'] . "&code=" . $_GET['code'];
	
	$response = file_get_contents($token_url);
	$params = json_decode($response);
	
	$graph_url = "https://graph.facebook.com/me?fields=id,name,first_name,last_name,email&access_token=" . $params->access_token;
	$user = json_decode(file_get_contents($graph_url));
	
	$_SESSION['fb_login'] = true;
	$_SESSION['fb_access_token'] = $params->access_token;
	
	$email 			 = $user->email;
	$facebook_id 	 = $user->id;
	$facebook_url	 = 'https://www.facebook.com/' . $facebook_id;
	$fb_access_token = $params->access_token;

	if( isset($_SESSION['UID']) ) {
		$email  = $_SESSION['UEMAIL'];
		$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_FB` = 'yes', `Facebook_ID` = '$facebook_id', `FB_Access_Token` = '$fb_access_token', `Facebook_URL` = '$facebook_url' WHERE `Email_ID` = '$email'";
		$rsU = $conn->execute($queryU);
		if( isset($_SESSION['SocialLoginFB']) ) {
			SMRedirect::go($config['BASE_URL'] . '/social-campaign/');
		} else {
			SMRedirect::go($config['BASE_URL'] . '/profile/' . $USERNAME . '/');
		}
	} else {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num == 0 ) {
			$_SESSION['fb_user'] = serialize($user);
			SMRedirect::go($config['BASE_URL'] . '/social-signup/?method=join-with-facebook');
		} else {
			$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_FB` = 'yes', `Facebook_ID` = '$facebook_id', `FB_Access_Token` = '$fb_access_token', `Facebook_URL` = '$facebook_url' WHERE `Email_ID` = '$email'";
			$rsU = $conn->execute($queryU);
			if ( $rsU ) {
				$user = $rs->getrows();
				$_SESSION['UID'] 	 = $user[0]['User_ID'];
				$_SESSION['UFNAME']  = $user[0]['First_Name'];
				$_SESSION['ULNAME']  = $user[0]['Last_Name'];
				$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
				$_SESSION['USER'] 	 = $user[0]['Username'];
				$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
				$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
				if ( $user[0]['User_Role'] == 'trainer' ) {
					SMRedirect::go($config['BASE_URL'] . '/dashboard/');
				} else {
					SMRedirect::go($config['BASE_URL'] . '/news/');
				}
			}
		}
	}
} elseif ( isset($_GET['gpTrue']) && isset($_GET['code']) ) {
	$client->authenticate($_GET['code']);
	$user = $service->userinfo->get();

	$_SESSION['gp_login'] 		 = true;
	$_SESSION['gp_access_token'] = $client->getAccessToken();
	$AccessTokenData 			 = json_decode($_SESSION['gp_access_token']);	
	
	$email 			 = $user->email;
	$google_id 		 = $user->id;
	$gp_access_token = $AccessTokenData->access_token;
	$google_url		 = isset($user->link) ? $user->link : '';
		
	if( isset($_SESSION['UID']) ) {
		$email  = $_SESSION['UEMAIL'];
		$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_GP` = 'yes', `Google_ID` = '$google_id', `GP_Access_Token` = '$gp_access_token',	`Google_URL` = '$google_url' WHERE `Email_ID` = '$email'";
		$rsU = $conn->execute($queryU);
		SMRedirect::go($config['BASE_URL'] . '/profile/' . $USERNAME . '/');
	} else {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs )
			$num = $rs->numrows();
		else
			$num = 0;
		if ( $num == 0 ) {
			$_SESSION['gp_user'] = serialize($user);
			SMRedirect::go($config['BASE_URL'] . '/social-signup/?method=join-with-google');
		} else {
			$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_GP` = 'yes', `Google_ID` = '$google_id', `GP_Access_Token` = '$gp_access_token',	`Google_URL` = '$google_url' WHERE `Email_ID` = '$email'";
			$rsU = $conn->execute($queryU);
			if ( $rsU ) {
				$user = $rs->getrows();
				$_SESSION['UID'] 	 = $user[0]['User_ID'];
				$_SESSION['UFNAME']  = $user[0]['First_Name'];
				$_SESSION['ULNAME']  = $user[0]['Last_Name'];
				$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
				$_SESSION['USER'] 	 = $user[0]['Username'];
				$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
				$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
				if ( $user[0]['User_Role'] == 'trainer' ) {
					SMRedirect::go($config['BASE_URL'] . '/dashboard/');
				} else {
					SMRedirect::go($config['BASE_URL'] . '/news/');
				}
			}
		}
	}
} elseif ( isset($_GET['twTrue']) && isset($_GET['code']) ) {
	if ( isset($_REQUEST['oauth_token']) && $_SESSION['token']  !== $_REQUEST['oauth_token'] ) {
		session_destroy();
		SMRedirect::go($config['BASE_URL'] . '/');
	} elseif ( isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token'] ) {
		$connection = new TwitterOAuth($config['tw_consumer_key'], $config['tw_consumer_secret'], $_SESSION['token'], $_SESSION['token_secret']);
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
		if ( $connection->http_code == '200') {
			$_SESSION['status'] = 'verified';
			$_SESSION['request_vars'] = $access_token;
			$user = $connection->get('account/verify_credentials', array('include_email'=>'true'));

			$_SESSION['tw_login'] 				= true;
			$_SESSION['tw_access_token'] 		= $access_token['oauth_token'];
			$_SESSION['tw_access_token_secret'] = $access_token['oauth_token_secret'];
			
			$email 			 		= isset($user->email) ? $user->email : '';
			$twitter_id 	 		= $user->id;
			$tw_access_token 		= $_SESSION['tw_access_token'];
			$tw_access_token_secret	= $_SESSION['tw_access_token_secret'];
			$twitter_url	 		= 'https://www.twitter.com/' . $user->screen_name;
			$twitter_screen_name	= $user->screen_name;

			if( isset($_SESSION['UID']) ) {
				$email  = $_SESSION['UEMAIL'];
				$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_TW` = 'yes', `Twitter_ID` = '$twitter_id', `TW_Access_Token` = '$tw_access_token', `TW_Access_Token_Secret` = '$tw_access_token_secret', `TW_Screen_Name` = '$twitter_screen_name', `Twitter_URL` = '$twitter_url' WHERE `Email_ID` = '$email'";
				$rsU = $conn->execute($queryU);
				SMRedirect::go($config['BASE_URL'] . '/profile/' . $USERNAME . '/');
			} else {
				$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
				$rs = $conn->execute($query);
				if ( $rs )
					$num = $rs->numrows();
				else
					$num = 0;
				if ( $num == 0 ) {
					$_SESSION['tw_user'] = serialize($user);
					SMRedirect::go($config['BASE_URL'] . '/social-signup/?method=join-with-twitter');
				} else {
					$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_TW` = 'yes', `Twitter_ID` = '$twitter_id', `TW_Access_Token` = '$tw_access_token', `TW_Access_Token_Secret` = '$tw_access_token_secret', `TW_Screen_Name` = '$twitter_screen_name', `Twitter_URL` = '$twitter_url' WHERE `Email_ID` = '$email'";
					$rsU = $conn->execute($queryU);
					if ( $rsU ) {
						$user = $rs->getrows();
						$_SESSION['UID'] 	 = $user[0]['User_ID'];
						$_SESSION['UFNAME']  = $user[0]['First_Name'];
						$_SESSION['ULNAME']  = $user[0]['Last_Name'];
						$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
						$_SESSION['USER'] 	 = $user[0]['Username'];
						$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
						$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
						if ( $user[0]['User_Role'] == 'trainer' ) {
							SMRedirect::go($config['BASE_URL'] . '/dashboard/');
						} else {
							SMRedirect::go($config['BASE_URL'] . '/news/');
						}
					}
				}
			}
			
			unset($_SESSION['token']);
			unset($_SESSION['token_secret']);
			SMRedirect::go($config['BASE_URL'] . '/');
		} else {
			die("error, try again later!");
		}
	} else {
		if ( isset($_GET["denied"]) ) {
			SMRedirect::go($config['BASE_URL'] . '/');
			die();
		}
		$connection = new TwitterOAuth($config['tw_consumer_key'], $config['tw_consumer_secret']);
		$request_token = $connection->getRequestToken($config['tw_oauth_callback']);
		
		$_SESSION['token'] 			= $request_token['oauth_token'];
		$_SESSION['token_secret'] 	= $request_token['oauth_token_secret'];
		
		if ( $connection->http_code == '200' ) {
			$twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
			SMRedirect::go($twitter_url);
		} else {
			die("error connecting to twitter! try again later!");
		}
	}
} elseif ( isset($_GET['liTrue']) && isset($_GET['code']) ) {
	if ( isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> "" ) {
		$_SESSION["err_msg"] = $_GET["oauth_problem"];
		SMRedirect::go($config['BASE_URL'] . '/');
		exit;
	}
	$client = new oauth_client_class;
	$client->debug = false;
	$client->debug_http = true;
	$client->redirect_uri = $config['li_redirect_uri'];
	$client->client_id = $config['li_client_id'];
	$application_line = __LINE__;
	$client->client_secret = $config['li_client_secret'];
	
	if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
		die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
			'create an application, and in the line '.$application_line.
			' set the client_id to Consumer key and client_secret with Consumer secret. '.
			'The Callback URL must be '.$client->redirect_uri).' Make sure you enable the '.
			'necessary permissions to execute the API calls your application needs.';
			
	$client->scope = $config['li_scope'];
	if (($success = $client->Initialize())) {
		if (($success = $client->Process())) {
			if (strlen($client->authorization_error)) {
				$client->error = $client->authorization_error;
				$success = false;
			} elseif (strlen($client->access_token)) {
				$success = $client->CallAPI(
					'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 
					'GET', array(
						'format'=>'json'
					), array('FailOnAccessError'=>true), 
					$user
				);
			}
		}
		$success = $client->Finalize($success);
	}
	
	if ($client->exit) exit;
	if ($success) {
		$_SESSION['li_login'] = true;
		$_SESSION['li_access_token'] = $client->access_token;
		
		$email 			 = isset($user->emailAddress) ? $user->emailAddress : '';
		$linkedin_id	 = $user->id;
		$li_access_token = $_SESSION['li_access_token'];
		$linkedin_url	 = isset($user->publicProfileUrl) ? $user->publicProfileUrl : '';
		
		if( isset($_SESSION['UID']) ) {
			$email  = $_SESSION['UEMAIL'];
			$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_LI` = 'yes', `Linkedin_ID` = '$linkedin_id', `LI_Access_Token` = '$li_access_token',	`LinkedIn_URL` = '$linkedin_url' WHERE `Email_ID` = '$email'";
			$rsU = $conn->execute($queryU);
			SMRedirect::go($config['BASE_URL'] . '/profile/' . $USERNAME . '/');
		} else {
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `Email_ID` = '$email' LIMIT 0, 1";
			$rs = $conn->execute($query);
			if ( $rs )
				$num = $rs->numrows();
			else
				$num = 0;
			if ( $num == 0 ) {
				$_SESSION['li_user'] = serialize($user);
				SMRedirect::go($config['BASE_URL'] . '/social-signup/?method=join-with-linkedin');
			} else {
				$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `Login_With_LI` = 'yes', `Linkedin_ID` = '$linkedin_id', `LI_Access_Token` = '$li_access_token',	`LinkedIn_URL` = '$linkedin_url' WHERE `Email_ID` = '$email'";
				$rsU = $conn->execute($queryU);
				if ( $rsU ) {
					$user = $rs->getrows();
					$_SESSION['UID'] 	 = $user[0]['User_ID'];
					$_SESSION['UFNAME']  = $user[0]['First_Name'];
					$_SESSION['ULNAME']  = $user[0]['Last_Name'];
					$_SESSION['UNAME'] 	 = $user[0]['Full_Name'];
					$_SESSION['USER'] 	 = $user[0]['Username'];
					$_SESSION['UEMAIL']  = $user[0]['Email_ID'];
					$_SESSION['UROLE'] 	 = $user[0]['User_Role'];
					if ( $user[0]['User_Role'] == 'trainer' ) {
						SMRedirect::go($config['BASE_URL'] . '/dashboard/');
					} else {
						SMRedirect::go($config['BASE_URL'] . '/news/');
					}
				}
			}
		}
	} else {
		$_SESSION["err_msg"] = $client->error;
	}
	SMRedirect::go($config['BASE_URL'] . '/');
	exit;
}
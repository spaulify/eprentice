<?php 
define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();

$query = "SELECT * FROM `" . $config['db_prefix'] . "twitter_campaign_schedule` AS `TCS`, `" . $config['db_prefix'] . "users` AS `USER` WHERE `TCS`.`Trainer_ID` = `USER`.`User_ID`";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$twSchedule = $rs->getrows();
} else {
	$twSchedule = array();
}

for($i=0;$i<count($twSchedule);$i++) {
	
	/* Getting Data from Database for Tweets */
	$TW_Schedule_Status		= '';
	$Current_DateTime 		= date('Y-m-d H:i:s');
	$Tweet_Schedule_ID		= $twSchedule[$i]['TW_Schedule_ID'];
	$Tweet_ScheduleDesc		= $twSchedule[$i]['TW_Schedule_Desc'];
	$Tweet_Schedule_Start	= $twSchedule[$i]['TW_Schedule_Start'];
	$Tweet_Schedule_End		= $twSchedule[$i]['TW_Schedule_End'];
	$Tweet_Schedule_Status	= $twSchedule[$i]['TW_Schedule_Status'];
	$Tweet_Trainer_ID		= $twSchedule[$i]['Trainer_ID'];
	$Twitter_ID				= $twSchedule[$i]['Twitter_ID'];
	$TW_Access_Token		= $twSchedule[$i]['TW_Access_Token'];
	$TW_Access_Token_Secret	= $twSchedule[$i]['TW_Access_Token_Secret'];
	$TW_Screen_Name			= $twSchedule[$i]['TW_Screen_Name'];
	$Twitter_URL			= $twSchedule[$i]['Twitter_URL'];
	
	if( $Tweet_Schedule_Status == 'Active' && (strtotime($Tweet_Schedule_Start) <= strtotime($Current_DateTime)) ) {
		/* Twitter Connection for OAuth */
		$connection = new TwitterOAuth($config['tw_consumer_key'], $config['tw_consumer_secret'], $TW_Access_Token, $TW_Access_Token_Secret);		
		/* Sending Tweets to Twitter Account */
		if( strlen($Tweet_ScheduleDesc) <= 140 ) {
			$status = $connection->post("statuses/update", array("status" => $Tweet_ScheduleDesc));
			/* Update Query for Deactive Tweets */
			if( strtotime($Tweet_Schedule_End) <= strtotime($Current_DateTime) ) {
				$updateQuery = "UPDATE `" . $config['db_prefix'] . "twitter_campaign_schedule` SET `TW_Schedule_Status` = 'Deactive' WHERE `TW_Schedule_ID` = $Tweet_Schedule_ID AND `Trainer_ID` = $Tweet_Trainer_ID";
				$rs = $conn->execute($updateQuery);
			}
		} else {
			echo 'Maximum 140 characters allowed!';
		}		
	}
		
}